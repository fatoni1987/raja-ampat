angular.module('pajak')
.controller( 'pengingatAktivitasCtrl', function( $scope, $http, $state, $rootScope, $modal, $translate){ // $cookieStore,
    $scope.currentPage = 0;
    $scope.maxSize = 10;
    $scope.userId = 0;
    $scope.totalItems = 0; 
    $scope.Cabangs;
    $scope.Cabangs2 = "";
    $scope.Regions;
    $scope.PAC;
    $scope.Rantings;
    $scope.getNuCard;
    $scope.getNuCardDetail;
    $scope.filesToUpload = [];
    $scope.gambar = "";
    $scope.data = {
        nama : "",
        tempatLahir : "",
        tanggalLahir : "",
        nik : "",
        alamat : "",
        noBpjs : "",
        CabangId : "",
        RegionId : "",
        PC : "",
        PAC : "",
        RantingId : "",
        isMuslimat : "",
        isRumahTangga : "",
        isGuruSd : "",
        isGuruPaud : "",
        isGuruMadin : "",
        isMedis : "",
        isDosen : "",
        isPengacara : "",
        isPengusaha : "",
        isDpr : "",
        isPns : "",
        isPetani : "",
        isSwasta : "",
        isNGO : "",
        Other1 : "",
        Other1Text : "",
        PenSD : "",
        PenSLTP : "",
        PenSLTA : "",
        PenS1 : "",
        PenS2 : "",
        PenS3 : "",
        LamaPesantren : "",
        Jenjang : "",
        PosisiPengurus : "",
        PelFormal : "",
        PelNu : "",
        PelNonFormalDaiyah : "",
        PelNonFormalWirausaha : "",
        PelNonFormalMc : "",
        PelNonFormalAdvokasi : "",
        PelGender : "",
        PelIT : "",
        Other3TotLKS : "",
        TotPKPNU : "",
        TotMKNU : "",
        TotMKP : "",
        Other4 : "",
        Other4Text : "",
        FasilLKD : "",
        FasilPKPNU : "",
        FasilMKNU : "",
        MKP : "",
        Other5 : "",
        Other5Text : "",
        IsuPendidikan : "",
        IsuKesehatan : "",
        IsuEkonomi : "",
        IsuHukum : "",
        IsuIslam : "",
        IsuGender : "",
        IsuLingkungan : "",
        IsuRadikal : "",
        IsuPolitik : "",
        Other6 : "",
        Other6Text : "",
        PengalamanPendampingan : "",
        KerjaSamaPemerintah : "",
        KerjaSamaNGO : "",
        KerjaSamaDonor : "",
        KerjaSamaPerusahaan : "",
        KerjaSamaTidakPernah : "",
        Other7 : "",
        Other7Text : "",
        UsahaMamin : "",
        UsahaFashion : "",
        HandyCraft : "",
        Other8 : "",
        Other8Text : ""
    }
    $scope.dataNuCard = {
        mst_cabang_id : "",
        nu_card_nama : "",
        nu_card_tempat_lahir : "",
        nu_card_tgl_lahir : "",
        nu_card_nik : "",
        nu_card_alamat : "",
        nu_card_no_bpjs : "",
        nu_card_anggota_pc : "",
        nu_card_anggota_pac : "",
        nu_card_anggota_ranting : "",
        nu_card_isi_data_muslimat : "",
        nu_card_gambar : ""
    }

    $scope.dataNuCardDetail = {nu_card_id : "",nu_card_detil_variabel : "",nu_card_detil_keterangan : "",nu_card_no_urut : ""}
    $scope.Pekerjaan = [
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "Guru SD/SMP/SMA",nu_card_detil_keterangan : false,nu_card_no_urut : 1},
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "Guru PAUD",nu_card_detil_keterangan : false,nu_card_no_urut : 2},
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "Guru Madin / TPQ",nu_card_detil_keterangan : false,nu_card_no_urut : 3},
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "Tenaga Kesehatan",nu_card_detil_keterangan : false,nu_card_no_urut : 4},
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "Dosen",nu_card_detil_keterangan : false,nu_card_no_urut : 5},
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "Pengacara",nu_card_detil_keterangan : false,nu_card_no_urut : 6},
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "Pengusaha",nu_card_detil_keterangan : false,nu_card_no_urut : 7},
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "Anggota Dewan",nu_card_detil_keterangan : false,nu_card_no_urut : 8},
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "PNS / ASN",nu_card_detil_keterangan : false,nu_card_no_urut : 9},
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "Petani",nu_card_detil_keterangan : false,nu_card_no_urut : 10},
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "Swasta",nu_card_detil_keterangan : false,nu_card_no_urut : 11},
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "Bekerja untuk NGO",nu_card_detil_keterangan : false,nu_card_no_urut : 12},
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "Kepala Desa",nu_card_detil_keterangan : false,nu_card_no_urut : 13},
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "Camat",nu_card_detil_keterangan : false,nu_card_no_urut : 14},
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "Bupati/Walikota/Wabup/Wawakot",nu_card_detil_keterangan : false,nu_card_no_urut : 15},
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "Pengelola Rumah Tangga",nu_card_detil_keterangan : false,nu_card_no_urut : 16},
        {nu_card_id : "",nu_card_variabel : "Pekerjaan / Profesi",nu_card_detil_variabel : "",nu_card_detil_keterangan : false,nu_card_no_urut : 17}
    ];
    $scope.PendidikanFormal = [
        {nu_card_id : "",nu_card_variabel : "Pendidikan Formal",nu_card_detil_variabel : "SD",nu_card_detil_keterangan : false,nu_card_no_urut : 1},
        {nu_card_id : "",nu_card_variabel : "Pendidikan Formal",nu_card_detil_variabel : "SLTP Sederajat",nu_card_detil_keterangan : false,nu_card_no_urut : 2},
        {nu_card_id : "",nu_card_variabel : "Pendidikan Formal",nu_card_detil_variabel : "SLTA Sederajat",nu_card_detil_keterangan : false,nu_card_no_urut : 3},
        {nu_card_id : "",nu_card_variabel : "Pendidikan Formal",nu_card_detil_variabel : "Strata Satu",nu_card_detil_keterangan : false,nu_card_no_urut : 4},
        {nu_card_id : "",nu_card_variabel : "Pendidikan Formal",nu_card_detil_variabel : "Strata Dua",nu_card_detil_keterangan : false,nu_card_no_urut : 5},
        {nu_card_id : "",nu_card_variabel : "Pendidikan Formal",nu_card_detil_variabel : "Strata Tiga",nu_card_detil_keterangan : false,nu_card_no_urut : 6}
    ];
    $scope.PendidikanPesantren = [
        {nu_card_id : "",nu_card_variabel : "Lama Pendidikan Pesantren",nu_card_detil_variabel : "Lama Pendidikan Pesantrn",nu_card_detil_keterangan : false,nu_card_no_urut : 1}
    ];
    $scope.JenjangPesantren = [
        {nu_card_id : "",nu_card_variabel : "Jenjang Pendidikan Pesantren",nu_card_detil_variabel : "Jenjang Pendidikan Pesantren",nu_card_detil_keterangan : false,nu_card_no_urut : 1}
    ];
    $scope.PosisiPengurus = [
        {nu_card_id : "",nu_card_variabel : "Posisi Pengurus",nu_card_detil_variabel : "Posisi Pengurus",nu_card_detil_keterangan : false,nu_card_no_urut : 1}
    ];
    $scope.Pelatihan = [
        {nu_card_id : "",nu_card_variabel : "Pelatihan yang pernah diikuti",nu_card_detil_variabel : "1. Pelatihan Formal",nu_card_detil_keterangan : "",nu_card_no_urut : 1},
        {nu_card_id : "",nu_card_variabel : "Pelatihan yang pernah diikuti",nu_card_detil_variabel : "2. Pelatihan NU",nu_card_detil_keterangan : "",nu_card_no_urut : 2},
        {nu_card_id : "",nu_card_variabel : "Pelatihan yang pernah diikuti, non formal",nu_card_detil_variabel : "3. Pelatihan Non Formal, Pelatihan Daiyah",nu_card_detil_keterangan : false,nu_card_no_urut : 3},
        {nu_card_id : "",nu_card_variabel : "Pelatihan yang pernah diikuti, non formal",nu_card_detil_variabel : "3. Pelatihan Non Formal, Pelatihan Wirausaha",nu_card_detil_keterangan : false,nu_card_no_urut : 4},
        {nu_card_id : "",nu_card_variabel : "Pelatihan yang pernah diikuti, non formal",nu_card_detil_variabel : "3. Pelatihan Non Formal, Pelatihan MC",nu_card_detil_keterangan : false,nu_card_no_urut : 5},
        {nu_card_id : "",nu_card_variabel : "Pelatihan yang pernah diikuti, non formal",nu_card_detil_variabel : "3. Pelatihan Non Formal, Pelatihan Advokasi",nu_card_detil_keterangan : false,nu_card_no_urut : 6},
        {nu_card_id : "",nu_card_variabel : "Pelatihan yang pernah diikuti, non formal",nu_card_detil_variabel : "3. Pelatihan Non Formal, Pelatihan Gender",nu_card_detil_keterangan : false,nu_card_no_urut : 7},
        {nu_card_id : "",nu_card_variabel : "Pelatihan yang pernah diikuti, non formal",nu_card_detil_variabel : "3. Pelatihan Non Formal, Pelatihan IT",nu_card_detil_keterangan : false,nu_card_no_urut : 8},
        {nu_card_id : "",nu_card_variabel : "Pelatihan yang pernah diikuti, non formal",nu_card_detil_variabel : "",nu_card_detil_keterangan : "",nu_card_no_urut : 9},
        {nu_card_id : "",nu_card_variabel : "Pelatihan yang pernah diikuti, tot",nu_card_detil_variabel : "4. TOT yang pernah di ikuti, LKD/LKL",nu_card_detil_keterangan : false,nu_card_no_urut : 10},
        {nu_card_id : "",nu_card_variabel : "Pelatihan yang pernah diikuti, tot",nu_card_detil_variabel : "4. TOT yang pernah di ikuti, PKPNU",nu_card_detil_keterangan : false,nu_card_no_urut : 11},
        {nu_card_id : "",nu_card_variabel : "Pelatihan yang pernah diikuti, tot",nu_card_detil_variabel : "4. TOT yang pernah di ikuti, MKNU",nu_card_detil_keterangan : false,nu_card_no_urut : 12},
        {nu_card_id : "",nu_card_variabel : "Pelatihan yang pernah diikuti, tot",nu_card_detil_variabel : "4. TOT yang pernah di ikuti, MKP",nu_card_detil_keterangan : false,nu_card_no_urut : 13},
        {nu_card_id : "",nu_card_variabel : "Pelatihan yang pernah diikuti, tot",nu_card_detil_variabel : "",nu_card_detil_keterangan : "",nu_card_no_urut : 14},
    ];
    $scope.Fasilitator = [
        {nu_card_id : "",nu_card_variabel : "Pengalaman menjadi fasilitator",nu_card_detil_variabel : "LKD / LKL",nu_card_detil_keterangan : false,nu_card_no_urut : 1},
        {nu_card_id : "",nu_card_variabel : "Pengalaman menjadi fasilitator",nu_card_detil_variabel : "PKPNU",nu_card_detil_keterangan : false,nu_card_no_urut : 2},
        {nu_card_id : "",nu_card_variabel : "Pengalaman menjadi fasilitator",nu_card_detil_variabel : "MKNU",nu_card_detil_keterangan : false,nu_card_no_urut : 3},
        {nu_card_id : "",nu_card_variabel : "Pengalaman menjadi fasilitator",nu_card_detil_variabel : "MKP",nu_card_detil_keterangan : false,nu_card_no_urut : 4},
        {nu_card_id : "",nu_card_variabel : "Pengalaman menjadi fasilitator",nu_card_detil_variabel : "Lainnya...",nu_card_detil_keterangan : "",nu_card_no_urut : 5}
    ];
    $scope.Materi = [
        {nu_card_id : "",nu_card_variabel : "Materi (isu) yang dikuasai",nu_card_detil_variabel : "Pendidikan",nu_card_detil_keterangan : false,nu_card_no_urut : 1},
        {nu_card_id : "",nu_card_variabel : "Materi (isu) yang dikuasai",nu_card_detil_variabel : "Kesehatan",nu_card_detil_keterangan : false,nu_card_no_urut : 2},
        {nu_card_id : "",nu_card_variabel : "Materi (isu) yang dikuasai",nu_card_detil_variabel : "Ekonomi",nu_card_detil_keterangan : false,nu_card_no_urut : 3},
        {nu_card_id : "",nu_card_variabel : "Materi (isu) yang dikuasai",nu_card_detil_variabel : "Hukum",nu_card_detil_keterangan : false,nu_card_no_urut : 4},
        {nu_card_id : "",nu_card_variabel : "Materi (isu) yang dikuasai",nu_card_detil_variabel : "Islam dan kebangsaan",nu_card_detil_keterangan : false,nu_card_no_urut : 5},
        {nu_card_id : "",nu_card_variabel : "Materi (isu) yang dikuasai",nu_card_detil_variabel : "Gender",nu_card_detil_keterangan : false,nu_card_no_urut : 6},
        {nu_card_id : "",nu_card_variabel : "Materi (isu) yang dikuasai",nu_card_detil_variabel : "Lingkungan",nu_card_detil_keterangan : false,nu_card_no_urut : 7},
        {nu_card_id : "",nu_card_variabel : "Materi (isu) yang dikuasai",nu_card_detil_variabel : "Radikalisme dan Ekstrimisme",nu_card_detil_keterangan : false,nu_card_no_urut : 8},
        {nu_card_id : "",nu_card_variabel : "Materi (isu) yang dikuasai",nu_card_detil_variabel : "Politik",nu_card_detil_keterangan : false,nu_card_no_urut : 9},
        {nu_card_id : "",nu_card_variabel : "Materi (isu) yang dikuasai",nu_card_detil_variabel : "Lainnya...",nu_card_detil_keterangan : "",nu_card_no_urut : 10},
    ];
    $scope.Pendampingan = [
        {nu_card_id : "", nu_card_variabel : "Pengalaman Pendampingan",nu_card_detil_variabel : "Pendidikan",nu_card_detil_keterangan : false,nu_card_no_urut : 1}
    ];
    $scope.KerjaSama = [
        {nu_card_id : "", nu_card_variabel : "Pengalaman bekerja sama dengan lembaga lain",nu_card_detil_variabel : "Pemerintah",nu_card_detil_keterangan : false,nu_card_no_urut : 1},
        {nu_card_id : "", nu_card_variabel : "Pengalaman bekerja sama dengan lembaga lain",nu_card_detil_variabel : "NGO",nu_card_detil_keterangan : false,nu_card_no_urut : 2},
        {nu_card_id : "", nu_card_variabel : "Pengalaman bekerja sama dengan lembaga lain",nu_card_detil_variabel : "Lembaga Donor",nu_card_detil_keterangan : false,nu_card_no_urut : 3},
        {nu_card_id : "", nu_card_variabel : "Pengalaman bekerja sama dengan lembaga lain",nu_card_detil_variabel : "Perusahaan",nu_card_detil_keterangan : false,nu_card_no_urut : 4},
        {nu_card_id : "", nu_card_variabel : "Pengalaman bekerja sama dengan lembaga lain",nu_card_detil_variabel : "Lainnya...",nu_card_detil_keterangan : "",nu_card_no_urut : 5}
    ]
    $scope.Usaha = [
        {nu_card_id : "", nu_card_variabel : "Memiliki usaha ekonomi Kreatif",nu_card_detil_variabel : "Mamin",nu_card_detil_keterangan : false,nu_card_no_urut : 1},
        {nu_card_id : "", nu_card_variabel : "Memiliki usaha ekonomi Kreatif",nu_card_detil_variabel : "Fashion",nu_card_detil_keterangan : false,nu_card_no_urut : 2},
        {nu_card_id : "", nu_card_variabel : "Memiliki usaha ekonomi Kreatif",nu_card_detil_variabel : "HandyCraft",nu_card_detil_keterangan : false,nu_card_no_urut : 3},
        {nu_card_id : "", nu_card_variabel : "Memiliki usaha ekonomi Kreatif",nu_card_detil_variabel : "Lainnya...",nu_card_detil_keterangan : "",nu_card_no_urut : 4},
    ]


    $scope.loadDef = function() {        
        $http.post($rootScope.url_api + "mstCabang/select", {
            "keyword": "%%",
            "offset": 0,
            "limit": $scope.maxSize
        }).success(function(reply) {
            if (reply.status === 200) {
                var data = reply.result.data;
                $scope.Cabangs = data.data;
                $scope.Cabangs2 = data.data;
                $scope.totalItems = data.count;
                
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data jumlah Cabang!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        });
        
    };

    $scope.getNext = function(){
        $scope.Cabangs2 = null;
        var idRegion = $scope.Cabangs.find(function(row){
            return row.mst_cabang_id == $scope.data.PC;
        });
        $scope.Cabangs2 = idRegion;
        $http.post($rootScope.url_api + "mstRegion/selectize", {
            "keyword": idRegion.mst_region_id,
            "offset": 0,
            "limit": $scope.maxSize
        }).success(function(reply) {
            if (reply.status === 200) {
                var data = reply.result.data;
                $scope.Regions = data.data;
                $scope.totalItems = data.count;
                
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data jumlah Region!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        });

        $http.post($rootScope.url_api + "mstPAC/selectize", {
            "keyword": idRegion.mst_cabang_id,
            "offset": 0,
            "limit": $scope.maxSize
        }).success(function(reply) {
            if (reply.status === 200) {
                var data = reply.result.data;
                $scope.PAC = data.data;
                $scope.totalItems = data.count;
                
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data jumlah Region!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        });
    }

    $scope.getNext2 = function(){
        var idPAC = $scope.PAC.find(function(row){
            return row.mst_pac_id == $scope.data.PAC;
        });

        $http.post($rootScope.url_api + "mstRanting/selectize", {
            "keyword": idPAC.mst_pac_id,
            "offset": 0,
            "limit": $scope.maxSize
        }).success(function(reply) {
            if (reply.status === 200) {
                var data = reply.result.data;
                $scope.Rantings = data.data;
                $scope.totalItems = data.count;
                
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data jumlah Region!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        });
    }
    $scope.myConfig = {
        create: true,
        // required: true,
        maxItems: 1,
        searchField: ['mst_cabang_nama_cabang'],
        valueField: 'mst_cabang_id',
        labelField: 'mst_cabang_nama_cabang',
      }
    $scope.myConfig2 = {
        create: true,
        // required: true,
        maxItems: 1,
        searchField: ['mst_region_name'],
        valueField: 'mst_region_id',
        labelField: 'mst_region_name',
      }
    $scope.myConfig3 = {
        create: true,
        persist: false,
        // required: true,
        maxItems: 1,
        searchField: ['mst_cabang_nama_cabang'],
        valueField: 'mst_cabang_id',
        labelField: 'mst_cabang_nama_cabang',
    }
    $scope.myConfig4 = {
        create: true,
        persist: false,
        // required: true,
        maxItems: 1,
        searchField: ['mst_pac_nama'],
        valueField: 'mst_pac_id',
        labelField: 'mst_pac_nama',
    }
    $scope.myConfig5 = {
        create: true,
        // persist: false,
        // required: true,
        maxItems: 1,
        searchField: ['mst_ranting_nama'],
        valueField: 'msr_ranting_id',
        labelField: 'mst_ranting_nama',
        onChange: function(value){
            $http.post($rootScope.url_api + "nuCard/select", {
                "keyword" : value
            }).success(function(reply) {
                if (reply.status === 200) {
                    $scope.getNuCard = reply.result.data.data;
                }
            }).error(function(err) {
                $.growl.error({
                    message: "Gagal Akses API >" + err
                });
                return
            });
        }
    }
    $scope.myConfig6 = {
        create: true,
        // persist: false,
        // required: true,
        maxItems: 1,
        searchField: ['nu_card_nama'],
        valueField: 'nu_card_nama',
        labelField: 'nu_card_nama',
        onChange: function(value){
            // console.log(value);
            var nuCardID = value;
            if(nuCardID != undefined){
                $http.post($rootScope.url_api + "nuCard/selectize", {
                    "keyword" : nuCardID
                }).success(function(reply) {
                    if (reply.status === 200) {
                        $scope.getDataNuCardSelectize = reply.result.data.data;
                        $scope.getNuCardDetail = reply.result.data.detail;
                        console.log($scope.getDataNuCardSelectize);
                        $scope.data.tempatLahir = $scope.getDataNuCardSelectize[0].nu_card_tempat_lahir;
                        $scope.data.tanggalLahir = $scope.getDataNuCardSelectize[0].nu_card_tgl_lahir;
                        $scope.data.nik = $scope.getDataNuCardSelectize[0].nu_card_nik;
                        $scope.data.alamat = $scope.getDataNuCardSelectize[0].nu_card_alamat;
                        $scope.data.noBpjs = $scope.getDataNuCardSelectize[0].nu_card_no_bpjs;
                        $scope.data.isMuslimat = $scope.getDataNuCardSelectize[0].nu_card_isi_data_muslimat;
                        $scope.gambar = $scope.getDataNuCardSelectize[0].nu_card_gambar;
                        var checkedPekerjaan = new Array();
                        var checkedPendidikanFormal = new Array();
                        var pelatihanFormal = new Array();
                        var pelatihanTot = new Array();
                        var fasilitator = new Array();
                        var materi = new Array();
                        var kerjaSama = new Array();
                        var mamin = new Array();
                        var index = 0;
                        var index1 = 0;
                        var index2 = 0;
                        var index3 = 0;
                        var index4 = 0;
                        var index5 = 0;
                        var index6 = 0;
                        var index7 = 0;
                        $scope.getNuCardDetail.forEach(function(row){
                            if(row.nu_card_variabel == "Pengalaman Pendampingan"){
                                $scope.Pendampingan[0].nu_card_detil_keterangan = row.nu_card_detil_keterangan;
                            }
                            else if(row.nu_card_variabel == "Pelatihan yang pernah diikuti"){
                                if(row.nu_card_detil_variabel == "1. Pelatihan Formal"){
                                    $scope.Pelatihan[0].nu_card_detil_keterangan = row.nu_card_detil_keterangan;
                                }else if(row.nu_card_detil_variabel == "2. Pelatihan NU"){
                                    $scope.Pelatihan[1].nu_card_detil_keterangan = row.nu_card_detil_keterangan;
                                }
                            }
                            else if(row.nu_card_variabel == "Posisi Pengurus"){
                                $scope.PosisiPengurus[0].nu_card_detil_keterangan = row.nu_card_detil_keterangan;
                            }
                            else if(row.nu_card_variabel == "Jenjang Pendidikan Pesantren"){
                                $scope.JenjangPesantren[0].nu_card_detil_keterangan = row.nu_card_detil_keterangan;
                            }
                            else if(row.nu_card_variabel == "Lama Pendidikan Pesantren"){
                                $scope.PendidikanPesantren[0].nu_card_detil_keterangan = row.nu_card_detil_keterangan;
                            }
                            else if(row.nu_card_variabel == "Pekerjaan / Profesi"){
                                checkedPekerjaan[index] = row;
                                index++;
                            }else if(row.nu_card_variabel == "Pendidikan Formal"){
                                checkedPendidikanFormal[index1] = row;
                                index1++;
                            }else if(row.nu_card_variabel == "Pelatihan yang pernah diikuti, non formal"){
                                pelatihanFormal[index2] = row;
                                index2++;
                            }else if(row.nu_card_variabel == "Pelatihan yang pernah diikuti, tot"){
                                pelatihanTot[index3] = row;
                                index3++;
                            }else if(row.nu_card_variabel == "Pengalaman menjadi fasilitator"){
                                fasilitator[index4] = row;
                                index4++;
                            }
                            else if(row.nu_card_variabel == "Materi (isu) yang dikuasai"){
                                materi[index5] = row;
                                index5++;
                            }
                            else if(row.nu_card_variabel == "Pengalaman bekerja sama dengan lembaga lain"){
                                kerjaSama[index6] = row;
                                index6++;
                            }
                            else if(row.nu_card_variabel == "Memiliki usaha ekonomi Kreatif"){
                                mamin[index7] = row;
                                index7++;
                            }
                        });
                        $scope.Pekerjaan.forEach(function(row){
                            for(var i = 0; i< checkedPekerjaan.length; i++){
                                if(row.nu_card_detil_variabel == checkedPekerjaan[i]['nu_card_detil_variabel']){
                                    row.nu_card_detil_keterangan = (checkedPekerjaan[i]['nu_card_detil_keterangan'] == 1) ? true : false;
                                }
                            }
                        });
                        $scope.PendidikanFormal.forEach(function(row){
                            for(var i = 0; i< checkedPendidikanFormal.length; i++){
                                if(row.nu_card_detil_variabel == checkedPendidikanFormal[i]['nu_card_detil_variabel']){
                                    row.nu_card_detil_keterangan = (checkedPendidikanFormal[i]['nu_card_detil_keterangan'] == 1) ? true : false;
                                }
                            }
                        });
                        $scope.Pelatihan.forEach(function(row){
                            for(var i = 0; i< pelatihanFormal.length; i++){
                                if(row.nu_card_detil_variabel == pelatihanFormal[i]['nu_card_detil_variabel']){
                                    row.nu_card_detil_keterangan = (pelatihanFormal[i]['nu_card_detil_keterangan'] == 1) ? true : false;
                                }
                            }
                        });
                        $scope.Pelatihan.forEach(function(row){
                            for(var i = 0; i< pelatihanTot.length; i++){
                                if(row.nu_card_detil_variabel == pelatihanTot[i]['nu_card_detil_variabel']){
                                    row.nu_card_detil_keterangan = (pelatihanTot[i]['nu_card_detil_keterangan'] == 1) ? true : false;
                                }
                            }
                        });
                        $scope.Fasilitator.forEach(function(row){
                            for(var i = 0; i< fasilitator.length; i++){
                                if(row.nu_card_detil_variabel == fasilitator[i]['nu_card_detil_variabel']){
                                    row.nu_card_detil_keterangan = (fasilitator[i]['nu_card_detil_keterangan'] == 1) ? true : false;
                                }
                            }
                        });
                        $scope.Materi.forEach(function(row){
                            for(var i = 0; i< materi.length; i++){
                                if(row.nu_card_detil_variabel == materi[i]['nu_card_detil_variabel']){
                                    row.nu_card_detil_keterangan = (materi[i]['nu_card_detil_keterangan'] == 1) ? true : false;
                                }
                            }
                        });
                        $scope.KerjaSama.forEach(function(row){
                            for(var i = 0; i< kerjaSama.length; i++){
                                if(row.nu_card_detil_variabel == kerjaSama[i]['nu_card_detil_variabel']){
                                    row.nu_card_detil_keterangan = (kerjaSama[i]['nu_card_detil_keterangan'] == 1) ? true : false;
                                }
                            }
                        });
                        $scope.Usaha.forEach(function(row){
                            for(var i = 0; i< mamin.length; i++){
                                if(row.nu_card_detil_variabel == mamin[i]['nu_card_detil_variabel']){
                                    row.nu_card_detil_keterangan = (mamin[i]['nu_card_detil_keterangan'] == 1) ? true : false;
                                }
                            }
                        });
                        console.log(checkedPekerjaan);
                    }
                }).error(function(err) {
                    $.growl.error({
                        message: "Gagal Akses API >" + err
                    });
                    return
                });
            }
        }
    }

    $scope.loadData = function(){

    }

    $scope.fileMChanged = function (elm) {
        $scope.fileDocument = elm.files;
        if ($scope.fileDocument !== '' || $scope.fileDocument !== undefined) {
            for (var i = 0; i < $scope.fileDocument.length; i++) {
                $scope.fName = $scope.fileDocument[i].name;
                $scope.fSize = ($scope.fileDocument[i].size / 1000).toFixed(1);
                //console.log($scope.fileDocument[i]);
                if ($scope.filesToUpload.length > 0) {
                    for (i = 0; i < $scope.filesToUpload.length; i++) {
                        if ($scope.fName === $scope.filesToUpload[i].fileName) {
                            $.growl.error({title: "[PERINGATAN]", message: "File sudah ada "});
                            document.getElementById('uploadFile').value = '';
                            $scope.fileDocument = '';
                            $scope.fName = '';
                            $scope.fSize = '';
                            return;
                        }
                    }
                }
            }
        }
    };
    $scope.removeFile = function (fileName) {
        var idx = -1;
        $.each($scope.filesToUpload, function (index, item) {
            if (item.fileName === fileName) {
                idx = index;
            }
        });
        $scope.filesToUpload.splice(idx, 1);
    };
    $scope.uploads = function () {
        var idx = -1;
        $.each($scope.filesToUpload, function (index, item) {
            if (item.fileName === $scope.fName) {
                idx = index;
            }
        });
        if ($scope.fileDocument === '' || $scope.fileDocument === undefined) {
            $.growl.error({title: "[PERINGATAN]", message: "File belum dipilih"});
            return;
        } else if (idx > -1) {
            $.growl.error({title: "[PERINGATAN]", message: "File sudah ada"});
            return;
        } else {
            var fileInput = $('.upload-file');
            var extFile = $('.upload-file').val().split('.').pop().toLowerCase();
            var maxSize = 100000;
            if (fileInput.get(0).files.length) {
                var fileSize = fileInput.get(0).files[0].size;
                if (fileSize > maxSize) {
                    $rootScope.unloadLoadingModal();
                    $.growl.error({title: "[WARNING]", message: "Ukuran file terlalu besar"});
                    return;
                } else {
                    var restrictedExt = ["jpg", "png","jpeg"]
                    if ($.inArray(extFile, restrictedExt) === -1) {
                        $rootScope.unloadLoadingModal();
                        $.growl.error({title: "[WARNING]", message: "Format file tidak valid"});
                        return;
                    } else {
                        $scope.filesToUpload.push({
                            fileName: $scope.fName,
                            fileSize: $scope.fSize,
                            file: $scope.fileDocument
                        });
                    }
                }
            }
        }
    };

    $scope.simpan = function(){
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Data berhasil di simpan',
            showConfirmButton: false,
            timer: 1500
          });
        //nuCard
        $scope.dataNuCard.mst_cabang_id = $scope.data.PC;
        $scope.dataNuCard.nu_card_nama = $scope.data.nama;
        // var ttl = $scope.data.ttl.split(",");
        $scope.dataNuCard.nu_card_tempat_lahir = $scope.data.tempatLahir;
        $scope.dataNuCard.nu_card_tgl_lahir = $scope.data.tanggalLahir;
        $scope.dataNuCard.nu_card_nik = $scope.data.nik;
        $scope.dataNuCard.nu_card_alamat = $scope.data.alamat;
        $scope.dataNuCard.nu_card_no_bpjs = $scope.data.noBpjs;
        $scope.dataNuCard.nu_card_anggota_pc = $scope.data.PC;
        $scope.dataNuCard.nu_card_anggota_pac = $scope.data.PAC;
        $scope.dataNuCard.nu_card_anggota_ranting = $scope.data.RantingId;
        $scope.dataNuCard.nu_card_isi_data_muslimat = $scope.data.isMuslimat;

        var fd = new FormData();
        for (var i = 0; i < $scope.filesToUpload.length; i++) {
            angular.forEach($scope.filesToUpload[i].file, function (item) {
                fd.append("uploads", item);
            });
        }
        $http.post($rootScope.url_api + "/upload/0/", fd,
                {
                    withCredentials: true,
                    transformRequest: angular.identity(),
                    headers: {'Content-Type': undefined}
                }).success(function(reply) {
                    if (reply.status === 200) {
                        $scope.dataNuCard.nu_card_gambar = reply.result.data.files[0].url;
                        $http.post($rootScope.url_api + "nuCard/insert", {
                            "data" : $scope.dataNuCard,
                        }).success(function(reply) {
                            if (reply.status === 200) {
                                var data = reply.result.data;
                                $scope.idCard = data.id;
                                $scope.cardDetail = [];
                                //Pekerjaan
                                $scope.Pekerjaan.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    if(row.nu_card_detil_keterangan === true && row.nu_card_detil_variabel != ""){
                                        $scope.cardDetail.push(row);
                                    }
                                });
                                
                                $scope.PendidikanFormal.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    if(row.nu_card_detil_keterangan === true){
                                        $scope.cardDetail.push(row);
                                    }
                                });
                                
                                $scope.PendidikanPesantren.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    $scope.cardDetail.push(row);
                                });
                                
                                
                                $scope.JenjangPesantren.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    $scope.cardDetail.push(row);
                                });
                
                                $scope.PosisiPengurus.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    $scope.cardDetail.push(row);
                                });
                
                                $scope.Pelatihan.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    if(row.nu_card_detil_keterangan != "" || row.nu_card_detil_keterangan == true){
                                        $scope.cardDetail.push(row);
                                    }
                                });
                
                                $scope.Fasilitator.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    if(row.nu_card_detil_keterangan != "" || row.nu_card_detil_keterangan == true){
                                        $scope.cardDetail.push(row);
                                    }
                                });
                                $scope.Materi.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    if(row.nu_card_detil_keterangan != "" || row.nu_card_detil_keterangan == true){
                                        $scope.cardDetail.push(row);
                                    }
                                });
                                $scope.Pendampingan.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    if(row.nu_card_detil_keterangan != "" || row.nu_card_detil_keterangan == true){
                                        $scope.cardDetail.push(row);
                                    }
                                });
                                $scope.KerjaSama.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    if(row.nu_card_detil_keterangan != "" || row.nu_card_detil_keterangan == true){
                                        $scope.cardDetail.push(row);
                                    }
                                });
                                $scope.Usaha.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    if(row.nu_card_detil_keterangan != "" || row.nu_card_detil_keterangan == true){
                                        $scope.cardDetail.push(row);
                                    }
                                });
                                console.log($scope.cardDetail);
                                $http.post($rootScope.url_api + "nuCard/insertDetail", {
                                    "data" : $scope.cardDetail
                                }).success(function(reply) {
                                    if (reply.status === 200) {
                                        Swal.fire({
                                            position: 'top-end',
                                            icon: 'success',
                                            title: 'Data Detail berhasil di simpan',
                                            showConfirmButton: false,
                                            timer: 1500
                                          });
                                          location.reload();
                                    }
                                }).error(function(err) {
                                    $.growl.error({
                                        message: "Gagal Akses API >" + err
                                    });
                                    return
                                });
                                
                            } else {
                                // $.growl.error({
                                //     message: "Gagal mendapatkan data jumlah Cabang!"
                                // });
                                // return
                            }
                        }).error(function(err) {
                            $.growl.error({
                                message: "Gagal Akses API >" + err
                            });
                            return
                        });                        
                    }
                }).error(function(err) {
                    $.growl.error({
                        message: "Gagal Akses API >" + err
                    });
                    return
                });
    }
    $scope.sent = function(){
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Data berhasil di simpan',
            showConfirmButton: false,
            timer: 1500
          });
        //nuCard
        $scope.dataNuCard.mst_cabang_id = $scope.data.PC;
        $scope.dataNuCard.nu_card_nama = $scope.data.nama;
        $scope.dataNuCard.nu_card_tempat_lahir = $scope.data.tempatLahir;
        $scope.dataNuCard.nu_card_tgl_lahir = $scope.data.tanggalLahir;
        $scope.dataNuCard.nu_card_nik = $scope.data.nik;
        $scope.dataNuCard.nu_card_alamat = $scope.data.alamat;
        $scope.dataNuCard.nu_card_no_bpjs = $scope.data.noBpjs;
        $scope.dataNuCard.nu_card_anggota_pc = $scope.data.PC;
        $scope.dataNuCard.nu_card_anggota_pac = $scope.data.PAC;
        $scope.dataNuCard.nu_card_anggota_ranting = $scope.data.RantingId;
        $scope.dataNuCard.nu_card_isi_data_muslimat = $scope.data.isMuslimat;
        $scope.dataNuCard.nu_card_is_active = 1;

        var fd = new FormData();
        for (var i = 0; i < $scope.filesToUpload.length; i++) {
            angular.forEach($scope.filesToUpload[i].file, function (item) {
                fd.append("uploads", item);
            });
        }
        $http.post($rootScope.url_api + "/upload/0/", fd,
                {
                    withCredentials: true,
                    transformRequest: angular.identity(),
                    headers: {'Content-Type': undefined}
                }).success(function(reply) {
                    if (reply.status === 200) {
                        $scope.dataNuCard.nu_card_gambar = reply.result.data.files[0].url;
                        $http.post($rootScope.url_api + "nuCard/insert", {
                            "data" : $scope.dataNuCard,
                        }).success(function(reply) {
                            if (reply.status === 200) {
                                var data = reply.result.data;
                                $scope.idCard = data.id;
                                $scope.cardDetail = [];
                                //Pekerjaan
                                $scope.Pekerjaan.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    if(row.nu_card_detil_keterangan === true && row.nu_card_detil_variabel != ""){
                                        $scope.cardDetail.push(row);
                                    }
                                });
                                
                                $scope.PendidikanFormal.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    if(row.nu_card_detil_keterangan === true){
                                        $scope.cardDetail.push(row);
                                    }
                                });
                                
                                $scope.PendidikanPesantren.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    $scope.cardDetail.push(row);
                                });
                                
                                
                                $scope.JenjangPesantren.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    $scope.cardDetail.push(row);
                                });
                
                                $scope.PosisiPengurus.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    $scope.cardDetail.push(row);
                                });
                
                                $scope.Pelatihan.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    if(row.nu_card_detil_keterangan != "" || row.nu_card_detil_keterangan == true){
                                        $scope.cardDetail.push(row);
                                    }
                                });
                
                                $scope.Fasilitator.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    if(row.nu_card_detil_keterangan != "" || row.nu_card_detil_keterangan == true){
                                        $scope.cardDetail.push(row);
                                    }
                                });
                                $scope.Materi.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    if(row.nu_card_detil_keterangan != "" || row.nu_card_detil_keterangan == true){
                                        $scope.cardDetail.push(row);
                                    }
                                });
                                $scope.Pendampingan.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    if(row.nu_card_detil_keterangan != "" || row.nu_card_detil_keterangan == true){
                                        $scope.cardDetail.push(row);
                                    }
                                });
                                $scope.KerjaSama.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    if(row.nu_card_detil_keterangan != "" || row.nu_card_detil_keterangan == true){
                                        $scope.cardDetail.push(row);
                                    }
                                });
                                $scope.Usaha.forEach(row => {
                                    row.nu_card_id = $scope.idCard
                                    if(row.nu_card_detil_keterangan != "" || row.nu_card_detil_keterangan == true){
                                        $scope.cardDetail.push(row);
                                    }
                                });
                                console.log($scope.cardDetail);
                                $http.post($rootScope.url_api + "nuCard/insertDetail", {
                                    "data" : $scope.cardDetail
                                }).success(function(reply) {
                                    if (reply.status === 200) {
                                        Swal.fire({
                                            position: 'top-end',
                                            icon: 'success',
                                            title: 'Data Detail berhasil di simpan',
                                            showConfirmButton: false,
                                            timer: 1500
                                          });
                                          location.reload();
                                    }
                                }).error(function(err) {
                                    $.growl.error({
                                        message: "Gagal Akses API >" + err
                                    });
                                    return
                                });
                                
                            } else {
                                $.growl.error({
                                    message: "Gagal mendapatkan data jumlah Cabang!"
                                });
                                return
                            }
                        }).error(function(err) {
                            $.growl.error({
                                message: "Gagal Akses API >" + err
                            });
                            return
                        });                        
                    }
                }).error(function(err) {
                    $.growl.error({
                        message: "Gagal Akses API >" + err
                    });
                    return
                });
    }
});
