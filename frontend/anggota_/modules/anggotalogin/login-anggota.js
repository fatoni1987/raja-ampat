angular.module("pajak")
.controller("loginAnggotaCtrl", function($scope, $cookieStore, $state,$cookies, $rootScope, $http, SocketService, $translate) {
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxSize = 1;
    $scope.srcText = "";
    $scope.pemenang = [];
    $scope.failedMsg = "";
    $scope.init = function() {
        $state.reload();
        $scope.Captcha()
    };
    $scope.Captcha = function() {
        var alpha = new Array("A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "m", "n", "o", "p", "q", "r", "s", "t",
            "u", "v", "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
        var tCtx = document.getElementById("textCanvas").getContext("2d");
        var imageElem = document.getElementById("image");
        for (var i = 0; i < 3; i++) {
            var a = alpha[Math.floor(Math.random() * alpha.length)];
            var b = alpha[Math.floor(Math.random() * alpha.length)];
            var c = alpha[Math.floor(Math.random() * alpha.length)];
            var d = alpha[Math.floor(Math.random() * alpha.length)]
        }
        var code = a + " " + b + " " + c + " " + d;
        tCtx.canvas.width = tCtx.measureText(code).width+3;
        tCtx.fillText(code, 0,
            10);
        imageElem.src = tCtx.canvas.toDataURL();
        $scope.passCaptcha = $.md5(removeSpaces(code))
    };

    function removeSpaces(string) {
        return string.split(" ").join("")
    }

    $scope.submitCaptcha = function(evt){
        let charCode = (evt.keyCode ? evt.keyCode : evt.which);
        if (charCode == 13) {
            $scope.login();
        }
    }

    $scope.validCaptcha = function() {
        var string1 = $scope.passCaptcha;
        var string2 = $.md5(removeSpaces(document.getElementById("txtInput").value));
        if (string1 === string2) return true;
        else {
            $.growl.error({
                title: "[WARNING]",
                message: "Kode yang anda masukan salah!"
            });
            $rootScope.unloadLoading();
            document.getElementById("txtInput").value = "";
            $scope.Captcha();
            return false
        }
    };
    $scope.login = function() {
        if ($.trim($scope.username) === "" || $.trim($scope.password) === "") {
            $scope.message = $translate.instant('hm193')
            $.growl.warning({
                message: $scope.message
            });
            return
        }
        
        $rootScope.loadLoading("Verifying credentials...");
        if ($scope.validCaptcha()) $http.post($rootScope.url_api + "auth/login/vendor", {
            username: $scope.username,
            password: $scope.password,
            api_key: $rootScope.api_key
        }).success(function(result) {
            if (result.status === 200) {
                var data = result.data;
                $rootScope.unloadLoading();
                if (data.success) {                    
                    $cookieStore.put("vendor_sessId",data.session_id);
                    $http.post($rootScope.url_api + "auth/get_session", {
                        session_id: $cookieStore.get("vendor_sessId")
                    }).success(function(result2) {
                        if (result2.status === 200) {
                            $rootScope.userSession = result2.data;
                            $.growl.notice({
                                message: "Login berhasil! Selamat datang, "
                            });
                            
                            // $state.go('homeadmin');
                            $state.go("pajak");
                            $rootScope.isLogged = true
                        }
                    }).error(function(err) {
                        $.growl.error({
                            message: "Gagal Akses API >" + err
                        })
                    })
                } else if (data.session_id === 0) {
                    $.growl.error({
                        message: data.message
                    });
                    return
                } else{                    
                    $http.post($rootScope.url_api + "auth/get_session", {
                        session_id: data.session_id
                    }).success(function(result2) {
                        if (result2.status === 200) {
                            var user = result2.data;
                            $http.post($rootScope.url_api + "auth/logout", {
                                session_id: data.session_id
                            }).success(function(result) {
                                if (result.status === 200)
                                    if (result.data.success) {
                                        $cookieStore.remove("vendor_sessId");
                                        $rootScope.isLogged = false;
                                        SocketService.emit("forceLogoff", user, function() {});
                                        $scope.login()
                                    } else $.growl.error({
                                        message: result.data.message
                                    })
                            }).error(function(err) {
                                $.growl.error({
                                    message: "Gagal Akses API >" +
                                        err
                                })
                            })
                        }
                    }).error(function(err) {
                        $.growl.error({
                            message: "Gagal Akses API >" + err
                        })
                    })
                } 
            } else {
                $rootScope.unloadLoading();
                $.growl.error({
                    message: result.message
                })
            }
        }).error(function(error) {
            $rootScope.unloadLoading();
            $.growl.error({
                message: "Gagal Akses API >" + error
            })
        });
        else return
    }
});