'use strict';
var $stateProviderRef = null;
var $urlRouterProviderRef = null;
var pajak = angular
    .module( 'pajak', [
        'oc.lazyLoad', 'ui.router', 'ui.bootstrap', 'ui.bootstrap.datetimepicker', 'angular-loading-bar','selectize', 'ngCookies', 'ngSanitize',
        'ui.tinymce', 'treeGrid', 'ngProgress', 'pascalprecht.translate','ui.tinymce'
    ])
    .factory('$exceptionHandler', ['$injector', function ($injector) {
        return function (...args) {
            const api = window.location.pathname.replace("pimpinan/", "") + "api/public/";
            var http = $injector.get('$http');
            var info = {
                url: window.location.href,
                exception: args[0].message,
                stack: args[0].stack
            };
            console.error(args[0].message);
            console.error(args[0].stack)
            http({ method: 'POST', data: info, url: `${api}loggingJS` })
                .then(response => console.log('exception info submitted to server...'))
                .catch(reason => console.error('unable to send exception info to server due to...', reason));
        }
    }]);

pajak.run(['$rootScope', '$state', '$stateParams', '$cookieStore','$cookies', '$http', 'SocketService', '$location', '$q', '$translate', '$timeout',
    function($rootScope, $state, $stateParams, $cookieStore,$cookies, $http, SocketService, $location, $q, $translate, $timeout) {
        
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.applicationName = "Raja Ampat";
        $rootScope.root_location = window.location.pathname;
        $rootScope.url = window.location.pathname + "#/";
        $rootScope.url_api = window.location.pathname + "api/public/";
        $rootScope.url_image = window.location.pathname;
        $rootScope.manual_root_url = $rootScope.url_api + "download-manual/";
        $rootScope.adminhost = window.location.pathname + "pajak/";
        $rootScope.isLogged = false;
        $rootScope.isVerifikasi = {};
        $rootScope.userlogged = "";
        $rootScope.employeename = "";
        $rootScope.destination = "";
        $rootScope.currencySymbol = 'Rp ';
        $rootScope.listconfig = [];
        $rootScope.serversekarang = '';
        $rootScope.countdirektur = [];
        $rootScope.adadirektur = false;
        $rootScope.hostname = window.location.hostname;
        $rootScope.statusIsiData = {};
        $rootScope.userSession = undefined;
        $rootScope.isUAT = false;
        // bahasa
        $rootScope.language;

        $rootScope.$on("$stateChangeSuccess", function(event, currentRoute) {
            $rootScope.pageTitle = currentRoute.pageTitle;
            $rootScope.namePage = currentRoute.name;
            $rootScope.api_key = 'c44acc2327ef633b4d754dcb69dd07e5';
            $rootScope.userSession = undefined;
            $rootScope.checkSession();
            // initTranslate()
            $rootScope.sessId = $cookieStore.get("vendor_sessId");
//            $rootScope.pageID = currentRoute.pageID;
        });

        $rootScope.$on('$translateChangeSuccess', function () {
            initTranslate();
        });
      
        SocketService.on('feed', function(data) {
            $rootScope.serversekarang = data.timestamp;
        });

        $rootScope.checkUAT = function(){
            var link = window.location.pathname.split("/");
            if(link[1] == 'uat' || link[1] == 'uat-new'){
                $rootScope.isUAT = true;
            }
            else{
                $rootScope.isUAT = false;
            }
        };

        $rootScope.upload = function(fileDocument, folder) {
            $rootScope.loadLoading("Uploading...");
            var deferred = $q.defer();
            var fd = new FormData();

            angular.forEach(fileDocument, function(item) {
                fd.append("uploads", item);
            });

            $http.post($rootScope.url_api + "upload/" + folder + "/", fd, {
                withCredentials: true,
                transformRequest: angular.identity(),
                headers: {'Content-Type': undefined}
            }).success(function(reply) {

                deferred.resolve(reply);
                $rootScope.unloadLoading();
                //console.info("reply di app " + JSON.stringify(reply));
            }).error(function(err) {

                deferred.reject(err);
                //console.info("error di app " + JSON.stringify(err));
                $rootScope.unloadLoading();
                $http.post($rootScope.url_api + "logging", {
                    message: $rootScope.message2 + JSON.stringify(err),
                    source: "app.js - upload"
                }).then(function(response) {
                });
                $.growl.error({title: "[WARNING]", message: $rootScope.message3 + " app.js"});
            });
            return deferred.promise;
        };

        $rootScope.uploadZip = function(files, folder) {
            $rootScope.loadLoading("Uploading...");
            var deferred = $q.defer();
            var fd = new FormData();
            for (var i = 0; i < files.length; i++) {
                angular.forEach(files[i].file, function(item) {
                    fd.append("uploads[]", item);
                });
            }
//            angular.forEach(fileDocument, function(item) {
//                fd.append("uploads", item);
//            });

            $http.post($rootScope.url_api + "uploadZip/" + folder , fd, {
                withCredentials: true,
                transformRequest: angular.identity(),
                headers: {'Content-Type': undefined}
            }).success(function(reply) {
                $rootScope.unloadLoading();
                deferred.resolve(reply);
            }).error(function(err) {
                $rootScope.unloadLoading();
                deferred.reject(err);
                $http.post($rootScope.url_api + "logging", {
                    message: $rootScope.message1 + JSON.stringify(err),
                    source: "app.js - uploadZip"
                }).then(function(response) {
                });
                $.growl.error({title: "[WARNING]", message: $rootScope.message3});
            });
            return deferred.promise;
        };

        $rootScope.uploadZipByUrl = function(url) {
//            $rootScope.loadLoading("Uploading...");
            var deferred = $q.defer();
            $http.post($rootScope.url_api + "uploadZip", {
                url: url
            }).success(function(reply) {
                $rootScope.unloadLoading();
                deferred.resolve(reply);
            }).error(function(err) {
//                $rootScope.unloadLoading();
                deferred.reject(err);
                $http.post($rootScope.url_api + "logging", {
                    message: $rootScope.message1 + JSON.stringify(err),
                    source: "app.js - uploadZip"
                }).then(function(response) {
                });
                $.growl.error({title: "[WARNING]", message: $rootScope.message3});
            });
            return deferred.promise;
        };

        $rootScope.customTinymce = {
            theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link forecolor backcolor",
            toolbar2: "",
            height: "200px",
            width: "auto",
            setup: function(editor) {
                $timeout(function(){ editor.focus(); })
            }
        }

        $rootScope.deleteFileSingle = function(url) {
            $rootScope.loadLoading("Deleting...");
            var deferred = $q.defer();
            $http.post($rootScope.url_api + "deleteFile", {
                url: url
            }).success(function(reply) {
                $rootScope.unloadLoading();
                deferred.resolve(reply);
            }).error(function(err) {
                $rootScope.unloadLoading();
                deferred.reject(err);
                $http.post($rootScope.url_api + "logging", {
                    message: $rootScope.message1 + JSON.stringify(err),
                    source: "app.js - delete"
                }).then(function(response) {
                });
                $.growl.error({title: "[WARNING]", message: $rootScope.message3});
            });
            return deferred.promise;
        };

        $rootScope.deleteFileMultiple = function(urls) {
            $rootScope.loadLoading("Deleting...");
            var deferred = $q.defer();
            $http.post($rootScope.url_api + "deleteFile", {
                urls: urls
            }).success(function(reply) {
                $rootScope.unloadLoading();
                deferred.resolve(reply);
            }).error(function(err) {
                $rootScope.unloadLoading();
                deferred.reject(err);
                $http.post($rootScope.url_api + "logging", {
                    message: $rootScope.message1 + JSON.stringify(err),
                    source: "app.js - delete"
                }).then(function(response) {
                });
                $.growl.error({title: "[WARNING]", message: $rootScope.message3});
            });
            return deferred.promise;
        };

        $rootScope.fileuploadconfig = function(obj) {
            $http.post($rootScope.url_api + "limitfile", {id_page_config: obj, layer: "rekanan"})
                .success(function(reply) {
                    if (reply.status === 200) {
                        var data = reply.result.data;
                        $rootScope.listconfig = data; /* untuk config file upload */
                        $rootScope.ukuranfile = data[0].limit_size;
                        var ukuranfilefixed = Number($rootScope.ukuranfile);
                        var hasil = ukuranfilefixed * 1024;
                        $rootScope.filemaksimal = hasil.toString();

                        $rootScope.filetypelist = [];
                        $rootScope.limitfiletype = [];
                        for (var i = 0; i < $rootScope.listconfig.length; i++) {
                            $rootScope.filetypelist.push({
                                file_type_name: $rootScope.listconfig[i].file_type_name
                            });

                            $rootScope.limitfiletype.push(
                                $rootScope.listconfig[i].file_type_name
                            );
                        }
                    } else {
                        $.growl.error({message: $rootScope.message4}); // // Gagal dapat rekanan.configuration.limitfile
                        return;
                    }
                })
                .error(function(err) {
                    $.growl.error({message: $rootScope.message1 + err});
                    return;
                });
        };

        SocketService.on('actionLogoff', function(data) {
            if ($cookieStore.get("vendor_sessId") == data.session_id) {
                alert("You are going to logged out!");
                $cookieStore.remove("vendor_sessId");
                // $cookieStore.remove("IP");
                // $cookieStore.remove("token");

                $state.go('login-rekanan');
                $rootScope.isLogged = false;
            }
        });

        $rootScope.checkSession = function() {
            var session = $cookieStore.get('vendor_sessId');
            ////console.log("Session id: " + JSON.stringify(session));
            if (session !== undefined && session !== null) {
                $rootScope.loadLoading("Authenticating. . .");
                $http.post($rootScope.url_api + "auth/get_session", {
                    session_id: $cookieStore.get("vendor_sessId")
                })
                .success(function(result2) {
                    if (result2.status === 200) { // session still active
                        if(result2.data.is_maintenance == true){
                            // $state.go('beranda');
                            window.location.reload();
                        }
                        else{
                            $rootScope.userSession = result2.data;
                            // $rootScope.jenis_vendor = $rootScope.userSession.session_data.jenis_vendor;
                            // $rootScope.trigger_verified = $rootScope.userSession.session_data.trigger_verified;
                            //$rootScope.accessMenu($rootScope.userSession.session_data.role_id);
                            if ($state.current.name === 'login-rekanan') {
                                $state.go('fatayatNUCard');
                            }
                            // $rootScope.userLogin = $rootScope.userSession.session_data.username;
                            $rootScope.isLogged = true;
                            // $rootScope.cekStatusIsiData();
                        }
                        // return true;
                    } else {
                        var msg = "";
                        if (result2.status === 403) {
                            console.log($rootScope.message5)
                            msg = $rootScope.message5;
                        } else {
                            msg = $rootScope.message6;
                        }

                        $rootScope.isLogged = false;
                        if ($state.current.name !== 'beranda'
                                && $state.current.name !== 'daftar'
                                && $state.current.name !== 'pengumuman-pengadaan-client'
                                && $state.current.name !== 'lupa-password-rekanan'
                                && $state.current.name !== 'confirmStatusReady'
                                && $state.current.name !== 'detil-pengumuman-pengadaan'
                                && $state.current.name !== 'berita'
                                && $state.current.name !== 'pemasukan-harga-proses-rfq') {
                            if ($state.current.name !== 'login-rekanan'){
                                $.growl.error({message: msg});
                            }
                            $state.go('login-rekanan');
                        }
                    }
                    $rootScope.unloadLoading();
                })
                .error(function(err) {
                    $.growl.error({message: $rootScope.message1 + err});
                    $rootScope.unloadLoading();
                });
            } else {
                $rootScope.isLogged = false;
                if ($state.current.name !== 'beranda'
                        && $state.current.name !== 'daftar'
                        && $state.current.name !== 'pengumuman-pengadaan-client'
                        && $state.current.name !== 'lupa-password-rekanan'
                        && $state.current.name !== 'confirmStatusReady'
                        && $state.current.name !== 'detil-pengumuman-pengadaan'
                        && $state.current.name !== 'berita'
                        && $state.current.name !== 'pemasukan-harga-proses-rfq'){
                            $state.go('login-rekanan');
                        }
            }
        };

        $rootScope.authorize = function(fn) {
            // var session = $cookieStore.get('vendor_sessId');
            // $http.post($rootScope.url_api + "auth/authorize", {session_id: session})
            //     .success(function(reply) {
            //         if (reply.status === 200) {
            //             if (reply.success) {
            //                 $rootScope.auth_cek = true;
            //                 $rootScope.checkSession();
            //                 eval(fn);
            //             }
            //         }
            //     })
            //     .error(function(err) {
            //         $.growl.error({message: $rootScope.message1 + err});
            //     });
        };

        $rootScope.logout = function() {
            $rootScope.loadLoading("Logging off...");
            $http.post($rootScope.url_api + "auth/logout", {
                session_id: $cookieStore.get("vendor_sessId")
            })
            .success(function(result) {
                if (result.status === 200) {
                    if (result.data.success) {
                        $.growl.notice({message: result.data.message});
                        $state.go('login-rekanan');
                        $rootScope.isLogged = false;
                        $cookieStore.remove("vendor_sessId");
                        // $cookieStore.remove("IP");
                        // $cookieStore.remove("token");

                    } else {
                        $.growl.error({message: result.data.message});
                        $state.go('login-rekanan');
                    }
                } else {
                    $.growl.error({message: result.data.message});
                }
                $rootScope.unloadLoading();
            })
            .error(function(err) {
                $.growl.error({message: $rootScope.message1 + err});
                $rootScope.unloadLoading();
            });
        };

        $rootScope.advanceLogout = function(){
            $rootScope.loadLoading("Logging off...");
            $http.post($rootScope.url_api + "auth/advLogout", {
                session_id: $cookieStore.get("vendor_sessId")
            })
            .success(function(result) {
                if (result.status === 200) {
                    if (result.data.success) {
                        $.growl.notice({message: result.data.message});
                        $state.go('login-rekanan');
                        $rootScope.isLogged = false;
                        $cookieStore.remove("vendor_sessId");
                        // $cookieStore.remove("IP");
                        // $cookieStore.remove("token");

                    } else {
                        $.growl.error({message: result.data.message});
                        $state.go('login-rekanan');
                    }
                } else {
                    $.growl.error({message: result.data.message});
                }
                $rootScope.unloadLoading();
            })
            .error(function(err) {
                $.growl.error({message: $rootScope.message1 + err});
                $rootScope.unloadLoading();
            });
        }

        $rootScope.getSession = function() {
            return new Promise(function(resolve,reject){
                $http.post($rootScope.url_api + "auth/get_session", {
                    session_id: $cookieStore.get("vendor_sessId")
                }).success(function(data){
                    var temp = {};
                    temp.data = data
                    if(temp.data.is_maintenance == true){
                        location.reload();
                    }
                    else{
                        resolve(temp)
                    }
                }).error(function(err){
                    reject(err);
                });
            })
        };

        $rootScope.cekAdaDirektur = function(obj) {
            $rootScope.loadLoading();
            $http.get($rootScope.url_api + "pengurusperusahaan/count_director_vendor/" + obj)
                .success(function(response) {
                    if (response.status === 200) {
                        $rootScope.countdirektur = Number(response.result.data[0]);
                        $rootScope.countmanager = Number(response.result.data[1]);
                        $rootScope.adadirektur = false;
                        $rootScope.adamanager = false;
                        if ($rootScope.countdirektur > 0) {
                            $rootScope.adadirektur = true;
                        }

                        if ($rootScope.countmanager > 0) {
                            $rootScope.adamanager = true;
                        }

                    } else {
                        $.growl.error({message: result.message});
                    }
                    $rootScope.unloadLoading();
                })
                .error(function(response) {
                    $.growl.error({message: $rootScope.message1 + err});
                    $rootScope.unloadLoading();
                });
        };

        $rootScope.cekStatusIsiData = function() {
            $rootScope.rekananid = $rootScope.userSession.session_data.rekanan_id;
            $http.get($rootScope.url_api + "anggota/isi_data/status/" + $rootScope.rekananid)
                .success(function(response) {
                    if (response.status === 200)
                    {
                        $rootScope.statusIsiData = response.result.data[0];
                        for (var key in $rootScope.statusIsiData) {
                            if ($rootScope.statusIsiData.hasOwnProperty(key)) {
                                $rootScope.statusIsiData[key] = Number($rootScope.statusIsiData[key]);

                            }
                        }
                        // console.log($rootScope.statusIsiData)
                    }
                })
                .error(function(err) { });
        };

        $rootScope.isActive = function(viewLocation) {
            return viewLocation === $location.path();
        };

        $rootScope.loadLoading = function(msg) {
            //new spinner
            $.blockUI({
                message: '<div class="half-circle-spinner"><div class="circle circle-1"></div><div class="circle circle-2"></div></div><div class="spinner-text">'+msg+'<div>',
                css: {border: '1px solid a49db3', opacity: '0.9', filter: 'alpha(opacity=90)', 'font-size': '18px', 'font-family': 'afta_sansregular, Arial, sans-serif!important'}
            });
        };

        $rootScope.loadLoadingModal = function(msg) {
            $('.modal-content').block({
                message: '<div class="half-circle-spinner"><div class="circle circle-1"></div><div class="circle circle-2"></div></div><div class="spinner-text">'+msg+'<div>',
                css: {border: '1px solid a49db3', opacity: '0.9', filter: 'alpha(opacity=90)', 'font-size': '18px', 'font-family': 'afta_sansregular, Arial, sans-serif!important'}
            });
        };

        $rootScope.unloadLoadingModal = function() {
            $('.modal-content').unblock();
        };

        $rootScope.unloadLoading = function() {
            $.unblockUI();
        };

        /* convert string to boolean */
        $rootScope.strtobool = function( z ){
            return z === "1" || z === "true" || z === 1 || z === true;
        };

        $rootScope.isInputNotValid = function( z ){
            return !z || z === undefined || z === null || z === '' || z === 'N/A';
        };

        $rootScope.arrayFullMonths = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

        /* input: dd-MM-yyyy | output: yyyy-MM-dd */
        $rootScope.convertTanggalRoot = function( z ){
            return $rootScope.isInputNotValid(z) ? '-' : z.substring(6,10) +'-'+ z.substring(3,5) +'-'+ z.substring(0,2);
        };

        /* input: dd-MM-yyyy | output: dd-mm-yyyy */
        $rootScope.convertTanggalRootIndonesia = function( z ){
            return $rootScope.isInputNotValid(z) ? '-' : z.substring(0,2) +'-'+ z.substring(3,5) +'-'+ z.substring(6,10);
        };

        /* input: dd-MM-yyyy | output: dd-fullmonth-yyyy */
        $rootScope.convertLongDate2 = function( z ){
            return $rootScope.isInputNotValid(z) ? '-' : z.substring(0,2) +'-'+ $rootScope.arrayFullMonths[ parseInt(z.substring(3,5)) - 1 ] +'-'+ z.substring(6,10);
        };

        /* input: yyyy-mm-dd hh:mm | output: dd-mm-yyyy */
        $rootScope.convertTanggal = function( z ){
            return $rootScope.isInputNotValid(z) ? '-' : z.substring(8,10) +'-'+ z.substring(5,7) +'-'+ z.substring(0,4);
        };

        $rootScope.convertTanggalRootAwal = function( z ){
            return $rootScope.convertTanggal(z);
        };

        $rootScope.dateFormat = function(param) {

            param = new Date(param);
                    var m_names = new Array("Jan", "Feb", "Mar",
          "Apr", "May", "Jun", "Jul", "Aug", "Sep",
          "Oct", "Nov", "Dec");
            var curr_date = param.getDate();
            var curr_month = param.getMonth();
            var curr_year = param.getFullYear();
            var date =  curr_date + "-" + m_names[curr_month] + "-"+ curr_year;
            return date;
        }


        /* input: yyyy-mm-dd hh:mm | output: dd-mm-yyyy hh:mm */
        $rootScope.convertTanggalWaktu = function( z ){
            return $rootScope.isInputNotValid(z) ? '-' : z.substring(8,10) +'-'+ z.substring(5,7) +'-'+ z.substring(0,4) +' '+ z.substring(11,16);
        };

        /* input: yyyy-mm-dd hh:mm | output: dd-fullmonth-yyyy hh:mm */
        $rootScope.convertDateTime = function( z ){
            return $rootScope.isInputNotValid(z) ? '-' : z.substring(8,10) +'-'+ $rootScope.arrayFullMonths[ parseInt(z.substring(5,7)) - 1 ] +'-'+ z.substring(0,4) +' '+ z.substring(11,16);
        };

        /* input: yyyy-mm-dd hh:mm | output: dd-fullmonth-yyyy */
        $rootScope.convertLongDate = function( z ){
            return $rootScope.isInputNotValid(z) ? '-' : z.substring(8,10) +'-'+ $rootScope.arrayFullMonths[ parseInt(z.substring(5,7)) - 1 ] +'-'+ z.substring(0,4);
        };
        $rootScope.convertTanggalSuratPernyataan = function( z ){
            return $rootScope.convertLongDate(z);
        };

        $rootScope.insertStatusIsiData = function(rekanan_id, type, value) {
            $http.post($rootScope.url_api + "Rekanan/InsertStatus", {
                rekanan_id: rekanan_id,
                type: type,
                value: value
            }).success(function(reply) {
                if (reply.status === 200) {
                    if (reply.result.data === true) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }).error(function(err) {
                $.growl.error({message: $rootScope.message1 + err});
                return false;
            });
        };

        $rootScope.changeLanguage = function (lang){
            $translate.use(lang);
            // $rootScope.language = lang;
        }
    }
]);

pajak.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$translateProvider', function( $stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $translateProvider ){
    var publicPath = "frontend/";
    var zPath = "frontend/";
    var zApp = "pajak";
    // var zAppPanitia = "rajampat";
    // var zPathPanitia = "application/modules/";

    $ocLazyLoadProvider.config({
        debug: false,
        events: true
    });
    // $translateProvider.translations('en', translations_en);
    // $translateProvider.translations('id', translations_id);
    // $translateProvider.preferredLanguage('id');
    // $translatePartialLoaderProvider.addPart('main');
    // (sementara mematikan ini karenan setiap tanda baca jadi rusak)
    // $translateProvider.useSanitizeValueStrategy('sanitize');

    $stateProvider    
    .state('login-rekanan', { //name: 'login-rekanan',
        url: '/login-anggota',
        templateUrl: zPath + 'anggota/modules/anggotalogin/login-anggota.html',
        pageTitle: 'Login Anggota',
        controller: 'loginAnggotaCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: [ zPath + 'anggota/modules/anggotalogin/login-anggota.js' ] }); } }
    })
    
    .state('pajak', { //name: 'fatayatNUCard',
        url: '/fatayatNUCard',
        templateUrl: zPath + 'anggota/modules/fatayatNUCard/fatayatNUCard.html',
        pageTitle: 'Fatayat NU Card',
        controller: 'pengingatAktivitasCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: [ zPath + 'anggota/modules/fatayatNUCard/fatayatNUCard.js' ] }); } }
    })

    .state('beranda', {
        templateUrl: publicPath + 'client/modules/beranda/beranda.html',
        url: '/beranda',
        pageTitle: 'Beranda',
        controller: 'berandaCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: [ publicPath + 'client/modules/beranda/berandaController.js' ] }); } }
    })
    
    .state('berita', { //name: 'berita',
        url: '/berita/:id_berita',
        templateUrl: publicPath + 'client/modules/beranda/berita.html',
        pageTitle: 'Berita',
        controller: 'beritaCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: [ publicPath + 'client/modules/beranda/berandaController.js' ] }); } }
    })

    .state('daftar', { //name: 'daftar',
        url: '/daftar',
        templateUrl: publicPath + 'client/modules/daftar/daftar.html',
        pageTitle: 'Pendaftaran Calon Anggota',
        controller: 'daftarCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: [ publicPath + 'client/modules/daftar/daftar.js' ] }); } }
    })

    // .state('homeadmin', { //name: 'homeadmin',
    //     url: '/homeadmin',
    //     templateUrl: zPathPanitia + 'dashboard/dashboard.html',
    //     pageTitle: 'Dashboard',
    //     controller: 'dashboardCtrl',
    //     resolve: {
    //         loadMyFiles: function($ocLazyLoad) {
    //             return $ocLazyLoad.load({
    //                 name: zAppPanitia, files: [ zPathPanitia + 'dashboard/dashboardCtrl.js' ] }); } }
    // })

    $urlRouterProvider.otherwise('/beranda');
    $stateProviderRef = $stateProvider;
    $urlRouterProviderRef = $urlRouterProvider;
}]);

pajak.filter("ucwords", function() {
    return function(input) {
        if (input) { //when input is defined the apply filter
            input = input.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
        }
        return input;
    };
});

pajak.directive('ambiltanggal', function() {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function(scope, el, attr, ngModel) {
            $(el).datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true,
                onSelect: function(dateText) {
                    scope.$apply(function() {
                        ngModel.$setViewValue(dateText);
                    });
                }
            });
        }
    };
});
