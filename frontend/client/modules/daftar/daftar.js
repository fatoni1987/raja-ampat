angular.module('pajak')
.controller('daftarCtrl', function ($scope, $state, $rootScope, $http, SocketService, $modal, $translate)
{        
    $scope.daftar = {
        'username' : '',
        'nik' : '',
        'email' : '',
        'password' : '',
        'noTelp' : '',
        'type_identitas' : 'KTP'
    }

    $scope.init = function () {

    };   

    $scope.getValue = function(param){
        $scope.daftar.type_identitas = param;
    }
    
  
    $scope.daftarUser = function(){        
        if($scope.daftar.username == ""){
            $.growl.error({title: "[WARNING]", message: "Nama depan tidak boleh kosong"});
        }else if($scope.daftar.nik == ""){
            $.growl.error({title: "[WARNING]", message: "No KTP tidak boleh kosong"});
        }else if($scope.daftar.email == ""){
            $.growl.error({title: "[WARNING]", message: "Password tidak boleh kosong"});
        }else if($scope.daftar.password == ""){
            $.growl.error({title: "[WARNING]", message: "Tanggal lahir tidak boleh kosong"});
        }else if($scope.daftar.noTelp == ""){
            $.growl.error({title: "[WARNING]", message: "Bulan lahir tidak boleh kosong"});
        }else {
            // console.info($scope.daftar);
            $http.post($rootScope.url_api + 'pendaftaran/cekUsername', {
                daftar: $scope.daftar
            }).success(function(reply) {
                if (reply.status == 200) {
                    $state.go('beranda');
                    $.growl.notice({title: "[Success]", message: "Anda berhasil mendaftar!! Silahkan tunggu, data anda dalam proses verifikasi admin"});
                }else{
                    $.growl.error({title: "[WARNING]", message: "Anda sudah terdaftar, Silahkan login menggunakan No KTP anda"});
                }
            });            
            
        }
    }    
}); 