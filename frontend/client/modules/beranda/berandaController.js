angular.module("pajak", []).filter('limitHtml', function() {
    return function(text, limit) {

        var changedString = String(text).replace(/<[^>]+>/gm, '');
        var length = changedString.length;
        var suffix = ' ...';

        return length > limit ? changedString.substr(0, limit - 1) + suffix : changedString;
    }
})
.controller("berandaCtrl", function($scope, $state, $rootScope, $http, $translate) {    
    $rootScope.$on('$translateChangeSuccess', function () {
        initTranslate();
    });
    $scope.berita;
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxSize = 5;
    $scope.keyword = "";
    
    $scope.init = function() {
        loadNews(1);
        initTranslate();
        $rootScope.checkUAT();
    };
    function initTranslate(){
        $scope.message1 = $translate.instant('hm7')
        $scope.message2 = $translate.instant('hm6')
    }
    function loadNews(current) {   
        $scope.currentPage = current;
        $scope.offset = (current * 6) - 6;
        var arr = [];
        arr.push($scope.offset);
        var limit = 6;
        arr.push(limit);   
        $http.post($rootScope.url_api + "news/list", {
            "offset": $scope.offset, 
            "pageSize": $scope.maxSize, 
            "limit": limit,
            "keyword": $scope.keyword
        }).success(function(output) {
            if (output.status === 200) {
                var data = output.result.data.data;
                $scope.totalItems = output.result.data.count;
                $scope.berita = data;
            } else 
            $.growl.error({
                message: $scope.message1
            })
        }).error(function(err) {
            $.growl.error({
                message: $scope.message2 + err
            })
        })
    }

    $scope.loadNewsPage = function(current){
        loadNews(current);
    }

    $scope.cek = function(key){
        $translate.use(key);
    }

    $scope.lihatBerita = function(idberita) {
        $state.transitionTo("berita", {
            id_berita: idberita
        })
    }
})

.controller("beritaCtrl", function($scope, $stateParams, $rootScope, $http, $translate) {
    $rootScope.$on('$translateChangeSuccess', function () {
        initTranslate();
    });
    $scope.id_berita = Number($stateParams.id_berita);
    $scope.berita;
    $scope.init = function() {
        loadDetailNews();
        initTranslate();
    };
    function initTranslate(){
        $scope.message11 = $translate.instant('hm7')
        $scope.message12 = $translate.instant('hm6')
    }
    function loadDetailNews() {
        $http.post($rootScope.url_api + "news/detail/" + $scope.id_berita).success(function(output) {
            if (output.status === 200) {
                var data = output.result.data;
                $scope.berita = data[0]
            } else 
            $.growl.error({
                message: $scope.message11
            })
        }).error(function(err) {
            $.growl.error({
                message: $scope.message12 + err
            })
        })
    }
});
