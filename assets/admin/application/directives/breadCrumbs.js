rajampat
.directive('breadCrumbs', function($http, $rootScope) {
    return {
      templateUrl:'asset/admin/application/partials/breadcrumb.html',
      restrict: 'E',
      replace: true,
      scope: {
      },
      controller:function($scope){
        $scope.selectedMenu = 'dashboard';
        $scope.collapseVar = 0;
        $scope.multiCollapseVar = 0;
        $scope.menus = [];
        $scope.init = function() {
          $http.get($rootScope.url_api + "menus/tree")
          .success(function(result){
            if(result.status === 200) {
              $scope.menus = result.result.data;
              for (var i = 0; i < $scope.menus.length; i++) {
                $scope.menus[i].index = 10 + i;
              };
              console.info("Menus: " + JSON.stringify($scope.menus.length));
                
            } else {
              console.info("Response: " + JSON.stringify(result));
            }
          })
          .error(function(err){
            console.info("Error: " + JSON.stringify(err));
          });
          ;
        };

        $scope.init();
        
        $scope.check = function(x){
          console.info("check collapseVar : " + x);
          if(x==$scope.collapseVar)
            $scope.collapseVar = 0;
          else
            $scope.collapseVar = x;
        };
        
        $scope.multiCheck = function(y){
          
          if(y==$scope.multiCollapseVar)
            $scope.multiCollapseVar = 0;
          else
            $scope.multiCollapseVar = y;
        };
      }
    }
  });