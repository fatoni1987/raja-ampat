rajampat.directive('stRatio',function(){
    return {
        link:function(scope, element, attr){
            var ratio=+(attr.stRatio);
            element.css('max-width',ratio+'%');
        }
    };
});

rajampat.directive('tanggalwaktu', function() {
    return {
        restrict: 'A',
        link: function(scope, elt, attrs) {
            elt.addClass('date-picker-background');
            elt.attr('readonly',true);
            $(elt).datetimepicker({
                format: 'yyyy-mm-dd hh:ii',
                todayBtn: true,
                autoclose:true,
                minuteStep: 5,
                pickerPosition: "bottom-left"
            });
        }
    };
});

rajampat.directive('tanggalwaktuskrng', function() {
    return {
        restrict: 'A',
        link: function(scope, elt, attrs) {
            elt.addClass('date-picker-background');
            elt.attr('readonly',true);
            $(elt).datetimepicker({
                format: 'yyyy-mm-dd',
                todayBtn: true,
                autoclose:true,
                todayHighlight: true,
                minView: 2,
                startDate: new Date(),
                pickerPosition: "bottom-left"
            });
        }
    };
});

rajampat.directive('tanggalwaktu2', function() {
    return {
        restrict: 'A',
        link: function(scope, elt, attrs) {
            elt.addClass('date-picker-background');
            elt.attr('readonly',true);
            $(elt).datetimepicker({
                format: 'hh:ii',
                todayBtn: true,
                autoclose:true,
                minuteStep: 5,
                pickerPosition: "bottom-left"
            });
        }
    };
});

rajampat.directive('tanggalpicker', function() {
    return {
        restrict: 'A',
        link: function(scope, elt, attrs) {
            elt.addClass('date-picker-background');
            elt.attr('readonly',true);
            $(elt).datepicker({
                format: 'dd-mm-yyyy',
                todayBtn: "linked",
                todayHighlight: true,
                autoclose:true,                
                pickerPosition: "bottom-left"
            });
        }
    };
});

rajampat.directive('ambiltanggalfix', function() {
    return {
        restrict: 'A',
        link: function(scope, elt, attrs) {
            elt.addClass('date-picker-background');
            elt.attr('readonly',true);
            $(elt).datetimepicker({
                format: 'yyyy-mm-dd',
                todayBtn: true,
                autoclose:true,
                todayHighlight: true,
                minView: 2,
                pickerPosition: "bottom-left"
            });
        }
    };
});

rajampat.directive('ambilwaktu', function() {
    return {
        restrict: 'A',
        link: function(scope, elt, attrs) {
            elt.addClass('date-picker-background');
            elt.attr('readonly',true);
            $(elt).datetimepicker({
                format: 'hh:ii',
                todayBtn: true,
                autoclose:true,
                todayHighlight: true,
                minView: 2,
                pickerPosition: "bottom-left"
            });
        }
    };
});

rajampat.directive('ambiltanggal', function() {
    
    function link(scope, element, attrs) {
        element.addClass('date-picker-background');
        element.attr('readonly',true);
        var val;
        if(attrs.minDate == "today"){
            val = new Date();
        }
        else{
            val = -Infinity;
        }
        
        $(element).datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
            startDate: val,
            onSelect: function(dateText) {
                scope.$apply(function() {
                    ngModel.$setViewValue(dateText);
                });
            }
        });
        
      }
    
      return {
        require: 'ngModel',
        restrict: 'A',        
        link: link
      };
     
    // return {
    //     require: 'ngModel',
    //     restrict: 'A',
    //     link: function(scope, el, attr, ngModel) {
    //         $(el).datepicker({
    //             format: 'yyyy-mm-dd',
    //             autoclose: true,
    //             todayHighlight: true,
    //             onSelect: function(dateText) {
    //                 scope.$apply(function() {
    //                     ngModel.$setViewValue(dateText);
    //                 });
    //             }
    //         });
    //     }
    // };
});

rajampat.directive('ambiltanggal2', function() {
    
    function link(scope, element, attrs) {
        element.addClass('date-picker-background');
        element.attr('readonly',true);
        var val;
        if(attrs.minDate == "today"){
            val = new Date();
        }
        else{
            val = -Infinity;
        }
        
        $(element).datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            todayHighlight: true,
            startDate: val,
            onSelect: function(dateText) {
                scope.$apply(function() {
                    ngModel.$setViewValue(dateText);
                });
            }
        });
        
      }
    
      return {
        require: 'ngModel',
        restrict: 'A',        
        link: link
      };
     
    // return {
    //     require: 'ngModel',
    //     restrict: 'A',
    //     link: function(scope, el, attr, ngModel) {
    //         $(el).datepicker({
    //             format: 'yyyy-mm-dd',
    //             autoclose: true,
    //             todayHighlight: true,
    //             onSelect: function(dateText) {
    //                 scope.$apply(function() {
    //                     ngModel.$setViewValue(dateText);
    //                 });
    //             }
    //         });
    //     }
    // };
});

rajampat.directive('ambilbulan', function() {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function(scope, el, attr, ngModel) {
            el.addClass('date-picker-background');
            el.attr('readonly',true);
            $(el).datepicker({
                format: 'yyyy-mm',
                autoclose: true,
                todayHighlight: true,
                viewMode: 'months',
                minViewMode: 'months',
                onSelect: function(dateText) {
                    scope.$apply(function() {
                        ngModel.$setViewValue(dateText);
                    });
                }
            });
        }
    };
});

rajampat.directive('numberMasking', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function(inputValue) {
                if (inputValue == undefined)
                    return ''
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});

rajampat.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    // console.log(attr);
                    var maxNumber = Number(attr.maxNumber);
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if(transformedInput > maxNumber){
                        transformedInput = "100";
                    }
                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

rajampat.directive('numbersAndDecimal', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    // console.log(attr);
                    var maxNumber = Number(attr.maxNumber);
                    var transformedInput = text.replace(/^(\d{0,4}\.\d{0,5}|\d{0,9}|\.\d{0,8}).*/, '$1');

                    if(transformedInput > maxNumber){
                        transformedInput = "100";
                    }
                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

rajampat.directive('scoreMasking', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function(inputValue) {
                if (inputValue == undefined)
                    return ''
                var transformedInput = inputValue.replace(/[^\d.]+/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});

rajampat.directive('experienceMasking', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function(inputValue) {
                if (inputValue == undefined)
                    return ''
                var transformedInput = inputValue.replace(/[^\d,]+/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});

rajampat.directive('maxChar', function() {
	return {
		require: 'ngModel',
		link: function (scope, element, attrs, ngModelCtrl) {
			var maxlength = Number(attrs.maxChar);
			function fromUser(text) {
				  if (text.length > maxlength) {
					var transformedInput = text.substring(0, maxlength);
					ngModelCtrl.$setViewValue(transformedInput);
					ngModelCtrl.$render();
					return transformedInput;
				  } 
				  return text;
			}
			ngModelCtrl.$parsers.push(fromUser);
		}
	}; 
});