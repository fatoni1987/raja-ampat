myApp.factory('uploaderSvc', function($http, $q) {
    return {
        url: '/upload',
        uploadFile: function(fileUpload, paket_id) {
            var deferred = $q.defer();
            var fd = new FormData();
            angular.forEach(fileUpload, function(file) {
                fd.append("file", file);
            });
            //param1 : Relative or absolute URL specifying the destination of the request
            //param2 : Request content
            //param3 : Optional configuration object
            $http.post(this.url + "/" + "lelang_" + paket_id, fd,
                    {
                        withCredentials: true,
                        transformRequest: angular.identity(),
                        headers: {'Content-Type': undefined}
                    }).success(function(data) {
                deferred.resolve(data);
            }).error(function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
    };
});

myApp.factory('uploaderSvc03', function($http, $q) {
    return {
        url: '/upload02',
        uploadFile: function(fileUpload, pr_id) {
            var deferred = $q.defer();
            var fd = new FormData();
            angular.forEach(fileUpload, function(file) {
                fd.append("file", file);
            });
            $http.post(this.url + "/" + "pr_id_" + pr_id, fd,
                    {
                        withCredentials: true,
                        transformRequest: angular.identity(),
                        headers: {'Content-Type': undefined}
                    }).success(function(data) {
                deferred.resolve(data);
            }).error(function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
    };
});


myApp.factory('uploaderSvc04', function($http, $q) {
    return {
        url: '/uploadPR',
        uploadFile: function(fileUpload, pr_id, halo, username) {
            var deferred = $q.defer();
            var fd = new FormData();
            angular.forEach(fileUpload, function(file) {
                fd.append("file", file);
            });
            $http.post(this.url + "/" + "pr_id_" + pr_id + "/" + halo + "/" + username, fd,
                    {
                        withCredentials: true,
                        transformRequest: angular.identity(),
                        headers: {'Content-Type': undefined}
                    }).success(function(data) {
                deferred.resolve(data);
            }).error(function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
    };
});