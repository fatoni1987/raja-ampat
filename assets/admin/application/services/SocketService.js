rajampat.factory( 'SocketService', function( $rootScope ){
// var socket = io.connect( 'http://localhost:8001', 
   var socket = io.connect( window.location.protocol + '//' + window.location.hostname + ':8001', 
    {
        'reconnection': true,
        'reconnectionDelay': 1000,
        'reconnectionDelayMax' : 5000,
        'reconnectionAttempts': 5 
    });
  return {
    on: function( eventName, callback ){
      socket.on( eventName, function(){  
        var args = arguments;
        $rootScope.$apply( function(){
          callback.apply( socket, args );
        });
      });
    },
    emit: function( eventName, data, callback ){
      socket.emit( eventName, data, function(){
        var args = arguments;
        $rootScope.$apply( function(){
          if( callback ){
            callback.apply( socket, args );
          }
        });
      });
    }
  };
});