myApp.factory('partUploaderSvc', function ($http, $q) {
    return {
        url: '/uploadsparepart',
        uploadFile: function (fileUpload) {
            var deferred = $q.defer();
            var fd = new FormData();
            angular.forEach(fileUpload, function(file) {
                fd.append("file", file);
            });
            $http.post(this.url, fd,
            {
                withCredentials: true,
                transformRequest: angular.identity(),
                headers: {'Content-Type': undefined}
            }).success(function(data) {
                deferred.resolve(data);
            }).error(function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }
    };
});