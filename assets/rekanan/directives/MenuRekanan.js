pajak
.directive('menuRekanan', function() {
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'assets/rekanan/partials/menu-rekanan.html'
	};
});