pajak.directive('tanggalwaktu', function() {
    return {
        restrict: 'A',
        link: function(scope, elt, attrs) {
            elt.addClass('date-picker-background');
            elt.attr('readonly',true);
            $(elt).datetimepicker({
                format: 'yyyy-mm-dd hh:ii',
                todayBtn: true,
                minuteStep: 5,
                pickerPosition: "bottom-left"
            });
        }
    };
});

pajak.directive('tanggalwaktu2', function() {
    return {
        restrict: 'A',
        link: function(scope, elt, attrs) {
            elt.addClass('date-picker-background');
            elt.attr('readonly',true);
            $(elt).datetimepicker({
                format: 'dd-mm-yyyy hh:ii',
                todayBtn: true,
                autoclose:true,
                minuteStep: 5,
                pickerPosition: "bottom-left"
            });
        }
    };
});

pajak.directive('ambiltanggal', function() {
    return {
         require: 'ngModel',
        restrict: 'A',
        link: function(scope, el, attr, ngModel) {
            el.addClass('date-picker-background');
            el.attr('readonly',true);
            $(el).datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true,
                onSelect: function(dateText) {
                    scope.$apply(function() {
                        ngModel.$setViewValue(dateText);
                    });
                }
            });
        }
    };
});
pajak.directive('ambiltanggal2', function() {
    return {
         require: 'ngModel',
        restrict: 'A',
        link: function(scope, el, attr, ngModel) {
            el.addClass('date-picker-background');
            el.attr('readonly',true);
            $(el).datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                todayHighlight: true,
                onSelect: function(dateText) {
                    scope.$apply(function() {
                        ngModel.$setViewValue(dateText);
                    });
                }
            });
        }
    };
});

pajak.directive('numberMasking', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function(inputValue) {
                if (inputValue == undefined)
                    return ''
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});

pajak.directive('experienceMasking', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function(inputValue) {
                if (inputValue == undefined)
                    return ''
                var transformedInput = inputValue.replace(/[^\d,]+/g, '');
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});

pajak.directive('maxChar', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            var maxlength = Number(attrs.maxChar);
            function fromUser(text) {
                if (text.length > maxlength) {
                    var transformedInput = text.substring(0, maxlength);
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                    return transformedInput;
                }
                return text;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});