pajak.directive('ambiltanggal', function() {
            return {
                require: 'ngModel',
                restrict: 'A',
                link: function(scope, el, attr, ngModel) {
                    $(el).datepicker({
                        format: 'dd/mm/yyyy',
                        autoclose: true,
                        todayHighlight: true,
                        onSelect: function(dateText) {
                            scope.$apply(function() {
                                ngModel.$setViewValue(dateText);
                            });
                        }
                    });
                }
            };
        })
        .directive('tanggalpicker', function() {
            return {
                restrict: 'A',
                link: function(scope, elt, attrs) {
                    elt.addClass('date-picker-background');
                    elt.attr('readonly',true);
                    $(elt).datepicker({
                        format: 'dd-mm-yyyy',
                        todayBtn: "linked",
                        todayHighlight: true,
                        autoclose:true,                
                        pickerPosition: "bottom-left"
                    });
                }
            };
        })
        .directive('numberMasking', function() {
            return {
                require: 'ngModel',
                link: function(scope, element, attrs, modelCtrl) {
                    modelCtrl.$parsers.push(function(inputValue) {
                        if (inputValue == undefined)
                            return ''
                        var transformedInput = inputValue.replace(/[^0-9]/g, '');
                        if (transformedInput != inputValue) {
                            modelCtrl.$setViewValue(transformedInput);
                            modelCtrl.$render();
                        }
                        return transformedInput;
                    });
                }
            };
        })
        .directive('tanggalwaktu', function() {
            return {
                restrict: 'A',
                link: function(scope, elt, attrs) {
                    $(elt).datetimepicker({
                        format: 'yyyy-mm-dd hh:ii',
                        todayBtn: true,
                        minuteStep: 5,
                        pickerPosition: "bottom-left"
                    });
                }
            };
        })
        .directive('tanggalrekanan', function() {
            return {
                restrict: 'A',
                link: function(scope, elt, attrs) {
                    $(elt).datetimepicker({
                        format: 'dd-mm-yyyy',
                        todayBtn: true,
                        startView: 'decade',
                        minView: 'month',
                        autoclose: true
                    });
                }
            };
        })
        .directive('experienceMasking', function() {
            return {
                require: 'ngModel',
                link: function(scope, element, attrs, modelCtrl) {
                    modelCtrl.$parsers.push(function(inputValue) {
                        if (inputValue == undefined)
                            return ''
                        var transformedInput = inputValue.replace(/[^\d,]+/g, '');
                        if (transformedInput != inputValue) {
                            modelCtrl.$setViewValue(transformedInput);
                            modelCtrl.$render();
                        }
                        return transformedInput;
                    });
                }
            };
        })
        .directive('maxChar', function() {
            return {
                require: 'ngModel',
                link: function(scope, element, attrs, ngModelCtrl) {
                    var maxlength = Number(attrs.maxChar);
                    function fromUser(text) {
                        if (text.length > maxlength) {
                            var transformedInput = text.substring(0, maxlength);
                            ngModelCtrl.$setViewValue(transformedInput);
                            ngModelCtrl.$render();
                            return transformedInput;
                        }
                        return text;
                    }
                    ngModelCtrl.$parsers.push(fromUser);
                }
            };
        });

