$(document).ready(function() {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
    CKEDITOR.replace('editorPengalaman');
    CKEDITOR.replace('editorPendidikan');
    CKEDITOR.replace('editorSertifikat');
    CKEDITOR.replace('editorBahasa');
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
});