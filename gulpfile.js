// script gulp sudah bisa generate .map
const gulp = require('gulp');
const rev = require('gulp-rev');
const revRewrite = require('gulp-rev-rewrite');
const revDelete = require('gulp-rev-delete-original');
const through = require('through2');
const modify = require('modify-filename');
const fs = require("fs");
const javascriptObfuscator = require('gulp-javascript-obfuscator');
const ngAnnotate = require('gulp-ng-annotate')
const gutil = require('gulp-util');
const babel = require('gulp-babel');

//  ================================ FrontEnd ================================

// Task dengan tujuan enkripsi kodingan seluruh JS rekanan menggunakan obfuscator
// https://github.com/javascript-obfuscator/javascript-obfuscator
gulp.task('encryptClient',() => {
  return gulp.src(['frontend/client/**/*.js','frontend/rekanan/**/*.js','frontend/app.js'])
    .pipe(babel({
      presets: ['@babel/env'],
      sourceType: "script",
      plugins: ['@babel/plugin-transform-destructuring']
    }))
    .pipe(ngAnnotate())
    .pipe(javascriptObfuscator({
      compact: true,
      // sourceMap: true
    }))
    .pipe(gulp.dest(function (file) {
      return file.base;
    }));
});

// Task dengan tujuan menambahkan format waktu-menit-detik ke nama file html dan js
// untuk mengatasi masalah cache di komputer client
gulp.task('revisionClient', ['encryptClient'], () => {
  return gulp.src(['frontend/**/*.html','frontend/**/*.js'])
    .pipe(rev())
    .pipe(through.obj(function (file, enc, cb) {
      if (file.revHash) {
        // write the NEW path
        var date = new Date().getHours().toString() + new Date().getMinutes().toString() + new Date().getSeconds().toString();
        file.path = modify(file.revOrigPath, function (name, ext) {
          var namaBaru = name.split("__");
          return [namaBaru[0], date + ext].join('__');
        });
      }
      // send back to stream
      cb(null, file);
    }))
    .pipe(revDelete())
    .pipe(gulp.dest('frontend'))
    .pipe(rev.manifest({
      merge: true
    }))
    .pipe(gulp.dest('frontend'))
});

// Task dengan tujuan merubah refrensi file di app.js (karena ada perubahan nama file setelah penambahan format jam-menit-detik)
gulp.task('revRewriteClient', ['revisionClient'], function () {
  const manifest = gulp.src('frontend/rev-manifest.json');
  var rawData = fs.readFileSync("frontend/rev-manifest.json", "utf8");
  var manifestJson = JSON.parse(rawData);

  return gulp.src('frontend/' + manifestJson[Object.keys(manifestJson)[0]])
    .pipe(revRewrite({ manifest }))
    .pipe(gulp.dest('frontend'));

});

// Task dengan tujuan ganti nama file app.js ke yang terbaru (setelah penambahan format jam-menit-detik) di html
gulp.task('revIndexClient', ['revRewriteClient'], function () {
  const manifest = gulp.src('frontend/rev-manifest.json');
  gulp.src('index.html')
    .pipe(revRewrite({ manifest }))
    .pipe(gulp.dest(''));

});
//  ================================ Batas Frontend ================================

//  ================================ Admin ================================

// Task dengan tujuan enkripsi kodingan seluruh JS panitia menggunakan obfuscator
// https://github.com/javascript-obfuscator/javascript-obfuscator
gulp.task('encryptAdmin',() => {
  return gulp.src(['panitia/application/modules/**/*.js','panitia/app.js'])
    .pipe(babel({
      presets: ['@babel/env'],
      sourceType: "script",
      plugins: ['@babel/plugin-transform-destructuring']
    }))
    .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
    .pipe(ngAnnotate())
    .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
    .pipe(javascriptObfuscator({
      compact: true,
      // sourceMap: true
    }))
    .pipe(gulp.dest(function (file) {
      return file.base;
    }));
});

// Task dengan tujuan menambahkan format waktu-menit-detik ke nama file html dan js
// untuk mengatasi masalah cache di komputer client
gulp.task('revisionAdmin', ['encryptAdmin'], () => {
  return gulp.src(['panitia/**', '!panitia/rev-manifest.json', '!panitia/index.html', '!panitia/directives/**'])
    .pipe(rev())
    .pipe(through.obj(function (file, enc, cb) {
      if (file.revHash) {
        // write the NEW path
        var date = new Date().getHours().toString() + new Date().getMinutes().toString() + new Date().getSeconds().toString();
        file.path = modify(file.revOrigPath, function (name, ext) {
          var namaBaru = name.split("__");
          return [namaBaru[0], date + ext].join('__');
        });
      }
      cb(null, file);
    }))
    .pipe(revDelete())
    .pipe(gulp.dest('panitia'))
    .pipe(rev.manifest({
      merge: true
    }))
    .pipe(gulp.dest('panitia'))
});

// Task dengan tujuan merubah refrensi file di app.js (karena ada perubahan nama file setelah penambahan format jam-menit-detik)
gulp.task('revRewriteAdmin', ['revisionAdmin'], function () {
  const manifest = gulp.src('panitia/rev-manifest.json');
  var rawData = fs.readFileSync("panitia/rev-manifest.json", "utf8");
  var manifestJson = JSON.parse(rawData);

  return gulp.src('panitia/' + manifestJson[Object.keys(manifestJson)[0]])
    .pipe(revRewrite({ manifest }))
    .pipe(gulp.dest('panitia'));
});

// Task dengan tujuan ganti nama file app.js ke yang terbaru (setelah penambahan format jam-menit-detik) di html
gulp.task('revIndexAdmin', ['revRewriteAdmin'], function () {
  const manifest = gulp.src('panitia/rev-manifest.json');
  gulp.src('panitia/index.html')
    .pipe(revRewrite({ manifest }))
    .pipe(gulp.dest('panitia'));

});
//  ================================ Batas Admin ================================



//  ================================ Katalog ================================

// Task dengan tujuan enkripsi kodingan seluruh JS katalog menggunakan obfuscator
// https://github.com/javascript-obfuscator/javascript-obfuscator
gulp.task('encryptKatalog',() => {
  return gulp.src(['katalog/application/modules/**/*.js','katalog/app.js'])
    .pipe(ngAnnotate())
    .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
    .pipe(javascriptObfuscator({
      compact: true,
      // sourceMap: true
    }))
    .pipe(gulp.dest(function (file) {
      return file.base;
    }));
});

// Task dengan tujuan menambahkan format waktu-menit-detik ke nama file html dan js
// untuk mengatasi masalah cache di komputer client
gulp.task('revisionKatalog', ['encryptKatalog'], () => {
  return gulp.src(['katalog/application/**','katalog/app.js'])
    .pipe(rev())
    .pipe(through.obj(function (file, enc, cb) {
      if (file.revHash) {
        // write the NEW path
        var date = new Date().getHours().toString() + new Date().getMinutes().toString() + new Date().getSeconds().toString();
        file.path = modify(file.revOrigPath, function (name, ext) {
          var namaBaru = name.split("__");
          return [namaBaru[0], date + ext].join('__');
        });
      }
      cb(null, file);
    }))
    .pipe(revDelete())
    .pipe(gulp.dest('katalog'))
    .pipe(rev.manifest({
      merge: true
    }))
    .pipe(gulp.dest('katalog'))
});

// Task dengan tujuan merubah refrensi file di app.js (karena ada perubahan nama file setelah penambahan format jam-menit-detik)
gulp.task('revRewriteKatalog', ['revisionKatalog'], function () {
  const manifest = gulp.src('katalog/rev-manifest.json');
  var rawData = fs.readFileSync("katalog/rev-manifest.json", "utf8");
  var manifestJson = JSON.parse(rawData);

  return gulp.src('katalog/' + manifestJson[Object.keys(manifestJson)[0]])
    .pipe(revRewrite({ manifest }))
    .pipe(gulp.dest('katalog'));
});

// Task dengan tujuan ganti nama file app.js ke yang terbaru (setelah penambahan format jam-menit-detik) di html
gulp.task('revIndexKatalog', ['revRewriteKatalog'], function () {
  const manifest = gulp.src('katalog/rev-manifest.json');
  gulp.src('katalog/index.html')
    .pipe(revRewrite({ manifest }))
    .pipe(gulp.dest('katalog'));

});
//  ================================ Batas Katalog ================================