<?php

namespace Utils;

use \Utils\Security as Security;
use \Model\UserSession as UserSession;
use \Nettuts\Controller as Controller;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon as Carbon;

class SessionManager extends Controller {
	
	// register constants for session array
	const API = 'api';
	const ACCESS_LEVEL = 'access_level';
	const USERS = 'users';
	const SESSION_ID = 'session_id';
	const SESSION_LAST_ACTIVITY = 'session_last_activity';
	const SESSION_DATA = 'session_data';
	const LANGUAGE = 'lang';
	const KEY = 'encyption_key';

	/** *
	* Session storage
	  sessions = [
			users => [
				[$2] => [
					api => $0,
					access_level => $1,
					session_id => $2
					session_last_activity => $3,
					session_data => [
						user_id => $4,
						username => $5,
						name => $6
					]
				]
			],
			lang => $7	
		]
	*	
	** */
	private $sessions = null;
	private $encyption_key;
	

	public function __construct() {
		parent::__construct();
		if($this->sessions === null || count($sessions) <= 0)
			$this->startSession();
		
	}

	/** *
	* config = [
	*	'session' => [
	*			'timeout' => $0,
	*			'prefix' => $1,
	*			'format' => $2,
	*			'save_to_database'=> $3 
	*		],
	*	'api' => [
	*			'key' => $4,
	*			'storage' => $5,
	*			'apply_to_all' => $6
	*		]
	* ]
	* 
	**/
	public function getConfig() {

		return $this->config;
	}

	public function getApiConfig() {

		return $this->config["api"];
	}

	public function getSessionConfig() {
		return $this->config["session"];
	}

	/** *
	* value = [
	*	'api_key' => $0,
	*	'user_type' => $1,
	*	'ip_address' => $2,
	*	'user_id' => $3
	* ]
	* 
	**/
	public function openSession($sid, $value) {	
		if(!$this->sessionExist($sid)) {
			$userSession = UserSession::find($sid);
			if($userSession === null) {
				$userID = $value['username'];
				print_r($userID);
				$success = UserSession::insert([
				'session_id' => $sid,
				'api_key' => $value['api_key'],
				'user_id' => $userID,
				'user_type' => $value['user_type'],
				'remote_ip' => $value['ip_address'],
				'last_access' => Carbon::now('Asia/Jakarta'),
				'is_active' => 1
				]);
				// print_r("masuk");
				if($success) {
					$userSession = UserSession::find($sid);
					if($userSession->session_id === $sid) { // insert success
						$user = DB::table('pegawai_view')->where(['username' => $value['username']])->first();
						// if(intval($userSession->user_type) === 1) { // admin
						// 	$user = DB::table('rekanan')->where(['username' => $value['username']])->first();
						// } else { // vendor
						// 	$user = DB::table('pegawai')->where(['username' => $value['username']])->first();
						// }print_r($user);
						$this->sessions[$sid] = [
							'session_id' => $userSession->session_id,
							'api_key' => $userSession->api_key,
							'remote_ip' => $userSession->remote_ip,
							'last_access' => $userSession->last_access,
							'session_data' => $user
						];						
						return true;
					}
				}
			} else {//print_r("masuk1");
				$userSession = UserSession::find($sid);
				$userSession->api_key = $value['api_key'];
				$userSession->remote_ip = $value['ip_address'];
				$userSession->is_active = 1;
				$userSession->last_access = Carbon::now('Asia/Jakarta');
				if($userSession->save()) {
					$user = DB::table('pegawai_view')->where(['username' => $value['username']])->first();
					// if(intval($userSession->user_type) === 1) { // admin
					// 	$user = DB::table('rekanan')->where(['username' => $value['username']])->first();
					// } else { // vendor
					// 	$user = DB::table('pegawai')->where(['username' => $value['username']])->first();
					// }
					// print_r($userSession->user_type);
					$this->sessions[$sid] = [
						'session_id' => $userSession->session_id,
						'api_key' => $userSession->api_key,
						'remote_ip' => $userSession->remote_ip,
						'last_access' => $userSession->last_access,
						'session_data' => $user
					];
					return true;
				}
			}
			
			return false;
		}
		else {//print_r("masuk2");
			$userSession = UserSession::find($sid);
			$userSession->api_key = $value['api_key'];
			$userSession->remote_ip = $value['ip_address'];
			$userSession->is_active = 1;
			$userSession->last_access = Carbon::now('Asia/Jakarta');
			if($userSession->save()) {
				$user = DB::table('pegawai_view')->where(['username' => $value['username']])->first();
				// if(intval($userSession->user_type) === 1) { // admin
				// 	$user = DB::table('rekanan')->where(['username' => $value['username']])->first();
				// } else { // vendor
				// 	$user = DB::table('pegawai')->where(['username' => $value['username']])->first();
				// }
				$this->sessions[$sid] = [
					'session_id' => $userSession->session_id,
					'api_key' => $userSession->api_key,
					'remote_ip' => $userSession->remote_ip,
					'last_access' => $userSession->last_access,
					'session_data' => $user
				];

				return true;
			}
			
			return false;
		}
	}

	public function closeSession($sid) {
		if($this->sessionExist($sid)) {
			$userSession = UserSession::find($sid);
			if($userSession !== null) {
				$userSession->is_active = 0;
				if($userSession->save()){
					session_unset($this->sessions[$sid]);
					return true;
				}
			}
		}
		return false;
	}

	public function deleteSession($sid){
		if($this->sessionExist($sid)) {
			$deleteSession = UserSession::where('session_id', $sid)->delete();
			if($deleteSession) {
				session_unset($this->sessions[$sid]);
				return true;
			}
		}
		return false;
	}

	public function getSession($sid) {
		return $this->sessions[$sid];
	}

	public function sessionExist($sid) {

		$data = UserSession::find($sid);

		if(!isset($data))
			return false;
		
		if($data === null)
			return false;

		//print_r('Data' . $data->is_active);
		//print_r('Sid' . $data->session_id);
		if(intval($data->is_active) === 1) {

			return true;
		}

		return false;
		/*if($this->sessions === null) {
			return false;
		}

		if(!isset($this->sessions[$sid])){
			return false;
		}

		$data = UserSession::find($sid);
		if($data === null || $data->is_active === 0) {
			return false;
		}

		return true;*/
	}

	public function updateSessionLastActive($sid) {
		if($this->sessionExist($sid)) {
			$userSession = UserSession::find($sid);
			if($userSession !== null) {
				$userSession->last_access = Carbon::now('Asia/Jakarta');
				return $userSession->save();
			}
		}
		return false;
	}

	public function updateSession($sid) {
		if($this->sessionExist($sid)) {
			$userSession = UserSession::find($sid);
			if($userSession !== null) {
				$userSession->is_active = 0;
				if($userSession->save()){
					session_unset($this->sessions[$sid]);
				}
			}
		}
	}

	public function listSessions() {

		return $this->sessions;
	}

	protected function initSession() {
		$this->encyption_key = Crypto::createNewRandomKey();
				// initialize the session array if has not been exist
		$session_data = UserSession::where(['is_active' => 1])->get()->toArray();
		// print_r($session_data);
		// print_r($session_data[0]['user_id']);
		// print_r("masuk1");
		for ($i = 0; $i < count($session_data); $i++) {
			$user = DB::table('pegawai_view')->where(['username' => $session_data[$i]['user_id']])->first();
			// $user = [];				
			// 	if(intval($session_data[$i]['user_type']) === 1) { // admin
			// 		$user = DB::table('rekanan')->where(['username' => $session_data[$i]['user_id']])->first();
			// 	} else { // vendor
			// 		$user = DB::table('pegawai')->where(['username' => $session_data[$i]['user_id']])->first();
			// 	}
			$this->sessions[$session_data[$i]['session_id']] = [
				'session_id' => $session_data[$i]['session_id'],
				'api_key' => $session_data[$i]['api_key'],
				'remote_ip' => $session_data[$i]['remote_ip'],
				'last_access' => $session_data[$i]['last_access'],
				'session_data' => $user
			];
		}
	}

		public function startSession() {


			try {
				$this->initSession();
				
			//$ciphertext = Crypto::encrypt($message, $key);
			} catch (CryptoTestFailedException $ex) {
				die('Cannot safely perform encryption');
			} catch (CannotPerformOperationException $ex) {
				die('Cannot safely perform encryption');
			}
		}

		public function destroySession() {
			session_destroy();
		}
	}