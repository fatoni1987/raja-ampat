<?php

namespace Utils;

use Illuminate\Database\Eloquent\Model as Eloquent;


class QueryBuilder
{

	/** *
	*	
	*	Specify query command as corresponding key => value array
	*
	*	Available Keys: select, from, join, criteria, offset, limit
	*	- select key provide array of columns to get, set '*' to get all columns
	*	- from key provide initial table of query, multiple table
	*	- join provide array of join statements with table spesified in from key
	*	- criteria provide criteria statements		 
	*	with, where, or, 	
	*	
	* **/
	
	public static function translate($builder, $query) {
		if($query === null || empty($query)) {

			return $builder;
		} else {
			$columns = "*";
			$from = "";
			$joins = [];
			$criteria = [];

			if(array_key_exists("select", $query))
				$columns = $query["select"]; 

			if(array_key_exists("from", $query))
				$from = $query["from"];

			if(array_key_exists("join", $query))
				$joins = $query["join"];

			if(array_key_exists("criteria", $query))
				$criteria = $query["criteria"];

			return $builder;
		}
	}


	/** * 
	*
	*	$criteria = array(
	*		['column' => 'column1', 'operator' => '=', 'value' => 'value1']
	*	)
	* **/
	public static function filter($builder, $criteria) {
		if($criteria === null || !is_array( $criteria) || empty($criteria)) {
			return $builder;
		} else {
			//print_r($criteria);
			foreach ($criteria as $value) {
					$builder->where($value['column'], $value['operator'], $value['value']);
				}
			
			return $builder;
		}
	}

	/** * 
	*
	*	$joins = array(
	*		['table' => 'table1', 'local_column' => 'table1.id', 'foreign_column' => 'table2.id', 'operator' => '=']
	*	)
	* **/
	public static function join($builder, $joins) {
		if($joins === null || !is_array($joins) || empty($joins)) {
			return $builder;			
		} else {

			foreach ($joins as $value) {
				$builder->join($value['table'], $value['local_column'], $value['operator'], $value['foreign_column']);
			}
			return $builder;
		}
	}
}
