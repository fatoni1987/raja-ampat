<?php

namespace Utils;

use \Nettuts\Controller as Controller;
require '../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
use SMTP;
class Mailer extends Controller {

    private $mailConfig;
    private $recipients;
    private $attachments;
    private $ccs;

    public function __construct() {
        parent::__construct();
        $this->mailConfig = $this->config["email"];
        $this->recipients = [];
        $this->attachments = [];
        $this->ccs = [];
    }

    public function addRecipient($email, $name = '') {
        $this->recipients[$email] = $name;
    }

    public function addCC($email, $name = '') {
        $this->ccs[$email] = $name;
    }

    public function addCCs($emails, $names = array()) {
        $use_names = true;

        if(count($names) < count($emails))
            $use_names = false;
                
        for($i = 0; $i < count($emails); $i++) {
            if($use_names) {
                $this->addCC($emails[$i], $names[$i]); 
            } else {
                $this->addCC($emails[$i]);        
            }
        }
    }

    public function addRecipients($emails, $names = array()) {
        $use_names = true;

        if(count($names) < count($emails))
            $use_names = false;
                
        for($i = 0; $i < count($emails); $i++) {
            if($use_names) {
                $this->addRecipient($emails[$i], $names[$i]); 
            } else {
                $this->addRecipient($emails[$i]);        
            }
        }
        
    }

    public function addAttachment($path) {
        array_push($this->attachments, $path);
    }

    public function sendEmail($subject, $body) {

        set_time_limit(0);

        //Create a new PHPMailer instance
        $mail = new \PHPMailer;
        $mail->SMTPDebug = $this->mailConfig['debug_level'];
        $mail->isMail();
        $mail->isSMTP();
        $mail->Host = $this->mailConfig['host'];
        $mail->SMTPAuth = true;
        $mail->Username = $this->mailConfig['username'];
        $mail->Password = $this->mailConfig['password'];
        if($this->mailConfig['use_tls_encryption']) {
            $mail->SMTPSecure = 'tls';
        }
            
        $mail->Port = $this->mailConfig['port'];
        $mail->Debugoutput = 'html';
        $mail->From = $this->mailConfig['sender']['email'];
        //Set who the message is to be sent from
        $mail->setFrom($this->mailConfig['sender']['email'], $this->mailConfig['sender']['name']);
        //Set an alternative reply-to address
        //$mail->addReplyTo('rohmad.tc09@gmail.com', 'Rohmad Raharjo');
        //Set who the message is to be sent to
        foreach ($this->recipients as $key => $value) {
            //echo $key . ": " . $value;
            $mail->addAddress($key, $value);
        }
        // Sett who the message will e sent also as cc
        foreach ($this->ccs as $key => $value) {
            //echo $key . ": " . $value;
            $mail->addCC($key, $value);
        }
        //$mail->addAddress('rohmad.tc09@gmail.com', 'Rohmad Raharjo');
        //Set the subject line
        $mail->Subject = $subject;
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        //$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
        //Replace the plain text body with one created manually
        $mail->Body = $body;
        $mail->AltBody = 'This is a plain-text alterative message body';


        foreach ($this->attachments as $item) {
            //Attach an image file
            $realpath = realpath($item);
            $mail->AddAttachment($realpath);
            //print_r('Realpath : ' . $realpath);
        }
        SMTP::setTimeout(0);
        //send the message, check for errors
        //$mail->Send();
        //$mail->SmtpClose();
        // disable SSL Certificate verification
        $mail->smtpConnect(
                array(
                    "ssl" => array(
                        "verify_peer" => false,
                        "verify_peer_name" => false,
                        "allow_self_signed" => true
                    )
                )
        );
        if (!$mail->send()) {
            return "Mailer Error: " . $mail->ErrorInfo;
        } else {
            return true;
        }
    }

}
