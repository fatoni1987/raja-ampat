<?php

namespace Utils;
use \Nettuts\Controller as Controller;
use Carbon\Carbon as Carbon;

class Logger extends Controller {

	private $loggingConfig;

	public function __construct() {
		parent::__construct();
		$this->loggingConfig = $this->config["logging"];
	}

	public function log($error, $source) {
		$now = Carbon::now('Asia/Jakarta');
		$filename = $this->loggingConfig["filename"];
		if($this->loggingConfig["use_timestamp"]) {
			$filename .= " " . explode(" ",  str_replace("-", "", $now->toDateTimeString()))[0] . '.log';
		} else {
			$filename .= " " . uniqid() .'.log';
		}
		
		$path = $this->loggingConfig["log_folder"];
		if(!file_exists($path)) {
			mkdir($path);
		}
		$path .= '/' . $filename;
		$message = "Logging date: {$now->formatLocalized('%A %d %B %Y')}" . PHP_EOL
				. "Error : " . $error . PHP_EOL
				. "Source : " . $source	.PHP_EOL . PHP_EOL . PHP_EOL;

		error_log($message, 3, $path);
		return [
			"path" => $path,
			"message" => $message
		];
		
	}
}
