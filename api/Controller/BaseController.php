<?php

namespace Controller;

use Illuminate\Database\Capsule\Manager as DB;
use \Utils\SessionManager as SessionManager;
use \Controller\Admin\UserLogController as userLogController;
use \Model\BaseModel as BaseModel;
use Illuminate\Database\QueryException;
use Whoops\Handler\Handler;
use Whoops\Handler\Inspector;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Psr7;
use Carbon\Carbon as Carbon;
use ZipArchive as zipArchive;
use DateTime as DateTime;

require('../public/app/config.php');

class BaseController extends \Nettuts\Controller {
    // Access Level Enumeration// 0 = public, 1 = vendor, 2 = admin
    // Works as constants, please don't set the value everywhere else
    const OPEN = 0;
    const VENDOR = 1;
    const ADMIN = 2;

    protected $resource;
    protected $api_key = ''; // => 'islamismylife'
    protected $tableName = 'dual';
    protected $entity;
    protected $idColumn = "";
    protected $session_user = "userlogin";
    protected $date_now;
    protected $user_id = 1;
    protected $accessLevel = self::OPEN;
    protected $requiredAccesslevel = self::OPEN;
    protected $authorized = false;
    protected $hasKey = false;
    protected $bypassAuthentication = false;
    protected $loggingConfig;
    protected $nilaiAuth = "ZXByb2M6ZXByb2MxMjM";
    public $url2 = 'http://127.0.0.1:82';
    protected $url = 'http://127.0.0.1:82';


    public function __construct() {
        try {
            parent::__construct();
            $this->loggingConfig = parent::getLoggingConfig();
            $this->eprocErrorHandler();
            $entity = new BaseModel();
            // ITDC menggunakan GMT +8 -> jadi timezone diganti Asia/Singapore
            $this->date_now = Carbon::now('Asia/Singapore');
            date_default_timezone_set('Asia/Singapore');
            if (DB::connection()->getDatabaseName()) {
                // Success connect to database
            } else {
                echo json_encode(['status' => 500, 'result' => 'Cannot connect to database!']);
                die();
            }
        } catch (\PDOException $e) {
            echo json_encode(['status' => 500, 'result' => 'Cannot connect to database!', 'exception' => $e->getMessage()]);
            die();
        }
    }

    private function eprocErrorHandler(){
        $whoops = new \Whoops\Run;
        // catch error
        $whoops->pushHandler(function($exception, $inspector, $run){
            $nodeException = "";
            // Aplikasi Development mengirim index detail error
            $errorException = array(
                'date' => Carbon::now(),
                'type' => 'PHP',
                'message' => $exception->getMessage(),
                'url' => $this->request->getRootUri().$this->request->getResourceUri(),
                'request payload' => json_decode($this->request->getBody(),1)
            );
            if($this->loggingConfig['type'] == 'development'){
                $errorException['error detail'] = array(
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'message' => $exception->getMessage()
                );
            }
            // Aplikasi Production mengirim email berisi detail error
            else if($this->loggingConfig['type'] == 'production'){
                $errorExceptionDetail = $errorException;
                $errorExceptionDetail['error detail'] = array(
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'message' => $exception->getMessage()
                );
                // kirim ke node.js 
                try{
                    $client = new Client();
                    $req = $client->request('POST',$this->loggingConfig['logging_url'],[
                        'json' => ['data' => $errorExceptionDetail]
                    ]);
        
                }
                catch(ConnectException $e){
                    $nodeException = array(
                    'type' => 'PHP',
                    'message' => "Node.js Error Reporting Server Error : ".$e->getMessage()
                    );
                }
            }
            
            $this->resource = array(
                'status' => 500,
                'data' => $nodeException != "" ? $nodeException : $errorException
            );

            $this->sendResponse();
            return Handler::DONE;
        });
        $whoops->register();
    }

    protected function jsErrorReporting($param){
        // kirim ke node.js untuk kirim email jika di mode production
        if($this->loggingConfig['type'] == 'production'){
            $nodeException = "";
            $errorException = array(
                'date' => Carbon::now(),
                'type' => 'JS',
                'url' => $param['url'],
                'message' => $param['exception'],
                'error detail' => $param['stack']
            );
            try{
                $client = new Client();
                $req = $client->request('POST',$this->loggingConfig['logging_url'],[
                    'json' => ['data' => $errorException]
                ]);
    
            }
            catch(ConnectException $e){
                $nodeException = array(
                    'type' => 'js',
                    'message' => "Node.js Error Reporting Server Error : ".$e->getMessage()
                );
            }
            $this->resource = array(
                'status' => 500,
                'data' => $nodeException != "" ? $nodeException : $errorException
            );
        }
        else{
            $this->resource = array(
                'status' => 200,
                'data' => 'ada error di js'
            );
        }
        $this->sendResponse();
    }

    protected function buildResponse($status = 200, $data = array()) {
        try {
            $this->response->setStatus($status);
            $this->response->headers()->set('Content-Type', 'application/json');

            // reponse standard: 
            // 		list (status, result(size, data))
            //		single (status, result(size, data))
            // 		error (status, result(message, stack_trace))
            $size = 1;
            if ($data === null) {
                $size = 0;
            } else if (count($data) != count($data, 1)) {
                $size = sizeof($data);
            } else {
                $size = count($data, 1);
            }

            return array(
                'status' => $status,
                'result' => array(
                    'size' => $size,
                    'data' => $data
                )
            );
        } catch (\Exception $e) {
            return array(
                'status' => 500,
                'result' => array(
                    'message' => $e->getMessage(),
                    'stack_trace' => $e->getTraceAsString()
                )
            );
        }
    }

    protected function validateApiKey($key) {
        return $key == $this->$api_key;
    }

    protected function parseParameters($keys, $param) {
        $result = array();
        foreach ($keys as $key) {
            $result[$key] = $param[$key];
        }

        return $result;
    }

    // override in subclasses, returns query result as array
    protected function selectAll($foreignKeys = array(), $offset = -1, $limit = -1) {
        try {
            $instance = $this->entity;
            $dt = $instance::selectAll($foreignKeys, $offset, $limit);
            $data = $dt->toArray();


            return $data;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    protected function selectActive($activeColumn = 'flag_active', $activeValue = '1', $foreignKeys = array(), $offset = -1, $limit = -1) {
        try {
            $instance = $this->entity;
            $data = $instance::selectActive($activeColumn, $activeValue, $foreignKeys, $offset, $limit);
            //$data = $dt->toArray();


            return $data;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    protected function selectById($id, $foreignKeys = array()) {
        try {
            $instance = $this->entity;
            if (intval($id) == 0) {
                $count = $instance::count();
                $data = $count;
            } else {
                $criteria = array(["column" => $this->idColumn, "operator" => "=", "value" => $id]);
                $data = $instance::selectWhere($criteria, $foreignKeys);
                if ($data === null) {
                    // do nothing	
                } else {

                    $tm = $data;
                    //$data = $data->toArray();
                }
                $count = 1;
            }
            return $data;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    protected function select($criteria, $foreignKeys = array(), $offset = -1, $limit = -1) {
        try {
            $instance = $this->entity;
            $data = $instance::selectWhere($criteria, $foreignKeys, $offset, $limit);
            return $data;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    protected function insertRow($data) {
        //print_r($data);
        try {
            if ($data == null) {
                $status = 500;
                $message = "{$this->tableName} create failed!";
                $result = array('affected' => false, 'message' => $message);
            } else {
                $instance = $this->entity;
                //print_r($instance);
                $status = 200;
                $res = $instance::insert($data);
                $message = "{$this->tableName} created!";
                $result = array('', 'inserted_id' => $res, 'message' => $message);
            }
            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    protected function updateRow($where, $data) {
        try {
            $instance = $this->entity;
            $status = 200;
            //print_r($where);
            //print_r($data);
            $criteria = array();
            foreach ($where as $key => $value) {
                array_push($criteria, ["column" => $key, 'operator' => '=', 'value' => $value]);
            }
            //print_r($criteria);
            $old = $instance::where($criteria);
            if ($old === null) {
                $status = 500;
                $message = "{$this->tableName} not found!";
                $result = array('affected' => false, 'message' => $message);
            } else {
                $status = 200;
                //print_r($old);
                //print_r($data);

                $affected = $instance->update($where, $data);
                $message = "{$this->tableName} updated!";
                $result = array('affected' => $affected, 'message' => $message);
            }
            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    protected function deleteRow($where = array(), $softDelete = true, $statusColumn = 'flag_active', $statusDeleted = false) {
        try {
            $status = 200;
            $instance = $this->entity;
            $data = $instance::where($where)->first();
            if ($data === null) {
                $status = 500;
                $message = "{$this->tableName} not found!";
                $data = array();
            } else {
                $status = 200;
                $data->save();
                $message = "{$this->tableName} deleted!";
            }

            return $data;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    protected function size($criteria = array()) {
        $instance = $this->entity;
        if (empty($criteria))
            return $instance::count();
        else
            return sizeof($instance::selectWhere($criteria, array(), -1, -1));
    }

    protected function sendResponse() {

        $data = $this->resource;
        $response = $this->buildResponse($data['status'], $data['data']);
        echo json_encode($response);
        /* if($this->hasKey) {
          if($this->authorized) {
          $data = $this->resource;
          $response = $this->buildResponse($data['status'], $data['data']);
          echo json_encode($response);
          } else {
          $response = $this->buildResponse(503, "You don't have enough previlege to request the API, please contact your service provider to get access!");
          echo json_encode($response);
          }

          } else {

          $response = $this->buildResponse(503, "You don't have API key to request the API, please contact your service provider to get one!");
          echo json_encode($response);
          } */
    }

    public function setRequiredAccessLevel($accessLevel) {
        $this->accessLevel = $accessLevel;
    }

    /**     *
     * address: authorize
     * path: /auth/authorize
     * method: POST
     * parameter: {session_id: $0 }
     * * */
    public function authorize() {
        $param = json_decode($this->request()->getBody(), true);
        $session_id = $param['session_id'];

        try {
            if ($this->manager->sessionExist($session_id)) {
                $this->resource = array(
                    'status' => 200,
                    'success' => true,
                    'data' => $this->manager->getSession($session_id)
                );
            } else {
                $this->resource = array(
                    'status' => 404,
                    'success' => false,
                    'data' => [],
                    'message' => 'Specified session id not found in server! Please contact the system administrator!'
                );
            }
            echo json_encode($this->resource);
        } catch (\Exception $e) {
            $this->response->setStatus(500);
            $this->response->headers()->set('Content-Type', 'application/json');
            echo json_encode(array(
                'status' => 500,
                'result' => array(
                    'message' => $e->getMessage(),
                    'source' => 'BaseController - authorize()'
                )
            ));
        }
    }

    public function index() {
        $this->resource = array(
            'status' => 200,
            'data' => $this->selectAll()
        );
        $this->sendResponse();
    }

    public function count() {
        $this->resource = array(
            'status' => 200,
            'data' => $this->size()
        );
        $this->sendResponse();
    }

    public function show($id) {
        $this->resource = array(
            'status' => 200,
            'data' => $this->selectById($id)
        );
        $this->sendResponse();
    }

    public function put() {
        $data = json_decode($this->request()->getBody(), true);
        //print_r($data);
        if ($data === null) {
            $this->resource = array(
                'status' => 500,
                'data' => null
            );
        } else {
            $this->resource = array(
                'status' => 200,
                'data' => $this->updateRow($data['where'], $data['data'])
            );
        }
        $this->sendResponse();
    }

    public function create() {
        $data = json_decode($this->request()->getBody(), true);
        if ($data === null) {
            $this->resource = array(
                'status' => 500,
                'data' => null
            );
        } else {
            $this->resource = array(
                'status' => 200,
                'data' => $this->insertRow($data)
            );
        }
        $this->sendResponse();
    }

    public function destroy($id) {
        $data = json_decode($this->request()->getBody(), true);
        if ($data === null) {
            $this->resource = array(
                'status' => 500,
                'data' => null
            );
        } else {
            $this->resource = array(
                'status' => 200,
                'data' => $this->deleteRow($id)
            );
        }
        $this->sendResponse();
    }

    public function find() {
        try {
            // params: $keyword (string), $offset (int), $per_page (int)
            $data = $this->request()->getBody();
            $params = json_decode($this->request()->getBody(), true);

            if ($params == null) {
                echo json_encode(
                        array(
                            'status' => 500,
                            'result' => array(
                                'size' => 0,
                                'data' => $data
                            )
                        )
                );
            } else {
                $data = $this->select($params);
                echo json_encode(
                        array(
                            'status' => 200,
                            'result' => array(
                                'size' => sizeof($data),
                                'data' => $data
                            )
                        )
                );
            }
        } catch (\Exception $e) {
            $this->response->setStatus(500);
            $this->response->headers()->set('Content-Type', 'application/json');
            echo json_encode(array(
                'status' => 500,
                'result' => array(
                    'message' => $e->getMessage(),
                    'stack_trace' => $e->getTraceAsString()
                )
            ));
        }
    }

    public function getNow(){
        return date("Y-m-d H:i");
    }

    public function getCurrentDate(){
        return date('Y-m-d');
    }
}