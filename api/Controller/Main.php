<?php

namespace Controller;

use Illuminate\Database\Capsule\Manager as DB;
require_once '../public/app/config.php';
class Main extends \Nettuts\Controller
{
	
	public function index()
	{
		$this->render("test");
	}
	
	public function test()
	{
		$this->render("test", array("title" => $this->data->message, "name" => "Test"));
	}

	public function notFound()
	{
		$this->render('error', array(), 404);
	}
}