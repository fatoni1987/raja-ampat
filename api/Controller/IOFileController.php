<?php

namespace Controller;

use \Controller\BaseController as BaseController;
use Controller\Admin\UserLogController as UserLogController;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon as Carbon;

require('../public/app/config.php');

class IOFileController extends BaseController {
    private $uploadConfig;
    private $target;
    private $overwrite;
    private $path_alias;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->uploadConfig = $this->config["upload_config"];
        $this->target = $this->uploadConfig["target"];
        $this->overwrite = $this->uploadConfig["overwrite"];
        $this->path_alias = $this->uploadConfig["path_alias"];
        if(!file_exists($this->target))
            mkdir($this->target, 0777, true);

    }

    //path => uploadZip
    public function uploadZipByUrl() {
        set_time_limit(0);
//        $sekarang = date("YmdHisu");
//        $sekarang = $date->format("YmdHisu");

	
        $sekarang = $this->udate('YmdHisu');
        $stat = [];
        $param = json_decode($this->request()->getBody(), true);
        $urls = $param["url"];
        echo $urls;
        $errors = array();
        $imgs = array();
        $folder = '';

//	print_r($param);
	
        $zipName = $sekarang . '.zip';
        $realRar = realpath('../vendor/winrar/');
        $password = $this->makePassw();
        $passwordEnc = $this->encPassw($password);

//        $urls = explode("--", $url);
        $cnt = count($urls);
        for ($i = 0; $i < count($urls); $i++) {
            $file = explode("/", $urls[$i]);
            $target = $this->target;
            $tempTarget = 'tempzip';
            $jmFile = count($file);
            for ($index = 0; $index < $jmFile; $index++) {
                if ($file[$index] != $this->path_alias && $index != ($jmFile - 1)) {
                    $folder = "/" . $file[$index];
                    $target .= $folder;
                    $tempTarget .= $folder;
                }
                if ($index == ($jmFile - 1)) {
                    $name = $file[$index];
                }
            }
            $realTarget = realpath($target);
            //Tiap file dari server storage disalin ke folder temporary di dalam server aplikasi untuk di-zip.
            $content = file_get_contents($target . '/' . $name);
            if (!file_exists($tempTarget)) {
                mkdir($tempTarget, 0777, true);
            }
            if(file_put_contents($tempTarget . '/' . $name, $content)){
                if (!$this->zipPassword($tempTarget, $realRar, $zipName, $name, $password)) {
                    array_push($errors, 'Zipping file ' . $name . ' failed!');
                    
                } else {
                    $stat[] = array($name . ' has add to ' . $zipName);
                    unlink($target . '/' . $name);
                    unlink($tempTarget . '/' . $name);
                }
            } else {
                array_push($errors, 'Retrieving file ' . $name . ' to temporary failed!');
            }
        }
        
        //mengirim hasil zip ke server storage
        if (count($stat) == $cnt) {
            $zippedContent = file_get_contents($tempTarget . "/" . $zipName);
            if (file_put_contents($target . '/' . $zipName, $zippedContent) > 0) {
                $status = 200;
                $imgs[] = array(
                'url' => $this->path_alias . $folder . "/" . $zipName,
                    'name' => $zipName,
                    'password' => $passwordEnc
                );
            } else {
                array_push($errors, 'Upload file ' . $zipName . ' failed!');
            }
            unlink($tempTarget . '/' . $zipName);
        } else {
            $status = 500;
            $imgs[] = array(
                'msg' => "failed"
            );
        }
//            }
        $imageCount = count($stat);


        $data = array(
            'stat' => $stat,
            'message' => $imageCount . ' of ' . $cnt . ' uploaded!',
            'uploaded' => $imageCount,
            'files' => $imgs,
            'errors' => $errors
        );


        $this->resource = array(
            'status' => $status,
            'data' => $data
        );
        $this->sendResponse();
    }

    //path => uploadZip/:id/
    public function uploadZip($id) {
//        $date = new DateTime();
//        $sekarang = date("YmdHisu");
//        $sekarang = $date->format("YmdHisu");
ini_set('memory_limit', '-1');       
 $sekarang = $this->udate('YmdHisu');
        $message;
        $param = json_decode($this->request()->getBody(), true);
        $folder = '';
	
	//print_r($_FILES['uploads']);

        $stat = [];
        if (!isset($_FILES['uploads'])) {
            $status = 500;
            $message = "No files uploaded!!";

            $data = array('message' => $message);
        } else {
            $imgs = array();

            $files = $_FILES['uploads'];
            $cnt = count($files['name']);
            $data = array();
            $errors = array();

            //$target = $this->target . '/' . $id;
            $target = 'tempzip' . '/' . $id; //zip dulu di server aplikasi

            if (!file_exists($target)) {
                mkdir($target, 0777, true);
            }

            $folder = $id;
            if (isset($param["rekanan_id"])) {
                $rekananId = $param["rekanan_id"];
                $folder = $id . '/' . $rekananId;
                $target .= '/' . $rekananId;

                if (!file_exists($target)) {
                    mkdir($target, 0777, true);
                }
            }

            $zipName = $sekarang . '.zip';
            $realRar = realpath('../vendor/winrar/');
            $realTarget = realpath($target);
            $password = $this->makePassw();
            $passwordEnc = $this->encPassw($password);

            
            for ($i = 0; $i < $cnt; $i++) {
                if ($files['error'][$i] === 0) {
                    //$name = $sekarang . "_" . str_replace(" ", "_", $files['name'][$i]);
                    $name = str_replace(" ", "_", $files['name'][$i]);
                    $content =  file_get_contents($files['tmp_name'][$i]);
                    if (file_put_contents($target . '/' . $name, $content) > 0) {
                        if (!$this->zipPassword($target, $realRar, $zipName, $name, $password)) {
                            array_push($errors, 'Zipping file ' . $name . ' failed!');
                        } else {
                            $stat[] = array($files['name'][$i] . ' has add to ' . $zipName);
                            unlink($target . '/' . $name);
                            
                        }
                    } else {
                        array_push($errors, 'Upload file ' . $name . ' failed!');
                    }
                    unlink($files['tmp_name'][$i]);
                }
            }

            //mengirim hasil zip ke server storage
            if (count($stat) == $cnt) {                
                $zippedContent = file_get_contents($target . "/" . $zipName);
                $storageTarget = $this->target . '/' . $id; //lokasi server storage
                if (!file_exists($storageTarget)) {
                    mkdir($storageTarget, 0777, true);
                }
                if (file_put_contents($storageTarget . '/' . $zipName, $zippedContent) > 0) {
                    $imgs[] = array(
                        'url' => $this->path_alias . "/" . $folder . "/" . $zipName,
                        'name' => $zipName,
                        'password' => $passwordEnc
                    );
                } else {
                    array_push($errors, 'Upload file ' . $zipName . ' failed!');
                }
                unlink($target . "/" . $zipName);
            }           
            $imageCount = count($stat);
            
            
            $data = array(
                'stat' => $stat,
                'message' => $imageCount . ' of ' . $cnt . ' uploaded!',
                'uploaded' => $imageCount,
                'files' => $imgs,
                'errors' => $errors
            );
        }

        $this->resource = array(
            'status' => 200,
            'data' => $data
        );
        $this->sendResponse();
    }

    //path => upload/:id/
//     public function upload($id) {
// //        $date = new DateTime();
// //        $sekarang = date("YmdHisu");
// //        $sekarang = $date->format("YmdHisu");
//         $sekarang = $this->udate('YmdHisu');
//         $message;
//         $folder = '';

//         $param = json_decode($this->request()->getBody(), true);
//         // print_r($_FILES);

//         if (!isset($_FILES['uploads'])) {
//             $status = 500;
//             $message = "No files uploaded!!";

//             $data = array('message' => $message);
//         } else {
//             $imgs = array();

//             $files = $_FILES['uploads'];
//             $cnt = count($files['name']);
//             $data = array();
//             $errors = array();

//             $target = $this->target . '/' . $id;

// //            $server = $_SERVER["HTTP_REFERER"] . "/api/public/";
//             if (!file_exists($target)) {
//                 mkdir($target, 0777, true);
//             }

//             $folder = $id;
//             if (isset($param["rekanan_id"])) {
//                 $rekananId = $param["rekanan_id"];
//                 $folder = $id . '/' . $rekananId;
//                 $target .= '/' . $rekananId;

//                 if (!file_exists($target)) {
//                     mkdir($target, 0777, true);
//                 }
//             }

//             if ($cnt > 1) {
//                 for ($i = 0; $i < $cnt; $i++) {
//                     if ($files['error'][$i] === 0) {
//                         $name = $files['name'][$i];
//                         //jika > 57 akan menyebabkan nama file ter truncate
//                         if( strlen($name) > 57){
//                             $extension = pathinfo($name)['extension'];
//                             $name = substr($name, 0, 40).'.'.$extension;
//                         }
//                         // aseli
//                         // $name = $sekarang . "_" . str_replace(" ", "_", $files['name']);
//                         //$name = $sekarang . "_" . str_replace(" ", "_", $name);
//                         $name = str_replace(" ", "_", $name);
//                         //sanitasi karakter
//                         $name = $this->sanitasiString($name);

//                         $content = file_get_contents($files['tmp_name'][$i]);
//                         if (file_put_contents($target . '/' . $name, $content) > 0) {
//                             $imgs[] = array(
//                                 'url' => $this->path_alias . "/" . $folder . "/" . $name,
//                                 'name' => $files['name'][$i]
//                             );
//                             // 'url' => $target . $name,
//                         } else {
//                             array_push($errors, 'Upload file ' . $name . ' failed!');
//                         }
//                         unlink($files['tmp_name'][$i]);
//                     }
//                 }
//             } else {
//                 if ($files['error'] === 0) {
//                     $name = $files['name'];
//                     //jika > 57 akan menyebabkan nama file ter truncate
//                     if( strlen($name) > 57){
//                         $extension = pathinfo($name)['extension'];
//                         $name = substr($name, 0, 40).'.'.$extension;
//                     }
//                     // aseli
//                     // $name = $sekarang . "_" . str_replace(" ", "_", $files['name']);
//                     //$name = $sekarang . "_" . str_replace(" ", "_", $name);
//                     $name = str_replace(" ", "_", $name);
//                     //sanitasi karakter
//                     $name = $this->sanitasiString($name);

//                     $content = file_get_contents($files['tmp_name']);
//                     if (file_put_contents($target . '/' . $name, $content) > 0) {
//                         $imgs[] = array(
//                             'url' => $this->path_alias . "/" . $folder . "/" . $name,
//                             'name' => $files['name']
//                         );
//                     } else {
//                         array_push($errors, 'Upload file ' . $name . ' failed!');
//                     }
//                     unlink($files['tmp_name']);
//                 }
//             }

//             $imageCount = count($imgs);


//             $data = array(
//                 'message' => $imageCount . ' of ' . $cnt . ' uploaded!',
//                 'uploaded' => $imageCount,
//                 'files' => $imgs,
//                 'errors' => $errors
//             );
//         }

//         $this->resource = array(
//             'status' => 200,
//             'data' => $data
//         );
//         $this->sendResponse();
//     }

public function upload($id) {
    $sekarang = $this->udate('YmdHisu');
    $message;
    $folder = '';
    $param = json_decode($this->request()->getBody(), true);
   
    if (!isset($_FILES['uploads'])) {
        $status = 500;
        $message = "No files uploaded!!";

        $data = array('message' => $message);
    } else {
        $imgs = array();
       
        $files = $_FILES['uploads'];
        $cnt = count($files['name']);
        $data = array();
        $errors = array();
        
        $target = $this->target;
        if (!file_exists($target)) {
            mkdir($target, 0777, true);
        }
        
        if ($cnt > 1) {
            echo $cnt;
            die();
            for ($i = 0; $i < $cnt; $i++) {
                if ($files['error'][$i] === 0) {
                    $name = $files['name'][$i];
                    //jika > 57 akan menyebabkan nama file ter truncate
                    if( strlen($name) > 57){
                        $extension = pathinfo($name)['extension'];
                        $name = substr($name, 0, 40).'.'.$extension;
                    }
                    // aseli
                    // $name = $sekarang . "_" . str_replace(" ", "_", $files['name']);
                    $name = $sekarang . "_" . str_replace(" ", "_", $name);
                    //sanitasi karakter
                    $name = $this->sanitasiString($name);

                    $content = file_get_contents($files['tmp_name'][$i]);
                    if (file_put_contents($target . '/' . $name, $content) > 0) {
                        $imgs[] = array(
                            'url' => $target . $name,
                            'name' => $files['name'][$i]
                        );
                    } else {
                        array_push($errors, 'Upload file ' . $name . ' failed!');
                    }
                    unlink($files['tmp_name'][$i]);
                }
            }
            $imageCount = count($imgs);
            $data['message'] = $cnt . ' uploaded!';
            $data['uploaded'] = $imageCount;
            $data['files'] = $imgs;
            $data['errors'] = $errors;
        } else {
            if ($files['error'] == 0) {
                $name = $files['name'];
                //jika > 57 akan menyebabkan nama file ter truncate
                if( strlen($name) > 57){
                    $extension = pathinfo($name)['extension'];
                    $name = substr($name, 0, 40).'.'.$extension;
                }
                // aseli
                // $name = $sekarang . "_" . str_replace(" ", "_", $files['name']);
                $name = $sekarang . "_" . str_replace(" ", "_", $name);
                //sanitasi karakter
                $name = $this->sanitasiString($name);
                $content = file_get_contents($files['tmp_name']);
                if (file_put_contents($target . '/' . $name, $content) > 0) {
                    $imgs[] = array(
                        'url' => $this->path_alias."/".str_replace('+','%2B',$name),
                        'name' => $files['name']
                    );
                } else {
                    array_push($errors, 'Upload file ' . $name . ' failed!');
                }
                unlink($files['tmp_name']);

                $imageCount = count($imgs);
                $data['message'] = $imageCount . ' of ' . $cnt . ' uploaded!';
                $data['uploaded'] = $imageCount;
                $data['files'] = $imgs;
                $data['errors'] = $errors;
            }
        }
    }

    $this->resource = array(
        'status' => 200,
        'data' => $data
    );
    $this->sendResponse();
}

    //path => deleteFile
    public function deleteFile() {
        $param = json_decode($this->request()->getBody(), true);
        if (isset($param["url"])) {
            $url = $param["url"];
            $urls = explode("/", $url);
            $file = $this->target;
            for ($index = 0; $index < count($urls); $index++) {
                if ($urls[$index] != $this->path_alias) {
                    $file .= "/" . $urls[$index];
                }
            }
            if (file_exists($file)) {
                if (unlink($file)) {
                    $status = 200;
                    $message = [
                        "message" => 'data ' . $file . ' has ben deleted'
                    ];
                } else {
                    $status = 500;
                    $message = [
                        "message" => 'data ' . $file . ' delete failed'
                    ];
                }
            } else {
                $status = 404;
                $message = [
                    "message" => 'data ' . $file . ' cannot be found'
                ];
            }
        } else if ((isset($param["urls"]))) {
            $url = $param["urls"];
            $i = 0;
            foreach ($url as $val) {
                $urls = explode("/", $val);
                $file = $this->target;
                for ($index = 0; $index < count($urls); $index++) {
                    if ($urls[$index] != $this->path_alias) {
                        $file .= "/" . $urls[$index];
                    }
                }
                if (file_exists($file)) {
                    if (unlink($file)) {
                        $i++;
                        $message[] = array(
                            "message" => 'data ' . $file . ' has ben deleted'
                        );
                    } else {
                        $status = 500;
                        $message[] = array(
                            "message" => 'data ' . $file . ' delete failed'
                        );
                        break;
                    }
                } else {
                    $status = 404;
                    $message[] = array(
                        "message" => 'data ' . $file . ' cannot be found'
                    );
                    break;
                }
            }
            if ($i == count($url)) {
                $status = 200;
            }
        }

        $this->resource = array(
            'status' => $status,
            'data' => $message
        );
        $this->sendResponse();
    }

    public function delete($url, $action = 'single') {
        if ($action == 'single') {
            $urls = explode("/", $url);
            $file = $this->target;
            for ($index = 0; $index < count($urls); $index++) {
                if ($urls[$index] != $this->path_alias) {
                    $file .= "/" . $urls[$index];
                }
            }
            if (unlink($file)) {
                $status = true;
                $message = [
                    "message" => 'data ' . $file . ' has ben deleted'
                ];
            } else {
                $status = false;
                $message = [
                    "message" => 'data ' . $file . ' delete failed'
                ];
            }
        } else {
            $i = 0;
            foreach ($url as $val) {
                $urls = explode("/", $val);
                $file = $this->target;
                for ($index = 0; $index < count($urls); $index++) {
                    if ($urls[$index] != $this->path_alias) {
                        $file .= "/" . $urls[$index];
                    }
                }
                if (unlink($file)) {
                    $i++;
                    $message[] = array(
                        "message" => 'data ' . $file . ' has ben deleted'
                    );
                } else {
                    $message[] = array(
                        "message" => 'data ' . $file . ' delete failed'
                    );
                }
            }
            if ($i == count($url)) {
                $status = true;
            } else {
                $status = false;
            }
        }

        return $status;
    }

    //path => download/:id/:file
    public function download($id, $file) {
        $known_mime_types = array(
            "htm" => "text/html",
            "exe" => "application/octet-stream",
            "zip" => "application/zip",
            "doc" => "application/msword",
            "docx" => "application/msword",
            "jpg" => "image/jpg",
            "php" => "text/plain",
            "xls" => "application/vnd.ms-excel",
            "xlsx" => "application/vnd.ms-excel",
            "ppt" => "application/vnd.ms-powerpoint",
            "pptx" => "application/vnd.ms-powerpoint",
            "gif" => "image/gif",
            "pdf" => "application/pdf",
            "txt" => "text/plain",
            "html" => "text/html",
            "png" => "image/png",
            "jpeg" => "image/jpg"
        );
        //ob_start();
        $file = $this->target . "/" . $id . "/" . $file;
        $file_extension = strtolower(substr(strrchr(basename($file), "."), 1));
        //ob_clean();
        if (array_key_exists($file_extension, $known_mime_types)) {
            $mime_type = $known_mime_types[$file_extension];
        } else {
            $mime_type = "application/force-download";
        };
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: ' . $mime_type);
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();

            readfile($file);
        } else {
            echo('<h1>404</h1><br /><br /><p>file could not be found</p>');
        }
    }

    //path => download/:id/:file/:session
    public function download2($id, $file, $session) {
        $known_mime_types = array(
            "htm" => "text/html",
            "exe" => "application/octet-stream",
            "zip" => "application/zip",
            "doc" => "application/msword",
            "docx" => "application/msword",
            "jpg" => "image/jpg",
            "php" => "text/plain",
            "xls" => "application/vnd.ms-excel",
            "xlsx" => "application/vnd.ms-excel",
            "ppt" => "application/vnd.ms-powerpoint",
            "pptx" => "application/vnd.ms-powerpoint",
            "gif" => "image/gif",
            "pdf" => "application/pdf",
            "txt" => "text/plain",
            "html" => "text/html",
            "png" => "image/png",
            "jpeg" => "image/jpg"
        );
        $data = DB::table("user_session")->where("session_id", "=", $session)->get();
        if ($data) {
            ob_start();
            $file = $this->target . "/" . $id . "/" . $file;
            $file_extension = strtolower(substr(strrchr(basename($file), "."), 1));
            if (array_key_exists($file_extension, $known_mime_types)) {
                $mime_type = $known_mime_types[$file_extension];
            } else {
                $mime_type = "application/force-download";
            };
            if (file_exists($file)) {
                $ctrl = new UserLogController();
                if ($data[0]->user_type == 1) {
                    $user = DB::table("pegawai")->where("pegawai_id", "=", $data[0]->user_id)->get(["username"]);
                    $datalog = ['username' => $user[0]->username, 'user_activity_id' => 166, 'detail' => 'Mendownload file ' . basename($file), 'tanggal' => $this->date_now];
                    $ctrl->insertLogUser($datalog);
                } else {
                    $user = DB::table("rekanan")->where("rekanan_id", "=", $data[0]->user_id)->get(["username"]);
                    $datalog = ['username' => $user[0]->username, 'user_activity_id' => 166, 'detail' => 'Mendownload file ' . basename($file), 'tanggal' => $this->date_now];
                    $ctrl->insertLogRekanan($datalog);
                }

                header('Content-Description: File Transfer');
                header('Content-Type: ' . $mime_type);
                header('Content-Disposition: attachment; filename=' . basename($file));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                ob_clean();

                readfile($file);
            } else {
                echo('<h1>404</h1><p>file could not be found</p>');
            }
        } else {
            echo("<h1>404</h1><p>The route you were looking for could not be found</p>");
        }
    }

    function zipPassword($realTarget, $realRar, $zipName, $name, $pass) {
        if (substr(php_uname(), 0, 7) == "Windows") {
            $result = system("cd \"{$realTarget}\"" . "&& " . "\"{$realRar}\"\\Winrar a  \"{$zipName}\"  \"{$name}\"");
            
            //return $result; //nilai $result selalu blank meskipun proses zipping berhasil
            return true;
        } else {
            $cmd = "cd \"{$realTarget}\"" . "&& " . "zip  \"{$zipName}\"  \"{$name}\"";
            ob_start();
            system($cmd);
            $result = ob_get_contents();
            ob_end_clean();

            //return $result; //nilai $result selalu blank meskipun proses zipping berhasil
            return true;
        }
    }

    function makePassw() {
        $str = "abcdefghijklmnopqrstuvwxyz";
        $chars = (str_split($str));
        $pass = "";
        for ($i = 0; $i < 5; $i++) {
            $pass.= $chars[rand(1, count($chars) - 1)];
        }
        return $pass;
    }

    function encPassw($pass) {
        $chars = (str_split($pass));
        $passEnc = "";
        for ($i = 0; $i < count($chars); $i++) {
            $next_ch = ++$chars[$i];
            if (strlen($next_ch) > 1) { // if you go beyond z or Z reset to a or A
                $next_ch = $next_ch[0];
            }
            $passEnc .= $next_ch;
        }
        return $passEnc;
    }

    function decPassw($pass) {
        $chars = (str_split($pass));
        $passEnc = "";
        for ($i = 0; $i < count($chars); $i++) {
            $next_ch = '';
            if ($chars[$i] === 'a') {
                $next_ch = 'z';
            } elseif ($chars[$i] === 'A') {
                $next_ch = 'Z';
            } else {
                $next_ch = chr(ord($chars[$i]) - 1);
            }
            $passEnc .= $next_ch;
        }
        return $passEnc;
    }

    function sanitasiString($str){
        $bad = array(
            '../', '<!--', '-->', '<', '>',
            "'", '"', '&', '$', '#',
            '{', '}', '[', ']', '=',
            ';', '?','+'
        );
        return str_replace($bad, '', $str);
    }

    public function udate($format = 'u', $utimestamp = null) {
        if (is_null($utimestamp)) {
            $utimestamp = microtime(true);
        }

        $timestamp = floor($utimestamp);
        $milliseconds = round(($utimestamp - $timestamp) * 1000000);

        return date(preg_replace('`(?<!\\\\)u`', $milliseconds, $format), $timestamp);
    }
}
