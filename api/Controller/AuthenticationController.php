<?php
namespace Controller;

use \Model\Pegawai as Pegawai;
use \Model\Rekanan as Rekanan;
use \Model\UserSession as UserSession;
use \Utils\SessionManager as SessionManager;
use \Controller\SessionController as SessionController;
use Carbon\Carbon as Carbon ;
use Illuminate\Database\Capsule\Manager as DB;
require('../public/app/config.php');

class AuthenticationController extends SessionController {
	protected $manager;

	public function __construct() {
		parent::__construct();
			
	}

	private function getIpAddress() {
		//Just get the headers if we can or else use the SERVER global
		if ( function_exists( 'apache_request_headers' ) ) {
			$headers = apache_request_headers();
		} else {
			$headers = $_SERVER;
		}
		//Get the forwarded IP if it exists
		if ( array_key_exists( 'X-Forwarded-For', $headers ) && filter_var( $headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
			$the_ip = $headers['X-Forwarded-For'];
		} elseif ( array_key_exists( 'HTTP_X_FORWARDED_FOR', $headers ) && filter_var( $headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 )
		) {
			$the_ip = $headers['HTTP_X_FORWARDED_FOR'];
		} else {
			
			$the_ip = filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 );
		}
		if(!isset($the_ip) || $the_ip == 0 || $the_ip == null || $the_ip == '')
			$the_ip = '127.0.0.1';
		
		return $the_ip;
	}

	/** *
	* address: -
	* path: /auth/login/admin
	* method: POST
	* parameter: {username: $0, password: $1, $api_key }
	** */
	public function adminLogin() {
		try {
			$param = json_decode($this->request()->getBody(), true);
			$param['flag_active'] = true;
			$param['password'] = md5($param['password']);
			$api_key_param = $param['api_key'];
			$usernames = $param['username'];
			$result = DB::table("mst_user")->where("username","=",$param['username'])->where("password","=",$param['password'])->get();
			// print_r($result);
			$api_key = $this->manager->getApiConfig()['key'];
			$ip = $_SERVER['REMOTE_ADDR'];
			$session_id = md5($result[0]->username . '_admin');
			
			if(count($result) > 0) {
				// login success
				$session_data = [
					'api_key' => $api_key,
					'user_type' => $result[0]->user_type,
					'username' => $result[0]->username,//$id,
					'ip_address' => $ip
				];
				
				if(!$this->manager->sessionExist($session_id)) {
					if($api_key_param === $api_key) {
						if($this->manager->openSession($session_id, $session_data)) {
							$this->manager->updateSessionLastActive($session_id);
							$this->resource = array(
							'status' => 200,
							'data' => [
								'success' => true,
								'session_id' => $session_id,
								'ip'=>$ip,
								'session_data' => $session_data,
								'message' => 'Login Successful!'
								]
							);							
						} else {
							$this->resource = array(
							'status' => 200,
							'data' => [
								'success' => false,
								'session_id' => $session_id,
								'session_data' => $session_data,
								'ip'=>$ip,
								'message' => 'Login Successful, but failed to create session!'
								]
							);
                                                }
					} else {
						$this->resource = array(
						'status' => 200,
						'data' => [
								'success' => false,
								'session_id' => $session_id,
								'session_data' => $session_data,
								'ip'=>$ip,
								'message' => 'Login failed, your api key not valid. Please contact the system administrator!'
								]
						);
					}
				} else {
					$this->resource = array(
					'status' => 200,
					'data' => [
							'success' => false,
							'session_id' => $session_id,
							'session_data' => $session_data,
							'ip'=>$ip,
							'message' => 'Login failed, your session still active. Please contact the system administrator!'
							]
					);
				}
				//$this->manager->updateSessionLastActive($session_id);
			} else {
				$this->resource = array(
					'status' => 200,
					'data' => [
						'success' => false,
						'session_id' => 0,
						'ip'=>0,
						'message' => 'Login failed, username or password invalid. Please contact the system administrator!'
						]
				);
			}
			echo json_encode($this->resource);
		} catch (\Exception $e) {
			$this->response->setStatus(500);
			$this->response->headers()->set('Content-Type', 'application/json');
			echo json_encode(
				array(
					'status' => 500,
					'result' => array(
						'message' => $e->getMessage(),
						'source' => 'AuthenticationController - adminLogin()'
					)
				)
			);
		}
	}

	/** *
	* address: -
	* path: /auth/login/vendor
	* method: POST
	* parameter: {username: $0, password: $1, $api_key }
	** */
	public function vendorLogin() {
		try {
			$param = json_decode($this->request()->getBody(), true);
			// $param['is_activated'] = true;
			$param['pswd'] = md5($param['password']);
			// echo $param['pswd'] ;
			$api_key_param = $param['api_key'];

			$result = DB::table("mst_user")->where("no_ktp","=",$param['username'])->where("password","=",md5($param['password']))->get();
			// print_r($result);
			$session_id = md5($result[0]->id_user . '_vendor');
			$api_key = $this->manager->getApiConfig()['key'];
			$ip = $_SERVER['REMOTE_ADDR'];
			
			if(count($result) > 0) {
				// login success
				$session_data = [
					'api_key' => $api_key,
					'user_type' => 2,
					'user_id' => $result[0]->id_user,
					'ip_address' => $ip
				];

				if(!$this->manager->sessionExist($session_id)) {
					if($api_key_param === $api_key) {
						if(!$result[0]->flag_active || $result[0]->flag_active == null){

							$this->resource = array(
								'status' => 200,
								'data' => [
									'success' => false,
									'session_id' => 0,
									'ip'=>$ip,
									'message' => 'Login failed, please activate your account first!'
									]
								);

						}else{//rint_r($session_data);
							if($this->manager->openSession($session_id, $session_data)) {
								//$this->manager->updateSessionLastActive($session_id);
								$this->resource = array(
								'status' => 200,
								'data' => [
									'success' => true,
									'session_id' => $session_id,
									'ip'=>$ip,
									'message' => 'Login Successful!'
									]
								);
							}
							else
								$this->resource = array(
								'status' => 200,
								'data' => [
									'success' => false,
									'session_id' => $session_id,
									'ip'=>$ip,
									'message' => 'Login Successful, but failed to create session!'
									]
								);
						}
						

					} else {
						$this->resource = array(
						'status' => 200,
						'data' => [
							'success' => false,
							'session_id' => $session_id,
							'ip'=>$ip,
							'message' => 'Login failed, your api key not valid. Please contact the system administrator!'
							]
						);
					}
				} else {
					$this->resource = array(
					'status' => 200,
					'data' => [
						'success' => false,
						'session_id' => $session_id,
						'ip'=>$ip,
						'message' => 'Login failed, your session still active. Please contact the system administrator!'
						]
					);
				}

				//$this->manager->updateSessionLastActive($session_id);
			} else {
				$this->resource = array(
					'status' => 200,
					'data' => [
						'success' => false,
						'session_id' => 0,
						'ip'=>0,
						'message' => 'Login failed, username or password invalid. Please contact the system administrator!'
						]
					);
			}
			//$this->sendResponse();
			echo json_encode($this->resource);

		} catch (\Exception $e) {
			$this->response->setStatus(500);
			$this->response->headers()->set('Content-Type', 'application/json');
			echo json_encode(array(
				'status' => 500,
				'result' => array(
					'message' => $e->getMessage(),
					'source' => 'AuthenticationController - vendorLogin()'

					)
				));
		}
	}

	/** *
	* address: -
	* path: /auth/logout
	* method: POST
	* parameter: {session_id: $0 }
	** */
	public function logout() {
		$param = json_decode($this->request()->getBody(), true);
		$session_id = $param['session_id'];

		try {
			if($this->manager->sessionExist($session_id)) {
				$this->manager->closeSession($session_id);
				$this->resource = array(
				'status' => 200,
				'data' => [
					'success' => true,
					'message' => 'Logout Successful!'
					]
				);
			} else {
				$this->resource = array(
				'status' => 200,
				'data' => [
					'success' => false,
					'message' => 'Logout Failed! The specified session id not available in server!'
					]
				);
			}
			echo json_encode($this->resource);
		} catch (\Exception $e) {
			$this->response->setStatus(500);
			$this->response->headers()->set('Content-Type', 'application/json');
			echo json_encode(array(
				'status' => 500,
				'result' => array(
					'message' => $e->getMessage(),
					'source' => 'AuthenticationController - logout()'

					)
				));
		}
	}

	public function advanceLogOut(){
		$param = json_decode($this->request()->getBody(), true);
		$session_id = $param['session_id'];

		try {
			if($this->manager->sessionExist($session_id)) {
				$this->manager->deleteSession($session_id);
				$this->resource = array(
				'status' => 200,
				'data' => [
					'success' => true,
					'message' => 'Logout Successful!'
					]
				);
			} else {
				$this->resource = array(
				'status' => 200,
				'data' => [
					'success' => false,
					'message' => 'Logout Failed! The specified session id not available in server!'
					]
				);
			}
			echo json_encode($this->resource);
		} catch (\Exception $e) {
			$this->response->setStatus(500);
			$this->response->headers()->set('Content-Type', 'application/json');
			echo json_encode(array(
				'status' => 500,
				'result' => array(
					'message' => $e->getMessage(),
					'source' => 'AuthenticationController - logout()'

					)
				));
		}
	}

	/** *
	* address: -
	* path: /auth/get_session
	* method: POST
	* parameter: {session_id: $0 }
	** */
	public function getSessionBySid() {
		$param = json_decode($this->request()->getBody(), true);
		if(!array_key_exists('session_id',$param)){
			$this->resource = array(
				'status' => 404,
				'data' => []
				);
			echo json_encode($this->resource);
			return;
		}
		$session_id = $param['session_id'];		
		// print_r($session_id);
		try {
			if($this->manager->sessionExist($session_id)) {		
				$getUserSession = UserSession::find($session_id);
				// print_r($getUserSession);
				$user = DB::table('pegawai_view')->where(['nik' => $getUserSession->user_id])->first();				
				// if($getUserSession->user_type === 1) { // admin
				// 	$user = DB::table('rekanan')->where(['username' => $getUserSession->user_id])->first();
				// } else { // vendor
				// 	$user = DB::table('pegawai')->where(['username' => $getUserSession->user_id])->first();
				// }	
				// print_r($user);
				$sessionData = $this->manager->getSession($session_id);
				$now = Carbon::now();
				$sessionConfig = $this->manager->getSessionConfig();
				$timeout = $sessionConfig['timeout'];
				$last_access = Carbon::parse($sessionData["last_access"], 'Asia/Jakarta');
				$valid_timestamp = Carbon::parse($sessionData["last_access"], 'Asia/Jakarta')->addSeconds($timeout);
				$sessionData["timeout"] = $timeout . ' seconds';
				$sessionData["timeout_time"] = $valid_timestamp->toDateTimeString();
				$sessionData["server_time"] = $now->toDateTimeString();
				$sessionData["last_access"] = $last_access->toDateTimeString();
				$sessionData["timeout_diff"] = $valid_timestamp->diffInSeconds($now);
				$sessionData["timeout_status"] = $valid_timestamp->lt($now); 
				$sessionData['data'] = $user;
				if($sessionData["timeout_status"]) {

					$this->manager->closeSession($session_id);
					$this->resource = array(
						'status' => 403,
						'data' => 'Session has timed out!'
					);					

				} else {
					$this->manager->updateSessionLastActive($session_id);
					$this->resource = array(
						'status' => 200,
						'data' => $sessionData
					);		
				}
				// print_r($sessionData);			
				
			} else {
				$this->resource = array(
				'status' => 404,
				'data' => []
				);
			}
			echo json_encode($this->resource);
		} catch (\Exception $e) {
			$this->response->setStatus(500);
			$this->response->headers()->set('Content-Type', 'application/json');
			echo json_encode(array(
				'status' => 500,
				'result' => array(
					'message' => $e->getMessage(),
					'source' => 'AuthenticationController - adminLogin()'

					)
				));
		}
	}

	public function updateSessionLastAccess() {
		try {
			$param = json_decode($this->request()->getBody(), true);
			$session_id = $param['session_id'];
			$result = $this->manager->updateSessionLastActive($session_id);
			echo json_encode(array(
				'status' => 200,
				'result' => $result
				));

		} catch (\Exception $e) {
			$this->response->setStatus(500);
			$this->response->headers()->set('Content-Type', 'application/json');
			echo json_encode(array(
				'status' => 500,
				'result' => array(
					'message' => $e->getMessage(),
					'source' => 'AuthenticationController - updateSessionLastAccess()'

					)
				));
		}
	}

	public function getApi() {
		try {
			echo json_encode($this->manager->getApiConfig());	
		} catch (\Exception $e) {
			$this->response->setStatus(500);
			$this->response->headers()->set('Content-Type', 'application/json');
			echo json_encode(array(
				'status' => 500,
				'result' => array(
					'message' => $e->getMessage(),
					'source' => 'RoleController - selectRoleByType()'
					)
				));
		}		
	}

	public function getSession() {
		try {
			echo json_encode($this->manager->getSessionConfig());
			
		} catch (\Exception $e) {
			$this->response->setStatus(500);
			$this->response->headers()->set('Content-Type', 'application/json');
			echo json_encode(array(
				'status' => 500,
				'result' => array(
					'message' => $e->getMessage(),
					'source' => 'AuthenticationController - getSession()'
					)
				));
		}		
	}
}