<?php

namespace Controller\Visitor;

use \Controller\BaseController as BaseController;
use Model\Rekanan as rekanan;
use Model\MstKota as kota;
use Model\RekananIjinUsaha as rekananIjinUsaha;
use Model\RekananMaps as rekananMaps;

use Controller\Admin\EmailConfController as EmailConfController;
use Model\MstReference as reference;
use Carbon\Carbon as Carbon;
use Utils\QueryBuilder as QB;
use Illuminate\Database\Capsule\Manager as DB;
use Utils\Mailer as Mailer;

require('../public/app/config.php');

class PendaftaranController extends BaseController {
    
    public function cekUsername() {
        $param = json_decode($this->request()->getBody(), true);
        // print_r($param['daftar']['username']);
        // $username = $param['param'][2];
        // $password = $param['param'][3];
        $builder = DB::table('mst_user')->where('username', '=', $param['daftar']['email'])->where('flag_active','=',true)->get();

        if($builder){
            $this->resource = array(
                'status' => 300,
                'data' => "Data sudah ada"
            );
        }else{
            $data = [
                "username" => $param['daftar']['nik'],
                "password" => md5($param['daftar']['password']),
                "user_type" => "2"
            ];
            $save = DB::table('mst_user')->insert($data);

            $dataUser = [
                "no_identitas_user_pajak" => $param['daftar']['nik'],
                "email_user_pajak" => $param['daftar']['email'],
                "telp_user_pajak" => $param['daftar']['noTelp'],
                "tipe_identitas_user_pajak" => $param['daftar']['type_identitas']
            ];
            $save = DB::table('user_pajak')->insert($dataUser);

            $this->resource = array(
                'status' => 200,
                'data' => 'Data berhasil di simpan'
            );
        }                
        $this->sendResponse();
    }
}
