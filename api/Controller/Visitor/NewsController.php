<?php

namespace Controller\Visitor;

use \Model\MstNews as Entity;
use \Controller\BaseController as BaseController;
//use \Controller\Admin\UserLogController as userLogController;
use Carbon\Carbon as Carbon ;
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\DatabaseManager as Manager;
require('../public/app/config.php');

class NewsController extends BaseController
{	
    public function __construct() {
        parent::__construct();
        $this->entity = new Entity();
        //$this->bypassAuthentication = true;
    }

    public function yolo(){
        echo $this->getNow();
        // try{
        //     echo $a;
        // }
        // catch(Exception $e){
        //     echo $e->getMessage();
        // }
        // echo Carbon::now();
        // echo "<br>";
        // echo date("h:i:sa");
        // echo "<br>";
        // echo Carbon::now();
    }

    public function testEmail(){
        echo $_SERVER['SERVER_NAME'];
        // // alamat server
        // $url = $_SERVER['REQUEST_URI']; //returns the current URL
        // $URIpart = explode('/',$url);
        // // harcode url
        // $server = $_SERVER['SERVER_NAME'];
        // if($server != 'localhost'){
        //     $server = 'https://eproc.itdc.co.id';
        // }
        // if($URIpart[1] == "uat"){
        //     $finalUrl = 'https://eproc.itdc.co.id'."/".$URIpart[1];
        // }
        // else if($server == 'localhost'){
        //     $finalUrl = $server."/".$URIpart[1];
        // }
        // echo $finalUrl;
    }


    // news/list
    public function lists() {
        $param = json_decode($this->request()->getBody(), true);
        $offset = $param["offset"];
        $pageSize = $param["pageSize"];
        $limit = $param["limit"];
        // $keyword = "'%".strtoupper($param["keyword"])."%'";
        $builder = DB::table('mst_news')->where("flag_active", "=", TRUE);
        // if($param["keyword"] != ""){
        //     $builder = $builder->whereRaw( 'UPPER(title_news) LIKE ?', [ $keyword ] );
        // }
     
        $builderCount = $builder->count();
        $builder->orderBy("date_news", "DESC")->skip($offset)->take($limit); // old: id_news
    
        $this->resource = array(
            'status' => 200,
            'data' => ['data'=>$builder->get(), 'count'=>$builderCount]
            );
        $this->sendResponse();
    }

    // news/detail/id_news
    public function detail($id) {
        $this->tableName = 'MstNews';
        $this->idColumn = 'id_news';
        $this->resource = array(
            'status' => 200,
            'data' => $this->selectById($id)
            );
        $this->sendResponse();
    }

    
/*  ----------------------------------------------------------------------- dikomen krn gak ada yg make!
    public function paginate() {
        $param = json_decode($this->request()->getBody(), true);
        $this->resource = array(
            'status' => 200,
            'data' => $this->selectActive('flag_active', '1', array(),  $param["offset"], 
                $param["limit"])
            );
        $this->sendResponse();
    }

    public function insertData(){
        $data = json_decode($this->request()->getBody(), true);
        $this->tableName = 'MstNews';
        $save = $this->insertRow($data);  
        if($save){
            //nambah insert ke tabel log akses
            $datalog = ['username'=>$this->session_user,'user_activity_id'=>151,'detail'=>'Menginput Berita','tanggal'=>$this->date_now];
            $ctrl = new UserLogController();
            $savelog = $ctrl->insertLogUser($datalog);
            $this->resource = $savelog;
            $this->sendResponse();
        }
        else{
            $message = "failed to insert user log activity";
            $result = array('affected' => false, 'message' => $message);
            $this->resource = $result;
            $this->sendResponse();
        }
    }
    
    public function updateData(){
        $data = json_decode($this->request()->getBody(), true);
        $this->tableName = 'MstNews';
        $this->resource = array(
            'status' => 200,
            'data' => $this->insertRow($data)
            );
        $this->sendResponse();
    }
*/    
}