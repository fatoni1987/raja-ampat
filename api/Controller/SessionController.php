<?php

namespace Controller;

use \Utils\SessionManager as SessionManager;
use \Controller\BaseController as BaseController;
use Controller\Admin\UserLogController as UserLogController;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Capsule\Manager as DB;

require('../public/app/config.php');

class SessionController extends BaseController {

    protected $manager;

    public function __construct() {
        parent::__construct();
        $this->manager = new SessionManager();
    }

    public function listSessions() {
        try {
            $this->resource = array(
                'status' => 200,
                'data' => $this->manager->listSessions()
            );
            //$this->sendResponse();
            echo json_encode($this->resource);
        } catch (\Exception $e) {
            $this->response->setStatus(500);
            $this->response->headers()->set('Content-Type', 'application/json');
            echo json_encode(array(
                'status' => 500,
                'result' => array(
                    'message' => $e->getMessage(),
                    'source' => 'SessionController - listSessions()'
                )
            ));
        }
    }

    public function getApi() {
        try {
            echo json_encode($this->manager->getApiConfig());
        } catch (\Exception $e) {
            $this->response->setStatus(500);
            $this->response->headers()->set('Content-Type', 'application/json');
            echo json_encode(array(
                'status' => 500,
                'result' => array(
                    'message' => $e->getMessage(),
                    'source' => 'SessionController - getApi()'
                )
            ));
        }
    }

    public function getSession() {
        try {

            echo json_encode($this->manager->getSessionConfig());
        } catch (\Exception $e) {
            $this->response->setStatus(500);
            $this->response->headers()->set('Content-Type', 'application/json');
            echo json_encode(array(
                'status' => 500,
                'result' => array(
                    'message' => $e->getMessage(),
                    'source' => 'SessionController - getSession()'
                )
            ));
        }
    }

    public function selectSession() {
        //path: session/select
        //method: POST
        /* {
          "type" : 2,
          "nama" : "%%"
          }
         */
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("session_view");
        $builder->where("is_active", "=", true);
        if ($param["type"] == 1 || $param["type"] == 2) {
            $builder->where("user_type", "=", $param["type"]);
        }
        $builder->where(function ($query) {
            $param = json_decode($this->request()->getBody(), true);
            $query->whereRaw("lower(nama) like lower(?) OR lower(username) like lower(?)", array($param["nama"], $param["nama"]));
        });

        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    public function forceLogoff() {
        //path: session/forceLogoff
        //method: POST
        /* param:{
          "id" : 37,
          "type" : 1,
          "username": ""
          }
         */
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("user_session");
        $builder->where("user_id", "=", $param["id"]);
        $builder->where("user_type", "=", $param["type"]);
        $get = $builder->get();
        $data = ["is_active" => false];
        $builder2 = DB::table("session_view");
        $builder2->where("user_id", "=", $param["id"]);
        $builder2->where("user_type", "=", $param["type"]);
        $get2 = $builder2->get(["username"]);
        $update = $builder->update($data);
        if ($update) {
            //nambah insert ke tabel log akses
            $datalog = ['username' => $param["username"], 'user_activity_id' => 152, 'detail' => 'melakukan force logoff terhadap ' . $get2[0]->username, 'tanggal' => $this->date_now];
            $ctrl = new UserLogController();
            $savelog = $ctrl->insertLogUser($datalog);
            $this->resource = $savelog;
            $this->sendResponse();
        } else {
            $message = "failed to insert user log activity";
            $result = array('affected' => false, 'message' => $message);
            $this->resource = $result;
            $this->sendResponse();
        }
    }

}
