<?php

namespace Controller;

use \Controller\BaseController as BaseController;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Capsule\Manager as DB;

require('../public/app/config.php');

class ApiRoutesController extends BaseController {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$builder = DB::table('api_routes');
		$data = $builder->orderBy('id', 'desc')->get();

		for($i = 0; $i < count($data); $i++) {
			$paths = split(',', $data[$i]->path);
			$methods = split(',', $data[$i]->method);
			$functions = split(',', $data[$i]->functions);
			unset($data[$i]->path);
			unset($data[$i]->method);
			unset($data[$i]->functions);

			$routes = [];
			for ($j = 0; $j < count($paths); $j++) { 
				array_push($routes,
				[
					'path' => $paths[$j],
					'route' => $data[$i]->controller . ":" . $functions[$j] . "@" . $methods[$j]  
				]);
			}
			$data[$i]->routes = $routes;    
		}
		

		$this->resource = array(
			'status' => 200,
			'data' => $data
			);
		$this->sendResponse();
	}

	public function listByController() {
		$param = json_decode($this->request()->getBody(), true);
		$controller = $param['controller'];
		$builder = DB::table('api_routes');
		$data = $builder->where(['controller' => $controller])->get();
		$this->resource = array(
			'status' => 200,
			'data' => $data
			);
		$this->sendResponse();
	}

	public function pathExist() {
		$param = json_decode($this->request()->getBody(), true);
		$path = $param['path'];
		$builder = DB::table('api_routes');
		$data = $builder->where('path', 'like', "%{$path},%")->count();

		$this->resource = array(
			'status' => 200,
			'data' => $data > 0
			);
		$this->sendResponse();
	}

	public function create() {
		$data = json_decode($this->request()->getBody(), true);
		$this->resource = array(
			'status' => 200,
			'data' => DB::table("api_routes")->insert($data)
			);
		$this->sendResponse();	
	}

	public function put() {
		$param = json_decode($this->request()->getBody(), true);
		$id = $this->parseParameters(['id'], $param);
		$data = $this->parseParameters(['path', 'method', 'functions'], $param);
		$this->resource = array(
			'status' => 200,
			'data' => DB::table("api_routes")->where(['id' => $id])->update($data)
			);
		$this->sendResponse();	
	}



}
