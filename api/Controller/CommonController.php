<?php

namespace Controller;

use \Controller\BaseController as BaseController;
use Controller\Admin\EmailConfController as EmailConfController;
use Carbon\Carbon as Carbon;
use Utils\QueryBuilder as QB;
use Utils\Mailer as Mailer;
use Utils\Logger as Logger;
use Illuminate\Database\Capsule\Manager as DB;

require('../public/app/config.php');

class CommonController extends BaseController {

    private $uploadConfig;
    private $target;
    private $overwrite;
    private $path_alias;

    public function __construct() {
        parent::__construct();
        $this->uploadConfig = $this->config["upload_config"];
        $this->target = $this->uploadConfig["target"];
        $this->overwrite = $this->uploadConfig["overwrite"];
        $this->path_alias = $this->uploadConfig["path_alias"];
    }

    public function limitfile() {
        /**         *
         * address: rekanan.configuration.limitfile, itp.configuration.limitfile, itp.rekanan.configuration.limitfile
         * path: limitfile
         * method :POST
         * param:{
          "id_page_config": 8,
          "layer":"rekanan"
          }
         * * */
        $param = json_decode($this->request()->getBody(), true);
        $page_layer = '';
        if ($param["layer"] == 'rekanan') {
            $page_layer = 'RE';
        } else {
            $page_layer = 'BE';
        }
        $buider = DB::table('config_uploadfile_view');
        $buider->where("id_page_config", "=", $param['id_page_config']);
        $buider->where("page_layer", "=", $page_layer);
        $this->resource = array(
            'status' => 200,
            'data' => $buider->get(['page_layer', 'page_name', 'limit_size', 'file_type_name'])
        );
        $this->sendResponse();
    }

    /*     * *
     *
     * path: email/send
     * method :POST
     * param:{
      "subject": "Mailer Test Email ",
      "content": "<b>Mailer Test</b> Email Content",
      "recipients": [{
      "email": "rohmad.tc09@gmail.com",
      "name": "Rohmad Raharjo"
      },
      {
      "email": "rohmad.raharjo@yahoo.com",
      "name": "Rohmad Raharjo"
      }],
      "attachments": [
      "uploads/update-ticket.png",
      "uploads/notif-vendor-1.png"

      ]
      }
     * * */

    public function sendEmail() {
        $param = json_decode($this->request()->getBody(), true);
        $subject = $param['subject'];
        $body = $param["content"];
        $recipients = $param["recipients"];
        $attachments = $param["attachments"];
        
        $mailer = new Mailer();


        foreach ($recipients as $rcpt) {
            $mailer->addRecipient($rcpt["email"], $rcpt["name"]);
        }

        foreach ($attachments as $item) {
            $mailer->addAttachment($this->target . $item);
        }

        if(array_key_exists('cc',$param)){
            foreach ($param['cc'] as $emailCC) {
                $mailer->addCC( $emailCC["email"], $emailCC["name"] );
            }
        }
        $result = $mailer->sendEmail($subject, $body);


        if ($result) {
            $this->resource = array(
                'status' => 200,
                'data' => "Email sent!"
            );
        } else {
            $this->resource = array(
                'status' => 500,
                'data' => $result
            );
        }

        $this->sendResponse();
        /**
         * Response success: {"status":200,"result":{"size":1,"data":"Email sent!"}}
         * Response failed : {"status":500,"result":{"size":1,"data":"Error Message"}}
         * */
    }

    /**     *
     * address: -
     * path: reference/listbytype
     * method :POST
     * param:{
      "type": $0
      }
     * * */
    public function getReferenceByType() {
        $param = json_decode($this->request()->getBody(), true);
        $buider = DB::table('mst_reference');
        $buider->where("ref_type", $param['type']);
        $this->resource = array(
            'status' => 200,
            'data' => $buider->get()
        );
        $this->sendResponse();
    }

    /**     *
     * address: -
     * path: reference/listbycode
     * method :POST
     * param:{
      "code": $0
      }
     * * */
    public function getReferenceByCode() {
        $param = json_decode($this->request()->getBody(), true);
        $buider = DB::table('mst_reference');
        $buider->where("ref_group_code", $param['code']);
        $this->resource = array(
            'status' => 200,
            'data' => $buider->get()
        );
        $this->sendResponse();
    }

    /**     *
     * address: -
     * path: logging
     * method :POST
     * param:{
      "message": "We're run out of FOOs!",
      "source": "app.js"
      }
     * * */
    public function writeLog() {

        $param = json_decode($this->request()->getBody(), true);
        $logger = new Logger();

        $this->resource = array(
            'status' => 200,
            'data' => $logger->log($param["message"], $param["source"])
        );
        $this->sendResponse();
    }

    public function jsLog(){
        $param = json_decode($this->request()->getBody(), true);
        $this->jsErrorReporting($param);
    }
    
}
