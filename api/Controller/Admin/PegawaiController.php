<?php

namespace Controller\Admin;

use Model\PegawaiView as PegawaiView;
use Model\Pegawai as Pegawai;
use \Controller\BaseController as BaseController;
use Carbon\Carbon as Carbon;
use Utils\QueryBuilder as QB;
use Illuminate\Database\Capsule\Manager as DB;
use Utils\Mailer as Mailer;

require('../public/app/config.php');

class PegawaiController extends BaseController {

    public function __construct() {
        parent::__construct();
    }

    //itp.pegawai.selectReset
    //pegawai/find
    //parameter : offset,limit
    public function lists_reset() { //
        $this->entity = new PegawaiView();
        $param = json_decode($this->request()->getBody(), true);
        $offset = $param["offset"];
        $limit = $param["limit"];
        $criteria = array(["column" => "flag_active", "operator" => "=", "value" => "true"],
            ["column" => "minta_reset", "operator" => "=", "value" => "true"]);
        $builder = DB::table("pegawai_view");
        $builder = QB::filter($builder, $criteria);
        $builder->orderBy("tgl_minta", "DESC")->skip($offset)->take($limit); //jika kolom tertentu, disebutkan didalam get
        $query = $builder->get();
        $this->resource = array(
            'status' => 200,
            'data' => $query
        );
        $this->sendResponse();
    }

    //itp.akunku.select
    //pegawai/akunku
    //parameter: username
    public function akunSelect() {
        $this->entity = new PegawaiView();
        $param = json_decode($this->request()->getBody(), true);
        $arr = $param["param"];
        $criteria = array(["column" => "username", "operator" => "=", "value" => $arr[0]]);
        $this->resource = array(
            'status' => 200,
            'data' => $this->select($criteria)
        );
        $this->sendResponse();
    }

    public function selectAnggota() {
        $param = json_decode($this->request()->getBody(), TRUE);
        $par = $param["keyword"];
        $builder = DB::table("mst_user")->join("nu_card","nu_card.nu_card_nik","=","mst_user.no_ktp")->leftjoin("mst_cabang","mst_cabang.mst_cabang_id","=","nu_card.mst_cabang_id")
                        ->where(function ($query) use ($par) {
                            // $query->orWhere('mst_lembaga_nama_lembaga', 'like', $par);
                            })
                        ->skip($param["offset"])->take($param["limit"])->orderBy("mst_user.id_user");
        $buildercount = DB::table("mst_user")->join("nu_card","nu_card.nu_card_nik","=","mst_user.no_ktp")->leftjoin("mst_cabang","mst_cabang.mst_cabang_id","=","nu_card.mst_cabang_id")
                        ->where(function ($query) use ($par) {
                            // $query->orWhere('mst_lembaga_nama_lembaga', 'like', $par);
                            });
        $this->resource = array(
            'status' => 200,
            'data' => ["data" => $builder->get(), "count" => $buildercount->count()]
        );
        $this->sendResponse();
        
    }

    public function updateAnggota() {
        $param = json_decode($this->request()->getBody(), TRUE);
        // print_r($param["data"]);
        if($param['flag_active'] == false){
            $data = [
                'flag_active' => true
            ];
        }else{
            $data = [
                'flag_active' => false
            ];
        }
        $build = DB::table("mst_user")->where("id_user","=",$param["id_user"])->update($data);
        
        $this->resource = array(
            'status' => 200,
            'data' => "Berhasil mengubah"
        );
        $this->sendResponse();
        
    }

    //itp.pegawai.countReset
    //pegawai/hitungreset
    public function countReset() {
        $this->entity = new PegawaiView();
        $param = json_decode($this->request()->getBody(), true);
        $criteria = array(["column" => "flag_active", "operator" => "=", "value" => "true"],
            ["column" => "minta_reset", "operator" => "=", "value" => "true"]);
        $this->resource = array(
            'status' => 200,
            'data' => $this->size($criteria)
        );
        $this->sendResponse();
    }

//    public function editAkun() {
//        $this->entity = new Pegawai();
//        $param = json_decode($this->request()->getBody(), true);
//        $arr_data = $param['param'];
//
//        $where = array(["column" => "flag_active", "operator" => "=", "value" => $arr_data[5]]);
//        $data = array(["column" => "nik", ""]);
//    }

    //itp.pegawai.selectSearch
    //pegawai/pegawaisearch
    /* parameter:
     * offset
     * limit
     * nama_pegawai
     */
    public function pegawaiSearch() {
        $this->entity = new PegawaiView();
        $param = json_decode($this->request()->getBody(), true);
        $offset = $param["offset"];
        $limit = $param["limit"];
        $builder = DB::table("pegawai_view")->where("role_id","!=",11);
        $builder->whereRaw("lower(nama) like ? AND flag_active=true", [$param["nama_pegawai"]]);
        $count = $builder->count();
        $get = $builder->orderBy("nama", "ASC")->skip($offset)->take($limit)->get(); //jika kolom tertentu, disebutkan didalam get
        //$builder->whereRaw("flag_active=1");
        $this->resource = array(
            'status' => 200,
            'data' => array(
                'count' => $count,
                'pegawai' => $get
            )
        );
        $this->sendResponse();
    }

    public function pegawaiPengadaan() {
        $param = json_decode($this->request()->getBody(), true);
        
        $builder = DB::table("pegawai");
        //$builder->whereRaw("flag_active=1");
        $builder->orderBy("nama", "ASC"); //jika kolom tertentu, disebutkan didalam get
        $query = $builder->get();
        $this->resource = array(
            'status' => 200,
            'data' => $query
        );
        $this->sendResponse();
    }

    //itp.pegawai.countSearch
    //pegawai/pegawaicountsearch
    /* parameter:
     * nama_pegawai
     */
    public function pegawaiCountSearch() {
        $this->entity = new PegawaiView();
        $param = json_decode($this->request()->getBody(), true);
        $offset = $param["offset"];
        $limit = $param["limit"];
        $builder = DB::table("pegawai_view")->where("role_id","!=",11);
        $builder->whereRaw("lower(nama) like ? AND flag_active=true", [$param["nama_pegawai"]]);
        $query = $builder->count();
        $this->resource = array(
            'status' => 200,
            'data' => $query
        );
        $this->sendResponse();
    }

    //itp.pegawai.selectSearchPanitia
    //pegawai/selectsearchpanitia
    /* parameter :
     * nama_pegawai
     */
    public function selectSearchPanitia() {
        $this->entity = new PegawaiView();
        $param = json_decode($this->request()->getBody(), true);
        $offset = $param['offset']; 
        $limit = $param['limit'];
        $paketLelangId = $param['paket_lelang_id'];
        $nama_pegawai = $param["nama_pegawai"];
        $builder = DB::select("select pe.*, ro.authority, de.departemen_nama, pg.nama_pegawai AS nama_atasan,
        pg.nik AS nik_atasan from pegawai pe LEFT JOIN roles ro ON pe.role_id = ro.role_id
        LEFT JOIN departemen de ON pe.departemen = de.departemen_id
        LEFT JOIN pegawai pg ON pe.atasan_id = pg.pegawai_id 
        where pe.pegawai_id not in (select pegawai.pegawai_id from pegawai
        join panitia on panitia.pegawai_id = pegawai.pegawai_id and panitia.flag_active != 0 and panitia.paket_lelang_id = ".$paketLelangId." 
        ORDER BY pegawai.pegawai_id) and pe.pegawai_id not in (select panitia.employee_replaced as pegawai_id from panitia 
        where panitia.paket_lelang_id = ".$paketLelangId." and panitia.employee_replaced is not null) and lower(pe.nama_pegawai) like
        '%".$nama_pegawai."%'
        ORDER BY pe.pegawai_id LIMIT ".$limit." OFFSET ".$offset);
        // $builder->skip($offset)->take($limit);
       
        $this->resource = array(
            'status' => 200,
            'data' => $builder
        );
        $this->sendResponse();
    }

    public function selectSearchPanitiaTemplate(){
        $param = json_decode($this->request()->getBody(), true);

        $offset = $param['offset']; 
        $limit = $param['limit'];
        // $paketLelangId = $param['paket_lelang_id'];
        $nama_pegawai = $param["nama_pegawai"];

        $builder = DB::table("pegawai_view");
        $builder->whereRaw("lower(nama_pegawai) like ? OR lower(departemen_nama) like ? OR lower(nik) like ? OR lower(jabatan) like ? AND flag_active=true", [$param["nama_pegawai"], $param["nama_pegawai"], $param["nama_pegawai"], $param["nama_pegawai"]]);
        $builder->skip($offset)->take($limit);
        $query = $builder->get();

        $this->resource = array(
            'status' => 200,
            'data' => $query
        );
        $this->sendResponse();

    }

    //itp.pegawai.countSearchPanitia 
    //pegawai/countsearchpanitia
    //parameter : nama_pegawai
    public function countSearchPanitia() {
        $this->entity = new PegawaiView();
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("pegawai_view");
        $builder->whereRaw("lower(nama_pegawai) like ?  AND flag_active=true", [$param["nama_pegawai"]]);
        $query = $builder->count();
        $this->resource = array(
            'status' => 200,
            'data' => $query
        );
        $this->sendResponse();
    }

    //itp.pegawai.insert
    //pegawai/insertpegawai
    public function insertPegawai() {
        $this->entity = new Pegawai();
        $param = json_decode($this->request()->getBody(), true);
        if(array_key_exists('token',$param)){
            unset($param['token']);
        }
        $param["created_date"] = Carbon::now();
        $param['created_by'] = $this->user_id;
        $builder = DB::table("pegawai");
        $builder->whereRaw("upper(username) = UPPER(?)", [$param["username"]]);
        $builder->orWhereRaw("nik = (?)", [$param["nik"]]);
        $query = $builder->count();
        $data = [];

        if ($query > 0) {
            $this->resource = array(
                'status' => 300,
                'data' => "duplicate"
            );
            $this->sendResponse();
        } else {
            $datas = [
                'nik' => $param['nik'],
                'nama_pegawai' => $param['nama_pegawai'],
                'email' => $param['email'],
                'telepon' => $param['telepon'],
                'username' => $param['username'],
                'jabatan' => 'Staf Admin',
                'flag_active' => true,
                'role_id' => $param['role_id'],
                'created_by' => $param['created_by']
            ];

            $simpan = DB::table('pegawai')->insert($datas);

            $dataUser = [
                'username' => $param['nik'],
                'password' => $param['password'],
                'flag_active' => true,
                'user_type' => 1
            ];
            $simpan = DB::table('mst_user')->insert($dataUser);

            if ($simpan) {
                $datalog = ['username' => $param["username"], 'user_activity_id' => 17, 'detail' => 'Menambah Pegawai', 'tanggal' => $this->date_now];
                $ctrl = new UserLogController();
                $savelog = $ctrl->insertLogUser($datalog);
                $this->resource = $savelog;
                $this->sendResponse();
            } else {
                $message = "failed to insert user log activity";
                $result = array('affected' => false, 'message' => $message);
                $this->resource = $result;
                $this->sendResponse();
            }
        }
        
    }

    //itp.pegawai.cekusername
    //pegawai/cekusername
    //parameter: username
    public function cekUsername() {
        $this->entity = new Pegawai();
        $param = json_decode($this->request()->getBody(), true);
        $arr = $param["username"];
        $criteria = array(["column" => "username", "operator" => "=", "value" => $arr]);
        $count = $this->size($criteria);
        $data = [];
        if ($count > 0) {
            $data = "duplicate";
        } else {
            $data = "ok";
        }
        $this->resource = array(
            'status' => 200,
            'data' => $data
        );

        $this->sendResponse();
    }

    /*  address: itp.pegawai.edit
        path : pegawai/editpegawai
     *  param : {where: {pegawai_id: ?} , data : {nama_pegawai: ?, nik : ? dll}}
    */
    public function editPegawai(){
        $param = json_decode( $this->request()->getBody(), true );
        $builder = DB::table("pegawai");
        $builder->where("nik","=", $param['data']["nik"]);
        $builder->where("nik","<>", $param["old_nik"]);
        $query = $builder->count();
        if ($query > 0) {
            $this->resource = array(
                'status' => 300,
                'data' => "duplicate"
            );
            $this->sendResponse();
        }
        else{
            $datas = [
                'nik' => $param['data']['nik'],
                'nama_pegawai' => $param['data']['nama_pegawai'],
                'email' => $param['data']['email'],
                'telepon' => $param['data']['telepon'],
                'username' => $param['data']['username'],
                'jabatan' => 'Staf Admin',
                'flag_active' => true,
                'role_id' => $param['data']['role_id']
            ];
            // print_r($param['data']['nik']);
            $simpan = DB::table('pegawai')->where("nik","=", $param['data']['nik'])->update($datas);

            $dataUser = [
                'username' => $param['data']['nik'],
                'password' => $param['data']['password']
            ];
            $simpan = DB::table('mst_user')->where("username","=",$param['data']['nik'])->update($dataUser);
   
            if( $simpan ){
                $datalog = ['username' => $param['username'], 'user_activity_id' => 18, 'detail' => 'Mengubah Pegawai', 'tanggal' => $this->date_now];
                
                $ctrl = new UserLogController();                 
                $savelog = $ctrl->insertLogUser($datalog);            
                $this->resource = $savelog;
            } else {
                $message = "failed to insert user log activity";
                $result = array('affected' => false, 'message' => $message);
                $this->resource = $result;
            }
        
            $this->sendResponse();
        }
    }

    //itp.pegawai.delete
    //pegawai/deletepegawai
    /*
      {
      "where":{
      "pegawai_id":42
      },"data":{
      "flag_active":0
      }

      } */
    public function deletePegawai() {
        $this->entity = new Pegawai();
        $param = json_decode($this->request()->getBody(), true);
        
        $build = DB::table("mst_user")->where("username","=",$param['UserID']['nik'])->delete();
        $build1 = DB::table("pegawai")->where("pegawai_id","=",$param['UserID']['id'])->delete();
        
        
        $this->resource = array(
            'status' => 200,
            'data' => 'Data Berhasil Di Hapus'
        );

        $this->sendResponse();
    }

    //itp.pegawai.resetPassword
    //pegawai/resetpwd
    public function resetPassword() {
        $this->entity = new Pegawai();
        $param = json_decode($this->request()->getBody(), true);
        $pid = $param['pegawai_id'];
        $where = array('pegawai_id' => $pid);
        $pwd = $param['password'];
        $data = array('password' => $pwd);
        $this->tableName = 'pegawai';
        $simpan = $this->updateRow($where, $data);
        if ($simpan) {
            $datalog = ['username' => $param['username'], 'user_activity_id' => 107, 'detail' => 'Mereset Password Pegawai', 'tanggal' => $this->date_now];
            $ctrl = new UserLogController();
            $savelog = $ctrl->insertLogUser($datalog);
            $this->resource = $savelog;
            $this->sendResponse();
        } else {
            $message = "failed to insert user log activity";
            $result = array('affected' => false, 'message' => $message);
            $this->resource = $result;
            $this->sendResponse();
        }
    }

    //itp.pegawai.getEmail
    //pegawai/getemail
    //parameter : username
    public function getEmailPegawai() {
        $this->entity = new PegawaiView();
        $param = json_decode($this->request()->getBody(), true);
        $email = ['email'];
        $builder = DB::table("pegawai_view");
        $builder->whereRaw("UPPER(username) = UPPER(?)", [$param["username"]]);
        $query = $builder->get($email);
        $this->resource = array(
            'status' => 200,
            'data' => $query
        );
        $this->sendResponse();
    }

    //itp.akunku.editdata
    //pegawai/editdataakun
    public function editDataAkun() {
        $this->entity = new Pegawai();
        $param = json_decode($this->request()->getBody(), true);
//        $param['data']['updated_date'] = Carbon::now();
//        $param['data']['updated_by'] = $this->session_user;
        $uname = $param['username'];
        $where = array('username' => $uname);
        $data = array('nik' => $param["nik"], 'nama_pegawai' => $param["nama_pegawai"], 'email' => $param["email"], 'telepon' => $param["telepon"], 'updated_date' => Carbon::now(), 'updated_by' => $this->user_id);
        $this->tableName = 'pegawai';
        $simpan = $this->updateRow($where, $data);
        if ($simpan) {

            $datalog = ['username' => $param['userlogin'], 'user_activity_id' => 122, 'detail' => 'Mengubah Data Akun User Login', 'tanggal' => $this->date_now];
            $ctrl = new UserLogController();
            $savelog = $ctrl->insertLogUser($datalog);
            $this->resource = $savelog;
            $this->sendResponse();
        } else {
            $message = "failed to insert user log activity";
            $result = array('affected' => false, 'message' => $message);
            $this->resource = $result;
            $this->sendResponse();
        }
    }

    //itp.akunku.editpassword
    //pegawai/editpwdakun
    public function editPasswordAkun() {
        $this->entity = new Pegawai();
        $param = json_decode($this->request()->getBody(), true);
        $uname = $param['username'];
        $where = array('username' => $uname);
        $param['data']['updated_date'] = Carbon::now();
        $param['data']['updated_by'] = $this->user_id;
        $data = array('password' => $param['password']);
        $this->tableName = 'pegawai';
        $simpan = $this->updateRow($where, $data);
        if ($simpan) {
            $datalog = ['username' => $this->session_user, 'user_activity_id' => 164, 'detail' => 'Mengubah password akun', 'tanggal' => $this->date_now];
            $ctrl = new UserLogController();
            $savelog = $ctrl->insertLogUser($datalog);
            $this->resource = $savelog;
            $this->sendResponse();
        } else {
            $message = "failed to insert user log activity";
            $result = array('affected' => false, 'message' => $message);
            $this->resource = $result;
            $this->sendResponse();
        }
    }
    
    //itp.pegawai.count
    //pegawai/count
    //GET
    public function count(){
        $param = json_decode($this->request()->getBody(),true);
        $builder = DB::table("pegawai_view");
        $builder->where("flag_active",'=',TRUE);
        $query = $builder->count();
        $this->resource = array(
            'status' => 200,
            'data' => $query
        );
        $this->sendResponse();
    }

    //itp.pegawai.select
    //
  /*  "public function akunSelect()" jadi error kalo ada "public function select()"
   
    public function select(){
        $param = json_decode($this->request()->getBody(),true);
        $parr = $param['param'];
        $builder = DB::table("pegawai_view");
        $builder->where("flag_active",'=',TRUE);
        $builder->orderBy($parr[0]);
        $builder->skip($parr[1]);
        $builder->take($parr[2]);
        $query = $builder->get();
        $this->resource = array(
            'status' => 200,
            'data' => $query
        );
        $this->sendResponse();
    }
    */

    //itp.pegawai.selectId
    public function selectId(){
        $param = json_decode($this->request()->getBody(),true);
        $pegawai_id = $param['pegawai_id'];
        $builder = DB::table("pegawai_view");
        $builder->where("pegawai_id",'=',$pegawai_id);
        $query = $builder->get();
        $this->resource = array(
            'status' => 200,
            'data' => $query
        );
        $this->sendResponse();
    }
    /*GET*/
    // public function selectjabatan(){
    //     $builder = DB::table("pegawai");
    //     $builder->distinct();
    //     $builder->whereRaw("jabatan is not null AND jabatan <>''");
    //     $query = $builder->get(["jabatan"]);
    //     $this->resource = array(
    //         'status' => 200,
    //         'data' => $query
    //     );
    //     $this->sendResponse();
    // }
    public function selectjabatan(){
        $builder = DB::table("jabatan_panitia");
        $builder->distinct();
        $builder->whereRaw("jabatan_nama is not null AND jenis_role = 1 AND jabatan_nama <>'' AND flag_active = 1");
        $query = $builder->get(["jabatan_nama"]);
        $this->resource = array(
            'status' => 200,
            'data' => $query
        );
        $this->sendResponse();
    }

    public function selectPicPengadaan(){
        $builder = DB::table("pegawai as A");
        $builder->leftJoin('roles as B','A.role_id','=','B.role_id');
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    public function selectUser(){
        $builder = DB::table("mst_user")->join("user_pajak","user_pajak.no_identitas_user_pajak","=","mst_user.username")->where("mst_user.user_type","=",2);

        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    public function ubahStatus(){
        $param = json_decode($this->request()->getBody(),true);

        if($param['status'] == 1){
            $data = ['flag_active' => true];
        }else{
            $data = ['flag_active' => false];
        }
        // print_r($param['UserID']);
        $builder = DB::table("mst_user")->where("id_user","=", $param['UserID'])->update($data);
        
        $this->resource = array(
            'status' => 200,
            'data' => 'Berhasil di ubah'
        );
        $this->sendResponse();
    }
}

