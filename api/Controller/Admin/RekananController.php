<?php

namespace Controller\Admin;

use Model\Rekanan as Rekanan;
use Model\RekananTACV as RekananTACV;
use Model\DokumenExpiredView as DokumenExpiredView;
use Model\RekananIjinusahaBidusahaView as RekananIjinusahaBidusahaView;
use Model\RekananVerifikasi as RekananVerifikasi;
use Model\RekananDokumenAkta as RekananDokumenAkta;
use \Controller\BaseController as BaseController;
use Carbon\Carbon as Carbon;
use Utils\QueryBuilder as QB;
use Utils\Mailer as Mailer;
use Illuminate\Database\Capsule\Manager as DB;
use Controller\Admin\EmailConfController as EmailConfController;
use Controller\Admin\UserLogController as UserLogController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border;


require('../public/app/config.php');

class rekananController extends BaseController {

    public function __construct() {
        parent::__construct();
    }

    //Address   : itp.rekanan.getCountQueryByCompany
    //rekanan/countbycompany
    //param:
    //-status
    //-company
    //-waktumulai1
    //-waktumulai2
    public function countByCompany() {
        $this->entity = new Rekanan();
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("rekanan");
        $status = $param["status"];
        $company = $param["company"];
        switch ($status) {
            case 1:
                $builder->where('is_activated', '=', null);
                $builder->orWhere('is_activated', '=', false);
                $builder->where(function($query) {
                    $param = json_decode($this->request()->getBody(), true);
                    $query->where('created_date', '>=', $param["waktu_mulai1"])
                            ->where('created_date', '<=', $param["waktu_mulai2"]);
                });
                break;
            case 2:
                $builder->where('is_activated', '=', true);
                $builder->where('is_verified', '=', null);
                $builder->where('trigger_verified', '=', null);
                $builder->orWhere(function($query) {
                    $query->where('is_activated', '=', true)
                            ->where('is_verified', '=', false)
                            ->where('trigger_verified', '=', null);
                });
                $builder->orWhere(function($query) {
                    $query->where('is_activated', '=', true)
                            ->where('is_verified', '=', false)
                            ->where('trigger_verified', '=', false);
                });
                $builder->where('activated_date', '>=', $param["waktu_mulai1"]);
                $builder->where('activated_date', '<=', $param["waktu_mulai2"]);
                break;
            case 3:
                $builder->where('is_activated', '=', true);
                $builder->where('trigger_verified', '=', true);
                $builder->where('verified_send_date', '>=', $param["waktu_mulai1"]);
                $builder->where('verified_send_date', '<=', $param["waktu_mulai2"]);
                break;
            case 4:
                $builder->where('is_verified', '=', true);
                $builder->where('verified_date', '>=', $param["waktu_mulai1"]);
                $builder->where('verified_date', '<=', $param["waktu_mulai2"]);
                break;
            default:
                $builder->whereRaw('lower(nama_perusahaan) LIKE lower (?)', [$company]);
                break;
        }
        if ($company != "") {
            $builder->whereRaw('lower(nama_perusahaan) LIKE lower (?)', [$company]);
        }
        $this->resource = array(
            'status' => 200,
            'data' => $builder->count()
        );
        $this->sendResponse();
    }

    /* Address : itp.rekanan.getDataQueryByCompany
     * Path : rekanan/getdatabycompany
     * Parameter:
     * {
     *  status: 1,
     *  company: "%PT%",
     *  waktu_mulai1:"2015-09-25",
     *  maktu_mulai2:"2015-01-31"
     * }
     */
    public function getDataByCompany() {
        $this->entity = new Rekanan();
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("rekanan");
        $status = $param["stat"];
        $company = $param["company"];
        switch ($status) {
            case 1:
                $builder->whereNull('is_activated');
                $builder->orWhere('is_activated', '=', false);
                $builder->where(function($query) use ($param) {
                    $query->where('created_date', '>', $param["waktu_mulai1"])
                            ->where('created_date', '<', $param["waktu_mulai2"]);
                });
                break;
            case 2:
                $builder->where('is_activated', '=', true);
                $builder->whereNull('is_verified');
                $builder->whereNull('trigger_verified');
                $builder->orWhere(function($query) {
                    $query->where('is_activated', '=', true)
                            ->where('is_verified', '=', false)
                            ->whereNull('trigger_verified');
                });
                $builder->orWhere(function($query) {
                    $query->where('is_activated', '=', true)
                            ->where('is_verified', '=', false)
                            ->where('trigger_verified', '=', false);
                });
                $builder->where('activated_date', '>', $param["waktu_mulai1"]);
                $builder->where('activated_date', '<', $param["waktu_mulai2"]);
                break;
            case 3:
                $builder->where('is_activated', '=', true);
                $builder->where('trigger_verified', '=', true);
                $builder->where('verified_send_date', '>', $param["waktu_mulai1"]);
                $builder->where('verified_send_date', '<', $param["waktu_mulai2"]);
                break;
            case 4:
                $builder->where('is_verified', '=', true);
                $builder->where('verified_date', '>', $param["waktu_mulai1"]);
                $builder->where('verified_date', '<', $param["waktu_mulai2"]);
                break;
            default:
                $builder->whereRaw('lower(nama_perusahaan) LIKE lower (?)', [$company]);
                break;
        }
        if ($company != "") {
            $builder->whereRaw('lower(nama_perusahaan) LIKE lower (?) order by created_date', [$company])
                    ->skip($param["offset"])
                    ->take($param["limit"]);
            $q = $builder->get();
        } else {
            $builder->skip($param["offset"])
                    ->take($param["limit"]);
            $q = $builder->get();
        }

        $builder2 = DB::table("rekanan_ijinusaha");
        $builder2->whereIn('rekanan_id', function($query) use ($param) {
            $query->select('rekanan_id')
                    ->from('rekanan')
                    ->orderBy('created_date', 'desc')
                    ->skip(0)
                    ->take($param['limit']);
        }
        );
        $siup = $builder2->get(['rekanan_id', 'masa_berlaku']);
        $tgl = Carbon::now();
        $siupExpired = array();
        $j = 0;
        foreach ($siup as $s) {
            if ($s->masa_berlaku < $tgl) {
                $siupExpired[$j] = $s;
                $j++;
            } else if (is_null($s->masa_berlaku)) {
                exit;
            }
        }

        foreach ($q as $r) {
            foreach ($siupExpired as $se) {
                if ($r->rekanan_id === $se->rekanan_id) {
                    $r->keterangan = 'SIUP Kadaluarsa';
                }
            }
        }

        $this->resource = array(
            'status' => 200,
            'data' => $q
        );
        $this->sendResponse();
    }

    //address : itp.rekanan.deleteRekanan
    // path: rekanan/deleteRekanan
    public function deleteRekanan(){
        $this->entity = new Rekanan;

        DB::transaction( function(){ // auto-commit | https://laravel.com/docs/5.2/database#database-transactions
            $param = json_decode( $this->request()->getBody(), true );

            $data = $param['datarekanan'];
            unset( $data['rekanan_id'] );
            unset( $data['tolak_verifikasi'] );

            $msgGagal = 'Gagal kirim notifikasi Tolak Aktivasi Rekanan!';

            $simpan = DB::table("rekanan_ditolak")->insert($data);
            if( $simpan ){
                $hapus = DB::table("rekanan")->where( "rekanan_id", '=', $param['rekid'] )->delete();
                if( $hapus ){
                    $hapus2 = DB::table("rekanan_ijinusaha")->where( "rekanan_id", '=', $param['rekid'] )->delete();
                    if( $hapus2 ){
//                      $this->sendActivationMail( $param['rekid'], $param['keterangan'], 3, "tolak", $param["nmPrsh"] );
                        $this->sendActivationMail2( $param['email'], $param['emailPribadi'], $param["nmPrsh"], $param['keterangan'], 3, "tolak" );

                        $userLogController = new UserLogController(); // Menerima/Menolak Aktivasi Rekanan
                        $datalog = ['username' => $param["userlogin"], 'detail' => 'Menolak Aktivasi untuk vendor ' . $param["nmPrsh"], 'user_activity_id' => 179, 'tanggal' => Carbon::now()];
                        $userLogController->insertLogUser($datalog);

                        $this->sendResponse();
                        return;
                    } else {
                        DB::rollBack();
                        $this->resource = array( 'status' => 500, 'data' => $msgGagal );
                        $this->sendResponse();
                        return;
                    }
                } else {
                    DB::rollBack();
                    $this->resource = array( 'status' => 500, 'data' => $msgGagal );
                    $this->sendResponse();
                    return;
                }
            } else {
                DB::rollBack();
                $this->resource = array( 'status' => 500, 'data' => $msgGagal );
                $this->sendResponse();
                return;
            }
        }); // end DB::transaction
    } // end deleteRekanan

    public function nonaktifkanPenyedia()
    {
      $param = json_decode($this->request()->getBody(), true);
      DB::table('rekanan')->where('rekanan_id',$param['param'])->update(['is_activated' => 'f']);
      $userLogController = new UserLogController(); // Menerima/Menolak Aktivasi Rekanan
      $datalog = ['username' => $param['username'], 'detail' => 'Menonaktifkan Vendor ' . $param['param']['nama_perusahaan'], 'user_activity_id' => 195, 'tanggal' => Carbon::now()];
      $resource = $userLogController->insertLogUser($datalog);
      $this->resource = array(
          'status' => 200,
          'data' => 'success'
      );
      $this->sendResponse();
    }

    public function aktifkanPenyedia()
    {
      $param = json_decode($this->request()->getBody(), true);
      DB::table('rekanan')->where('rekanan_id',$param['param']['rekanan_id'])->update(['is_activated' => 't']);
      $userLogController = new UserLogController(); // Menerima/Menolak Aktivasi Rekanan
      $datalog = ['username' => $param['username'], 'detail' => 'Mengaktifkan Vendor ' . $param['param']['nama_perusahaan'], 'user_activity_id' => 196, 'tanggal' => Carbon::now()];
      $resource = $userLogController->insertLogUser($datalog);
      $this->resource = array(
          'status' => 200,
          'data' => 'success'
      );
      $this->sendResponse();
    }

    //address : rekanan/getContentEmailAktivasiDiterima
    public function getContentEmailAktivasiDiterima() {
        $param = json_decode($this->request()->getBody(), true);
        $nama_perusahaan = $param['nama_perusahaan'];
        $username = $param['username'];
        $ecc = new EmailConfController();
        $var1 = ['key' => 'prekanan', 'value' => $nama_perusahaan];
        $var2 = ['key' => 'username', 'value' => $username];
        $id = 4; //id_konten_email
        $variables = [$var1, $var2];
        $eccResult = $ecc->getMailContent($id, $variables);
        //$mailBody = $eccResult["mailBody"];
        $mailSubject = $eccResult["mailSubject"];
        $mailBody = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>" +
                "<html xmlns='http://www.w3.org/1999/xhtml'>" .
                "<head>" .
                "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />" .
                "<title></title>" .
                "</head>" .
                "<body>" .
                "<p>" . $eccResult["mailBody"] . "</p>" .
                "</body>" .
                "</html>";
        $result = [
            'mailBody' => $mailBody,
            'mailSubject' => $mailSubject
        ];
        $this->resource = array(
            'status' => 200,
            'data' => $result
        );
        $this->sendResponse();
        //print_r($mailBody);
        //return $result;
    }

    public function searchRekananNon() {
        $param = json_decode($this->request()->getBody(), true);
        try {
            $param = json_decode($this->request()->getBody(), true);
            $builder = DB::table("rekanan")
                            ->select(DB::raw("rekanan.*,pegawai.nama_pegawai, case when is_activated ='t' and (is_verified = 'f' or is_verified is null) and (tolak_verifikasi = 'f' or tolak_verifikasi is null ) and trigger_verified = 't' then 'atas' else 'bawah' end as status_rek,kota_nama,capitol"))
                            ->from('rekanan');

            $builder->leftJoin('badan_usaha','badan_usaha.badan_usaha_id','=','rekanan.jenis_perusahaan');
            $builder->join('mst_country','mst_country.id_country','=','rekanan.country_id');
            $builder->leftJoin('mst_kota','mst_kota.kota_id','=','rekanan.kota_id');
            $builder->leftJoin('pegawai','pegawai.pegawai_id','=','rekanan.updated_by_admin');

            //untuk Penyedia Verifikasi By Jovovid
            if(array_key_exists('ini_penyedia', $param)){
                $builder->where('is_verified','=',true);
            }

            $status = intval($param["status"]);
            $company = $param["company"];
            $orderColumn = "rekanan.created_date";
            $orderType = "desc";
            switch ($status) {
                case 0:
                    $builder->where('rekanan.created_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('rekanan.created_date', '<=', $param["waktu_mulai2"]);
                    // Penambahan kondisi waktu search untuk mencari nama penyedia kode username
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('rekanan.username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }

                    $builder->whereNotNull('rekanan.is_activated');
                    $orderColumn = "bisa_mengubah_data";
                    $orderType = "asc";
                    break;
                case 1:
                    $builder->where(function($query) {
                        $query->whereNull('rekanan.is_activated');
                        $query->orWhere('rekanan.is_activated', false);
                    });
                    $builder->where('is_registered', true);
                    $builder->where('rekanan.created_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('rekanan.created_date', '<=', $param["waktu_mulai2"]);
                    $builder->where('nama_perusahaan', 'ilike', $company);
                    break;
                case 2:
                    $builder->where(function($query) {
                        $query->where(function($query1) {
                            $query1->where('rekanan.is_activated', true);
                            $query1->whereNull('is_verified');
                            $query1->whereNull('trigger_verified');
                            $query1->whereNull('tolak_verifikasi');
                        });
                        $query->orWhere(function($query2) {
                            $query2->where('rekanan.is_activated', true);
                            $query2->where('is_verified', false);
                            $query2->whereNull('trigger_verified');
                        });
                        $query->orWhere(function($query3) {
                            $query3->where('rekanan.is_activated', true);
                            $query3->where('is_verified', false);
                            $query3->where('trigger_verified', false);
                            $query3->where('tolak_verifikasi', false);
                        });
                    });
                    $builder->where('activated_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('activated_date', '<=', $param["waktu_mulai2"]);
                    // Penambahan kondisi waktu search untuk mencari nama penyedia kode username

                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('rekanan.username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }
                    $orderColumn = "activated_date";
                    $orderType = "desc";
                    break;
                case 3:
                    $builder->where(function($query) {
                        $query->where(function($query1) {
                            $query1->where('rekanan.is_activated', true);
                            $query1->where(function($sql) {
                                $sql->where('is_verified', null);
                                $sql->orWhere('is_verified', '<>', true);
                            });
                            $query1->where('trigger_verified', true);
                        });
                        /*
                          $query->orWhere(function($query2) {
                          $query2->where('is_activated', '1');
                          $query2->where('is_verified', '0');
                          $query2->where('trigger_verified', '1');
                          }); */
                    });
                    $builder->where('verified_send_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('verified_send_date', '<=', $param["waktu_mulai2"]);

                    // Penambahan kondisi waktu search untuk mencari nama penyedia kode username
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('rekanan.username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }
                    $orderColumn = "verified_send_date";
                    $orderType = "desc";
                    break;
                case 4:
                    $builder->where('is_verified', true);
                    $builder->where('rekanan.is_activated', true);
                    $builder->where('verified_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('verified_date', '<=', $param["waktu_mulai2"]);

                    // Penambahan kondisi waktu search untuk mencari nama penyedia kode username
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('rekanan.username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }
                    $orderColumn = "verified_date";
                    $orderType = "desc";
                    break;
                case 5:
                    $orderColumn = "rekanan.created_date";
                    $orderType = "desc";
                    $builder->where('rekanan_id', $param['rekanan_id']);
                    break;
                case 6:
                    $builder->where(function($query) {
                        $query->where(function($query1) {
                            $query1->where('rekanan.is_activated', true);
                            $query1->where(function($sql) {
                                $sql->where('is_verified', false);
                            });
                            $query1->where('trigger_verified', false);
                            $query1->where('tolak_verifikasi', true);
                        });
                    });
                    $builder->where(function($query) use ($param) {
                        $query->where(function($query1) use ($param){
                            $query1->where('tolak_date', '>=', $param["waktu_mulai1"]);
                            $query1->where('tolak_date', '<=', $param["waktu_mulai2"]);
                        });
                        $query->orWhere(function($query1) {
                            $query1->whereNull('tolak_date');
                        });
                    });

                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('rekanan.username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }

                    // if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                    //     $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                    //     $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');


                    //      $var = "";
                    //         for ($i=0; $i < count($param['id_bu']) ; $i++) {
                    //             if($var == ""){
                    //                 $var = $param['id_bu'][$i]['id_bidang_usaha'];
                    //             }else{
                    //                 $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                    //             }
                    //         }
                    //        $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));

                    // }


                    $orderColumn = "tolak_date";
                    $orderType = "desc";
                    break;
                default:
                    break;
            }

            $size = $builder->where('is_activated','f')->count();

            $data = $builder->where('is_activated','f')->skip($param["offset"])->take($param["limit"])->orderBy('status_rek', 'asc')->get();
            $this->resource = array(
                'status' => 200,
                'data' => [
                    'count' => $size,
                    'data' => $data
                ]
            );
        } catch (\Exception $e) {
            $this->resource = array(
                'status' => 500,
                'data' => $e->getMessage()
            );
        }
        $this->sendResponse();
    }

    // rekanan/search
    public function searchRekanan() {
        $param = json_decode($this->request()->getBody(), true);
        try {
            $param = json_decode($this->request()->getBody(), true);
            $builder = DB::table("rekanan")
                            ->select(DB::raw("rekanan.*,pegawai.nama_pegawai, case when is_activated ='t' and (is_verified = 'f' or is_verified is null) and (tolak_verifikasi = 'f' or tolak_verifikasi is null ) and trigger_verified = 't' then 'atas' else 'bawah' end as status_rek,kota_nama,capitol"))
                            ->from('rekanan');

            $builder->leftJoin('badan_usaha','badan_usaha.badan_usaha_id','=','rekanan.jenis_perusahaan');
            $builder->join('mst_country','mst_country.id_country','=','rekanan.country_id');
            $builder->leftJoin('mst_kota','mst_kota.kota_id','=','rekanan.kota_id');
            $builder->leftJoin('pegawai','pegawai.pegawai_id','=','rekanan.updated_by_admin');

            //untuk Penyedia Verifikasi By Jovovid
            if(array_key_exists('ini_penyedia', $param)){
                $builder->where('is_verified','=',true);
            }

            $status = intval($param["status"]);
            $company = $param["company"];
            $orderColumn = "rekanan.created_date";
            $orderType = "desc";
            switch ($status) {
                case 0:
                    $builder->where('rekanan.created_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('rekanan.created_date', '<=', $param["waktu_mulai2"]);
                    // Penambahan kondisi waktu search untuk mencari nama penyedia kode username
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('rekanan.username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }

                    $builder->whereNotNull('rekanan.is_activated');
                    $orderColumn = "bisa_mengubah_data";
                    $orderType = "asc";
                    break;
                case 1:
                    $builder->where(function($query) {
                        $query->whereNull('rekanan.is_activated');
                        $query->orWhere('rekanan.is_activated', false);
                    });
                    $builder->where('is_registered', true);
                    $builder->where('rekanan.created_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('rekanan.created_date', '<=', $param["waktu_mulai2"]);
                    $builder->where('nama_perusahaan', 'ilike', $company);
                    break;
                case 2:
                    $builder->where(function($query) {
                        $query->where(function($query1) {
                            $query1->where('rekanan.is_activated', true);
                            $query1->whereNull('is_verified');
                            $query1->whereNull('trigger_verified');
                            $query1->whereNull('tolak_verifikasi');
                        });
                        $query->orWhere(function($query2) {
                            $query2->where('rekanan.is_activated', true);
                            $query2->where('is_verified', false);
                            $query2->whereNull('trigger_verified');
                        });
                        $query->orWhere(function($query3) {
                            $query3->where('rekanan.is_activated', true);
                            $query3->where('is_verified', false);
                            $query3->where('trigger_verified', false);
                            $query3->where('tolak_verifikasi', false);
                        });
                    });
                    $builder->where('activated_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('activated_date', '<=', $param["waktu_mulai2"]);
                    // Penambahan kondisi waktu search untuk mencari nama penyedia kode username

                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('rekanan.username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }
                    $orderColumn = "activated_date";
                    $orderType = "desc";
                    break;
                case 3:
                    $builder->where(function($query) {
                        $query->where(function($query1) {
                            $query1->where('rekanan.is_activated', true);
                            $query1->where(function($sql) {
                                $sql->where('is_verified', null);
                                $sql->orWhere('is_verified', '<>', true);
                            });
                            $query1->where('trigger_verified', true);
                        });
                        /*
                          $query->orWhere(function($query2) {
                          $query2->where('is_activated', '1');
                          $query2->where('is_verified', '0');
                          $query2->where('trigger_verified', '1');
                          }); */
                    });
                    $builder->where('verified_send_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('verified_send_date', '<=', $param["waktu_mulai2"]);

                    // Penambahan kondisi waktu search untuk mencari nama penyedia kode username
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('rekanan.username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }
                    $orderColumn = "verified_send_date";
                    $orderType = "desc";
                    break;
                case 4:
                    $builder->where('is_verified', true);
                    $builder->where('rekanan.is_activated', true);
                    $builder->where('verified_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('verified_date', '<=', $param["waktu_mulai2"]);

                    // Penambahan kondisi waktu search untuk mencari nama penyedia kode username
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('rekanan.username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }
                    $orderColumn = "verified_date";
                    $orderType = "desc";
                    break;
                case 5:
                    $orderColumn = "rekanan.created_date";
                    $orderType = "desc";
                    $builder->where('rekanan_id', $param['rekanan_id']);
                    break;
                case 6:
                    $builder->where(function($query) {
                        $query->where(function($query1) {
                            $query1->where('rekanan.is_activated', true);
                            $query1->where(function($sql) {
                                $sql->where('is_verified', false);
                            });
                            $query1->where('trigger_verified', false);
                            $query1->where('tolak_verifikasi', true);
                        });
                    });
                    $builder->where(function($query) use ($param) {
                        $query->where(function($query1) use ($param){
                            $query1->where('tolak_date', '>=', $param["waktu_mulai1"]);
                            $query1->where('tolak_date', '<=', $param["waktu_mulai2"]);
                        });
                        $query->orWhere(function($query1) {
                            $query1->whereNull('tolak_date');
                        });
                    });

                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('rekanan.username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }

                    // if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                    //     $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                    //     $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');


                    //      $var = "";
                    //         for ($i=0; $i < count($param['id_bu']) ; $i++) {
                    //             if($var == ""){
                    //                 $var = $param['id_bu'][$i]['id_bidang_usaha'];
                    //             }else{
                    //                 $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                    //             }
                    //         }
                    //        $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));

                    // }


                    $orderColumn = "tolak_date";
                    $orderType = "desc";
                    break;
                default:
                    break;
            }

            $size = $builder->count();

            $data = $builder->skip($param["offset"])->take($param["limit"])->orderBy('status_rek', 'asc')->get();
            $this->resource = array(
                'status' => 200,
                'data' => [
                    'count' => $size,
                    'data' => $data
                ]
            );
        } catch (\Exception $e) {
            $this->resource = array(
                'status' => 500,
                'data' => $e->getMessage()
            );
        }
        $this->sendResponse();
    }

    public function searchRekananJoinBuNon() {
        try {
            $param = json_decode($this->request()->getBody(), true);
            $status = intval($param["status"]);
            // $orderColumn = "rekanan.created_date";
            // $orderType = "desc";
            switch ($status) {
                case 0: $orderColumn = "bisa_mengubah_data";
                        $orderType = "asc";
                        break;
                case 1: $orderColumn = "rekanan.created_date";
                        $orderType = "desc";
                        break;
                case 2: $orderColumn = "activated_date";
                        $orderType = "desc";
                        break;
                case 3: $orderColumn = "verified_send_date";
                        $orderType = "desc";
                        break;
                case 4: $orderColumn = "verified_date";
                        $orderType = "desc";
                        break;
                case 5: $orderColumn = "rekanan.created_date";
                        $orderType = "desc";
                        break;
                case 6: $orderColumn = "verified_send_date";
                        $orderType = "desc";
                        break;
                //penambahan status baru untuk penyedia terverifikasi dari no 7 - 10 by avid

                case 7: $orderColumn = "verified_date";
                        $orderType = "desc";
                        break;
                case 8:
                        if(array_key_exists('ini_penyedia', $param)){
                            $orderColumn = "nama_perusahaan";
                            $orderType = "asc";

                        }else{
                            $orderColumn = "is_verified";
                            $orderType = "desc";
                        }
                        break;
                case 9:
                        if(array_key_exists('ini_penyedia', $param)){
                            $orderColumn = "kode_rekanan";
                            $orderType = "asc";

                        }else{
                             $orderColumn = "is_verified";
                            $orderType = "desc";
                        }
                        break;
                case 10:
                         if(array_key_exists('ini_penyedia', $param)){
                                $orderColumn = "username";
                                $orderType = "asc";

                         }else{
                                $orderColumn = "is_verified";
                                $orderType = "desc";
                         }
                        break;
                case 11:
                        if(array_key_exists('ini_penyedia', $param)){
                                $orderColumn = "username";
                                $orderType = "asc";
                        }else{
                                $orderColumn = 'bisa_mengubah_data';
                                $orderType = "asc";
                        }
                        break;

                default: break;
            }
            $data = DB::table("rekanan")
                        ->select(DB::raw("rekanan.*,pegawai.nama_pegawai, case when is_activated ='t' and (is_verified = 'f' or is_verified is null) and (tolak_verifikasi = 'f' or tolak_verifikasi is null ) and trigger_verified = 't' then 'atas' else 'bawah' end as status_rek,kota_nama,capitol"))
                        ->from('rekanan');

            $data->leftJoin('badan_usaha','badan_usaha.badan_usaha_id','=','rekanan.jenis_perusahaan');
            $data->join('mst_country','mst_country.id_country','=','rekanan.country_id');
            $data->leftJoin('mst_kota','mst_kota.kota_id','=','rekanan.kota_id');
            $data->leftjoin('pegawai','pegawai.pegawai_id','=','rekanan.updated_by_admin');



            //kondisi ketika dia berada di page penyedia terverifikasi
            if(array_key_exists('ini_penyedia', $param)){
                $data->where('is_verified','=',true);
            }

            $data->whereIn('rekanan_id',function($builder){

            $param = json_decode($this->request()->getBody(), true);
            $builder->select('rekanan.rekanan_id');
            $builder->from('rekanan');

            $status = intval($param["status"]);
            $company = $param["company"];

            switch ($status) {
                case 0:

                    $builder->where('rekanan.created_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('rekanan.created_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }
                    $builder->whereNotNull('rekanan.is_activated');
                    if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                        $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                        $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');


                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                           $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));

                    }
                    $orderColumn = "bisa_mengubah_data";
                    $orderType = "asc";
                    break;
                case 1:
                    $builder->where(function($query) {
                        $query->whereNull('rekanan.is_activated');
                        $query->orWhere('rekanan.is_activated', false);
                    });
                    $builder->where('is_registered', true);
                    $builder->where('rekanan.created_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('rekanan.created_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }
                    break;
                case 2:
                    $builder->where(function($query) {
                        $query->where(function($query1) {
                            $query1->where('rekanan.is_activated', true);
                            $query1->whereNull('is_verified');
                            $query1->whereNull('trigger_verified');
                            $query1->whereNull('tolak_verifikasi');
                        });
                        $query->orWhere(function($query2) {
                            $query2->where('rekanan.is_activated', true);
                            $query2->where('is_verified', false);
                            $query2->whereNull('trigger_verified');
                        });
                        $query->orWhere(function($query3) {
                            $query3->where('rekanan.is_activated', true);
                            $query3->where('is_verified', false);
                            $query3->where('trigger_verified', false);
                            $query3->where('tolak_verifikasi', false);
                        });
                    });




                    $builder->where('activated_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('activated_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }

                    if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                        $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                        $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');

                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                           $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));

                    }

                    $orderColumn = "activated_date";
                    $orderType = "desc";
                    break;
                case 3:
                    $builder->where(function($query) {
                        $query->where(function($query1) {
                            $query1->where('rekanan.is_activated', true);
                            $query1->where(function($sql) {
                                $sql->where('is_verified', null);
                                $sql->orWhere('is_verified', '<>', true);
                            });
                            $query1->where('trigger_verified', true);
                        });
                    });




                    $builder->where('verified_send_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('verified_send_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }

                    if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                        $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                        $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');


                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                           $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));

                    }

                    $orderColumn = "verified_send_date";
                    $orderType = "desc";
                    break;
                case 4:

                    $builder->where('is_verified', true);
                    $builder->where('rekanan.is_activated', true);
                    $builder->where('verified_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('verified_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }

                    if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                        $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                        $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');


                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                           $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));

                    }


                    $orderColumn = "verified_date";
                    $orderType = "desc";
                    break;
                case 5:
                    $orderColumn = "rekanan.created_date";
                    $orderType = "desc";
                    $builder->where('rekanan.rekanan_id', $param['rekanan_id']);
                    break;
                case 6:
                    $builder->where(function($query) {
                        $query->where(function($query1) {
                            $query1->where('rekanan.is_activated', true);
                            $query1->where(function($sql) {
                                $sql->where('is_verified', false);
                            });
                            $query1->where('trigger_verified', false);
                            $query1->where('tolak_verifikasi', true);
                        });
                    });
                    $builder->where(function($query) use ($param) {
                        $query->where(function($query1) use ($param){
                            $query1->where('tolak_date', '>=', $param["waktu_mulai1"]);
                            $query1->where('tolak_date', '<=', $param["waktu_mulai2"]);
                        });
                        $query->orWhere(function($query1) {
                            $query1->whereNull('tolak_date');
                        });
                    });
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }

                    if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                        $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                        $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');


                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                           $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));

                    }


                    $orderColumn = "tolak_date";
                    $orderType = "desc";
                    break;
                //penambahan status baru untuk penyedia terverifikasi dari no 7 - 10 by avid
                case 7:

                    $builder->where('rekanan.created_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('verified_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('ini_penyedia', $param)){
                        $builder->where('is_verified','=',true);

                    }
                    break;
                case 8:
                    $builder->where('nama_perusahaan', 'ilike', $company);
                    if(array_key_exists('ini_penyedia', $param)){
                        $builder->where('is_verified','=',true);

                    }
                    break;
                case 9:
                    $builder->where('kode_rekanan', 'ilike', $company);
                    if(array_key_exists('ini_penyedia', $param)){
                        $builder->where('is_verified','=',true);
                    }
                    break;
                case 10:
                    $builder->where('username', 'ilike', $company);
                    if(array_key_exists('ini_penyedia', $param)){
                        $builder->where('is_verified','=',true);

                    }
                    break;
                case 11:
                    $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                    $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');
                    if(array_key_exists('rekanan', $param)){
                    $builder->where('rekanan.created_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('rekanan.created_date', '<=', $param["waktu_mulai2"]);

                    }


                    if($param['id_bu'] != ''){
                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                           $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));
                     }
                     if(array_key_exists('ini_penyedia', $param)){
                     $builder->where('is_verified','=',true);
                     }
                     $orderColumn = "username";
                     $orderType = "asc";
                    break;


                default:
                    break;
            }


            });

    // print_r($orderType);
            $this->resource = array(
                'status' => 200,
                'data' => [
                    'count' => $data->count(),
                    'data' => $data->where('is_activated','f')->skip($param["offset"])->take($param["limit"])->orderBy($orderColumn, $orderType)->get()
                ]
            );

        } catch (\Exception $e) {
            $this->resource = array(
                'status' => 500,
                'data' => $e->getMessage()
            );
        }
        $this->sendResponse();
    }

    public function searchRekananJoinBu() {
        try {
            $param = json_decode($this->request()->getBody(), true);
            $status = intval($param["status"]);
            // $orderColumn = "rekanan.created_date";
            // $orderType = "desc";
            switch ($status) {
                case 0: $orderColumn = "bisa_mengubah_data";
                        $orderType = "asc";
                        break;
                case 1: $orderColumn = "rekanan.created_date";
                        $orderType = "desc";
                        break;
                case 2: $orderColumn = "activated_date";
                        $orderType = "desc";
                        break;
                case 3: $orderColumn = "verified_send_date";
                        $orderType = "desc";
                        break;
                case 4: $orderColumn = "verified_date";
                        $orderType = "desc";
                        break;
                case 5: $orderColumn = "rekanan.created_date";
                        $orderType = "desc";
                        break;
                case 6: $orderColumn = "verified_send_date";
                        $orderType = "desc";
                        break;
                //penambahan status baru untuk penyedia terverifikasi dari no 7 - 10 by avid

                case 7: $orderColumn = "verified_date";
                        $orderType = "desc";
                        break;
                case 8:
                        if(array_key_exists('ini_penyedia', $param)){
                            $orderColumn = "nama_perusahaan";
                            $orderType = "asc";

                        }else{
                            $orderColumn = "is_verified";
                            $orderType = "desc";
                        }
                        break;
                case 9:
                        if(array_key_exists('ini_penyedia', $param)){
                            $orderColumn = "kode_rekanan";
                            $orderType = "asc";

                        }else{
                             $orderColumn = "is_verified";
                            $orderType = "desc";
                        }
                        break;
                case 10:
                         if(array_key_exists('ini_penyedia', $param)){
                                $orderColumn = "username";
                                $orderType = "asc";

                         }else{
                                $orderColumn = "is_verified";
                                $orderType = "desc";
                         }
                        break;
                case 11:
                        if(array_key_exists('ini_penyedia', $param)){
                                $orderColumn = "username";
                                $orderType = "asc";
                        }else{
                                $orderColumn = 'bisa_mengubah_data';
                                $orderType = "asc";
                        }
                        break;

                default: break;
            }
            $data = DB::table("rekanan")
                        ->select(DB::raw("rekanan.*,pegawai.nama_pegawai, case when is_activated ='t' and (is_verified = 'f' or is_verified is null) and (tolak_verifikasi = 'f' or tolak_verifikasi is null ) and trigger_verified = 't' then 'atas' else 'bawah' end as status_rek,kota_nama,capitol"))
                        ->from('rekanan');

            $data->leftJoin('badan_usaha','badan_usaha.badan_usaha_id','=','rekanan.jenis_perusahaan');
            $data->join('mst_country','mst_country.id_country','=','rekanan.country_id');
            $data->leftJoin('mst_kota','mst_kota.kota_id','=','rekanan.kota_id');
            $data->leftjoin('pegawai','pegawai.pegawai_id','=','rekanan.updated_by_admin');



            //kondisi ketika dia berada di page penyedia terverifikasi
            if(array_key_exists('ini_penyedia', $param)){
                $data->where('is_verified','=',true);
            }

            $data->whereIn('rekanan_id',function($builder){

            $param = json_decode($this->request()->getBody(), true);
            $builder->select('rekanan.rekanan_id');
            $builder->from('rekanan');

            $status = intval($param["status"]);
            $company = $param["company"];

            switch ($status) {
                case 0:

                    $builder->where('rekanan.created_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('rekanan.created_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }
                    $builder->whereNotNull('rekanan.is_activated');
                    if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                        $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                        $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');


                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                           $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));

                    }
                    $orderColumn = "bisa_mengubah_data";
                    $orderType = "asc";
                    break;
                case 1:
                    $builder->where(function($query) {
                        $query->whereNull('rekanan.is_activated');
                        $query->orWhere('rekanan.is_activated', false);
                    });
                    $builder->where('is_registered', true);
                    $builder->where('rekanan.created_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('rekanan.created_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }
                    break;
                case 2:
                    $builder->where(function($query) {
                        $query->where(function($query1) {
                            $query1->where('rekanan.is_activated', true);
                            $query1->whereNull('is_verified');
                            $query1->whereNull('trigger_verified');
                            $query1->whereNull('tolak_verifikasi');
                        });
                        $query->orWhere(function($query2) {
                            $query2->where('rekanan.is_activated', true);
                            $query2->where('is_verified', false);
                            $query2->whereNull('trigger_verified');
                        });
                        $query->orWhere(function($query3) {
                            $query3->where('rekanan.is_activated', true);
                            $query3->where('is_verified', false);
                            $query3->where('trigger_verified', false);
                            $query3->where('tolak_verifikasi', false);
                        });
                    });




                    $builder->where('activated_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('activated_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }

                    if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                        $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                        $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');

                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                           $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));

                    }

                    $orderColumn = "activated_date";
                    $orderType = "desc";
                    break;
                case 3:
                    $builder->where(function($query) {
                        $query->where(function($query1) {
                            $query1->where('rekanan.is_activated', true);
                            $query1->where(function($sql) {
                                $sql->where('is_verified', null);
                                $sql->orWhere('is_verified', '<>', true);
                            });
                            $query1->where('trigger_verified', true);
                        });
                    });




                    $builder->where('verified_send_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('verified_send_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }

                    if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                        $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                        $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');


                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                           $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));

                    }

                    $orderColumn = "verified_send_date";
                    $orderType = "desc";
                    break;
                case 4:

                    $builder->where('is_verified', true);
                    $builder->where('rekanan.is_activated', true);
                    $builder->where('verified_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('verified_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }

                    if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                        $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                        $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');


                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }

                           $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));

                    }


                    $orderColumn = "verified_date";
                    $orderType = "desc";
                    break;
                case 5:
                    $orderColumn = "rekanan.created_date";
                    $orderType = "desc";
                    $builder->where('rekanan.rekanan_id', $param['rekanan_id']);
                    break;
                case 6:
                    $builder->where(function($query) {
                        $query->where(function($query1) {
                            $query1->where('rekanan.is_activated', true);
                            $query1->where(function($sql) {
                                $sql->where('is_verified', false);
                            });
                            $query1->where('trigger_verified', false);
                            $query1->where('tolak_verifikasi', true);
                        });
                    });
                    $builder->where(function($query) use ($param) {
                        $query->where(function($query1) use ($param){
                            $query1->where('tolak_date', '>=', $param["waktu_mulai1"]);
                            $query1->where('tolak_date', '<=', $param["waktu_mulai2"]);
                        });
                        $query->orWhere(function($query1) {
                            $query1->whereNull('tolak_date');
                        });
                    });
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }

                    if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                        $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                        $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');


                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                           $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));

                    }


                    $orderColumn = "tolak_date";
                    $orderType = "desc";
                    break;
                //penambahan status baru untuk penyedia terverifikasi dari no 7 - 10 by avid
                case 7:

                    $builder->where('rekanan.created_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('verified_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('ini_penyedia', $param)){
                        $builder->where('is_verified','=',true);

                    }
                    break;
                case 8:
                    $builder->where('nama_perusahaan', 'ilike', $company);
                    if(array_key_exists('ini_penyedia', $param)){
                        $builder->where('is_verified','=',true);

                    }
                    break;
                case 9:
                    $builder->where('kode_rekanan', 'ilike', $company);
                    if(array_key_exists('ini_penyedia', $param)){
                        $builder->where('is_verified','=',true);
                    }
                    break;
                case 10:
                    $builder->where('username', 'ilike', $company);
                    if(array_key_exists('ini_penyedia', $param)){
                        $builder->where('is_verified','=',true);

                    }
                    break;
                case 11:
                    $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                    $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');
                    if(array_key_exists('rekanan', $param)){
                    $builder->where('rekanan.created_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('rekanan.created_date', '<=', $param["waktu_mulai2"]);

                    }


                    if($param['id_bu'] != ''){
                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                           $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));
                     }
                     if(array_key_exists('ini_penyedia', $param)){
                     $builder->where('is_verified','=',true);
                     }
                     $orderColumn = "username";
                     $orderType = "asc";
                    break;


                default:
                    break;
            }


            });

    // print_r($orderType);
            $this->resource = array(
                'status' => 200,
                'data' => [
                    'count' => $data->count(),
                    'data' => $data->skip($param["offset"])->take($param["limit"])->orderBy($orderColumn, $orderType)->get()
                ]
            );

        } catch (\Exception $e) {
            $this->resource = array(
                'status' => 500,
                'data' => $e->getMessage()
            );
        }
        $this->sendResponse();
    }

    public function lastUpdate() {

        $param = json_decode($this->request()->getBody(), true);
        $val = strtolower('%'. $param['namaItem'].'%');
        $query = DB::select("select distinct a.rekanan_id, a.nama_perusahaan from rekanan a
        join (select rekanan_id, katalog.created_time from katalog join item on katalog.item_id = item.id
        where LOWER(item.name) like '$val' order by katalog.created_time desc)
        b on b.rekanan_id = a.rekanan_id limit 6");

        $this->resource = array(
            'status' => 200,
            'data' => $query
        );
        $this->sendResponse();
    }

    //rekanan/countbystatus
    //itp.rekanan.getCountQueryByStatus
    public function countByStatus() {
        $this->entity = new Rekanan();
        $param = json_decode($this->request()->getBody(), true);
        //var_dump($param);
        $builder = DB::table("rekanan");
        $status = $param["status"];
        $company = $param["company"];
        switch ($status) {
            case 1:
                $builder->where('is_activated', '=', null);
                $builder->where('is_registered', '=', true);
                $builder->where('created_date', '>=', $param["waktu_mulai1"]);
                $builder->where('created_date', '<=', $param["waktu_mulai2"]);
                $builder->orWhere(function($query) {
                    $param = json_decode($this->request()->getBody(), true);
                    $query->where('is_activated', '=', false)
                            ->where('is_registered', '=', true)
                            ->where('created_date', '>=', $param["waktu_mulai1"])
                            ->where('created_date', '<=', $param["waktu_mulai2"]);
                });
                break;
            case 2:
                $builder->where('is_activated', '=', true);
                $builder->where('is_verified', '=', null);
                $builder->where('trigger_verified', '=', null);
                $builder->where('activated_date', '>=', $param["waktu_mulai1"]);
                $builder->where('activated_date', '<=', $param["waktu_mulai2"]);
                $builder->orWhere(function($query) {
                    $param = json_decode($this->request()->getBody(), true);
                    $query->where('is_activated', '=', true)
                            ->where('is_verified', '=', false)
                            ->where('trigger_verified', '=', null)
                            ->where('activated_date', '>=', $param["waktu_mulai1"])
                            ->where('activated_date', '<=', $param["waktu_mulai2"]);
                });
                $builder->orWhere(function($query) {
                    $param = json_decode($this->request()->getBody(), true);
                    $query->where('is_activated', '=', true)
                            ->where('is_verified', '=', false)
                            ->where('trigger_verified', '=', false)
                            ->where('activated_date', '>=', $param["waktu_mulai1"])
                            ->where('activated_date', '<=', $param["waktu_mulai2"]);
                });
                break;
            case 3:
                $builder->where('is_activated', '=', true);
                $builder->where('trigger_verified', '=', true);
                $builder->where('is_verified', '=', null);
                $builder->where('verified_send_date', '>=', $param["waktu_mulai1"]);
                $builder->where('verified_send_date', '<=', $param["waktu_mulai2"]);
                $builder->orWhere(function($query) {
                    $param = json_decode($this->request()->getBody(), true);
                    $query->where('is_activated', '=', true)
                            ->where('trigger_verified', '=', true)
                            ->where('is_verified', '=', false)
                            ->where('verified_send_date', '>=', $param["waktu_mulai1"])
                            ->where('verified_send_date', '<=', $param["waktu_mulai2"]);
                });
                break;
            case 4:
                $builder->where('is_verified', '=', true);
                $builder->where('is_activated', '=', true);
                $builder->where('verified_date', '>=', $param["waktu_mulai1"]);
                $builder->where('verified_date', '<=', $param["waktu_mulai2"]);
                break;
            case 5:
                $builder->where('nama_perusahaan', 'ilike', $company);
                $builder->where('created_date', '>=', $param["waktu_mulai1"]);
                $builder->where('created_date', '<=', $param["waktu_mulai2"]);
            default:
                break;
        }
        $builder->where('nama_perusahaan', 'ilike', $company);


        $this->resource = array(
            'status' => 200,
            'data' => $builder->count()
        );
        $this->sendResponse();
    }

    // rekanan/getdatabystatus
    //itp.rekanan.getDataQueryByStatus
    public function getDataByStatus() {
        $this->entity = new Rekanan();
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("rekanan");
        $status = $param["status"];
        $company = $param["company"];
        switch ($status) {
            case 1:
                $builder->where('is_activated', '=', null);
                $builder->where('is_registered', '=', true);
                $builder->where('created_date', '>=', $param["waktu_mulai1"]);
                $builder->where('created_date', '<=', $param["waktu_mulai2"]);
                $builder->orWhere(function($query) {
                    $param = json_decode($this->request()->getBody(), true);
                    $query->where('is_activated', '=', false)
                            ->where('is_registered', '=', true)
                            ->where('created_date', '>=', $param["waktu_mulai1"])
                            ->where('created_date', '<=', $param["waktu_mulai2"]);
                });
                break;
            case 2:
                $builder->where('is_activated', '=', true);
                $builder->where('is_verified', '=', null);
                $builder->where('trigger_verified', '=', null);
                $builder->where('activated_date', '>=', $param["waktu_mulai1"]);
                $builder->where('activated_date', '<=', $param["waktu_mulai2"]);
                $builder->orWhere(function($query) {
                    $param = json_decode($this->request()->getBody(), true);
                    $query->where('is_activated', '=', true)
                            ->where('is_verified', '=', false)
                            ->where('trigger_verified', '=', null)
                            ->where('activated_date', '>=', $param["waktu_mulai1"])
                            ->where('activated_date', '<=', $param["waktu_mulai2"]);
                });
                $builder->orWhere(function($query) {
                    $param = json_decode($this->request()->getBody(), true);
                    $query->where('is_activated', '=', true)
                            ->where('is_verified', '=', false)
                            ->where('trigger_verified', '=', false)
                            ->where('activated_date', '>=', $param["waktu_mulai1"])
                            ->where('activated_date', '<=', $param["waktu_mulai2"]);
                });
                break;
            case 3:
                $builder->where('is_activated', '=', true);
                $builder->where('trigger_verified', '=', true);
                $builder->where('is_verified', '=', null);
                $builder->where('verified_send_date', '>=', $param["waktu_mulai1"]);
                $builder->where('verified_send_date', '<=', $param["waktu_mulai2"]);
                $builder->orWhere(function($query) {
                    $param = json_decode($this->request()->getBody(), true);
                    $query->where('is_activated', '=', true)
                            ->where('trigger_verified', '=', true)
                            ->where('is_verified', '=', false)
                            ->where('verified_send_date', '>=', $param["waktu_mulai1"])
                            ->where('verified_send_date', '<=', $param["waktu_mulai2"]);
                });
                break;
            case 4:
                $builder->where('is_verified', '=', true);
                $builder->where('is_activated', '=', true);
                $builder->where('verified_date', '>=', $param["waktu_mulai1"]);
                $builder->where('verified_date', '<=', $param["waktu_mulai2"]);
                break;
            case 5:
                $builder->where('nama_perusahaan', 'ilike', $company);
                $builder->where('created_date', '>=', $param["waktu_mulai1"]);
                $builder->where('created_date', '<=', $param["waktu_mulai2"]);
            default:
                break;
        }

        $builder->where('nama_perusahaan', 'ilike', $company);

        $builder->skip($param["offset"])
                ->take($param["limit"]);

        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    // rekanan/refreshData
    //itp.rekanan.refreshData
    public function refreshData() {
        $this->entity = new Rekanan();
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("rekanan");
        $status = $param["status"];
        $company = $param["company"];
        switch ($status) {
            case 1:
                $builder->where('is_activated', '=', null);
                $builder->where('is_registered', '=', true);
                $builder->orWhere(function($query) {
                    $param = json_decode($this->request()->getBody(), true);
                    $query->where('is_activated', '=', false)
                            ->where('is_registered', '=', true);
                });
                break;
            case 2:
                $builder->where('is_activated', '=', true);
                $builder->where('is_verified', '=', null);
                $builder->where('trigger_verified', '=', null);
                $builder->orWhere(function($query) {
                    $param = json_decode($this->request()->getBody(), true);
                    $query->where('is_activated', '=', true)
                            ->where('is_verified', '=', false)
                            ->where('trigger_verified', '=', null);
                });
                $builder->orWhere(function($query) {
                    $param = json_decode($this->request()->getBody(), true);
                    $query->where('is_activated', '=', true)
                            ->where('is_verified', '=', false)
                            ->where('trigger_verified', '=', false);
                });
                break;
            case 3:
                $builder->where('is_activated', '=', true);
                $builder->where('trigger_verified', '=', true);
                $builder->where('is_verified', '=', null);
                $builder->orWhere(function($query) {
                    $param = json_decode($this->request()->getBody(), true);
                    $query->where('is_activated', '=', true)
                            ->where('trigger_verified', '=', true)
                            ->where('is_verified', '=', false);
                });
                break;
            case 4:
                $builder->where('is_verified', '=', true);
                $builder->where('is_activated', '=', true);
                break;
            case 5:
                $builder->where('nama_perusahaan', 'ilike', $company);
            default:
                break;
        }
        $builder->skip($param["offset"])
                ->take($param["limit"]);

        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    //itp.rekanan.getCountRecord
    //rekanan/count
    public function countRecord() {
        $this->entity = new Rekanan();
        $this->resource = array(
            'status' => 200,
            'data' => $this->size()
        );
        $this->sendResponse();
    }

    //itp.rekanan.getRecord
    //rekanan/data
    //param:
    //-offset
    //-limit
    public function getRecord() {
        $this->entity = new Rekanan();
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("rekanan");
        $offset = $param["offset"];
        $limit = $param["limit"];
        $builder->orderBy("created_date", 'DESC')->skip($offset)->take($limit);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    //itp.rekanan.tenagaAhliCV_View
    //rekanan/tenagaahlicv
    //param:
    //-rekanan_ta_id
    //-jenis
    public function tenagaAhliCV_View() {
        $this->entity = new RekananTACV();
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table('rekanan_ta_cv');
        $builder->where('rekanan_ta_id', '=', $param['rekanan_ta_id'])
                ->where('jenis', '=', $param['jenis']);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    //itp.rekanan.getBadanUsahaName
    //rekanan/getBadanUsahaName method post
    public function getBadanUsahaName() {
        $param = json_decode($this->request()->getBody(), true);
        $rekanan_id = $param['rekanan_id'];
        $builder = DB::table('rekanan');
        $builder->join('badan_usaha', 'badan_usaha.badan_usaha_id', '=', 'rekanan.jenis_perusahaan');
        $builder->where('rekanan.rekanan_id', '=', $rekanan_id);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get(['badan_usaha.badan_usaha_nama'])
        );
        $this->sendResponse();
    }

    //itp.rekanan.getProvinceName
    //rekanan/getProvinceName method post
    public function getProvinceName() {
        $param = json_decode($this->request()->getBody(), true);
        $rekanan_id = $param['rekanan_id'];
        $builder = DB::table('rekanan');
        $builder->join('mst_provinsi', 'mst_provinsi.provinsi_id', '=', 'rekanan.provinsi_id');
        $builder->where('rekanan.rekanan_id', '=', $rekanan_id);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get(['mst_provinsi.provinsi_nama'])
        );
        $this->sendResponse();
    }

    //itp.rekanan.getTownName
    //rekanan/getTownName method post
    public function getTownName() {
        $param = json_decode($this->request()->getBody(), true);
        $rekanan_id = $param['rekanan_id'];
        $builder = DB::table('rekanan');
        $builder->join('mst_kota', 'mst_kota.kota_id', '=', 'rekanan.kota_id');
        $builder->where('rekanan.rekanan_id', '=', $rekanan_id);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get(['mst_kota.kota_nama'])
        );
        $this->sendResponse();
    }

    //itp.rekanan.getCountExpired
    public function countExpired() {
        $this->entity = new DokumenExpiredView();
        $param = json_decode($this->request()->getBody(), true);
        // $builder = DB::table('dokumen_expired_view');
        // $builder->whereRaw("nama_perusahaan like ?", [$param["nama_perusahaan"]]);
        // $builder->where(function($query) {
        //     $query->where('iu_expired', '>', 0)
        //             ->orWhere('pp_expired', '>', 0);
        //     //Tidak Perlu dicek di Jiwasraya
        //     //->orWhere('sert_expired', '>', 0)
        //     //->orWhere('lain2_expired', '>', 0)
        // });

        $builder = DB::table('rekanan')
        ->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id')
        ->where('rekanan_ijinusaha.masa_berlaku', '<', date('Y-m-d'))
        ->where(function($builder1) {
          $builder1->where('rekanan_ijinusaha.jenis_ijin', '=', 'SBU');
          $builder1->orWhere('rekanan_ijinusaha.jenis_ijin', '=', 'SIUJK');
          $builder1->orWhere('rekanan_ijinusaha.jenis_ijin', '=', 'SKL');
        })
        ->count();

        $this->resource = array(
            'status' => 200,
            'data' => $builder
        );
        $this->sendResponse();
    }

    //itp.rekanan.getExpired
    //parameter:
    //- nama_perusahaan
    //- offset
    //- limit
    public function getExpired() {
        $this->entity = new DokumenExpiredView();
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table('dokumen_expired_view');
        $builder->whereRaw("nama_perusahaan like ?", [$param["nama_perusahaan"]]);
        $builder->where(function($query) {
            $query->where('iu_expired', '>', 0)
                    ->orWhere('pp_expired', '>', 0)
            //Tidak Perlu dicek di Jiwasraya
            //->orWhere('sert_expired', '>', 0)
            //->orWhere('lain2_expired', '>', 0)
            ;
        });
        $builder->orderBy('rekanan_id')->skip($param['offset'])->take($param['limit']);
        $data = [];
        $qs = $builder->get();

        foreach ($qs as $q) {
            $keterangan = '';
            if ($q->iu_expired > 0) {
                if ($keterangan === '') {
                    $keterangan = $keterangan . "Ijin usaha kadaluarsa";
                } else {
                    $keterangan = $keterangan . ", Ijin usaha kadaluarsa";
                }
            }
            if ($q->pp_expired > 0) {
                if ($keterangan === '') {
                    $keterangan = $keterangan . "Data Pengurus Perusahaan kadaluarsa";
                } else {
                    $keterangan = $keterangan . ", Data Pengurus Perusahaan kadaluarsa";
                }
            }
            /* Tidak Perlu dicek di Jiwasraya
              if ($q->sert_expired > 0) {
              if ($keterangan === '') {
              $keterangan = $keterangan . "Data Sertifikat Tenaga Ahli expired";
              } else {
              $keterangan = $keterangan . ", Data Sertifikat Tenaga Ahli expired";
              }
              }
              if ($q->lain2_expired > 0) {
              if ($keterangan === '') {
              $keterangan = $keterangan . "Dokumen Lain - Lain expired";
              } else {
              $keterangan = $keterangan . ", Dokumen Lain - Lain expired";
              }
              }
             */
            $q->keterangan = $keterangan;
        }
        $this->resource = array(
            'status' => 200,
            'data' => $qs
        );

        $this->sendResponse();
    }

    public function listExpired() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table('rekanan');
        $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
        if($param['masa_berlaku1'] != "" && $param['masa_berlaku2'] != ""){
            $builder->whereBetween('rekanan_ijinusaha.masa_berlaku', [$param['masa_berlaku2'], $param['masa_berlaku1']]);
        }
        else{
            $builder->where('rekanan_ijinusaha.masa_berlaku', '<', $param['masa_berlaku1']);
        }
        $val = strtolower($param['nama_perusahaan']);
        $builder->whereRaw('LOWER(rekanan.nama_perusahaan) LIKE ?',[$val]);
        $builder->where(function($builder1) {
            $builder1->where('rekanan_ijinusaha.jenis_ijin', '=', 'SBU');
            $builder1->orWhere('rekanan_ijinusaha.jenis_ijin', '=', 'SIUJK');
            $builder1->orWhere('rekanan_ijinusaha.jenis_ijin', '=', 'SKL');
        });
        $builder->orderBy('rekanan_ijinusaha.rekanan_id')->skip($param['offset'])->take($param['limit']);

        $count = DB::table('rekanan');
        $count->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
        if($param['masa_berlaku1'] != "" && $param['masa_berlaku2'] != ""){
            $count->whereBetween('rekanan_ijinusaha.masa_berlaku', [$param['masa_berlaku2'], $param['masa_berlaku1']]);
        }
        else{
            $count->where('rekanan_ijinusaha.masa_berlaku', '<', $param['masa_berlaku1']);
        }
        $val = strtolower($param['nama_perusahaan']);
        $count->whereRaw('LOWER(rekanan.nama_perusahaan) LIKE ?',[$val]);
        $count->where(function($count1) {
            $count1->where('rekanan_ijinusaha.jenis_ijin', '=', 'SBU');
            $count1->orWhere('rekanan_ijinusaha.jenis_ijin', '=', 'SIUJK');
            $count1->orWhere('rekanan_ijinusaha.jenis_ijin', '=', 'SKL');
        });

        $countnya = $count->count();
        $countPenyedia = $count->distinct()->count(['rekanan.nama_perusahaan']);

        $this->resource = array(
            'status' => 200,
            'data' => ['data' => $builder->get(['rekanan.rekanan_id','rekanan.nama_perusahaan','rekanan_ijinusaha.jenis_ijin','rekanan_ijinusaha.masa_berlaku']), 'count' => $countnya,'count_penyedia'=>$countPenyedia]
        );
        $this->sendResponse();
    }

    // itp.rekanan.getBUByIjinUsaha
    // rekanan/getbubyijinusaha
    public function getBUByIjinUsaha() {
        $this->entity = new RekananIjinusahaBidusahaView();
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table('rekanan_ijinusaha_bidusaha_view');
        $builder->where('flag_active', '=', true)
                ->where('ijinusaha_id', '=', $param['ijinusaha_id'])
                ->orderBy('id', 'ASC');;
        $q = $builder->get();
        $this->resource = array(
            'status' => 200,
            'data' => $q
        );

        $this->sendResponse();
    }

    public function getRekananByBidangUsaha(){
        $param = json_decode($this->request()->getBody(), true);
        $offset = $param["offset"];
        $limit = $param["limit"];

        $builder = DB::table('rekanan_ijinusaha');
        $builder->select('rekanan_ijinusaha.rekanan_id');
        $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha.ijinusaha_id','=','rekanan_ijinusaha_bidusaha.ijinusaha_id');
        $data = $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',$param['id_bidang_usaha'])->distinct()->get();

        $rekanan_id = array();
        foreach($data as $dataRekanan){
            array_push($rekanan_id,$dataRekanan->rekanan_id);
        }
        // param id_bidang_usaha (array)
        $columnSelect = array(
            'nama_perusahaan',
            'alamat',
            'email'
        );
        $builder = DB::table('rekanan');
        $builder->select($columnSelect);
        $builder->whereIn('rekanan.rekanan_id',$rekanan_id);
        $builder->where('rekanan.is_verified',true);
        $builder->whereRaw("lower(nama_perusahaan) like ? ", [$param["nama_rekanan"]]);
        $count = $builder->count();
        $rekanan = $builder->skip($offset)->take($limit)->get();
        $this->resource = array(
            'status' => 200,
            'data' => array(
                'count' => $count,
                'rekanan' => $rekanan
            )
        );

        $this->sendResponse();
    }

    //itp.rekanan.updateValidVerifikasi
    public function updateValidVerifikasi() {
        $this->entity = new RekananVerifikasi();
        $param = json_decode($this->request()->getBody(), true);
        $rekanan_id = $param['rekanan_id'];
        $tabvalue = $param['tabValue'];
        $value = $param['value'];
        $builder = DB::table('rekanan_verifikasi');
        $builder->where('rekanan_id', '=', $rekanan_id);
        $q = $builder->count();
        $data = [];
        $datainsert = [];
        $where = array('rekanan_id' => $param['rekanan_id']);

        $this->tableName = 'rekanan_verifikasi';
        switch ($tabvalue) {
            case 'tabsp':
                $data = array("surat_pernyataan" => $value);
                $datainsert = array("rekanan_id" => $rekanan_id, "surat_pernyataan" => $value);
                break;
            case 'tabad':
                $data = array("data_administrasi" => $value);
                $datainsert = array("rekanan_id" => $rekanan_id, "data_administrasi" => $value);
                break;
            case 'tabiu':
                $data = array("izin_usaha" => $value);
                $datainsert = array("rekanan_id" => $rekanan_id, "izin_usaha" => $value);
                break;
            case 'tabap':
                $data = array("akta_pendirian" => $value);
                $datainsert = array("rekanan_id" => $rekanan_id, "akta_pendirian" => $value);
                break;
            case 'tabpp':
                $data = array("pengurus_perusahaan" => $value);
                $datainsert = array("rekanan_id" => $rekanan_id, "pengurus_perusahaan" => $value);
                break;
            case 'tabks':
                $data = array("kepemilikan_saham" => $value);
                $datainsert = array("rekanan_id" => $rekanan_id, "kepemilikan_saham" => $value);
                break;
            case 'tabdp':
                $data = array("data_pajak" => $value);
                $datainsert = array("rekanan_id" => $rekanan_id, "data_pajak" => $value);
                break;
            case 'tabta':
                $data = array("tenaga_ahli" => $value);
                $datainsert = array("rekanan_id" => $rekanan_id, "tenaga_ahli" => $value);
                break;
            case 'tabdpl':
                $data = array("data_perlengkapan" => $value);
                $datainsert = array("rekanan_id" => $rekanan_id, "data_perlengkapan" => $value);
                break;
            case 'tabdll':
                $data = array("dokumen_lain" => $value);
                $datainsert = array("rekanan_id" => $rekanan_id, "dokumen_lain" => $value);
                break;
            case 'tabdpg':
                $data = array("data_pengalaman" => $value);
                $datainsert = array("rekanan_id" => $rekanan_id, "data_pengalaman" => $value);
                break;
        }

        if ($q !== 0) {
            $simpan = $this->updateRow($where, $data);
        } else {
            $simpan = $this->insertRow($datainsert);
        }
        //var_dump($simpan);
        $this->resource = array(
            'status' => 200,
            'data' => $simpan
        );

        $this->sendResponse();
    }

    //itp.rekanan.detailView
    //rekanan/detailview
    public function detailView() {
        $param = json_decode($this->request()->getBody(), true);
        $parr = $param['param'];
        $builder = DB::select(' SELECT r.rekanan_id,
                    r.nama_perusahaan,
                    r.jenis_perusahaan,
                    r.id_bidang_usaha,
                    r.id_sub_bidang,
                    r.npwp,
                    r.alamat,
                    r.telepon,
                    r.fax,
                    r.email,
                    r.website,
                    r.kodepos,
                    r.kantor_cabang,
                    r.updated_by,
                    r.updated_date,
                    r.flag_blacklist,
                    r.flag_active,
                    r.username,
                    r.pswd AS password,
                    r.provinsi_id,
                    r.kota_id,
                    r.country_id,
                    r.is_activated,
                    r.is_verified,
                    p.provinsi_nama,
                    k.kota_nama,
                    c.country_name,
                    c.capitol,
                    r.dokumen_agreement,
                    r.no_ponsel,
                    r.email_pribadi,
                    r.trigger_verified,
                    r.pkp,
                    r.npwp_imgurl,
                    r.pkp_imgurl,
                    b.badan_usaha_nama,
                    r.bank_name,
                    r.jenis_vendor,
                    r.bank_account_name,
                    r.bank_account_no,
                    r.currency,
                    r.nama,
                    null as bidangusaha
                    FROM rekanan r
                    LEFT JOIN mst_provinsi p ON p.provinsi_id = r.provinsi_id
                    LEFT JOIN mst_kota k ON k.kota_id = r.kota_id
                    LEFT JOIN mst_country c ON c.id_country = r.country_id
                    LEFT JOIN badan_usaha b ON b.badan_usaha_id = r.jenis_perusahaan
                    WHERE rekanan_id = '.$parr[0].'
                    ');

        $tampung = $builder;

        $mst_bidangusaha = DB::table('rekanan_ijinusaha')
                                ->select('nama_bidang_usaha','rekanan_id')
                                ->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id')
                                ->join('mst_bidang_usaha','rekanan_ijinusaha_bidusaha.id_bidang_usaha','=','mst_bidang_usaha.id_bidang_usaha')
                                ->where('rekanan_id','=',array($parr))
                                ->where('level','=',3)
                                ->distinct()
                                ->get();

        for ($i=0; $i <count($mst_bidangusaha) ; $i++) {
            if($tampung[0]->bidangusaha == null){
                $tampung[0]->bidangusaha = $mst_bidangusaha[$i]->nama_bidang_usaha;
            }else if(($i+1) < count($mst_bidangusaha)){
                $tampung[0]->bidangusaha .= ','.$mst_bidangusaha[$i]->nama_bidang_usaha;

            }else{
                $tampung[0]->bidangusaha .= ', dan '.$mst_bidangusaha[$i]->nama_bidang_usaha;
            }
        }



        $this->resource = array(
            'status' => 200,
            'data' => $tampung
        );

        $this->sendResponse();
    }

    public function detailRekanan() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table('rekanan');
        $builder->join('katalog', 'katalog.rekanan_id', '=', 'rekanan.rekanan_id');
        $builder->where('katalog.item_id','=',$param['item_id'])->distinct();
        $this->resource = array(
            'status' => 200,
            'data' => ["data" => $builder->get(['rekanan.rekanan_id','rekanan.nama_perusahaan']), "count" => $builder->count()]
        );

        $this->sendResponse();
    }

    //itp.rekanan.getValidVerifikasi
    // rekanan/getvalidverifikasi
    public function getValidVerifikasi() {
        $param = json_decode($this->request()->getBody(), true);
        $rekanan_id = $param['rekanan_id'];
        $builder = DB::table('rekanan_verifikasi');
        $builder->where('rekanan_id', '=', $rekanan_id);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );

        $this->sendResponse();
    }

    //itp.rekanan.getSuratPernyataan
    //path : rekanan/getsuratpernyataan
    //parameter :
//    {
//        rekanan_id : 4162
//    }
    public function getSuratPernyataan() {
        $param = json_decode($this->request()->getBody(), true);
        $rekanan_id = $param['rekanan_id'];
        $builder = DB::table('rekanan');
        $builder->where('rekanan_id', '=', $rekanan_id);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get(['rekanan_id', 'nama_perusahaan', 'alamat', 'paktaintegritas_agreement', 'pernyataanminat_agreement', 'tanggal_setuju'])
        );
        $this->sendResponse();
    }

    //itp.rekanan.updateAction -> POST rekanan/updateAction
    public function updateAction(){
        $param = json_decode($this->request()->getBody(), true);
        $column = $param['column'];
        $column2 = $param['column2'];
        $action = $param['action'];
        $ket = $param['ket'];
        $rekid = $param['rekid'];
        $email = $param['email'];
        $emailPribadi = $param['emailPribadi'];
        $nama_perusahaan = $param['nama_perusahaan'];
        $username = $param['username'];
        $username1 = $param['username1'];
        $builder1 = DB::table('rekanan');

        if( $action == 'terima' ){
            $id_content = 4;
            $data1 = [
                $column => 1,
                $column2 => Carbon::now()
            ];
            $result = $builder1->where('rekanan_id', '=', $rekid)->update($data1);

            if( !$result ){
                $this->resource = ['status' => null];
                $this->sendResponse();
                return;
            }

            $userLogController = new UserLogController(); // Menerima/Menolak Aktivasi Rekanan
            $datalog = ['username' => $username1, 'detail' => 'Menerima Aktivasi untuk vendor ' . $nama_perusahaan, 'user_activity_id' => 179, 'tanggal' => Carbon::now()];
            $resource = $userLogController->insertLogUser($datalog);

            $this->resource = ['status' => 200, 'data' => 1];
            //$this->sendResponse();
        } else {
            $id_content = 3;
            $data1 = [
                $column => 0,
                'keterangan' => $ket,
                'tolak_date' => Carbon::now()
            ];
            $result = $builder1->where('rekanan_id', '=', $rekid)->update($data1);
            if (!$result) {
                $this->resource = ['status' => null];
                $this->sendResponse();
                return;
            }
            $userLogController = new UserLogController();
            $datalog = ['username' => $username1, 'detail' => 'Rekanan ' . $nama_perusahaan . ' Aktivasi di tolak', 'user_activity_id' => 179, 'tanggal' => Carbon::now()];
            $resource = $userLogController->insertLogUser($datalog);

            $this->resource = ['status' => 200, 'data' => 1];
            //$this->sendResponse();
        }

        $this->sendActivationMail($rekid, $ket, $id_content, $action);
        $this->sendResponse();
    }

    //itp.rekanan.ijinusahaView
    //path : rekanan/rekananijinusahaview
    //post
    //param : {rekanan_id : 100}
    public function ijinusahaView() {
        $param = json_decode($this->request()->getBody(), true);
        $rekanan_id = $param['rekanan_id'];
        $builder = DB::table('rekanan_ijinusaha_view');
        $builder->where('rekanan_id', '=', $rekanan_id);
        $check = DB::table('rekanan')->where('rekanan_id', '=', $rekanan_id)->first();

        if ($check->jenis_vendor == 3) {
          $builder->where('jenis_ijin', '!=' , 'x');
        }

        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    public function JenisPerusahaanView()
    {
      $param = json_decode($this->request()->getBody(), true);
      $rekanan_id = $param['rekanan_id'];
      $builder = DB::table('rekanan_ijinusaha')
      ->select('mst_bidang_usaha.id_bidang_usaha','mst_bidang_usaha.kode_bidang_usaha','mst_bidang_usaha.nama_bidang_usaha')
      ->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id')
      ->join('mst_bidang_usaha','rekanan_ijinusaha_bidusaha.id_bidang_usaha','=','mst_bidang_usaha.id_bidang_usaha')
      ->where('rekanan_ijinusaha.rekanan_id','=',$rekanan_id);
      $this->resource = array(
          'status' => 200,
          'data' => $builder->get()
      );
      $this->sendResponse();
    }

    //itp.rekanan.aktaView
    //path : rekanan/rekananaktaview
    public function aktaView() {
        $param = json_decode($this->request()->getBody(), true);
        $rekanan_id = $param['rekanan_id'];
        $builder = DB::table('rekanan_akta_view');
        $builder->where('rekanan_id', '=', $rekanan_id);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    //Gabungan beberapa address
    //itp.rekanan.ijinusahaView
    //itp.rekanan.aktaView
    //itp.rekanan.pengurusperusahaanView
    //itp.rekanan.sahamView
    //itp.rekanan.pajakView
    //itp.rekanan.tenagaAhliView
    //path : rekanan/properties
    /* {
      "rekanan_id":97,
      "fungsi" : "detail"
      } */
    public function rekananPropertiesView() {
        $param = json_decode($this->request()->getBody(), true);
        $parr = [$param['rekanan_id'], $param['fungsi']];

        $tabel = '';
        switch ($parr[1]) {
            case 'detail':
                $tabel = 'rekanan_view';
                break;
            case 'ijinusaha':
                $tabel = 'rekanan_ijinusaha_view';
                break;
            case 'akta':
                $tabel = 'rekanan_akta_view';
                break;
            case 'saham':
                $tabel = 'rekanan_saham_view';
                break;
            case 'pengurus':
                $tabel = 'rekanan_pengurusperusahaan_view';
                break;
            case 'pajak':
                $tabel = 'rekanan_pajak';
                break;
            case 'tenagaahli':
                $tabel = 'rekanan_ta_view';
                break;
            case 'alat':
                $tabel = 'rekanan_alat_view';
                break;
            case 'pengalaman':
                $tabel = 'rekanan_pengalaman_view';
                break;
            case 'uploaddokumen':
                $tabel = 'rekanan_uploaddokumen_view';
                break;
            default:
                break;
        }
        $builder = DB::table($tabel);
        $builder->where('rekanan_id', '=', $parr[0]);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    //itp.rekanan.ReportGetCountQueryByCompany
    //rekanan/reportcountbycompany
    //post
    //parameter : {} atau {"company":"%%"}
    public function reportGetCountQueryByCompany() {
        $param = json_decode($this->request()->getBody(), true);
        $parr = $param['company'];
        $tabel = 'rekanan';
        $builder = DB::table($tabel);
        if ($parr == "") {
            $builder->where('is_verified', '=', TRUE)
                    ->where('is_activated', '=', TRUE);
        } else {
            $builder->where('is_verified', '=', TRUE)
                    ->where('is_activated', '=', TRUE)
                    ->whereRaw('lower(nama_perusahaan) LIKE lower (?)', [$parr]);
        }
        $this->resource = array(
            'status' => 200,
            'data' => $builder->count()
        );
        $this->sendResponse();
    }

    //itp.rekanan.ReportGetCountRekanan
    //rekanan/countreportrekanan
    //get
    public function reportGetCountRekanan() {
        $tabel = 'rekanan';
        $builder = DB::table($tabel);
        $builder->where('is_verified', '=', TRUE)
                ->where('is_activated', '=', TRUE);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->count()
        );
        $this->sendResponse();
    }

    //itp.rekanan.ReportGetRekanan
    //post
    //rekanan/reportdatarekanan
    //parameter: {"offset":, "limit":}
    public function reportGetDataRekanan() {
        $param = json_decode($this->request()->getBody(), true);
        $offset = $param['offset'];
        $limit = $param['limit'];
        $tabel = 'rekanan';
        $builder = DB::table($tabel);
        $builder->where('is_verified', '=', TRUE)
                ->where('is_activated', '=', TRUE)
                ->orderBy('created_date', 'DESC');
        $builder->skip($offset)->take($limit);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    //itp.rekanan.ReportGetDataQueryByCompany
    //rekanan/reportdatabycompany
    //post
    //parameter : {"offset":,"limit":} atau {"offset":,"limit":,"company":"%%"}
    public function reportGetDataQueryByCompany() {
        $param = json_decode($this->request()->getBody(), true);
        $parr = $param['company'];
        $offset = $param['offset'];
        $limit = $param['limit'];
        $tabel = 'rekanan';
        $builder = DB::table($tabel);
        if ($parr == "") {
            $builder->where('is_verified', '=', TRUE)
                    ->where('is_activated', '=', TRUE);
        } else {
            $builder->where('is_verified', '=', TRUE)
                    ->where('is_activated', '=', TRUE)
                    ->whereRaw('lower(nama_perusahaan) LIKE lower (?)', [$parr]);
        }
        $builder->orderBy('created_date', 'DESC');
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    // laporan data rekanan
    //itp.rekanan.selectview ==> POST rekanan/selectview
    public function selectview() {
        $param = json_decode($this->request()->getBody(), true);
        $pencarian = '%' . $param["pencarian"] . '%';
        $pageSize = $param["pageSize"];
        $offset = $param['offset'];

        $builder = DB::table('rekanan');
        $buildercount = DB::table('rekanan');

        $builder->whereRaw("is_verified = true AND is_activated = true AND lower(nama_perusahaan) like lower(?)", [$pencarian])->orderBy("created_date", "DESC")->skip($offset)->limit($pageSize);
        $buildercount->whereRaw("is_verified = true AND is_activated = true AND lower(nama_perusahaan) like lower(?)", [$pencarian]);

        $this->resource = array(
            'status' => 200,
            'data' => ["data" => $builder->get(["rekanan_id", "nama_perusahaan"]), "count" => $buildercount->count()]
        );
        $this->sendResponse();
    }

    //itp.rekanan.getdocumeakta ==> POST rekanan/getdocumeakta
    public function getdocumeakta() {
        $param = json_decode($this->request()->getBody(), true);
        $rekanan_id = $param['rekanan_id'];
        $builder = DB::table('rekanan_dokumen_akta');
        $builder->where('rekanan_id', '=', $rekanan_id);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    //itp.rekanan.getNama -> POST rekanan/getnama
//    public function getNama() {
//        //select nama_perusahaan from rekanan where rekanan_id=?"
//        $param = json_decode($this->request()->getBody(), true);
//        $rekanan_id = $param['rekanan_id'];
//        $builder = DB::table('rekanan');
//        $builder->where('rekanan_id', '=', $rekanan_id);
//        $this->resource = array(
//            'status' => 200,
//            'data' => $builder->get(['nama_perusahaan'])
//        );
//        $this->sendResponse();
//    }

    //itp.rekanan.getNama
    //itp.rekanan.getBioadata
    //rekanan/getdatarekanan
    //POST -> rekanan_id
    public function getDataRekanan() {
        $param = json_decode($this->request()->getBody(), true);
        $rekanan_id = $param['rekanan_id'];
        $builder = DB::table('rekanan');
        $builder->where('rekanan_id', '=', $rekanan_id);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get(['rekanan_id', 'nama_perusahaan', 'alamat', 'telepon', 'fax', 'email', 'website', 'kodepos', 'jenis_vendor' ])
        );
        $this->sendResponse();
    }

    //rekanan/getallnamarekanan
    //POST
    public function getAllNamaRekanan() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table('rekanan')->whereRaw("lower(nama_perusahaan) like lower(?)", array($param["nama_perusahaan"]));
        $builder->orderBy('nama_perusahaan', 'asc')->take(10);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get(['nama_perusahaan', 'username'])
        );
        $this->sendResponse();
    }

    private function sendActivationMail( $rekanan_id, $keterangan, $id_content, $action, $name = '' ){
        $mailer = new Mailer();
        $vendor = DB::table("rekanan")->where(["rekanan_id" => $rekanan_id])->first();
        //print_r( $vendor );

        if( $vendor != null ){
            if( $vendor->email != null && $vendor->email != '' ){
                $mailer->addRecipient( $vendor->email, $vendor->nama_perusahaan );
            }

            if( $vendor->email_pribadi != null && $vendor->email_pribadi != '' && $vendor->email_pribadi != $vendor->email ){
                $mailer->addRecipient( $vendor->email_pribadi, $vendor->nama_perusahaan );
            }
        }

        $ref = DB::table('mst_reference')->whereIn("ref_group_code", ["purchasing_id", "legal_id"])->get(["ref_value"]);
        $valRef = [];
        foreach( $ref as $key => $object ){
            array_push( $valRef, $object->ref_value );
        }

        $adminEmails = DB::table("pegawai")->whereIn("role_id", $valRef)->get(["email", "nama_pegawai"]);
        foreach( $adminEmails as $key => $object ){
            if( $object->email != '' ){
                $mailer->addCC( $object->email, $object->nama_pegawai );
            }
        }

        $mailParam = [];
        if( $action == "terima" ){
            $mailParam = [
                ['key' => 'prekanan', 'value' => $vendor->nama_perusahaan],
                ['key' => 'username', 'value' => $vendor->username]
            ];
        } else {
            $mailParam = [
                ['key' => 'prekanan', 'value' => $name],
                ['key' => 'keterangan', 'value' => $keterangan]
            ];
        }

        $ecc = new EmailConfController();
        $eccResult_a = $ecc->getMailContent($id_content, $mailParam);
        $mailBody_a = $eccResult_a["mailBody"];
        $mailSubject_a = $eccResult_a["mailSubject"];
        $mailResult = $mailer->sendEmail($mailSubject_a, $mailBody_a);

        if( $mailResult ){
            $this->resource = array(
                'status' => 200,
                'data' => "Sukses mengirim notifikasi email ke rekanan dan panitia"
            );
        } else {
            $this->resource = array(
                'status' => 304,
                'data' => "Sukses mengirim aktivasi data perusahaan tetapi gagal mengirim email ke panitia!"
            );
        }
    } // end sendActivationMail

    private function sendActivationMail2( $email, $email_pribadi, $nama_perusahaan, $keterangan, $id_content, $action ){
        $mailer = new Mailer();
        $mailer->addRecipient( $email, $nama_perusahaan );
        $mailer->addRecipient( $email_pribadi, $nama_perusahaan );

        $ref = DB::table('mst_reference')->whereIn("ref_group_code", ["purchasing_id", "legal_id"])->get(["ref_value"]);
        $valRef = [];
        foreach( $ref as $key => $object ){
            array_push( $valRef, $object->ref_value );
        }

        $adminEmails = DB::table("pegawai")->whereIn("role_id", $valRef)->get(["email", "nama_pegawai"]);
        foreach( $adminEmails as $key => $object ){
            if( $object->email != '' ){
                $mailer->addCC( $object->email, $object->nama_pegawai );
            }
        }

        $mailParam = [];
        if( $action == "terima" ){
            $mailParam = [
                ['key' => 'prekanan', 'value' => $nama_perusahaan],
                ['key' => 'username', 'value' => $email]
            ];
        } else {
            $mailParam = [
                ['key' => 'prekanan', 'value' => $nama_perusahaan],
                ['key' => 'keterangan', 'value' => $keterangan]
            ];
        }

        $ecc = new EmailConfController();
        $eccResult_a = $ecc->getMailContent($id_content, $mailParam);
        $mailBody_a = $eccResult_a["mailBody"];
        $mailSubject_a = $eccResult_a["mailSubject"];
        $mailResult = $mailer->sendEmail($mailSubject_a, $mailBody_a);

        if( $mailResult ){
            $this->resource = array(
                'status' => 200,
                'data' => "Sukses mengirim notifikasi email ke rekanan dan panitia"
            );
        } else {
            $this->resource = array(
                'status' => 304,
                'data' => "Sukses mengirim aktivasi data perusahaan tetapi gagal mengirim email ke panitia!"
            );
        }
    } // end sendActivationMail2

    //Mengambil Bidang Usaha Rekanan yang Sudah Terverifikasi
    public function BidangUsahaTerverifikasi(){
        $param = json_decode($this->request()->getBody(), true);
        $arr = array();
        $builder2 = DB::table('rekanan_ijinusaha')
                    ->select('kualifikasi','rekanan_ijinusaha.ijinusaha_id','rekanan_ijinusaha_bidusaha.id_bidang_usaha')
                    ->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id')
                    ->where('rekanan_id','=',$param['rekanan_id']);


        $builder = DB::table('rekanan')
                    ->select('rekanan_ijinusaha_bidusaha.id_bidang_usaha',
                    'mst_bidang_usaha.string_bidang_usaha',
                    'id',
                    'rekanan_ijinusaha_bidusaha.ijinusaha_id',
                    'mst_bidang_usaha.nama_bidang_usaha',
                    'rekanan_ijinusaha.jenis_ijin',
                    'rekanan_ijinusaha.kualifikasi',
                    'mst_bidang_usaha.level',
                    'level1.nama_bidang_usaha as level1bidangusaha',
                    'parent_bidang_usaha.nama_bidang_usaha as nama_parent_bidang_usaha')
                    ->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id')
                    ->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id')
                    ->join('mst_bidang_usaha','mst_bidang_usaha.id_bidang_usaha','=','rekanan_ijinusaha_bidusaha.id_bidang_usaha')
                    // ->join('mst_bidang_usaha as parent_bidang_usaha','parent_bidang_usaha.id_bidang_usaha','=','mst_bidang_usaha.parent_id')
                    // ->join('mst_bidang_usaha as level1','level1.parent_id')
                    ->leftjoin('mst_bidang_usaha as parent_bidang_usaha', function($join)
                         {
                             $join->on('parent_bidang_usaha.level', '=', DB::raw('2'));
                             $join->on('parent_bidang_usaha.id_bidang_usaha','=','mst_bidang_usaha.parent_id');
                         })
                    ->leftjoin('mst_bidang_usaha as level1', function($join)
                         {
                             $join->on('level1.parent_id', '=', DB::raw(0));
                             $join->on('level1.id_bidang_usaha','=','parent_bidang_usaha.parent_id');
                         })
                    ->where('rekanan.rekanan_id','=',$param['rekanan_id']);

        $buildercount = $builder->count();
        $builder = $builder->orderBy('rekanan_ijinusaha_bidusaha.id','asc')->skip($param["offset"])->take($param["limit"]);


        $builder = $builder->get();

        $take_kual = $builder2->where('jenis_ijin','=','SBU')->get();

        if(count($take_kual) != 0){
            $pemisahkoma = explode(',', $take_kual[0]->kualifikasi);
            // print_r($pemisahkoma);
            for ($b=0; $b < count($builder) ; $b++) {
                # code...
                for ($i=0; $i <count($pemisahkoma) ; $i++) {
                # code...
                    if($builder[$b]->ijinusaha_id == $take_kual[$i]->ijinusaha_id && $builder[$b]->id_bidang_usaha == $take_kual[$i]->id_bidang_usaha){
                        // echo '\n'.$pemisahkoma[$i];
                        // echo '\n'.$b;
                        $builder[$b]->kualifikasi = $pemisahkoma[$i];
                    }


                }
            }
        }



        // print_r($arr);

        // print_r($take_kual->get());

         $this->resource = array(
                'status' => 200,
                'data' => [$builder,$buildercount]
            );

        $this->sendResponse();

    }//Akhir Dari Hubungan Aku dan BidangUsahaTerverifikasi


    public function getForDownload(){
            $param = json_decode($this->request()->getBody(), true);

            $arr = array();
            $sbu = DB::table('rekanan_ijinusaha')
                        ->select('kualifikasi','id_bidang_usaha','rekanan_ijinusaha.ijinusaha_id')
                        ->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id')
                        ->orderby('rekanan_ijinusaha_bidusaha.id','asc');
                        // ->where('rekanan_ijinusaha.rekanan_id','=',245);
                        // ->where('rekanan_id','=',$param['rekanan_id']);


            $ijinusaha = DB::table('rekanan')
                        ->select('rekanan.rekanan_id','rekanan_ijinusaha_bidusaha.id_bidang_usaha',
                        'mst_bidang_usaha.string_bidang_usaha',
                        'mst_bidang_usaha.nama_bidang_usaha',
                        'rekanan_ijinusaha.jenis_ijin',
                        'rekanan_ijinusaha.kualifikasi',
                        'level1.nama_bidang_usaha as level1bidangusaha',
                        'parent_bidang_usaha.nama_bidang_usaha as level2bidangusaha',
                        'rekanan_ijinusaha.ijinusaha_id')
                        ->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id')
                        ->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id')
                        // ->join('mst_bidang_usaha','mst_bidang_usaha.id_bidang_usaha','=','rekanan_ijinusaha_bidusaha.id_bidang_usaha')
                        ->leftjoin('mst_bidang_usaha', function($join)
                        {
                            $join->on('mst_bidang_usaha.level', '=', DB::raw('3'));
                            $join->on('mst_bidang_usaha.id_bidang_usaha','=','rekanan_ijinusaha_bidusaha.id_bidang_usaha');
                        })
                        ->leftjoin('mst_bidang_usaha as parent_bidang_usaha', function($join)
                        {
                            $join->on('parent_bidang_usaha.level', '=', DB::raw('2'));
                            // 'parent_bidang_usaha.id_bidang_usaha','=','mst_bidang_usaha.parent_id'
                            $join->on(DB::raw('(parent_bidang_usaha.id_bidang_usaha = mst_bidang_usaha.parent_id OR parent_bidang_usaha.id_bidang_usaha = rekanan_ijinusaha_bidusaha.id_bidang_usaha)'),DB::raw(''),DB::raw(''));
                        })
                        ->leftjoin('mst_bidang_usaha as level1', function($join)
                        {
                            $join->on('level1.parent_id', '=', DB::raw(0));
                            $join->on('level1.id_bidang_usaha','=','parent_bidang_usaha.parent_id');
                        });

            // $ijinusaha = $ijinusaha->where('rekanan_ijinusaha.rekanan_id','=',245);
            $ijinusaha = $ijinusaha->orderBy('rekanan_ijinusaha_bidusaha.id','asc');
            // $ijinusaha = $ijinusaha->where('rekanan_ijinusaha.ijinusaha_id','=',1064);

            $ijinusaha = $ijinusaha->get();

            $take_kual = $sbu->where('jenis_ijin','=','SBU')->get();
            // print_r($take_kual);
           // print_r($take_kual);

            $arr_kual = [];

            // print_r($pemisahkoma);
            for ($s=0; $s < count($take_kual) ; $s++) {
                # code...
                $pemisahkoma = explode(',', $take_kual[$s]->kualifikasi);
                // $arr_id = ["id_bidang_usaha"=>$take_kual[$s]->id_bidang_usaha,"ijinusaha_id"=>$take_kual[$s]->ijinusaha_id];
                // $pemisahkoma = ;

                array_push($arr_kual, $pemisahkoma);
            }

            // print_r($arr_kual);

            // print_r($take_kual[73/2]);
            // print_r($ijinusaha);
            // return;

            $index = 0;
                for ($b=0; $b < count($ijinusaha) ; $b++) {
                # code...
                    for ($i=0; $i <count($arr_kual) ; $i++) {
                    # code...
                    // echo $i;

                        if($ijinusaha[$b]->id_bidang_usaha == $take_kual[$i]->id_bidang_usaha && $ijinusaha[$b]->ijinusaha_id == $take_kual[$i]->ijinusaha_id && $ijinusaha[$b]->jenis_ijin == 'SBU'){
                            // echo $i;
                            // echo $index;
                            if($i != 0){
                                // print_r($take_kual[$i]);
                                if($take_kual[$i]->ijinusaha_id != $take_kual[($i-1)]->ijinusaha_id )
                                {
                                        $index = 0;
                                }
                            }

                            for ($k=0; $k < count($arr_kual[$i]) ; $k++) {
                                // echo $i;
                                // print_r($arr_kual[$i]);
                                // echo $index;
                                $ijinusaha[$b]->kualifikasi = $arr_kual[$i][$index];

                            }
                            $index += 1;


                        }


                    // echo $index;
                    // echo $i;

                    }
                }
            // print_r($ijinusaha);
            // print_r($pemisahkoma);
            $status = intval($param["status"]);
            // $orderColumn = "rekanan.created_date";
            // $orderType = "desc";
            switch ($status) {
                case 0: $orderColumn = "bisa_mengubah_data";
                        $orderType = "asc";
                        break;
                case 1: $orderColumn = "rekanan.created_date";
                        $orderType = "desc";
                        break;
                case 2: $orderColumn = "activated_date";
                        $orderType = "desc";
                        break;
                case 3: $orderColumn = "verified_send_date";
                        $orderType = "desc";
                        break;
                case 4: $orderColumn = "verified_date";
                        $orderType = "desc";
                        break;
                case 5: $orderColumn = "rekanan.created_date";
                        $orderType = "desc";
                        break;
                case 6: $orderColumn = "verified_send_date";
                        $orderType = "desc";
                        break;
                //penambahan status baru untuk penyedia terverifikasi dari no 7 - 10 by avid

                case 7: $orderColumn = "verified_date";
                        $orderType = "desc";
                        break;
                case 8:
                        if(array_key_exists('ini_penyedia', $param)){
                            $orderColumn = "nama_perusahaan";
                            $orderType = "asc";

                        }else{
                            $orderColumn = "is_verified";
                            $orderType = "desc";
                        }
                        break;
                case 9:
                        if(array_key_exists('ini_penyedia', $param)){
                            $orderColumn = "kode_rekanan";
                            $orderType = "asc";

                        }else{
                             $orderColumn = "is_verified";
                            $orderType = "desc";
                        }
                        break;
                case 10:
                         if(array_key_exists('ini_penyedia', $param)){
                                $orderColumn = "username";
                                $orderType = "asc";

                         }else{
                                $orderColumn = "is_verified";
                                $orderType = "desc";
                         }
                        break;
                case 11:
                        if(array_key_exists('ini_penyedia', $param)){
                                $orderColumn = "username";
                                $orderType = "asc";
                        }else{
                                $orderColumn = 'bisa_mengubah_data';
                                $orderType = "asc";
                        }
                        break;

                default: break;
            }
            $data = DB::table("rekanan")
                        ->select(DB::raw("rekanan.*,pegawai.nama_pegawai, case when is_activated ='t' and (is_verified = 'f' or is_verified is null) and (tolak_verifikasi = 'f' or tolak_verifikasi is null ) and trigger_verified = 't' then 'atas' else 'bawah' end as status_rek,kota_nama,capitol"))
                        ->from('rekanan');

            $data->leftJoin('badan_usaha','badan_usaha.badan_usaha_id','=','rekanan.jenis_perusahaan');
            $data->join('mst_country','mst_country.id_country','=','rekanan.country_id');
            $data->leftJoin('mst_kota','mst_kota.kota_id','=','rekanan.kota_id');
            $data->leftjoin('pegawai','pegawai.pegawai_id','=','rekanan.updated_by_admin');



            //kondisi ketika dia berada di page penyedia terverifikasi
            if(array_key_exists('ini_penyedia', $param)){
                $data->where('is_verified','=',true);
            }

            $data->whereIn('rekanan_id',function($builder){

            $param = json_decode($this->request()->getBody(), true);
            $builder->select('rekanan.rekanan_id');
            $builder->from('rekanan');

            $status = intval($param["status"]);
            $company = $param["company"];

            switch ($status) {
                case 0:

                    $builder->where('rekanan.created_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('rekanan.created_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }
                    $builder->whereNotNull('rekanan.is_activated');
                    if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                        $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                        $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');


                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                            if($var != ""){
                                $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));
                            }

                    }
                    $orderColumn = "bisa_mengubah_data";
                    $orderType = "asc";
                    break;
                case 1:
                    $builder->where(function($query) {
                        $query->whereNull('rekanan.is_activated');
                        $query->orWhere('rekanan.is_activated', false);
                    });
                    $builder->where('is_registered', true);
                    $builder->where('rekanan.created_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('rekanan.created_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }
                    break;
                case 2:
                    $builder->where(function($query) {
                        $query->where(function($query1) {
                            $query1->where('rekanan.is_activated', true);
                            $query1->whereNull('is_verified');
                            $query1->whereNull('trigger_verified');
                            $query1->whereNull('tolak_verifikasi');
                        });
                        $query->orWhere(function($query2) {
                            $query2->where('rekanan.is_activated', true);
                            $query2->where('is_verified', false);
                            $query2->whereNull('trigger_verified');
                        });
                        $query->orWhere(function($query3) {
                            $query3->where('rekanan.is_activated', true);
                            $query3->where('is_verified', false);
                            $query3->where('trigger_verified', false);
                            $query3->where('tolak_verifikasi', false);
                        });
                    });




                    $builder->where('activated_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('activated_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }

                    if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                        $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                        $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');

                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                        if($var != ""){
                            $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));

                        }

                    }

                    $orderColumn = "activated_date";
                    $orderType = "desc";
                    break;
                case 3:
                    $builder->where(function($query) {
                        $query->where(function($query1) {
                            $query1->where('rekanan.is_activated', true);
                            $query1->where(function($sql) {
                                $sql->where('is_verified', null);
                                $sql->orWhere('is_verified', '<>', true);
                            });
                            $query1->where('trigger_verified', true);
                        });
                    });




                    $builder->where('verified_send_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('verified_send_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }

                    if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                        $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                        $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');


                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                            if($var != ""){
                                $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));

                            }

                    }

                    $orderColumn = "verified_send_date";
                    $orderType = "desc";
                    break;
                case 4:

                    $builder->where('is_verified', true);
                    $builder->where('rekanan.is_activated', true);
                    $builder->where('verified_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('verified_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }

                    if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                        $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                        $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');


                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                            if($var != ""){
                                $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));
                            }

                    }


                    $orderColumn = "verified_date";
                    $orderType = "desc";
                    break;
                case 5:
                    $orderColumn = "rekanan.created_date";
                    $orderType = "desc";
                    $builder->where('rekanan.rekanan_id', $param['rekanan_id']);
                    break;
                case 6:
                    $builder->where(function($query) {
                        $query->where(function($query1) {
                            $query1->where('rekanan.is_activated', true);
                            $query1->where(function($sql) {
                                $sql->where('is_verified', false);
                            });
                            $query1->where('trigger_verified', false);
                            $query1->where('tolak_verifikasi', true);
                        });
                    });
                    $builder->where(function($query) use ($param) {
                        $query->where(function($query1) use ($param){
                            $query1->where('tolak_date', '>=', $param["waktu_mulai1"]);
                            $query1->where('tolak_date', '<=', $param["waktu_mulai2"]);
                        });
                        $query->orWhere(function($query1) {
                            $query1->whereNull('tolak_date');
                        });
                    });
                    if(array_key_exists('cariBer', $param)){
                        if($param['cariBer'] == 2){
                            $builder->where('nama_perusahaan', 'ilike', $company);
                        }else if($param['cariBer'] == 3){
                            $builder->where('kode_rekanan', 'ilike', $company);

                        }else if($param['cariBer'] == 4){
                            $builder->where('username', 'ilike', $company);

                        }
                    }else{
                        $builder->where('nama_perusahaan', 'ilike', $company);
                    }

                    if(array_key_exists('rekanan', $param) && $param['id_bu'] != ''){
                        $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                        $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');


                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                            if($var != ""){
                                $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));

                            }

                    }


                    $orderColumn = "tolak_date";
                    $orderType = "desc";
                    break;
                //penambahan status baru untuk penyedia terverifikasi dari no 7 - 10 by avid
                case 7:

                    $builder->where('rekanan.created_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('verified_date', '<=', $param["waktu_mulai2"]);
                    if(array_key_exists('ini_penyedia', $param)){
                        $builder->where('is_verified','=',true);

                    }
                    break;
                case 8:
                    $builder->where('nama_perusahaan', 'ilike', $company);
                    if(array_key_exists('ini_penyedia', $param)){
                        $builder->where('is_verified','=',true);

                    }
                    break;
                case 9:
                    $builder->where('kode_rekanan', 'ilike', $company);
                    if(array_key_exists('ini_penyedia', $param)){
                        $builder->where('is_verified','=',true);
                    }
                    break;
                case 10:
                    $builder->where('username', 'ilike', $company);
                    if(array_key_exists('ini_penyedia', $param)){
                        $builder->where('is_verified','=',true);

                    }
                    break;
                case 11:
                    $builder->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id');
                    $builder->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id');
                    if(array_key_exists('rekanan', $param)){
                    $builder->where('rekanan.created_date', '>=', $param["waktu_mulai1"]);
                    $builder->where('rekanan.created_date', '<=', $param["waktu_mulai2"]);

                    }


                    if($param['id_bu'] != ''){
                         $var = "";
                            for ($i=0; $i < count($param['id_bu']) ; $i++) {
                                if($var == ""){
                                    $var = $param['id_bu'][$i]['id_bidang_usaha'];
                                }else{
                                    $var .= ','.$param['id_bu'][$i]['id_bidang_usaha'];

                                }
                            }
                            if($var != ""){
                                $builder->whereIn('rekanan_ijinusaha_bidusaha.id_bidang_usaha',explode(',', $var));

                            }
                     }
                     if(array_key_exists('ini_penyedia', $param)){
                     $builder->where('is_verified','=',true);
                     }
                     $orderColumn = "username";
                     $orderType = "asc";
                    break;


                default:
                    break;
            }


            });

            $size = $data->count();
            $data = $data->get();

            // $data = $builder->orderBy('status_rek', 'asc')->get();

            for ($i=0; $i <count($ijinusaha) ; $i++) {
                $ijinusaha[$i]->nama_bidang_usaha = $ijinusaha[$i]->nama_bidang_usaha;
                $ijinusaha[$i]->kualifikasi = strtoupper($ijinusaha[$i]->kualifikasi);

            }
            // print_r($ijinusaha);

            for ($i=0; $i <count($data) ; $i++) {

                    $data[$i]->nama_perusahaan = strtoupper($data[$i]->nama_perusahaan);
                    $data[$i]->kode_rekanan = strtoupper($data[$i]->kode_rekanan);
                    $data[$i]->capitol = strtoupper($data[$i]->capitol);
                    $data[$i]->nama = strtoupper($data[$i]->nama);


                    $data[$i]->children = [];

                    for ($n=0; $n <count($ijinusaha) ; $n++) {
                        # code...
                        // print_r($ijinusaha);
                        if($ijinusaha[$n]->rekanan_id == $data[$i]->rekanan_id){

                            array_push($data[$i]->children, $ijinusaha[$n]);
                        }
                    }

            }

             $this->resource = array(
                'status' => 200,
                'data' => [$data]
            );

            $this->sendResponse();
    }

    public function downloadExcel(){

            $param = json_decode($this->request()->getBody(), true);
            // ini_set('max_execution_time', 300);

            // print_r($ijinusaha);

            $data = $param['data'];
            // $helper = new Sample();
            // if ($helper->isCli()) {
            //     $helper->log('This example should only be run from a Web Browser' . PHP_EOL);

            //     return;
            // }
            // Create new Spreadsheet object
            $spreadsheet = new Spreadsheet();

            $sheet = $spreadsheet->getActiveSheet();

            // Set document properties
            $spreadsheet->getProperties()->setCreator('Avid Aditya')
                ->setLastModifiedBy('Avid Aditya')
                ->setTitle('Office 2007 XLSX Test Document')
                // ->setSubject('Office 2007 XLSX Test Document')
                // ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
                ->setKeywords('office 2007 openxml php');
                // ->setCategory('Test result file');

            $sheet->getStyle("A2:L2")->getFont()->setBold(true);
            $sheet->getStyle("A4:L4")->getFont()->setBold(true);
            $sheet->getStyle("K5:L5")->getFont()->setBold(true);
            // $spreadsheet->getActiveSheet()->getStyle('A4:L4')->getBorders()->getAllBorders()->setBorderStyle($styleArray);
            // $spreadsheet->getActiveSheet()->getStyle('K5:L5')->getBorders()->getAllBorders()->setBorderStyle($styleArray);
            $sheet->mergeCells('A2:L2');
            $sheet->getStyle('A2:L2')->getAlignment()->applyFromArray(
                array(  'wrap' => true,
                        'horizontal' => 'center',
                        'vertical-align' => 'middle')
            );


            $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A2',  'Data Penyedia');


             $sheet->getStyle('A4:L4')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

            $sheet->getStyle('A5:L5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

            $sheet->getStyle('K5:L5')->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);



             $sheet->getStyle('A4:L4')->getAlignment()->applyFromArray(
                array(  'wrap' => true,
                        'horizontal' => 'center',
                        'vertical-align' => 'middle',

                    )
            );

            $sheet->getColumnDimension('A')->setWidth(4);
            $sheet->getColumnDimension('B')->setAutoSize(true);
            $sheet->getColumnDimension('C')->setAutoSize(true);
            $sheet->getColumnDimension('D')->setAutoSize(true);
            // $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $sheet->getColumnDimension('E')->setWidth(15);
            $sheet->getColumnDimension('F')->setAutoSize(true);
            $sheet->getColumnDimension('G')->setAutoSize(true);
            $sheet->getColumnDimension('H')->setAutoSize(true);
            $sheet->getColumnDimension('I')->setAutoSize(true);
            $sheet->getColumnDimension('J')->setAutoSize(true);
            $sheet->getColumnDimension('K')->setAutoSize(true);
            $sheet->getColumnDimension('L')->setAutoSize(true);
            $sheet->mergeCells('A4:A5');
            $sheet->mergeCells('B4:B5');
            $sheet->mergeCells('C4:C5');
            $sheet->mergeCells('D4:D5');
            $sheet->mergeCells('E4:E5');
            $sheet->mergeCells('F4:F5');
            $sheet->mergeCells('G4:G5');
            $sheet->mergeCells('H4:H5');
            $sheet->mergeCells('I4:I5');
            $sheet->mergeCells('J4:J5');
            $sheet->mergeCells('K4:L4');






// ->mergeCells('J4:J5');
            // Add some data
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A4', 'No')
                ->setCellValue('B4', 'Nama Perusahaan')
                ->setCellValue('C4', 'Kode Penyedia')
                ->setCellValue('D4', 'Email Perusahaan (Username)')
                ->setCellValue('E4', 'Lokasi')
                ->setCellValue('F4', 'Nama Kontak')
                ->setCellValue('G4', 'No Kontak')
                ->setCellValue('H4', 'Status')
                ->setCellValue('I4', 'Tanggal Daftar')
                ->setCellValue('J4', 'Tanggal Update')
                ->setCellValue('K4', 'Bidang Usaha')
                ->setCellValue('K5', 'Nama Bidang Usaha')
                ->setCellValue('L5', 'Kualifikasi');




            // Miscellaneous glyphs, UTF-8
                // echo count($data);
                // print_r($data);


                $lanjut = 0;

                for ($i=0; $i <count($data) ; $i++) {
                    # code...
                    if($i != 0){

                            $lanjut = $lanjut+1;


                    }else{
                        // echo "mask sini awal";
                            $lanjut = $lanjut +6;
                    }

                    // echo '<br>'.$lanjut;
                    $varnya = '';
                        if($data[$i]['is_activated'] == '1' && $data[$i]['is_verified'] == '1' && ($data[$i]['tolak_verifikasi'] == '0' || $data[$i]['tolak_verifikasi'] == null)){
                            $varnya = 'Terverifikasi';
                        }else if($data[$i]['is_activated'] == '1' && (($data[$i]['is_verified'] == '0' || $data[$i]['is_verified'] == null) && ($data[$i]['tolak_verifikasi'] == '0' || $data[$i]['tolak_verifikasi'] == null) && $data[$i]['trigger_verified'] == '1')){
                            $varnya = 'Belum Diverifikasi';
                        }else if($data[$i]['is_activated'] == '1' && (($data[$i]['is_verified'] == '0' || $data[$i]['is_verified'] == null) && $data[$i]['trigger_verified'] == '0' && $data[$i]['tolak_verifikasi'] == '1')){
                            $varnya = 'Koreksi';
                        }else if($data[$i]['is_activated'] == '1' && (($data[$i]['is_verified'] == '0' || $data[$i]['is_verified'] == null) && ($data[$i]['trigger_verified'] == '0' || $data[$i]['trigger_verified'] == null) && ($data[$i]['tolak_verifikasi'] == '0' || $data[$i]['tolak_verifikasi'] == null))){
                            $varnya = 'Teraktivasi';
                        }else {
                            $varnya = 'Belum Diaktivasi';
                        }

                    $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A'.$lanjut,  $i+1)
                    ->setCellValue('B'.$lanjut, $data[$i]['nama_perusahaan'])
                    ->setCellValue('C'.$lanjut, $data[$i]['kode_rekanan'])
                    ->setCellValue('D'.$lanjut, $data[$i]['username'])
                    ->setCellValue('E'.$lanjut, $data[$i]['kota_nama'])
                    ->setCellValue('F'.$lanjut, $data[$i]['nama'])
                    ->setCellValue('G'.$lanjut, $data[$i]['no_ponsel'])
                    ->setCellValue('H'.$lanjut, $varnya)
                    ->setCellValue('I'.$lanjut, $data[$i]['created_date'])
                    ->setCellValue('J'.$lanjut, $data[$i]['verified_send_date']);

                    if(count($data[$i]['children']) == 0){

                        $spreadsheet->setActiveSheetIndex(0)
                        ->setCellValue('K'.$lanjut, '-')
                        ->setCellValue('L'.$lanjut, '-');


                    }

                    $sheet->getStyle('A'.$lanjut.':J'.$lanjut)->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                    $sheet->getStyle('K'.$lanjut.':L'.$lanjut)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                // echo $lanjut
                    for ($f=0; $f <count($data[$i]['children']) ; $f++) {
                        # code...
                        $sekarangno = $lanjut+$f;

                        $spreadsheet->setActiveSheetIndex(0)

                        ->setCellValue('K'.$sekarangno, $data[$i]['children'][$f]['level1bidangusaha'].' - '.$data[$i]['children'][$f]['level2bidangusaha'].' - '.$data[$i]['children'][$f]['nama_bidang_usaha'])
                        ->setCellValue('L'.$sekarangno,$data[$i]['children'][$f]['kualifikasi']);



                        $sheet->getStyle('K'.$sekarangno.':L'.$sekarangno)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);


                            // echo '<br>'.$lanjut;
                        // echo '<br>'.count($data[$i]->children);
                            if(($f+1) == count($data[$i]['children'])){

                                $lanjut = ($lanjut +count($data[$i]['children'])-1);
                                // echo '<br>'.$lanjut;

                            }
                    }

                }

            // Rename worksheet
            $sheet->setTitle('Rekanan');

            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $spreadsheet->setActiveSheetIndex(0);

            // Redirect output to a client’s web browser (Xlsx)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            // header('Content-Disposition: attachment;filename="01simple.xlsx"');
            // header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            // header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            // header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');


        return;
    }

    public function getDetailKoreksi(){
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table('rekanan')
        ->where('rekanan_id', '=', $param['rekanan_id'])->get();

        $this->resource = array(
            'status' => 200,
            'data' => $builder
        );
        $this->sendResponse();
    }

    public function getDetailBidangUsaha(){
        $param = json_decode($this->request()->getBody(), true);

        $arr = array();
        $builder2 = DB::table('rekanan_ijinusaha')
                    ->select('kualifikasi','rekanan_ijinusaha.ijinusaha_id','rekanan_ijinusaha_bidusaha.id_bidang_usaha')
                    ->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id')
                    ->where('rekanan_id','=',$param['rekanan_id']);


        $builder = DB::table('rekanan')
                    ->select('rekanan_ijinusaha_bidusaha.id_bidang_usaha',
                    'mst_bidang_usaha.string_bidang_usaha',
                    'id',
                    'rekanan_ijinusaha_bidusaha.ijinusaha_id',
                    'mst_bidang_usaha.nama_bidang_usaha',
                    'rekanan_ijinusaha.jenis_ijin',
                    'rekanan_ijinusaha.kualifikasi',
                    'mst_bidang_usaha.level',
                    'level1.nama_bidang_usaha as level1bidangusaha',
                    'parent_bidang_usaha.nama_bidang_usaha as nama_parent_bidang_usaha')
                    ->join('rekanan_ijinusaha','rekanan_ijinusaha.rekanan_id','=','rekanan.rekanan_id')
                    ->join('rekanan_ijinusaha_bidusaha','rekanan_ijinusaha_bidusaha.ijinusaha_id','=','rekanan_ijinusaha.ijinusaha_id')
                    ->join('mst_bidang_usaha','mst_bidang_usaha.id_bidang_usaha','=','rekanan_ijinusaha_bidusaha.id_bidang_usaha')
                    // ->join('mst_bidang_usaha as parent_bidang_usaha','parent_bidang_usaha.id_bidang_usaha','=','mst_bidang_usaha.parent_id')
                    // ->join('mst_bidang_usaha as level1','level1.parent_id')
                    ->leftjoin('mst_bidang_usaha as parent_bidang_usaha', function($join)
                         {
                             $join->on('parent_bidang_usaha.level', '=', DB::raw('2'));
                             $join->on('parent_bidang_usaha.id_bidang_usaha','=','mst_bidang_usaha.parent_id');
                         })
                    ->leftjoin('mst_bidang_usaha as level1', function($join)
                         {
                             $join->on('level1.parent_id', '=', DB::raw(0));
                             $join->on('level1.id_bidang_usaha','=','parent_bidang_usaha.parent_id');
                         })
                    ->where('rekanan.rekanan_id','=',$param['rekanan_id']);

        $buildercount = $builder->count();
        $builder = $builder->orderBy('rekanan_ijinusaha_bidusaha.id','asc')->skip($param["offset"])->take($param["limit"]);


        $builder = $builder->get();

        $take_kual = $builder2->where('jenis_ijin','=','SBU')->get();

        if(count($take_kual) != 0){
            $pemisahkoma = explode(',', $take_kual[0]->kualifikasi);
            // print_r($pemisahkoma);
            for ($b=0; $b < count($builder) ; $b++) {
                # code...
                for ($i=0; $i <count($pemisahkoma) ; $i++) {
                # code...
                    if($builder[$b]->ijinusaha_id == $take_kual[$i]->ijinusaha_id && $builder[$b]->id_bidang_usaha == $take_kual[$i]->id_bidang_usaha){
                        // echo '\n'.$pemisahkoma[$i];
                        // echo '\n'.$b;
                        $builder[$b]->kualifikasi = $pemisahkoma[$i];
                    }


                }
            }
        }



        // print_r($arr);

        // print_r($take_kual->get());

         $this->resource = array(
                'status' => 200,
                'data' => [$builder,$buildercount]
            );

        $this->sendResponse();
    }

}
