<?php

namespace Controller\Admin;

use \Controller\BaseController as BaseController;
use Controller\Admin\UserLogController as UserLogController;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon as Carbon;

require('../public/app/config.php');

class SptpdController extends BaseController {
    //itp.berita.select -> POST berita/select
    public function select() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("sptpd_detail")->join("npwpd","npwpd.id_npwpd","=","sptpd_detail.id_npwpd")->where("sptpd_detail.id_npwpd","=",$param['id'])->orderby("sptpd_detail.flag")->get();

        $this->resource = array(
            'status' => 200,
            'data' => $builder
        );
        $this->sendResponse();
    }

    public function selectAll() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("sptpd")->join("npwpd","npwpd.id_npwpd","=","sptpd.id_npwpd")->join("data_pajak","data_pajak.id_data_pajak","=","npwpd.id_data_pajak")->whereOr("sptpd.status_sptpd","=","Proses Verifikasi")->whereOr("sptpd.status_sptpd","=","Verifikasi")->skip($param['offset'])->take($param['limit'])->orderby("sptpd.status_sptpd","ASC")->get();
        
        $builderCount = DB::table("sptpd")->join("npwpd","npwpd.id_npwpd","=","sptpd.id_npwpd")->where("sptpd.status_sptpd","=","'Proses Verifikasi'")->count();

        $this->resource = array(
            'status' => 200,
            'data' => ['list'=>$builder, 'count'=>$builderCount]
        );
        $this->sendResponse();
    }

    public function selectNpwpdById() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("npwpd")->join("data_pajak","data_pajak.id_data_pajak","=","npwpd.id_data_pajak")->where("npwpd.id_data_pajak","=",$param['id']);

        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    public function selectSptpdById() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("sptpd_detail")->join("npwpd","npwpd.id_npwpd","=","sptpd_detail.id_npwpd")->where("sptpd_detail.id_sptpd","=",$param['id'])->join("data_pajak","data_pajak.id_data_pajak","=","npwpd.id_data_pajak")->join("user_pajak","user_pajak.id_user_pajak","=","npwpd.id_user_pajak")->get();

        $this->resource = array(
            'status' => 200,
            'data' => $builder
        );
        $this->sendResponse();
    }

    public function delete() {
        $param = json_decode($this->request()->getBody(), true);
        // print_r($param['data']['jenis_usaha_pajak']);
        if($param['data']['jenis_usaha_pajak'] == 'Hotel'){
            $builderDelDet = DB::table("layanan_hotel")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();
        }else if ($param['data']['jenis_usaha_pajak'] == 'Parkir'){
            $builderDelDet = DB::table("layanan_parkir")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();
        }else if ($param['data']['jenis_usaha_pajak'] == 'Restaurant'){
            $builderDelDet = DB::table("layanan_resto")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();
        }else if ($param['data']['jenis_usaha_pajak'] == 'Hiburan'){
            $builderDelDet = DB::table("layanan_hiburan")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();
        };

        $builderDelNpwpd = DB::table("npwpd")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();

        $builderDelDataPajak = DB::table("data_pajak")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();

        $this->resource = array(
            'status' => 200,
            'data' => 'Data berhasil di hapus'
        );
        $this->sendResponse();
    }

    public function approve() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = [
            "flag" => '3',
            "status_sptpd" => 'Verifikasi',
            "no_sptpd" => date('m'). '.' . date('Y'). '.' . date('d') . '.' . $param['data'] . '.' . $param['id_data_pajak']
        ];
            // print_r(Carbon::now());
        $builder = DB::table('sptpd')->where("id_sptpd","=",$param['data'])->update($builder);

        $builderSkpd = [
            "id_sptpd" => $param['data'],
            "status_skpd" => "Draft",
            "nomor_skpd" => "SK". date('m'). '.' . date('Y'). '.' . date('d') . '.' . $param['data']
        ];
        $builder = DB::table('skpd')->where("id_sptpd","=",$param['data'])->insert($builderSkpd);

        $this->resource = array(
            'status' => 200,
            'data' => 'Data berhasil di hapus'
        );
        $this->sendResponse();
    }

    public function reject() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = [
            "flag" => '2',
            "status_sptpd" => 'Reject',
            "alasan_reject_sptpd" => $param['alasan']
        ];
            // print_r(Carbon::now());
        $builder = DB::table('sptpd')->where("id_sptpd","=",$param['data'])->update($builder);

        $this->resource = array(
            'status' => 200,
            'data' => 'Data berhasil di hapus'
        );
        $this->sendResponse();
    }

    public function updateFlag() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = [
            "flag" => $param['flag']
        ];
        
        $builder = DB::table('sptpd_detail')->where("id_detil_sptpd","=",$param['id'])->update($builder);
        
        $this->resource = array(
            'status' => 200,
            'data' => 'Data berhasil di reject'
        );
        $this->sendResponse();
    }

    public function deleteSptpd() {
        $param = json_decode($this->request()->getBody(), true);
        
        $builder = DB::table('sptpd_detail')->where("id_detil_sptpd","=",$param['id'])->delete();
        
        $this->resource = array(
            'status' => 200,
            'data' => 'Data berhasil di reject'
        );
        $this->sendResponse();
    }

    public function updateVerifikasi() {
        $param = json_decode($this->request()->getBody(), true);
        $select = DB::table('sptpd_detail')->where("id_npwpd","=",$param['id'])->where("flag","=",2)->where("status","=","Draft")->get();
        // print_r($select);
        if($select){
            $builder = [
                "status" => 'Proses Verifikasi'
            ];
            
            $builder = DB::table('sptpd_detail')->where("id_npwpd","=",$param['id'])->where("flag","=",2)->where("status","=",'Draft')->update($builder);
    
            $selectSptpd = DB::table("sptpd")->orderby("sptpd.id_sptpd","desc")->take(1)->get();
            if($selectSptpd){
                $urut = $selectSptpd[0]->id_sptpd + 1;
            }else{
                $urut = 1;
            }

            $temSelect = DB::select("Select sptpd_detail.id_npwpd, sum(sptpd_detail.pajak_terutang) from sptpd_detail where sptpd_detail.id_npwpd = '". $param['id'] ."' and sptpd_detail.flag = 2 and sptpd_detail.status = 'Proses Verifikasi' group by sptpd_detail.id_npwpd");
            // print_r($temSelect[0]->sum);

            $timestamp = strtotime(Carbon::now());

            $sptpd = [
                "id_npwpd" => $param['id'],
                "id_sptpd" => $urut,
                // "no_sptpd" => date('m'). '.' . date('Y'). '.' . date('d') . '.' . $urut . '.' . $select[0]->id_data_pajak, 
                "tanggal_sptpd" => Carbon::now(),
                "jenis_pajak_sptpd" => $param['jenis'],
                // "presentase_sptpd" => $param['data']['prosentase'],
                // "omset_dasar_sptpd" => $param['data']['omset'],
                "total_sptpd" => $temSelect[0]->sum,
                "jenis_input_sptpd" => $param['jenis_input'],
                "status_sptpd" => "Proses Verifikasi",
                "bulan" => date('m', $timestamp)           
                // "validasi_sptpd" =>  
            ];
    
            $save_sptpd = DB::table('sptpd')->insert($sptpd);
    
            $sptpd_detail = [
                "id_sptpd" => $urut
            ];
            $save_sptpd_detail = DB::table('sptpd_detail')->where("flag","=",2)->where("id_npwpd","=",$param['id'])->update($sptpd_detail);

            $this->resource = array(
                'status' => 200,
                'data' => 'Data berhasil di reject'
            );
            $this->sendResponse();
        }else{
            $this->resource = array(
                'status' => 300,
                'data' => 'Pilih Data terlebih dahulu'
            );
            $this->sendResponse();
        }
        
        
        
    }

    public function insert() {
        $param = json_decode($this->request()->getBody(), true);
        $select = DB::table("data_pajak")->join("npwpd","npwpd.id_data_pajak","=","data_pajak.id_data_pajak")->where("npwpd.id_npwpd","=",$param['id'])->get();

        $selectSptpd = DB::table("sptpd")->orderby("sptpd.id_sptpd","desc")->take(1)->get();
        $timestamp = strtotime($param['data']['date_new']);
        if($selectSptpd){
            $urut = $selectSptpd[0]->id_sptpd + 1;
        }else{
            $urut = 1;
        }

        // $sptpd = [
        //     "id_npwpd" => $select[0]->id_npwpd,
        //     // "no_sptpd" => date('m'). '.' . date('Y'). '.' . date('d') . '.' . $urut . '.' . $select[0]->id_data_pajak, 
        //     "tanggal_sptpd" => Carbon::now(),
        //     "jenis_pajak_sptpd" => $select[0]->jenis_usaha_pajak,
        //     "presentase_sptpd" => $param['data']['prosentase'],
        //     "omset_dasar_sptpd" => $param['data']['omset'],
        //     "total_sptpd" => $param['data']['pajak_terutang'],
        //     "jenis_input_sptpd" => $param['jenis_input'],
        //     "status_sptpd" => "Draft",
        //     "bulan" => date('m', $timestamp)           
        //     // "validasi_sptpd" =>  
        // ];

        // $save_sptpd = DB::table('sptpd')->insert($sptpd);
        // print_r(date('d', $timestamp));
        if($param['jenis_input'] == 'Bulanan'){
            $bulan = $param['data']['periode'];

            $sptpd_detail = [
                // "id_sptpd" => $urut,
                "tanggal_detil_sptpd" => date('d', $timestamp),
                "bulan_detil_sptpd" => date('m', $timestamp),
                "tahun_detil_sptpd" => date('y', $timestamp),
                "omset_detil_sptpd" => $param['data']['omset'],
                "keterangan_detil_sptpd" => $param['data']['keterangan'],
                // "bukti_omset_sptpd" => ,
                "is_bulanan_sptpd" => 2,
                "id_npwpd" => $select[0]->id_npwpd,
                "status" => "Draft",
                "pajak_terutang" => $param['data']['pajak_terutang'],
            ];
        }else{
            // $bulan = $param['data']['date_new'];
            $sptpd_detail = [
                // "id_sptpd" => $urut,
                "tanggal_detil_sptpd" => date('d', $timestamp),
                "bulan_detil_sptpd" => date('m', $timestamp),
                "tahun_detil_sptpd" => date('y', $timestamp),
                "omset_detil_sptpd" => $param['data']['omset'],
                "keterangan_detil_sptpd" => $param['data']['keterangan'],
                // "bukti_omset_sptpd" => ,
                "is_bulanan_sptpd" => 1,
                "id_npwpd" => $select[0]->id_npwpd,
                "status" => "Draft",
                "pajak_terutang" => $param['data']['pajak_terutang'],
            ];
        }

        $save_sptpd_detail = DB::table('sptpd_detail')->insert($sptpd_detail);

        // $sptpd = [
        //     "id_npwpd" => $select[0]->id_npwpd,
        //     // "no_sptpd" => date('m'). '.' . date('Y'). '.' . date('d') . '.' . $urut . '.' . $select[0]->id_data_pajak, 
        //     "tanggal_sptpd" => Carbon::now(),
        //     "jenis_pajak_sptpd" => $select[0]->jenis_usaha_pajak,
        //     "presentase_sptpd" => $param['data']['prosentase'],
        //     "omset_dasar_sptpd" => $param['data']['omset'],
        //     "total_sptpd" => $param['data']['pajak_terutang'],
        //     "jenis_input_sptpd" => $param['jenis_input'],
        //     "status_sptpd" => "Draft",
        //     "bulan" => date('m', $timestamp)           
        //     // "validasi_sptpd" =>  
        // ];

        // $save_sptpd = DB::table('sptpd')->update($sptpd);

        $this->resource = array(
            'status' => 200,
            'data' => 'Data berhasil di simpan'
        );
        $this->sendResponse();
    }

    // public function insert() {
    //     $param = json_decode($this->request()->getBody(), true);
    //     $user_pajak = [
    //         "warga_negara_user_pajak" => $param['data1']['kewarganegaraan'],
    //         "alamat_user_pajak" => $param['data1']['alamat'],
    //         "bukti_identitas_user_pajak" => $param['data1']['bukti_identitas_user_pajak'],
    //         "pemilik_user_pajak" => $param['data1']['nama'],
    //     ];
    //     $save_user_pajak = DB::table('user_pajak')->where("id_user_pajak","=",$param['userID'])->update($user_pajak);
        
    //     if($param['data2']['nama_bidang_usaha'] == 'Lainnya'){
    //         $bidang_usaha = $param['data2']['Lainnya'];
    //     };

    //     $data_pajak = [
    //         "nama_usaha_data_pajak" => $param['data2']['nama_badan_usaha'],
    //         "jenis_usaha_pajak" => $param['data1']['nama_badan_usaha'],
    //         "no_pbb_data_pajak" => $param['data3']['no_pbb'],
    //         "no_izin_data_pajak" => $param['data3']['no_izin_usaha'],
    //         "nama_izin_data_pajak" => $param['data3']['nama_izin_usaha'],
    //         "alamat_data_pajak" => $param['data2']['alamat_usaha'],
    //         "rt_rw_data_pajak" => $param['data2']['rt'],
    //         "kecamatan_data_pajak" => $param['data2']['kecamatan'],
    //         "kelurahan_data_pajak" => $param['data2']['kelurahan_usaha'],
    //         "kabupaten_data_pajak" => $param['data2']['kabupaten_usaha'],
    //         "kode_pos_data_pajak" => $param['data2']['kode_pos'],
    //         "telp_data_pajak" => $param['data2']['telp_usaha'],
    //         "fax_data_pajak" => $param['data2']['fax_usaha'],
    //         "email_data_pajak" => $param['data2']['email_usaha'],
    //         "tanggal_perkiraan_usaha" => $param['data3']['datePerkiraan'],
    //         "bidang_usaha_pajak" => $param['data2']['nama_bidang_usaha'],            
    //         "bukti_akta_data_pajak" => $param['data2']['bukti_akta_data_pajak'],
    //         "bukti_sewa_data_pajak" => $param['data2']['bukti_sewa_data_pajak'],
    //         "bukti_sertifikat_data_pajak" => $param['data2']['bukti_sertifikat_data_pajak'],
    //         "bukti_pelunasan_pbb_data_pajak" => $param['data3']['bukti_pelunasan_pbb_data_pajak'],
    //         "foto_depan_data_pajak" => $param['data3']['foto_depan_data_pajak'],
    //         "denah_data_pajak" => $param['data3']['denah_data_pajak'],
    //         "brosur_data_pajak" => $param['data3']['brosur_data_pajak'],
    //         "rekap_data_pajak" => $param['data3']['rekap_data_pajak'],
    //         "create_by" => $param['userID']
    //     ];
        
    //     $save_data_pajak = DB::table('data_pajak')->insert($data_pajak);
    //     $select = DB::table('data_pajak')->where("create_by","=",$param['userID'])->orderby("id_data_pajak", "desc")->take(1)->get();
    //     // print_r($select[0]->id_data_pajak);

    //     // print_r(date('mY'). '.' . date('d') . '.' . $param['userID'] . '.' . $select[0]->id_data_pajak);

    //     $npwpd = [
    //         "id_data_pajak" => $select[0]->id_data_pajak,
    //         "id_user_pajak" => $param['userID'],
    //         "no_registrasi_npwpd" => $select[0]->id_data_pajak,
    //         // "tgl_pengesahan_npwpd" => ,
    //         "no_npwpd" => date('m'). '.' . date('Y'). '.' . date('d') . '.' . $param['userID'] . '.' . $select[0]->id_data_pajak,
    //         "status_pengajuan_npwpd" => "Proses Verifikasi",
    //         'status_akhir_npwpd' => "Belum Aktif"
    //         // "tgl_masa_berlaku_npwpd" => 
    //     ];
        
    //     $save = DB::table('npwpd')->insert($npwpd);

    //     if($param['badanUsaha'] == 'Hotel'){
    //         // $dataPendukung = [
    //         //     "id_data_pajak" => $select[0]->id_data_pajak,
    //         //     "kategori_layanan_hotel" => $param['dataPendukung'][''],
    //         //     "nama_layanan_hotel" => $param['dataPendukung'][''],
    //         //     "jumlah2_layanan_hotel" => $param['dataPendukung'][''],
    //         //     "harga1_layanan_hotel" => $param['dataPendukung'][''],
    //         //     "harga2_layanan_hotel" => $param['dataPendukung'][''],
    //         //     "satuan1_layanan_hotel" => $param['dataPendukung'][''],
    //         //     "satuan2_layanan_hotel" => $param['dataPendukung']['']
    //         // ];

    //         // $savePendukung = DB::table('layanan_hotel')->insert($dataPendukung);
    //     }else if($param['badanUsaha'] == 'Parkir'){
    //         $dataPendukung = [
    //             "id_data_pajak" => $select[0]->id_data_pajak,
    //             "luas_parkir" => $param['dataPendukung']['luas_area'],
    //             "kapasitas_motor_parkir" => $param['dataPendukung']['kapasitas_motor'],
    //             "kapasitas_mobil_parkir" => $param['dataPendukung']['kapasitas_mobil'],
    //             "jumlah_pegawai_parkir" => $param['dataPendukung']['jumlah_pegawai']
    //         ];

    //         $savePendukung = DB::table('layanan_parkir')->insert($dataPendukung);
    //     }else if($param['badanUsaha'] == 'Hiburan'){
    //         $dataPendukung = [
    //             "id_data_pajak" => $select[0]->id_data_pajak,
    //             "jumlah_kursi_hiburan" => $param['dataPendukung']['jumlah_kursi'],
    //             // "jumlah_meja_hiburan" => $param['dataPendukung'][''],
    //             "jumlah_pegawai_hiburan" => $param['dataPendukung']['jumlah_pegawai'],
    //             "harga_tiket1_hiburan" => $param['dataPendukung']['jumlah_tiket'],
    //             "harga_tiket_2_hiburan" => $param['dataPendukung']['tarif'],
    //             // "jumlah_tiket_tersedia_hiburan" => $param['dataPendukung'][''],
    //         ];

    //         $savePendukung = DB::table('layanan_hiburan')->insert($dataPendukung);
    //     }else if($param['badanUsaha'] == 'Restaurant'){
    //         $dataPendukung = [
    //             "id_data_pajak" => $select[0]->id_data_pajak,
    //             "jam_buka_resto" => $param['dataPendukung']['mulai'],
    //             "jam_tutup_resto" => $param['dataPendukung']['buka'],
    //             "jumlah_meja_resto" => $param['dataPendukung']['meja'],
    //             "jumlah_kursi_resto" => $param['dataPendukung']['kursi'],
    //             "makanan_termurah_resto" => $param['dataPendukung']['makanan_termurah'],
    //             "makanan_termahal_resto" => $param['dataPendukung']['makanan_termahal'],
    //             "minuman_termurah_resto" => $param['dataPendukung']['minuman_termurah'],
    //             "minuman_termahal_resto" => $param['dataPendukung']['minuman_termahal'],
    //             // "jumlah_pegawai_resto" => $param['dataPendukung'][''],
    //             "alat_pembayaran_resto" => $param['dataPendukung']['bayar'],
    //         ];

    //         $savePendukung = DB::table('layanan_resto')->insert($dataPendukung);
    //     }

    //     $this->resource = array(
    //         'status' => 200,
    //         'data' => 'Data berhasil di simpan'
    //     );
    //     $this->sendResponse();
    // }

    public function approvalNpwpd(){
        $param = json_decode($this->request()->getBody(),true);
        $dt = Carbon::now();
        // print_r($dt->subMonth());
        $data = [
            'status_akhir_npwpd' => 'Aktif',
            'tgl_pengesahan_npwpd' => $dt->toDateString()

        ];

        $builder = DB::table("npwpd")->where("id_npwpd","=", $param['id_npwpd'])->update($data);
        
        $this->resource = array(
            'status' => 200,
            'data' => 'Berhasil di ubah'
        );
        $this->sendResponse();
    }

    public function viewData(){
        $param = json_decode($this->request()->getBody(),true);
        $builder = DB::table("data_pajak")->join("user_pajak","user_pajak.id_user_pajak","=","data_pajak.create_by")->where("id_data_pajak","=",$param['id'])->get();

        $builderNpwpd = DB::table("npwpd")->where("id_data_pajak","=",$param['id'])->get();
        
        $this->resource = array(
            'status' => 200,
            'data' => ['data_pajak' => $builder, 'npwpd' => $builderNpwpd]
        );
        $this->sendResponse();
    }

  
}