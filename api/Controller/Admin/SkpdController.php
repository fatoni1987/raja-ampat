<?php

namespace Controller\Admin;

use \Controller\BaseController as BaseController;
use Controller\Admin\UserLogController as UserLogController;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon as Carbon;

require('../public/app/config.php');

class SkpdController extends BaseController {
    //itp.berita.select -> POST berita/select
    public function select() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("sptpd")->join("npwpd","npwpd.id_npwpd","=","sptpd.id_npwpd")->where("sptpd.id_npwpd","=",$param['id'])->get();

        $this->resource = array(
            'status' => 200,
            'data' => $builder
        );
        $this->sendResponse();
    }

    public function selectSkpd() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("skpd")
                ->join("sptpd","sptpd.id_sptpd","=","skpd.id_sptpd")
                ->join("npwpd","npwpd.id_npwpd","=","sptpd.id_npwpd")
                ->join("user_pajak","user_pajak.id_user_pajak","=","npwpd.id_user_pajak");
                // ->where("skpd.id_skpd","=",$param['id'])                
        if(!empty($param['id'])){
            $builder = $builder->where("skpd.id_skpd","=",$param['id']);
        }
        $builder = $builder->skip($param['offset'])->take($param['limit'])->orderby("sptpd.status_sptpd","ASC")->get();
        
        $builderCount = DB::table("skpd")->join("sptpd","sptpd.id_sptpd","=","skpd.id_sptpd")->count();

        $this->resource = array(
            'status' => 200,
            'data' => ['list'=>$builder, 'count'=>$builderCount]
        );
        $this->sendResponse();
    }

    public function selectNpwpdById() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("npwpd")->join("data_pajak","data_pajak.id_data_pajak","=","npwpd.id_data_pajak")->where("npwpd.id_data_pajak","=",$param['id']);

        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    public function selectSptpdById() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("sptpd")->join("npwpd","npwpd.id_npwpd","=","sptpd.id_npwpd")->where("sptpd.id_sptpd","=",$param['id'])->join("data_pajak","data_pajak.id_data_pajak","=","npwpd.id_data_pajak")->join("user_pajak","user_pajak.id_user_pajak","=","npwpd.id_user_pajak")->get();

        $this->resource = array(
            'status' => 200,
            'data' => $builder
        );
        $this->sendResponse();
    }

    public function delete() {
        $param = json_decode($this->request()->getBody(), true);
        // print_r($param['data']['jenis_usaha_pajak']);
        if($param['data']['jenis_usaha_pajak'] == 'Hotel'){
            $builderDelDet = DB::table("layanan_hotel")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();
        }else if ($param['data']['jenis_usaha_pajak'] == 'Parkir'){
            $builderDelDet = DB::table("layanan_parkir")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();
        }else if ($param['data']['jenis_usaha_pajak'] == 'Restaurant'){
            $builderDelDet = DB::table("layanan_resto")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();
        }else if ($param['data']['jenis_usaha_pajak'] == 'Hiburan'){
            $builderDelDet = DB::table("layanan_hiburan")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();
        };

        $builderDelNpwpd = DB::table("npwpd")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();

        $builderDelDataPajak = DB::table("data_pajak")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();

        $this->resource = array(
            'status' => 200,
            'data' => 'Data berhasil di hapus'
        );
        $this->sendResponse();
    }

    public function approve() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = [
            "status_skpd" => 'Di terbitkan',
            "tgl_penetapan_skpd" => Carbon::now()
        ];
        
        $builder = DB::table('skpd')->where("id_skpd","=",$param['id'])->update($builder);

        $builderSspd = [
            "id_skpd" => $param['id'],
            "status_sspd" => "Draft",
            // "nomor_sspd" => "SS". date('m'). '.' . date('Y'). '.' . date('d') . '.' . $param['id']
        ];
        $builder = DB::table('sspd')->insert($builderSspd);

        $this->resource = array(
            'status' => 200,
            'data' => 'Data berhasil di hapus'
        );
        $this->sendResponse();
    }

    

  
}