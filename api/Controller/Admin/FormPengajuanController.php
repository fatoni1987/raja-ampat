<?php

namespace Controller\Admin;

use \Controller\BaseController as BaseController;
use Controller\Admin\UserLogController as UserLogController;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon as Carbon;

require('../public/app/config.php');

class FormPengajuanController extends BaseController {
    //itp.berita.select -> POST berita/select
    public function select() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = [];
        $builderBidang = [];
        $builderKamar = [];
        $builderBadanUsaha = [];
        $builderUserPajak = [];
        $builder = DB::table("form-pengajuan")->orderby("id_form","asc")->get();
        $builderBidang = DB::table("mst_bidang_usaha")->orderby("id_bidang_usaha","asc")->get();
        $builderKamar = DB::table("mst_jenis_kamar")->orderby("id_kamar","asc")->get();
        $builderBadanUsaha = DB::table("mst_badan_usaha")->where("flag_active", true)->orderby("id_badan_usaha","asc")->get();
        $builderDistrik = DB::table("mst_distrik")->orderby("nama_mst_distrik","asc")->get();
        $builderKelurahan = DB::table("mst_kelurahan")->orderby("nama_mst_kelurahan","asc")->get();

        if($param['role']==11){
            $builderUserPajak = DB::table("user_pajak")->where("id_user_pajak","=",$param["userID"])->orderby("id_user_pajak","asc")->get();
        }
        
        $this->resource = array(
            'status' => 200,
            'data' => ['form'=>$builder, 'bidang'=>$builderBidang, 'kamar'=>$builderKamar, 'badan'=>$builderBadanUsaha, 'user'=>$builderUserPajak, 'distrik'=>$builderDistrik, 'kelurahan'=>$builderKelurahan]
        );
        $this->sendResponse();
    }

    public function selectNpwpd() {
        $param = json_decode($this->request()->getBody(), true);
        $select = DB::table("pegawai_view")->where("id","=",$param['user_id'])->get();
        // print_r($select[0]->role_id);
        if($select[0]->role_id == 11){
            $builder = DB::table("npwpd")->join("data_pajak","data_pajak.id_data_pajak","=","npwpd.id_data_pajak")->where("create_by","=",$param['user_id']);

            if(isset($param['status'])){
                $builder = $builder->where("status_pengajuan_npwpd","=","Verifikasi");
            }
            $builder = $builder->orderby("status_pengajuan_npwpd","ASC")->skip($param['offset'])->take($param['limit']);
            
            $builderCount = DB::table("npwpd")->join("data_pajak","data_pajak.id_data_pajak","=","npwpd.id_data_pajak")->where("create_by","=",$param['user_id'])->count();
        }else{
            $builder = DB::table("npwpd")->join("data_pajak","data_pajak.id_data_pajak","=","npwpd.id_data_pajak");

            if(isset($param['status'])){
                $builder = $builder->where("status_pengajuan_npwpd","=","Verifikasi");
            }
            $builder = $builder->orderby("status_pengajuan_npwpd","ASC")->skip($param['offset'])->take($param['limit']);    

            $builderCount = DB::table("npwpd")->join("data_pajak","data_pajak.id_data_pajak","=","npwpd.id_data_pajak")->count();
        }

        $this->resource = array(
            'status' => 200,
            'data' => ['list'=>$builder->get(), 'count'=>$builderCount]
        );
        $this->sendResponse();
    }

    public function selectNpwpdById() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("npwpd")->join("data_pajak","data_pajak.id_data_pajak","=","npwpd.id_data_pajak")->where("npwpd.id_data_pajak","=",$param['id']);

        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    public function selectMstReklame() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("mst_jenis_reklame");

        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    public function delete() {
        $param = json_decode($this->request()->getBody(), true);
        // print_r($param['data']['jenis_usaha_pajak']);
        if($param['data']['jenis_usaha_pajak'] == 'Hotel'){
            $builderDelDet = DB::table("layanan_hotel")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();
        }else if ($param['data']['jenis_usaha_pajak'] == 'Parkir'){
            $builderDelDet = DB::table("layanan_parkir")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();
        }else if ($param['data']['jenis_usaha_pajak'] == 'Restaurant'){
            $builderDelDet = DB::table("layanan_resto")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();
        }else if ($param['data']['jenis_usaha_pajak'] == 'Hiburan'){
            $builderDelDet = DB::table("layanan_hiburan")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();
        };

        $builderDelNpwpd = DB::table("npwpd")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();

        $builderDelDataPajak = DB::table("data_pajak")->where("id_data_pajak","=",$param['data']['id_data_pajak'])->delete();

        $this->resource = array(
            'status' => 200,
            'data' => 'Data berhasil di hapus'
        );
        $this->sendResponse();
    }

    public function approve() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = [
            "status_pengajuan_npwpd" => 'Verifikasi',
            "status_akhir_npwpd" => 'Aktif',
            "tgl_pengesahan_npwpd" => Carbon::now(),
            "tgl_masa_berlaku_npwpd" => Carbon::now()->addYears(5)
        ];
            // print_r(Carbon::now());
        $builder = DB::table('npwpd')->where("id_data_pajak","=",$param['data'])->update($builder);

        $this->resource = array(
            'status' => 200,
            'data' => 'Data berhasil di hapus'
        );
        $this->sendResponse();
    }

    public function reject() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = [
            "status_pengajuan_npwpd" => 'Tolak',
            "status_akhir_npwpd" => 'Belum Aktif',
            "alasan_tolak_npwpd" => $param['alasan']
        ];
        
        $builder = DB::table('npwpd')->where("id_data_pajak","=",$param['data'])->update($builder);
        
        $this->resource = array(
            'status' => 200,
            'data' => 'Data berhasil di reject'
        );
        $this->sendResponse();
    }

    public function insert() {
        $param = json_decode($this->request()->getBody(), true);
        $user_pajak = [
            "warga_negara_user_pajak" => $param['data1']['kewarganegaraan'],
            "alamat_user_pajak" => $param['data1']['alamat'],
            // "bukti_identitas_user_pajak" => $param['data1']['bukti_identitas_user_pajak'],
            "pemilik_user_pajak" => $param['data1']['nama'],
        ];
        $save_user_pajak = DB::table('user_pajak')->where("id_user_pajak","=",$param['userID'])->update($user_pajak);
        
        if($param['data2']['nama_bidang_usaha'] == 'Lainnya'){
            $bidang_usaha = $param['data2']['Lainnya'];
        };

        $data_pajak = [
            "nama_usaha_data_pajak" => $param['data2']['nama_badan_usaha'],
            "jenis_usaha_pajak" => $param['data1']['nama_badan_usaha'],
            "no_pbb_data_pajak" => $param['data3']['no_pbb'],
            "no_izin_data_pajak" => $param['data3']['no_izin_usaha'],
            "nama_izin_data_pajak" => $param['data3']['nama_izin_usaha'],
            "alamat_data_pajak" => $param['data2']['alamat_usaha'],
            "rt_rw_data_pajak" => $param['data2']['rt'],
            "kecamatan_data_pajak" => $param['data2']['kecamatan'],
            "kelurahan_data_pajak" => $param['data2']['kelurahan_usaha'],
            "kabupaten_data_pajak" => $param['data2']['kabupaten_usaha'],
            "kode_pos_data_pajak" => $param['data2']['kode_pos'],
            "telp_data_pajak" => $param['data2']['telp_usaha'],
            "fax_data_pajak" => $param['data2']['fax_usaha'],
            "email_data_pajak" => $param['data2']['email_usaha'],
            "tanggal_perkiraan_usaha" => $param['data3']['datePerkiraan'],
            "bidang_usaha_pajak" => $param['data2']['nama_bidang_usaha'],            
            "bukti_akta_data_pajak" => $param['data2']['bukti_akta_data_pajak'],
            "bukti_sewa_data_pajak" => $param['data2']['bukti_sewa_data_pajak'],
            "bukti_sertifikat_data_pajak" => $param['data2']['bukti_sertifikat_data_pajak'],
            "bukti_pelunasan_pbb_data_pajak" => $param['data3']['bukti_pelunasan_pbb_data_pajak'],
            "foto_depan_data_pajak" => $param['data3']['foto_depan_data_pajak'],
            "denah_data_pajak" => $param['data3']['denah_data_pajak'],
            "brosur_data_pajak" => $param['data3']['brosur_data_pajak'],
            "rekap_data_pajak" => $param['data3']['rekap_data_pajak'],
            "create_by" => $param['userID']
        ];
        
        $save_data_pajak = DB::table('data_pajak')->insert($data_pajak);
        $select = DB::table('data_pajak')->where("create_by","=",$param['userID'])->orderby("id_data_pajak", "desc")->take(1)->get();
        // print_r($select[0]->id_data_pajak);

        // print_r(date('mY'). '.' . date('d') . '.' . $param['userID'] . '.' . $select[0]->id_data_pajak);

        $npwpd = [
            "id_data_pajak" => $select[0]->id_data_pajak,
            "id_user_pajak" => $param['userID'],
            "no_registrasi_npwpd" => $select[0]->id_data_pajak,
            // "tgl_pengesahan_npwpd" => ,
            "no_npwpd" => date('m'). '.' . date('Y'). '.' . date('d') . '.' . $param['userID'] . '.' . $select[0]->id_data_pajak,
            "status_pengajuan_npwpd" => "Proses Verifikasi",
            'status_akhir_npwpd' => "Belum Aktif"
            // "tgl_masa_berlaku_npwpd" => 
        ];
        
        $save = DB::table('npwpd')->insert($npwpd);

        if($param['badanUsaha'] == 'Hotel'){
            // $dataPendukung = [
            //     "id_data_pajak" => $select[0]->id_data_pajak,
            //     "kategori_layanan_hotel" => $param['dataPendukung'][''],
            //     "nama_layanan_hotel" => $param['dataPendukung'][''],
            //     "jumlah2_layanan_hotel" => $param['dataPendukung'][''],
            //     "harga1_layanan_hotel" => $param['dataPendukung'][''],
            //     "harga2_layanan_hotel" => $param['dataPendukung'][''],
            //     "satuan1_layanan_hotel" => $param['dataPendukung'][''],
            //     "satuan2_layanan_hotel" => $param['dataPendukung']['']
            // ];

            // $savePendukung = DB::table('layanan_hotel')->insert($dataPendukung);
        }else if($param['badanUsaha'] == 'Parkir'){
            $dataPendukung = [
                "id_data_pajak" => $select[0]->id_data_pajak,
                "luas_parkir" => $param['dataPendukung']['luas_area'],
                "kapasitas_motor_parkir" => $param['dataPendukung']['kapasitas_motor'],
                "kapasitas_mobil_parkir" => $param['dataPendukung']['kapasitas_mobil'],
                "jumlah_pegawai_parkir" => $param['dataPendukung']['jumlah_pegawai']
            ];

            $savePendukung = DB::table('layanan_parkir')->insert($dataPendukung);
        }else if($param['badanUsaha'] == 'Hiburan'){
            $dataPendukung = [
                "id_data_pajak" => $select[0]->id_data_pajak,
                "jumlah_kursi_hiburan" => $param['dataPendukung']['jumlah_kursi'],
                // "jumlah_meja_hiburan" => $param['dataPendukung'][''],
                "jumlah_pegawai_hiburan" => $param['dataPendukung']['jumlah_pegawai'],
                "harga_tiket1_hiburan" => $param['dataPendukung']['jumlah_tiket'],
                "harga_tiket_2_hiburan" => $param['dataPendukung']['tarif'],
                // "jumlah_tiket_tersedia_hiburan" => $param['dataPendukung'][''],
            ];

            $savePendukung = DB::table('layanan_hiburan')->insert($dataPendukung);
        }else if($param['badanUsaha'] == 'Restaurant'){
            $dataPendukung = [
                "id_data_pajak" => $select[0]->id_data_pajak,
                "jam_buka_resto" => $param['dataPendukung']['mulai'],
                "jam_tutup_resto" => $param['dataPendukung']['buka'],
                "jumlah_meja_resto" => $param['dataPendukung']['meja'],
                "jumlah_kursi_resto" => $param['dataPendukung']['kursi'],
                "makanan_termurah_resto" => $param['dataPendukung']['makanan_termurah'],
                "makanan_termahal_resto" => $param['dataPendukung']['makanan_termahal'],
                "minuman_termurah_resto" => $param['dataPendukung']['minuman_termurah'],
                "minuman_termahal_resto" => $param['dataPendukung']['minuman_termahal'],
                // "jumlah_pegawai_resto" => $param['dataPendukung'][''],
                "alat_pembayaran_resto" => $param['dataPendukung']['bayar'],
            ];

            $savePendukung = DB::table('layanan_resto')->insert($dataPendukung);
        }

        $this->resource = array(
            'status' => 200,
            'data' => 'Data berhasil di simpan'
        );
        $this->sendResponse();
    }

    public function approvalNpwpd(){
        $param = json_decode($this->request()->getBody(),true);
        $dt = Carbon::now();
        // print_r($dt->subMonth());
        $data = [
            'status_akhir_npwpd' => 'Aktif',
            'tgl_pengesahan_npwpd' => $dt->toDateString()

        ];

        $builder = DB::table("npwpd")->where("id_npwpd","=", $param['id_npwpd'])->update($data);
        
        $this->resource = array(
            'status' => 200,
            'data' => 'Berhasil di ubah'
        );
        $this->sendResponse();
    }

    public function viewData(){
        $param = json_decode($this->request()->getBody(),true);
        $builder = DB::table("data_pajak")->join("user_pajak","user_pajak.id_user_pajak","=","data_pajak.create_by")->where("id_data_pajak","=",$param['id'])->get();

        $builderNpwpd = DB::table("npwpd")->where("id_data_pajak","=",$param['id'])->get();
        
        $this->resource = array(
            'status' => 200,
            'data' => ['data_pajak' => $builder, 'npwpd' => $builderNpwpd]
        );
        $this->sendResponse();
    }

  
}