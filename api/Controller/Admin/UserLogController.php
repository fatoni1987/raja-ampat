<?php

namespace Controller\Admin;

use Model\UserLogView as userLogView;
use Model\UserLog as userLog;
use \Controller\BaseController as BaseController;
use Carbon\Carbon as Carbon ;
use Illuminate\Database\Capsule\Manager as DB;
require('../public/app/config.php');

class UserLogController extends BaseController
{
	
    public function __construct() {        
        parent::__construct();
        $this->entity = new userLog();
    }

    public function select() {
        $this->entity = new userLogView();
        $param = json_decode($this->request()->getBody(), true);
        $this->resource = array(
                'status' => 200,
                'data' => $this->selectAll(array(),  $param["offset"], $param["limit"])
        );
        $this->sendResponse();
    }

    

    public function selectby() {
        $this->entity = new userLogView();
        $param = json_decode($this->request()->getBody(), true);
        $offset = $param["offset"];
        $limit = $param["limit"];
        $builder = DB::table("rekanan_log_view");
        if($param["act"] == "Username"){
            $val = strtolower($param['value1']);
            $builder->whereRaw('LOWER(username) LIKE ?',[$val]);
        }
        elseif ($param["act"] == "Jenis") {
            $val = strtolower($param['value1']);
            $builder->whereRaw('LOWER(user_activity_description) LIKE ?',[$val]);
        }
        elseif($param["act"] == "Tanggal"){
            $date1 = $param['date1']; $date2 = $param['date2'];
            $builder->whereBetween('tanggal',[$date1, $date2]);
        }
        $builder->orderBy('rekanan_log_id', 'DESC')->skip($offset)->take($limit);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

        public function countby() {
        $this->entity = new userLogView();
        $param = json_decode($this->request()->getBody(), true);
       
        $builder = DB::table("rekanan_log_view");
        if($param["act"] == "Username"){
            $val = strtolower($param['value1']);
            $builder->whereRaw('LOWER(username) LIKE ?',[$val]);
        }
        elseif ($param["act"] == "Jenis") {
            $val = strtolower($param['value1']);
            $builder->whereRaw('LOWER(user_activity_description) LIKE ?',[$val]);
        }
        elseif($param["act"] == "Tanggal"){
            $date1 = $param['date1']; $date2 = $param['date2'];
            $builder->whereBetween('tanggal',[$date1, $date2]);
        }
       // $builder->orderBy('user_log_id', 'DESC');
        $this->resource = array(
            'status' => 200,
            'data' => $builder->count()
        );
        $this->sendResponse();
    }
    
    
    public function insertData(){
        $data = json_decode($this->request()->getBody(), true);
        $this->tableName = 'user_log';
        $this->resource = array(
            'status' => 200,
            'data' => $this->insertRow($data)
        );
        $this->sendResponse();
    }
    
    public function insertLogUser($data){
        $this->tableName = 'user_log';
        $this->resource = array(
            'status' => 200,
            'data' => $this->insertRow($data)
        );
        return $this->resource;
    }
    
    public function insertLogRekanan($data){
        $builder = DB::table('rekanan_log');
        $this->resource = array(
            'status' => 200,
            'data' => $builder->insert($data)
        );
        return $this->resource;
    }

    public function updateData(){
        $data = json_decode($this->request()->getBody(), true);
        $this->tableName = 'MstNews';
        $this->resource = array(
            'status' => 200,
            'data' => $this->insertRow($data)
        );
        $this->sendResponse();
    }

    //Menambah syntax untuk Log Aktivitas User Untuk Log Akses di menu
    //-> POST userlogview/selectlogview
    public function selectlogview(){
        $param = json_decode($this->request()->getBody(), true);
        //print_r($param);
        $jenis = $param["jenis"];
        $txtcari = '%' . $param["txtcari"] . '%';
        $tglawalcari = $param["tglawalcari"];
        $tglakhircari = $param["tglakhircari"];
        $offset = $param["offset"];
        $pageSize = $param["pageSize"];     
        
        $builder =  DB::table("user_log_view");
        switch ($jenis) {
            case "Username" :
                $builder->whereRaw("lower(username) like lower(?)", array($txtcari))->orderBy("tanggal", "DESC")->skip($offset)->take($pageSize);
            break;
            case "Jenis" : 
                $builder->whereRaw("lower(user_activity_description) like lower(?)", array($txtcari))->orderBy("tanggal", "DESC")->skip($offset)->take($pageSize);
            break;
            case "Tanggal" :
                $builder->whereBetween('tanggal',[$tglawalcari, $tglakhircari]);
                //$builder->whereRaw("to_date(tanggal, 'YYYY-MM-DD') >= to_date(?, 'YYYY-MM-DD') and to_date(tanggal, 'YYYY-MM-DD') <= to_date(?, 'YYYY-MM-DD')", [$tglawalcari,$tglakhircari])->orderBy("tanggal", "DESC")->skip($offset)->take($pageSize);
            break;
                
        }
        $builder->orderBy("tanggal", "DESC")->skip($offset)->take($pageSize);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    //-> POST userlogview/selectlogcount
    public function selectlogcount(){
        $param = json_decode($this->request()->getBody(), true);
        $jenis = $param["jenis"];
        $txtcari = '%' . $param["txtcari"] . '%';
        $tglawalcari = $param["tglawalcari"];
        $tglakhircari = $param["tglakhircari"];
        $builder = DB::table("user_log_view");
        switch ($jenis) {
            case "Username" :
                $builder->whereRaw("lower(username) like lower(?)", array($txtcari));
            break;
            case "Jenis" : 
                $builder->whereRaw("lower(user_activity_description) like lower(?)", array($txtcari));
            break;
            case "Tanggal" :
                $builder->whereBetween('tanggal',[$tglawalcari, $tglakhircari]);
                //$builder->whereRaw("to_date(tanggal, 'YYYY-MM-DD') >= to_date(?, 'YYYY-MM-DD') and to_date(tanggal, 'YYYY-MM-DD') <= to_date(?, 'YYYY-MM-DD')", [$tglawalcari,$tglakhircari]);
            break;
            default :
                $builder;
        }
        $this->resource = array(
            'status' => 200,
            'data' => $builder->count()
        );
        $this->sendResponse();
    }

    //End Kodingan Hari
}