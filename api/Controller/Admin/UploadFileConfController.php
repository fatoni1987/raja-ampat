<?php

namespace Controller\Admin;

use \Controller\BaseController as BaseController;
use Controller\Admin\UserLogController as UserLogController;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon as Carbon;

require('../public/app/config.php');

class UploadFileConfController extends BaseController {

    public function __construct() {
        parent::__construct();
    }

    public function count() {
        //address: itp.uploadfileconfig.count
        //path: fileconf/count
        //method: POST
        /* {
          "srcTextNamaHalaman": "%%",
          "srcTextFileType": "%%",
          "sisiHalamanTercentang": false,
          "namaHalamanTercentang": false,
          "fileTypeTercentang": false,
          "sisiHalaman": "%%"
          }
         * 
         */
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("cfg_file_filter");
        $sisiHalamanTercentang = $param["sisiHalamanTercentang"];
        $namaHalamanTercentang = $param["namaHalamanTercentang"];
        $fileTypeTercentang = $param["fileTypeTercentang"];

        if ($sisiHalamanTercentang == false && $namaHalamanTercentang == false && $fileTypeTercentang == false) {
            $builder->distinct();
        } else if ($sisiHalamanTercentang === true && $namaHalamanTercentang === false && $fileTypeTercentang === false) {
            $builder->whereRaw("page_layer ilike ?", array($param["sisiHalaman"]));
            $builder->distinct();
        } else if ($sisiHalamanTercentang === false && $namaHalamanTercentang === true && $fileTypeTercentang === false) {
            $builder->whereRaw("page_name ilike ?", array($param["srcTextNamaHalaman"]));
            $builder->distinct();
        } else if ($sisiHalamanTercentang === false && $namaHalamanTercentang === false && $fileTypeTercentang === true) {
            $builder = DB::table("config_uploadfile_view");
            $builder->whereRaw("file_type_name ilike ?", array($param["srcTextFileType"]));
            $builder->distinct();
        }
        $this->resource = array(
            'status' => 200,
            'data' => $builder->count()
        );
        $this->sendResponse();
    }
    
    public function select() {
        //address: itp.uploadfileconfig.selectTableView
        //path: fileconf/select
        //method: POST
        /* {
          "srcTextNamaHalaman": "%%",
          "srcTextFileType": "%%",
          "sisiHalamanTercentang": false,
          "namaHalamanTercentang": false,
          "fileTypeTercentang": false,
          "sisiHalaman": "%%"
          }
         * 
         */
        $param = json_decode($this->request()->getBody(), true);
        //$builder = DB::table("config_uploadfile_view");
        $sisiHalamanTercentang = $param["sisiHalamanTercentang"];
        $namaHalamanTercentang = $param["namaHalamanTercentang"];
        $fileTypeTercentang = $param["fileTypeTercentang"];
        $offset = $param["offset"]; $limit = $param["limit"];
        
        if ($sisiHalamanTercentang == false && $namaHalamanTercentang == false && $fileTypeTercentang == false) {
            $builder = DB::select(DB::raw('SELECT DISTINCT id_page_config, page_layer, page_name, limit_size FROM '
                    . 'config_uploadfile_view ORDER BY id_page_config ASC offset ' . $offset . ' '
                    . 'rows fetch next ' . $limit . ' rows only'));
        } else if ($sisiHalamanTercentang === true && $namaHalamanTercentang === false && $fileTypeTercentang === false) {
            $builder = DB::select(DB::raw('SELECT DISTINCT id_page_config, page_layer, page_name, limit_size FROM '
                    . 'config_uploadfile_view WHERE LOWER(page_layer) like lower(?) ORDER BY id_page_config ASC offset ' . $offset . ' '
                    . 'rows fetch next ' . $limit . ' rows only'), [$param["sisiHalaman"]]);
            //$builder->whereRaw("LOWER(page_layer) like ?", array($param["sisiHalaman"]));
        } else if ($sisiHalamanTercentang === false && $namaHalamanTercentang === true && $fileTypeTercentang === false) {
            $builder = DB::select(DB::raw('SELECT DISTINCT id_page_config, page_layer, page_name, limit_size FROM '
                    . 'config_uploadfile_view WHERE LOWER(page_name) like lower(?) ORDER BY id_page_config ASC offset ' . $offset . ' '
                    . 'rows fetch next ' . $limit . ' rows only'), [$param["srcTextNamaHalaman"]]);
            //$builder->whereRaw("LOWER(page_name) like ?", array($param["srcTextNamaHalaman"]));
        } else if ($sisiHalamanTercentang === false && $namaHalamanTercentang === false && $fileTypeTercentang === true) {
            //$builder->whereRaw("LOWER(file_type_name) like ?", array($param["srcTextFileType"]));
            $builder = DB::select(DB::raw('SELECT DISTINCT id_page_config, page_layer, page_name, limit_size FROM '
                    . 'config_uploadfile_view WHERE LOWER(file_type_name) like lower(?) ORDER BY id_page_config ASC offset ' . $offset . ' '
                    . 'rows fetch next ' . $limit . ' rows only'), [$param["srcTextFileType"]]);
        }
       //$builder->orderBy("id_page_config", "asc")->offset($offset)->limit($limit);
        
        $this->resource = array(
            'status' => 200,
            'data' => $builder //$builder->get(["id_page_config", "page_layer", "page_name", "limit_size"])
        );
        $this->sendResponse();
    }

    public function selectPageLayer() {
        //address: itp.uploadfileconfig.itemSisiHalaman
        //path: fileconf/selectpagelayer
        //method: GET
        $builder = DB::table("config_uploadfile_view")->distinct();
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get(["page_layer"])
        );
        $this->sendResponse();
    }

    public function selectPageName() {
        //address: itp.uploadfileconfig.itemNamaHalaman
        //path: fileconf/selectpagename
        //method: GET
        $builder = DB::table("config_uploadfile_view")->distinct();
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get(["page_name"])
        );
        $this->sendResponse();
    }

    public function selectTypeFile() {
        //address: itp.uploadfileconfig.itemFileType
        //path: fileconf/selecttypefile
        //method: GET
        $builder = DB::table("config_uploadfile_view")->distinct();
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get(["file_type_name"])
        );
        $this->sendResponse();
    }
    
    public function cekTypeFile(){
        /* itp.uploadfileconfig.cekFileType
         * Path: fileconf/cektype , method: POST
         */
        $param = json_decode($this->request()->getBody(), true);
        $data = $param['param'];
        $builder = DB::table("config_uploadfile_view");
        $builder->whereIn("id_page_config",$data);
        $result = $builder->get();
        $this->resource = array(
            'status' => 200,
            'data' => $result
        );
        $this->sendResponse();
    }
    
    public function selectActiveFile(){
        /* itp.uploadfilefilter.selectActive
         * Path: fileconf/selectActiveFile , method: POST
         */
        $param = json_decode($this->request()->getBody(), true);
        $pageLayer = $param['param'];
        $builder = DB::table("cfg_file_filter");
        $builder->where(["flag_active"=>true, "page_layer"=>$pageLayer[0]]);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }
    
    public function selectAllFileType(){
        /* itp.uploadfiletype.selectAll
         * Path: fileconf/selectAllFileType , method: get
         */
        $builder = DB::table("cfg_file_type");
        $builder->where(["flag_active"=>true]);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }
    
    public function selectAllFilePage(){
        /* itp.uploadfiletype.selectByPage
         * Path: fileconf/selectAllFilePage , method: post
         */
        $builder = DB::table("cfg_filetype_temp");
        $param = json_decode($this->request()->getBody(), true);
        $builder->where(["id_page_config"=>$param['param'][0]]);
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    public function updateFileConf(){
        /* itp.uploadfileconfig.update
         * Path: fileconf/updateFileConf , method: post
         */
        
        $param = json_decode($this->request()->getBody(), true);
        $data = $param['param']; $username = $param['username'];
        $updatedata = FALSE;
        foreach ($data as $dt) {
            $builder = DB::table("cfg_filetype_temp");
            $dataup =  [
                'flag_active' => $dt[0]];
            $updatedata = $builder->where(['id_page_config'=> $dt[1],'id_file_type'=>$dt[2]])->update($dataup);
        }
        if ($updatedata) {
           //nambah insert ke tabel log akses
            $datalog = ['username' => $username, 'user_activity_id' => 146, 'detail' => 'Memodifikasi konfigurasi file upload ', 'tanggal' =>Carbon::now()];
            $ctrl = new UserLogController();
            $savelog = $ctrl->insertLogUser($datalog);
            $this->resource = $savelog;
            $this->sendResponse();
        } else {
            $message = "failed to insert user log activity";
            $result = array('affected' => false, 'message' => $message);
            $this->resource = $result;
            $this->sendResponse();
        }
    }
    
    public function insertFileConf(){
        /* itp.uploadfileconfig.insert
         * path : fileconf/insertFileConf , POST
         */
        $param = json_decode($this->request()->getBody(), true);
        $dataIns = $param['param']; $username = $param['username'];
        $data = array();
         foreach ($dataIns as $dt) {
             array_push($data, [
                 'id_page_config' => $dt[0],
                 'id_file_type' => $dt[1],
                 'flag_active' => $dt[2]]);
        }
        $save= DB::table('cfg_filetype_temp')->insert($data);
        if ($save) {
            //nambah insert ke tabel log akses
            $datalog = ['username' => $username, 'user_activity_id' => 145, 'detail' => 'Menambah konfigurasi file upload ', 'tanggal' =>Carbon::now()];
            $ctrl = new UserLogController();
            $savelog = $ctrl->insertLogUser($datalog);
            $this->resource = $savelog;
            $this->sendResponse();
        } else {
            $message = "failed to insert user log activity";
            $result = array('affected' => false, 'message' => $message);
            $this->resource = $result;
            $this->sendResponse();
        }
    }
    
    public function updateLimitSize(){
        /* itp.uploadfileconfig.ubahlimitsize 
         * path: fileconf/updateLimitSize , POST
         */
        $param = json_decode($this->request()->getBody(), true);
        $limit_size = $param['limit_size']; $username = $param['username'];
        $id_page_config = $param['id_page_config'];
        $builder = DB::table("cfg_file_filter");
        $updatedata = $builder->where(['id_page_config'=> $id_page_config])->update(["limit_size"=>$limit_size]);
        if ($updatedata) {
           //nambah insert ke tabel log akses
            $datalog = ['username' => $username, 'user_activity_id' => 170, 'detail' => 'Mengubah limit size file ', 'tanggal' =>Carbon::now()];
            $ctrl = new UserLogController();
            $savelog = $ctrl->insertLogUser($datalog);
            $this->resource = $savelog;
            $this->sendResponse();
        } else {
            $message = "failed to insert user log activity";
            $result = array('affected' => false, 'message' => $message);
            $this->resource = $result;
            $this->sendResponse();
        }
    }
}
