<?php

namespace Controller\Admin;

use Model\MstNews as MstNews;
use \Controller\BaseController as BaseController;
use Controller\Admin\UserLogController as UserLogController;
use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon as Carbon;

require('../public/app/config.php');

class NewsController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this->entity = new MstNews();
        $this->tableName = "mst_news";
        $this->idColumn = "id_news";
    }

    //itp.berita.count -> -> POST berita/count
    public function count() {
        $param = json_decode($this->request()->getBody(), true);
        $keyword = $param["keyword"];
        $builder = DB::table("mst_news");
        $builder->where("flag_active", "=", true)->where("author","=",$param["author"])
                ->where(function ($sql) use ($keyword){
                    $sql->whereRaw("lower(title_news) like lower(?) or strip_tags(content_news) LIKE ?", [$keyword,$keyword]);     
                });
        $this->resource = array(
            'status' => 200,
            'data' => $builder->count()
        );
        $this->sendResponse();
    }

    //itp.berita.select -> POST berita/select
    public function select() {
        $param = json_decode($this->request()->getBody(), true);
        $offset = $param["offset"];
        $pageSize = $param["pageSize"];
        $search = '%' . $param["search"] . '%';
        $limit = $param["limit"];
        $keyword = $param["keyword"];
        $builder = DB::table("mst_news");
        $builder->where("flag_active", "=", true)->where("author","=",$param["author"])
                ->where(function ($sql) use ($keyword){
                    $sql->whereRaw("lower(title_news) like lower(?) OR strip_tags(content_news) like ?", [$keyword,$keyword]);
                })->orderBy("date_news", "DESC")->skip($offset)->take($limit); // old: id_news
        $this->resource = array(
            'status' => 200,
            'data' => $builder->get()
        );
        $this->sendResponse();
    }

    public function insertDoc() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table("mst_news");
        $document = $param["doc"];
        $data = array();
        $dataToSend = array();
        foreach ($document as $det) {
            $data = [
                'file_name' => $det["file_name"],
                'url' => $det["url"],
                'flag_active' => false
            ];
            //$id = $builder->insertGetId($data);
            $mNews = new MstNews();
            $mNews->fromArray($data);
            $mNews->save();
            $id = $mNews->id_news;
            array_push($dataToSend, [
                'id' => $id
            ]);
        }

        if (count($dataToSend) == count($document)) {
            $status = 200;
            $message = "success insert doc";
        } else {
            $status = 500;
            $message = "failed insert doc";
        }

        $response = array(
            'status' => $status,
            'message' => $message,
            'result' => [
                'data' => $dataToSend,
                'size' => count($dataToSend)
            ]
        );
        echo json_encode($response);
    }

    //itp.berita.save -> -> POST berita/save
    public function save() {
        $param = json_decode($this->request()->getBody(), true);
        $action = $param["action"];
        $parr = $param["param"];
        $username = $param["username"];
        $result = false;
        $data = array();
        switch ($action) {
            case "add":
                $data = array('title_news' => $parr[0],
                    'content_news' => $parr[1],
                    'picture_news' => $parr[2],
                    'date_news' => $parr[3],
                    'author' => $parr[6],
                    'sumber' => 'Admin',
                    'flag_active' => true);

                $builder = DB::table("mst_news");
                //$result = $builder->insertGetId($data);
                $mNews = new MstNews();
                $mNews->fromArray($data);
                $mNews->save();
                $result = $mNews->id_news;
                break;
            case "edit":
                $data = array(
                    'title_news' => $parr[0],
                    'content_news' => $parr[1],
                    'picture_news' => $parr[2],
                    'date_news' => $parr[3],
                    'author' => $parr[4]
                );
                $result = $this->updateRow(array('id_news' => $parr[5]), $data);
                break;
            case "delete":
                $data = array(
                    'flag_active' => false,
                    'author' => $username
                );
                $result = $this->updateRow(array('id_news' => $parr[1]), $data);
                break;
            default:
                break;
        }

        if ($result) {
            $status = 200;
            $userLogController = new UserLogController();
            $info =  ' Berita: ' . $parr[0];
            switch ($action) {
                case "add":
                    $datalog = ['username' => $username, 'detail' => 'Membuat' . $info, 'user_activity_id' => 151, 'tanggal' => Carbon::now()];
                    break;
                case "edit":
                    $datalog = ['username' => $username, 'detail' => 'Merubah' . $info, 'user_activity_id' => 151, 'tanggal' => Carbon::now()];
                    break;
                case "delete":
                    $datalog = ['username' => $username, 'detail' => 'Menghapus' . $info, 'user_activity_id' => 151, 'tanggal' => Carbon::now()];
                    break;
                default:
                    break;
            }
            $resource = $userLogController->insertLogUser($datalog);            
            $this->resource = $resource;       
        }else {
            $this->resource = $result;
        }
        
        if($action == 'add' && $status == 200){
            $data['id'] = $result; 
            $this->resource = array(
                'status' => $status,
                'data' => $data
            );
        }else{
            $this->resource = array(
                'status' => $status,
                'data' => $data
            );
        }
        $this->sendResponse();
        
    }
}