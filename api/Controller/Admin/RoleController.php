<?php

namespace Controller\Admin;

use \Model\Roles as Entity;
use \Model\Menu as Menu;
use \Controller\BaseController as BaseController;
use \Controller\Admin\UserLogController as UserLogController;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Capsule\Manager as DB;

require('../public/app/config.php');

class RoleController extends BaseController {

    public function __construct() {
        parent::__construct();
        $this->entity = new Entity();
    }

    /**     *
     * address: itp.role.select
     * path: /roles/role_by_type
     * method: POST
     * parameter: {jenis_role: $0 }
     * * */
    public function selectRoleByType() {

        try {
            $param = json_decode($this->request()->getBody(), true);
            $param['is_active'] = true;
            if(array_key_exists('token',$param)){
                unset($param['token']);
            }
            $query = DB::table("roles");
            $criteria = array();
            foreach ($param as $key => $value) {
                $criteria[$key] = $value;
            }
            $query->where($criteria);
            $query->orderBy('role_id', 'asc');
            $data = $query->get();

            $this->resource = array(
                'status' => 200,
                'data' => $data
            );
            $this->sendResponse();
        } catch (\Exception $e) {
            $this->response->setStatus(500);
            $this->response->headers()->set('Content-Type', 'application/json');
            echo json_encode(array(
                'status' => 500,
                'result' => array(
                    'message' => $e->getMessage(),
                    'source' => 'RoleController - selectRoleByType()'
                )
            ));
        }
    }

    /**     *
     * address: itp.role.selectAll
     * path: /roles/list
     * method: GET
     * * */
    public function lists() {

        try {
            $this->resource = array(
                'status' => 200,
//				'data' => $this->selectActive('is_active', '1', array(),  -1, -1)
                'data' => DB::table('roles')->where(["is_active" => true])->orderBy('authority', 'asc')->get()
            );
            $this->sendResponse();
        } catch (\Exception $e) {
            $this->response->setStatus(500);
            $this->response->headers()->set('Content-Type', 'application/json');
            echo json_encode(array(
                'status' => 500,
                'result' => array(
                    'message' => $e->getMessage(),
                    'source' => 'RoleController - selectRoleByType()'
                )
            ));
        }
    }

    public function paginate() {

        $param = json_decode($this->request()->getBody(), true);
        $this->resource = array(
            'status' => 200,
            'data' => $this->selectActive('is_active', true, array(), $param["offset"], $param["limit"])
        );
        $this->sendResponse();
    }

    /**     *
     * address: itp.role.getDetail
     * path: /roles/detail/:id
     * method: GET
     * * */
    public function detail($token,$id) {

        $this->resource = array(
            'status' => 200,
            'data' => DB::table('view_access')->where(["role_id" => $id])->orderBy("menu_id")->get()
        );
        $this->sendResponse();
    }

    public function listMenu() {
        $this->resource = array(
            'status' => 200,
            'data' => DB::table('menus')->where(["flag_active" => true])->orderBy('menu_id', 'asc')->get()
        );
        $this->sendResponse();
    }

    /**     *
     * address: itp.role.insert
     * path: /roles/create
     * method: POST
     * parameter: 
     * { 
     * 	master: {authority: $0, jenis_role: $1 }
     * 	details: [
     * 	{
     * 		ispermitted: $0, 
     * 		page_id: $1, 
     * 		bisa_mengatur: $2, 
     * 		bisa_tambah: $3, 
     * 		bisa_ubah: $4, 
     * 		bisa_hapus: $5 
     *
     * 	}
     * 	],
     * 	logging: 
     * 	{
     * 		username: $6,
     * 		user_activity_id: $7
     * 	}	 
     * }
     * * */
    public function insertRole() {

        $data = [];
        $status = 200;
        $param = json_decode($this->request()->getBody(), true);
        $master = $param["master"];
        $master['is_active'] = true;

        $details = $param["details"];
        $address = $param["address"];
        $logging = $param["logging"];

        try {
            DB::beginTransaction();
            $affected = 0;
            $statements = count($details) + 1;
            //$insertedRoleId = DB::table("roles")->insertGetId($master);
            $roleObj = new Entity();
            $roleObj->jenis_role = 1;
            $roleObj->authority = $master['authority'];
            $roleObj->is_active = $master['is_active'];
            // $roleObj->fromArray($master);
            $roleObj->save();
            $insertedRoleId = $roleObj->role_id;
            if (isset($insertedRoleId) && $insertedRoleId !== null) {
                $affected++;
                foreach ($details as $key => $value) {
                    $details[$key]["role_id"] = $insertedRoleId;

                    if (DB::table('access')->insert($details[$key]))
                        $affected++;
                }
                foreach ($address as $key => $value) {
                    $simpan = DB::table('detail_address');
                    if($address[$key]['bisa_hapus']){
                        $data = [
                            'role_id' => $insertedRoleId,
                            'address_id' => $address[$key]['page_id'],
                            'page_id' => $address[$key]['page_id'],
                            'is_active' => true,
                            'bisa_mengatur' => true,
                            'jenis_mengatur' => 4];
                        $result = $simpan->insert($data);
                    }
                    else if(!$address[$key]['bisa_hapus']){
                        $data = [
                            'role_id' => $insertedRoleId,
                            'address_id' => $address[$key]['page_id'],
                            'page_id' => $address[$key]['page_id'],
                            'is_active' => true,
                            'bisa_mengatur' => false,
                            'jenis_mengatur' => 4];
                        $result = $simpan->insert($data);
                    }
                    if($address[$key]['bisa_mengatur']){
                        $data = [
                            'role_id' => $insertedRoleId,
                            'address_id' => $address[$key]['page_id'],
                            'page_id' => $address[$key]['page_id'],
                            'is_active' => true,
                            'bisa_mengatur' => true,
                            'jenis_mengatur' => 1];
                        $result = $simpan->insert($data);
                    }
                    else if(!$address[$key]['bisa_mengatur']){
                        $data = [
                            'role_id' => $insertedRoleId,
                            'address_id' => $address[$key]['page_id'],
                            'page_id' => $address[$key]['page_id'],
                            'is_active' => true,
                            'bisa_mengatur' => false,
                            'jenis_mengatur' => 1];
                        $result = $simpan->insert($data);
                    }
                    if($address[$key]['bisa_tambah']){
                        $data = [
                            'role_id' => $insertedRoleId,
                            'address_id' => $address[$key]['page_id'],
                            'page_id' => $address[$key]['page_id'],
                            'is_active' => true,
                            'bisa_mengatur' => true,
                            'jenis_mengatur' => 2];
                        $result = $simpan->insert($data);
                    }
                    else if(!$address[$key]['bisa_tambah']){
                        $data = [
                            'role_id' => $insertedRoleId,
                            'address_id' => $address[$key]['page_id'],
                            'page_id' => $address[$key]['page_id'],
                            'is_active' => true,
                            'bisa_mengatur' => false,
                            'jenis_mengatur' => 2];
                        $result = $simpan->insert($data);
                    }
                    if($address[$key]['bisa_ubah']){
                        $data = [
                            'role_id' => $insertedRoleId,
                            'address_id' => $address[$key]['page_id'],
                            'page_id' => $address[$key]['page_id'],
                            'is_active' => true,
                            'bisa_mengatur' => true,
                            'jenis_mengatur' => 3];
                        $result = $simpan->insert($data);
                    }
                    else if(!$address[$key]['bisa_ubah']){
                        $data = [
                            'role_id' => $insertedRoleId,
                            'address_id' => $address[$key]['page_id'],
                            'page_id' => $address[$key]['page_id'],
                            'is_active' => true,
                            'bisa_mengatur' => false,
                            'jenis_mengatur' => 3];
                        $result = $simpan->insert($data);
                    }
                }
                DB::commit();
                $status = 200;
                $datalog = [
                    'username' => $logging['username'],
                    'user_activity_id' => $logging['user_activity_id'],
                    'detail' => 'Membuat role dengan nama ' . $master['authority'],
                    'tanggal' => $this->date_now
                ];
                $ctrl = new UserLogController();
                $savelog = $ctrl->insertLogUser($datalog);
                $this->resource = $savelog;	
            }

            $data = [
                'affected' => $affected . ' rows of ' . $statements . ' statements',
                'action' => 'commited',
                'logger' => $savelog
            ];
        } catch (\PDOException $e) {
            $status = 500;
            $data = ['affected' => false, 'action' => 'rollback', 'exception' => $e->getMessage()];
            DB::rollBack();
        }
        $this->resource = array(
            'status' => $status,
            'data' => $insertedRoleId
        );
        $this->sendResponse();
    }

    /**     *
     * address: itp.role.update
     * path: /roles/update
     * method: POST
     * parameter: 
     * { 
     * 	role: {role_id: $0, jenis_role: $1, authority: $3 }
     * 	paramsInsert: 
     * 	[{
     * 		ispermitted: 1, 
     * 		page_id: 1, 
     * 		bisa_mengatur: 1, 
     * 		bisa_tambah: 1, 
     * 		bisa_ubah: 1, 
     * 		bisa_hapus: 1 
     * 	}],
     * 	paramsUpdate: 
     * 	[{
     * 		ispermitted: 1, 
     * 		page_id: 1, 
     * 		bisa_mengatur: 1, 
     * 		bisa_tambah: 1, 
     * 		bisa_ubah: 1, 
     * 		bisa_hapus: 1 
     * 	}],
     * 	logging: 
     * 	{
     * 		username: $5,
     * 		user_activity_id: $6,
     * 	},
     * 	page_id: $7	 
     * }
     * * */
    public function updateRole() {
        $data = [];
        $status = 200;
        $param = json_decode($this->request()->getBody(), true);
        $role = $param['role'];
        $paramsInsert = $param['paramsInsert'];
        $paramsUpdate = $param['paramsUpdate'];
        $logging = $param['logging'];
        $insert = (count($paramsInsert) > 0) ? count($paramsInsert) : 0;
        $update = (count($paramsUpdate) > 0) ? count($paramsUpdate) : 0;
        try {
            DB::beginTransaction();
            $statements = 1 + $insert + $update;
            $affected = 0;
            $result = DB::table('roles')->where(['role_id' => $role['role_id']])->update(['jenis_role' => $role["jenis_role"]]);
            
            if ($result) {
                $affected++;
                if ($update > 0) {
                    foreach ($paramsUpdate as $updateData) {
                        $where = ['role_id' => $role['role_id'], 'page_id' => $updateData['page_id']];
                        $columns = ['ispermitted', 'bisa_mengatur', 'bisa_tambah', 'bisa_ubah', 'bisa_hapus'];
                        $data = [];
                        
                        foreach ($columns as $value) {
                            $data[$value] = $updateData[$value];
                        }
                        if ($result = DB::table('access')->where($where)->update($data)) {
                            $affected++;
                        }
                    }
                    $bisa_mengatur = true;
                    $jenis_mengatur = 1;
                    foreach ($paramsUpdate as $key => $value){
                        if($paramsUpdate[$key]['bisa_hapus']){
                            $bisa_mengatur = true;
                            $builder = DB::table('detail_address')
                                    -> where('role_id',$role['role_id'])
                                    -> where('page_id',$paramsUpdate[$key]['page_id'])
                                    -> where('jenis_mengatur','=',4)
                                    -> update(['bisa_mengatur'=>true]);
                        }
                        else if(!$paramsUpdate[$key]['bisa_hapus']){
                            $bisa_mengatur = false;
                            $builder = DB::table('detail_address')
                                    -> where('role_id',$role['role_id'])
                                    -> where('page_id',$paramsUpdate[$key]['page_id'])
                                    -> where('jenis_mengatur','=',4)
                                    -> update(['bisa_mengatur'=>false]);
                        }
                        if($paramsUpdate[$key]['bisa_mengatur']){
                            $bisa_mengatur = true;
                            $builder = DB::table('detail_address')
                                    -> where('role_id',$role['role_id'])
                                    -> where('page_id',$paramsUpdate[$key]['page_id'])
                                    -> where('jenis_mengatur','=',1)
                                    -> update(['bisa_mengatur'=>true]);
                        }
                        else if(!$paramsUpdate[$key]['bisa_mengatur']){
                            $bisa_mengatur = false;
                            $builder = DB::table('detail_address')
                                    -> where('role_id',$role['role_id'])
                                    -> where('page_id',$paramsUpdate[$key]['page_id'])
                                    -> where('jenis_mengatur','=',1)
                                    -> update(['bisa_mengatur'=>false]);
                        }
                        if($paramsUpdate[$key]['bisa_tambah']){
                            $bisa_mengatur = true;
                            $builder = DB::table('detail_address')
                                    -> where('role_id',$role['role_id'])
                                    -> where('page_id',$paramsUpdate[$key]['page_id'])
                                    -> where('jenis_mengatur','=',2)
                                    -> update(['bisa_mengatur'=>true]);
                        }
                        else if(!$paramsUpdate[$key]['bisa_tambah']){
                            $bisa_mengatur = false;
                            $builder = DB::table('detail_address')
                                    -> where('role_id',$role['role_id'])
                                    -> where('page_id',$paramsUpdate[$key]['page_id'])
                                    -> where('jenis_mengatur','=',2)
                                    -> update(['bisa_mengatur'=>false]);
                        }
                        if($paramsUpdate[$key]['bisa_ubah']){
                            $bisa_mengatur = true;
                            $builder = DB::table('detail_address')
                                    -> where('role_id',$role['role_id'])
                                    -> where('page_id',$paramsUpdate[$key]['page_id'])
                                    -> where('jenis_mengatur','=',3)
                                    -> update(['bisa_mengatur'=>true]);
                        }
                        else if(!$paramsUpdate[$key]['bisa_ubah']){
                            $bisa_mengatur = false;
                            $builder = DB::table('detail_address')
                                    -> where('role_id',$role['role_id'])
                                    -> where('page_id',$paramsUpdate[$key]['page_id'])
                                    -> where('jenis_mengatur','=',3)
                                    -> update(['bisa_mengatur'=>false]);
                        }
                    }
                    
                }
                if ($insert > 0) {
                    foreach ($paramsInsert as $key => $value) {
                        $paramsInsert[$key]["role_id"] = $role["role_id"];
                        if (DB::table('access')->insert($paramsInsert[$key])) {
                            $affected++;
                        }
                    }
                    $roleID = $role['role_id'];
                    foreach ($paramsInsert as $key => $value) {
                        $simpan = DB::table('detail_address');
                        if($paramsInsert[$key]['bisa_hapus']){
                            $data = [
                                'role_id' => $roleID,
                                'address_id' => $paramsInsert[$key]['page_id'],
                                'page_id' => $paramsInsert[$key]['page_id'],
                                'is_active' => true,
                                'bisa_mengatur' => true,
                                'jenis_mengatur' => 4];
                            $result = $simpan->insert($data);
                    }
                    else if(!$paramsInsert[$key]['bisa_hapus']){
                        $data = [
                            'role_id' => $roleID,
                            'address_id' => $paramsInsert[$key]['page_id'],
                            'page_id' => $paramsInsert[$key]['page_id'],
                            'is_active' => true,
                            'bisa_mengatur' => false,
                            'jenis_mengatur' => 4];
                        $result = $simpan->insert($data);
                    }
                    if($paramsInsert[$key]['bisa_mengatur']){
                        $data = [
                            'role_id' => $roleID,
                            'address_id' => $paramsInsert[$key]['page_id'],
                            'page_id' => $paramsInsert[$key]['page_id'],
                            'is_active' => true,
                            'bisa_mengatur' => true,
                            'jenis_mengatur' => 1];
                        $result = $simpan->insert($data);
                    }
                    else if(!$paramsInsert[$key]['bisa_mengatur']){
                        $data = [
                            'role_id' => $roleID,
                            'address_id' => $paramsInsert[$key]['page_id'],
                            'page_id' => $paramsInsert[$key]['page_id'],
                            'is_active' => true,
                            'bisa_mengatur' => false,
                            'jenis_mengatur' => 1];
                        $result = $simpan->insert($data);
                    }
                    if($paramsInsert[$key]['bisa_tambah']){
                        $data = [
                            'role_id' => $roleID,
                            'address_id' => $paramsInsert[$key]['page_id'],
                            'page_id' => $paramsInsert[$key]['page_id'],
                            'is_active' => true,
                            'bisa_mengatur' => true,
                            'jenis_mengatur' => 2];
                        $result = $simpan->insert($data);
                    }
                    else if(!$paramsInsert[$key]['bisa_tambah']){
                        $data = [
                            'role_id' => $roleID,
                            'address_id' => $paramsInsert[$key]['page_id'],
                            'page_id' => $paramsInsert[$key]['page_id'],
                            'is_active' => true,
                            'bisa_mengatur' => false,
                            'jenis_mengatur' => 2];
                        $result = $simpan->insert($data);
                    }
                    if($paramsInsert[$key]['bisa_ubah']){
                        $data = [
                            'role_id' => $roleID,
                            'address_id' => $paramsInsert[$key]['page_id'],
                            'page_id' => $paramsInsert[$key]['page_id'],
                            'is_active' => true,
                            'bisa_mengatur' => true,
                            'jenis_mengatur' => 3];
                        $result = $simpan->insert($data);
                    }
                    else if(!$paramsInsert[$key]['bisa_ubah']){
                        $data = [
                            'role_id' => $roleID,
                            'address_id' => $paramsInsert[$key]['page_id'],
                            'page_id' => $paramsInsert[$key]['page_id'],
                            'is_active' => true,
                            'bisa_mengatur' => false,
                            'jenis_mengatur' => 3];
                        $result = $simpan->insert($data);
                    }
                    }
                }
            }
            DB::commit();
            $status = 200;
            $datalog = [
                'username' => $logging['username'],
                'user_activity_id' => $logging['user_activity_id'],
                'detail' => 'Mengubah hak akses role ' . $role['authority'],
                'tanggal' => $this->date_now
            ];
            $ctrl = new UserLogController();
            $savelog = $ctrl->insertLogUser($datalog);
            $data = [
                'affected' => $affected . ' rows of ' . $statements . ' statements',
                'action' => 'commited',
                'logger' => $savelog
            ];
        } catch (\PDOException $e) {
            $status = 500;
            $data = ['affected' => false, 'action' => 'rollback', 'exception' => $e->getMessage()];
            DB::rollBack();
        }
        $this->resource = array(
            'status' => $status,
            'data' => $data
        );
        $this->sendResponse();
    }

    /**     *
     * address: 
     * 	itp.role.cekBisaMengatur , jenis: 1
     * 	itp.role.cekBisaMenambah , jenis: 2
     * 	itp.role.cekBisaMengubah , jenis: 3 
     * 	itp.role.cekBisaMenghapus , jenis: 4
     * path: /roles/check_authority
     * method: POST
     * parameter: {username: $0,  page_id: $1, jenis_mengatur: $2}
     * * */
    public function checkAuthority() {
        try {
            $param = json_decode($this->request()->getBody(), true);
            if(array_key_exists('token',$param)){
                unset($param['token']);
            }
            $data = DB::table("view_user_bisa_mengatur_halaman")->where($param)->get();
            $this->resource = array(
                'status' => 200,
                'data' => $data
            );
            $this->sendResponse();
        } catch (\Exception $e) {
            $this->response->setStatus(500);
            $this->response->headers()->set('Content-Type', 'application/json');
            echo json_encode(array(
                'status' => 500,
                'result' => array(
                    'message' => $e->getMessage(),
                    'source' => 'RoleController - checkAuthority()'
                )
            ));
        }
    }

    /**     * address: itp.role.getRoleUser
     * path: /roles/get_role_user
     * method: POST
     * parameter: {jenis_role : 'USER' , is_active : 1}
     * * */
    public function getRoleUser() {
        try {
            $param = json_decode($this->request()->getBody(), true);
            $data = [
                'is_active'=>$param['is_active'],
                // 'jenis_role'=>$param['jenis_role']
            ];
            $data = DB::table("roles")->where($data)->get();
            $this->resource = array(
                'status' => 200,
                'data' => $data
            );
            $this->sendResponse();
        } catch (\Exception $e) {
            $this->response->setStatus(500);
            $this->response->headers()->set('Content-Type', 'application/json');
            echo json_encode(array(
                'status' => 500,
                'result' => array(
                    'message' => $e->getMessage(),
                    'source' => 'RoleController - selectRoleByType()'
                )
            ));
        }
    }

    /**     *
     * address: itp.role.selectAll
     * path: /roles/menu_by_role
     * method: POST
     * parameter: {role_id: $0}
     * * */
    public function getMenuByRole() {
        try {
            $param = json_decode($this->request()->getBody(), true);
            $data = DB::table("access as ac")
            ->select([
                'ac.*',
                'wp.page_name',
                'wp.page_link',
                'wp.page_class',
                'mn.menu_name',
                'mn.menu_parent',
                'mn.menu_id',
                'mn.order'])
            ->join('webpages as wp', 'wp.page_id', '=', 'ac.page_id')
            ->join('menus as mn', 'mn.page_id', '=', 'wp.page_id')
            ->where([
                'ac.role_id' => $param['role_id'],
                'mn.flag_active' => 1,
                'mn.masuk_sidebar' => true,
                'ac.ispermitted' => true])
            ->orderBy('mn.order', 'asc')
            ->distinct('mn.menu_id')
            ->get();
            $this->resource = array(
                'status' => 200,
                'data' => $data
            );
            $this->sendResponse();
        } catch (\Exception $e) {
            $this->response->setStatus(500);
            $this->response->headers()->set('Content-Type', 'application/json');
            echo json_encode(array(
                'status' => 500,
                'result' => array(
                    'message' => $e->getMessage(),
                    'source' => 'RoleController - getMenuByRole()'
                )
            ));
        }
    }

    /**     *
     * address: itp.menu.select
     * path: /menu/select
     * method: POST
     * parameter: {} -> TIDAK ADA
     * * */
    public function selectMenu() {
        $param = json_decode($this->request()->getBody(), true);
        $builder = DB::table('menus');
        $builder->where('flag_active', '=', true);
        // if($param['jenisRole'] == 'USER'){
        //     $builder->whereIn('role_type', [1, 2]);
        // }
        // else if($param['jenisRole'] == 'KEPANITIAAN'){
        //     $builder->whereIn('role_type', [2]);
        // }
        $builder->orderBy('menu_id');
        $this->resource = [
            'status' => 200,
            'data' => $builder->get([
                'menu_id',
                'menu_name',
                'menu_parent',
                'page_id',
                'role_type'])
        ];
        $this->sendResponse();
    }

//    POST => Pembagian Home Data Pengadaan roles/gethomedtpengadaan
    public function gethomedtpengadaan() {
        $param = json_decode($this->request()->getBody(), TRUE);
        $builder = DB::table("view_user_bisa_mengatur_halaman")->whereIn("page_id", [154, 155])
                        ->where("username", "=", $param["username"])->where("ispermitted", "=", 1)->distinct();

        $this->resource = array(
            'status' => 200,
            'data' => $builder->get(['username', 'page_id', 'menu_name', 'ispermitted', 'role_id'])
        );
        $this->sendResponse();
    }

}
