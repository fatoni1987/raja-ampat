<?php

namespace Nettuts;
use Utils\SessionManager as SessionManager;
use Illuminate\Database\Capsule\Manager as DB;
use Controller\SessionController as SessionController;


Class Router
{
	protected $routes;
	protected $request;
	protected $errorHandler;

	public function __construct()
	{
		$env = \Slim\Environment::getInstance();
		$this->request = new \Slim\Http\Request($env);
		$this->routes = array();

	}


	protected function processCallback($path)
	{
		$class = "RouteController";

		if (strpos($path, ":") !== false) {
			list($class, $path) = explode(":", $path);
		}

		$function = ($path != "") ? $path : "index";

		$func = function () use ($class, $function) {
			$class = '\Controller\\' . $class;
			$class = new $class();

			$args = func_get_args();

			return call_user_func_array(array($class, $function), $args);
		};

		return $func;
	}


	// Accepts Class:Function@Method
	public function addRoutes($routes)
	{
		foreach ($routes as $route => $path) {

			$method = "any";

			if (strpos($path, "@") !== false) {
				list($path, $method) = explode("@", $path);
			}

			$func = $this->processCallback($path);

			$r = new \Slim\Route($route, $func);
			$r->setHttpMethods(strtoupper($method));

			array_push($this->routes, $r);
		}
	}

	public function set404Handler($path)
	{
		$this->errorHandler = $this->processCallback($path);
	}



	public function run()
	{
		$display404 = true;
		$displayUnauthorize = false;
		$uri = $this->request->getResourceUri();
		$method = $this->request->getMethod();


		// print_r($uri);
		// echo $this->cekToken();

        	foreach ($this->routes as $i => $route) {
				if ($route->matches($uri)) {
					if ($route->supportsHttpMethod($method) || $route->supportsHttpMethod("ANY")) {
						if(($this->cekToken() == 1 )){
							call_user_func_array($route->getCallable(), array_values($route->getParams()));
							$display404 = false;
						}
						else{
							$displayUnauthorize = true;
							$display404 = false;
						}


					}
				}
			}



		if ($display404) {
			if (is_callable($this->errorHandler)) {
				call_user_func($this->errorHandler);
			} else {
				echo "404 - route not found";
			}
		}

		if($displayUnauthorize){
			// echo " ";
			echo json_encode(['status'=>403,'result'=>['size'=>0,'data'=>'Token Tidak Aktif, Tidak bisa diakses']]);
		}
	}

	public function cekToken(){
		return true;
		$param = json_decode($this->request->getBody(), true);
		$method = $this->request->getMethod();
		$uri = $this->request->getResourceUri();
		$urinya = explode('/',$uri);
		$parameter = end($urinya);

		$val = 1;
		// echo $urinya[1];
		// print_r($urinya);
		if($uri == '/auth/login/admin' || $uri == '/pemasukanHargaRFQ/cekPenunjukan' || $uri == '/pemasukanhargaRFQ/selectPaketRFQ' || $uri == '/DAC/getrekanan' || $uri == '/pemasukanhargaRFQ/insert' || $uri == '/pemasukanhargaRFQ/update' || $uri == '/paket/byid' || $uri == '/pemasukanhargaRFQ/prline' || $uri == '/rekanan/paket/cekhabistanggalRFQ' || $uri == '/hargapenawaran/dokumenpenawaran' || $uri == '/hargapenawaran/pemasukanRFQ/select' || $uri == '/paket/detail/info4settingdate' || $uri == '/limitfile' || $uri == '/MstCurrency/get' || $uri == '/pemasukanhargaRFQ/selectPaket' || $uri == '/daftarLelang/cekBertahan' || $uri == '/rekanan/paket/getinfo' || $uri == '/auth/login/vendor' || $urinya[1] == 'upload' || $urinya[1] == 'uploadZip' || $urinya[1] == 'download' || $urinya[1] == 'news' || $urinya[1] == 'PR' || $urinya[1] == 'lupaPassword' || $urinya[1] == 'loggingJS' || $urinya[1] == 'negosiasi'){
			return $val;
		}
			// echo $method;
			if($method != 'GET'){
				if(array_key_exists('token', $param) ){

					// $ip = $param['ip'];
					$token = $param['token'];

					$cek = DB::table('user_session')
						// ->where('remote_ip','=',$ip)
						->where('is_active','=',1)
						->where('session_id','=',$token)
						->get();


					if (count($cek) == 0 ) {

						$val = 0;

					}
				}
			}else{

				if($uri == '/news' || $uri == '/provinsi' || $uri == '/negara/GetNegara' || $uri == '/PR/currencyall' || $uri == '/news/list' || $uri == '/news/detail/'.$parameter || $uri == '/download-manual/'.$parameter || $uri == '/pendaftaran/email/'.$parameter || $uri == '/rekanan/isi_data/status/'.$parameter){
					return $val;

				}
				// echo count($uri);
				if(count($urinya) < 3){

					return 0;
				}
				$token = $urinya[1];
				// $ip    = $urinya[1];



				$cek = DB::table('user_session')
						// ->where('remote_ip','=',$ip)
						->where('session_id','=',$token)
						->where('is_active','=',1)
						->get();


				if (count($cek) == 0 ) {


					$val = 0;
				}

			}


		return $val;

	}

}
