<?php

namespace Nettuts;

Class Controller extends \Slim\Slim
{
	protected $data;
	protected $config;
	
	public function __construct()
	{
		$settings = require("../settings.php");
		if (isset($settings['model'])) {
			$this->data = $settings['model'];
			$this->config = $settings['config'];
		}
		parent::__construct($settings);
	}

	public function render($name, $data = array(), $status = null)
	{
		if (strpos($name, ".php") === false) {
			$name = $name . ".php";
		}
		parent::render($name, $data, $status);
	}

	public function getLoggingConfig(){
		return array(
			'type' => $this->config['type'],
			'logging_url' => $this->config['logging_url'],
		);
	}
}