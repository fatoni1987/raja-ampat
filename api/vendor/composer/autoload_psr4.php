<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Whoops\\' => array($vendorDir . '/filp/whoops/src/Whoops'),
    'Symfony\\Component\\Translation\\' => array($vendorDir . '/symfony/translation'),
    'Svg\\' => array($vendorDir . '/phenx/php-svg-lib/src/Svg'),
    'Stringy\\' => array($vendorDir . '/danielstjules/stringy/src'),
    'Spipu\\Html2Pdf\\' => array($vendorDir . '/spipu/html2pdf/src'),
    'Slim\\Views\\' => array($vendorDir . '/slim/views'),
    'Psr\\SimpleCache\\' => array($vendorDir . '/psr/simple-cache/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'PhpOffice\\PhpSpreadsheet\\' => array($vendorDir . '/phpoffice/phpspreadsheet/src/PhpSpreadsheet'),
    'League\\OAuth2\\Client\\' => array($vendorDir . '/league/oauth2-client/src', $vendorDir . '/league/oauth2-google/src'),
    'Illuminate\\Support\\' => array($vendorDir . '/illuminate/support'),
    'Illuminate\\Database\\' => array($vendorDir . '/illuminate/database'),
    'Illuminate\\Contracts\\' => array($vendorDir . '/illuminate/contracts'),
    'Illuminate\\Container\\' => array($vendorDir . '/illuminate/container'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'FontLib\\' => array($vendorDir . '/phenx/php-font-lib/src/FontLib'),
    'Dompdf\\' => array($vendorDir . '/dompdf/dompdf/src'),
    'Complex\\' => array($vendorDir . '/markbaker/complex/classes/src'),
    'Carbon\\' => array($vendorDir . '/nesbot/carbon/src/Carbon'),
);
