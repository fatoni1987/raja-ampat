<?php
$settings = array(
    'view' => new \Slim\Views\Twig(),
    'templates.path' => '../View',
    'model' => (Object)array(
        "message" => "Hello World",
    ),
    'config' =>  array(
        'type' => 'development',
        'logging_url' => 'http://localhost:79/email',
        'session' => array(
            'timeout' => 3600, // in seconds
            'prefix' => '',
            'format' => 'array',
            'save_to_database'=> false
        ),
        'api' => array(
            'key' => 'c44acc2327ef633b4d754dcb69dd07e5',
            'storage' => 'session',
            'apply_to_all' => true
        ),
        'email' => array( // --------------------------------------------------- setting email development
            'debug_level' => 0, // 0: live, 1: server logs, 2: all logs
            // 'host' => 'mail.itdc.co.id', 
            // 'username' => 'eproc@itdc.co.id', 
            // 'password' => 'itdcjaya', 
            'port' => 587, 
            'use_tls_encryption' => true, 
            'sender' => [
                'name' => 'Pajak Badan Pengelolaan Pajak dan Retribusi Daerah', 
                'email' => 'noreply@pbppr.co.id' 
            ]
        ),
        'logging' => array(
            'frequency' => 'daily',
            'filename' => 'pajak_log',
            'use_timestamp' => true,
            'log_folder' => 'd:/pajak' // value samakan dengan 'target' di 'upload_config'
        ),
        'upload_config' => array(
            'target' => $_SERVER['DOCUMENT_ROOT'] . "/pajak/download", // /var/www/html/eproc-itdc/uploads
            'overwrite' => true,
            'path_alias' => 'download',
            'manual_panitia' => 'manual-panitia.pdf',
            'manual_vendor' => 'manual-vendor.pdf'
        )
    )
);
return $settings;