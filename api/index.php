<!-- <html lang="en">
<head>
	<title>API Routes</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body ng-app="app">
	<div class="col-md-12">
		<div class="container panel panel-primary">
			<h3>API Routes List</h3>
			<div class="clearfix"></div>
			<div class="form-horizontal container">
				<form>
					<div class="form-group">
						<div class="col-md-2"><label>Path</label></div>
						<div class="col-md-5">
							<input type="text" class="form-control" placeholder="path/to/route" ng-model="$root.data.path" required/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-2"><label>Controller</label></div>
						<div class="col-md-5">
							<input type="text" class="form-control" placeholder="TestController" ng-model="$root.data.controller" required/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-2"><label>Function</label></div>
						<div class="col-md-5">
							<input type="text" class="form-control" placeholder="getAll" ng-model="$root.data.function" required/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-2"><label>Request Method</label></div>
						<div class="col-md-5">
							<input type="text" class="form-control" placeholder="post" ng-model="$root.data.method" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4"><button type="submit" class="btn btn-success" ng-click="$root.save()">Add</button></div>
					</div>
				</form>
			</div>

			<div class="alert alert-primary" ng-repeat="route in $root.routes">
				<h4>{{route.controller}}</h4>
				<table class="table table-responsive">
					<thead>
						<tr>
							<th width="40px">#</th>
							<th width="40%">Path</th>
							<th width="60%">Route Definition</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="r in route.routes">
							<td>{{$index + 1}}</td>
							<td>{{r.path}}</td>
							<td>{{r.route}}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

	</div>
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular.min.js"></script>
	<script type="text/javascript">
	var app = angular.module("app", []);
	app.run(['$rootScope', '$http' , '$location', function ($rootScope, $http, $location) {
		$http.get(window.location.pathname + "public/routes").then(function(result){
			$rootScope.routes = result.data.result.data;
			//console.info($rootScope.routes);
			//$rootScope.$apply();
		});

		$rootScope.save = function() {
			if(	$rootScope.data.path === "" || 
				$rootScope.data.controller === "" || 
				$rootScope.data.function === "" ||
				$rootScope.data.method === ""
				) {
				alert("Fill all fields!");
				return;
			}
			$http.post(window.location.pathname + "public/routes/exist", {"path": $rootScope.data.path}).then(function(result){
				var exist = result.data.result.data;

				if(exist) {
					alert("Path already exist!");
				}
				else {
					$http.post(window.location.pathname + "public/routes/find", {controller: $rootScope.data.controller}).then(function(result){
						$rootScope.controller = result.data.result.data[0];
						var controllerExist = result.data.result.size > 0;
						if(controllerExist) {
							//console.info("Old Data: " + JSON.stringify($rootScope.controller));
							
							var newdata = $rootScope.controller;
							newdata.path += "," + $rootScope.data.path;
							newdata.functions += "," + $rootScope.data.function;
							newdata.method += "," + $rootScope.data.method;

							//console.info("New Data: " + JSON.stringify(newdata));
							$http.post(window.location.pathname + "public/routes/update", newdata)
							.then(function(response){
								console.info("Update response: " + JSON.stringify(response.data));
							});
						} else {
							var newdata = {};
							newdata.path = $rootScope.data.path;
							newdata.functions = $rootScope.data.function;
							newdata.method = $rootScope.data.method;
							newdata.controller = $rootScope.data.controller;
							$http.post(window.location.pathname + "public/routes/create", newdata)
							.then(function(response){
								console.info("Create response: " + JSON.stringify(response.data));
							});	
						}

						window.location.reload(true);

					});
				}
			});	
		};

	}]);

	</script>
</body>
 -->