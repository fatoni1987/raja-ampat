<?php
namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use \Model\BaseModel as BaseModel;

class UserLog extends BaseModel
{

	protected $fillable = ['username', 'user_activity_id', 'detail', 'tanggal' ];
	protected $table = 'user_log';
	protected $primaryKey = 'user_log_id';
	public $timestamps = false;
	
	public function userActivity() {
		return $this->belongsTo('Model\MstUserActivity', 'user_activity_id', 'user_activity_id');
	}
}