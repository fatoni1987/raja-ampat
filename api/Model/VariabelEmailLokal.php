<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use \Model\BaseModel as BaseModel;

class VariabelEmailLokal extends BaseModel {

    protected $fillable = [
        'nama', 'keterangan', 'id_konten_email'];
    protected $table = 'variabel_email_lokal';
    protected $primaryKey = 'id_variabel_lokal';
    public $timestamps = false;

    public function kontenEmail() {
        return $this->belongsTo('Model\KontenEmail', 'id_konten_email', 'id_konten_email');
    }

    public function __construct() {
        parent::__construct();
        self::$modelName = $this->table;
    }

}
