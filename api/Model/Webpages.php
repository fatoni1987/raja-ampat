<?php
namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use \Model\BaseModel as BaseModel;

class Webpages extends BaseModel
{

	protected $fillable = [
            'page_id', 'page_name', 'page_link', 'page_class'];
	protected $table = 'webpages';
	protected $primaryKey = 'page_id';
	public $timestamps = false;
        
}