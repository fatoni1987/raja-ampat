<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Model\BaseModel as BaseModel;

class Menus extends BaseModel {

    protected $fillable = [
        'menu_name', 
        'menu_parent', 
        'created_at', 
        'updated_at', 
        'flag_active', 
        'page_id',
        'updated_id',
        'created_id',
        'icon', 
        'ada_sub_pengecekan', 
        'masuk_sidebar', 
        'order', 
        'ada_sub_pengecekan_bisa_tambah',
        'ada_sub_pengecekan_bisa_ubah',
        'ada_sub_pengecekan_bisa_hapus'];
    
    protected $table = 'menus';
    protected $primaryKey = 'menu_id';
    public $timestamps = false;

    public function webPages() {
        return $this->belongsTo('Model\WebPages', 'page_id', 'page_id');
    }
    
    public function updatedby() {
        return $this->belongsTo('Model\Pegawai', 'updated_id', 'pegawai_id');
    }

    public function createdby() {
        return $this->belongsTo('Model\Pegawai', 'created_id', 'pegawai_id');
    }

}
