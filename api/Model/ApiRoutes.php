<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Model\BaseModel as BaseModel;

class ApiRoutes extends BaseModel {

	public function __construct() {
		parent::__construct();
		self::$modelName = $this->table;
	}

    protected $fillable = [
        'controller',
        'path',
        'method'];
    
    protected $table = 'api_routes';
    
    public $timestamps = false;
    
}
