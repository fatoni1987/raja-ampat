<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Model\BaseModel as BaseModel;

class ConfigUploadFileView extends BaseModel
{
	protected $table = 'config_uploadfile_view';
	public $timestamps = false;
	
}

