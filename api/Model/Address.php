<?php
namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;


class Address extends Eloquent
{



	protected $fillable = ["address_id","address_name"];
	protected $table = 'address';
	protected $primaryKey = 'address_id';
	public $timestamps = false;
	
	//	public function createdby() {
	//	return $this->belongsTo('Model\Pegawai', 'created_by', 'pegawai_id');
	//}

	//public function updatedby() {
	//	return $this->belongsTo('Model\Pegawai', 'update_by', 'pegawai_id');
	//}
	
}