<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Model\BaseModel as BaseModel;

class Roles extends BaseModel
{
	public function __construct() {
		parent::__construct();
		self::$modelName = $this->table;
	}
	protected $fillable = ['authority', 'is_active', 'jenis_role'];
	protected $table = 'roles';
	protected $primaryKey = 'role_id';
	public $timestamps = false;
	
       
	
}

