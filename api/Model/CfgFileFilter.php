<?php
namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class CfgFileFilter extends Eloquent
{
	protected $fillable = ["id_page_config","page_name","page_layer","limit_size","flag_active"];
	protected $table = 'Cfg_file_filter';
	protected $primaryKey = 'id_page_config';
	public $timestamps = false;
	
}