<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use \Model\BaseModel as BaseModel;

class PegawaiView extends BaseModel {

    protected $table = 'template_panitia_view';
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        self::$modelName = $this->table;
    }

}
