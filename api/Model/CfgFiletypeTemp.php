<?php
namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;


class CfgFiletypeTemp extends Eloquent
{



	protected $fillable = ["id_cfg_temp",
"id_page_config",
"id_file_type",
"flag_active"

];
	protected $table = 'cfg_filetype_temp';
	protected $primaryKey = 'id_cfg_temp';
	public $timestamps = false;
	
	//	public function createdby() {
	//	return $this->belongsTo('Model\Pegawai', 'created_by', 'pegawai_id');
	//}

	//public function updatedby() {
	//	return $this->belongsTo('Model\Pegawai', 'update_by', 'pegawai_id');
	//}
	
}