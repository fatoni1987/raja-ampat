<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Model\BaseModel as BaseModel;

class KontenEmail extends BaseModel {

    protected $fillable = ['nama_email', 'konten_email', 'subject_email'];
    protected $table = 'konten_email';
    protected $primaryKey = 'id_konten_email';
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        self::$modelName = $this->table;
    }
}
