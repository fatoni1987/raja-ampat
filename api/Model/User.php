<?php
namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;



class User extends Eloquent
{

	protected $fillable = ['username', 'password', 'user_level_id', 'modified_user', 'created_user'];
	protected $table = 'user';
	protected $primaryKey = 'user_id';
	protected $hidden = ['username', 'password'];

	public $timestamps = ['created_date', 'modified_date'];

	public function createdby() {
		return $this->belongsTo('Model\User', 'created_user', 'user_id');
	}

	public function updatedby() {
		return $this->belongsTo('Model\User', 'modified_user', 'user_id');
	}

}