<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Model\BaseModel as BaseModel;

class MstNews extends BaseModel {

    public function __construct() {
        parent::__construct();
        self::$modelName = $this->table;
    }
    
    protected $fillable = [
        'title_news',
        'content_news',
        'picture_news',
        'date_news',
        'author',
        'flag_active'];
    
    protected $table = 'mst_news';
    protected $primaryKey = 'id_news';
    public $timestamps = false;


}
