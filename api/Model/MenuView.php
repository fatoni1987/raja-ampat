<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Model\BaseModel as BaseModel;

class MenuView extends BaseModel
{
	protected $table = 'view_menu';
	public $timestamps = false;
	
}

