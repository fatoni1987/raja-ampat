<?php
namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Model\BaseModel as BaseModel;

class UserSession extends BaseModel
{
    public function __construct() {
        parent::__construct();
        self::$modelName = $this->table;
    }
	protected $fillable = ["session_id", "user_id","last_access","remote_ip", "is_active", "user_type"];
	protected $table = 'user_session';
	protected $primaryKey = 'session_id';
	public $timestamps = false;

	
	public function pegawai() {
		return $this->belongsTo('Model\Pegawai', 'user_id', 'pegawai_id');
	}

	public function vendor() {
		return $this->belongsTo('Model\Rekanan', 'user_id', 'rekanan_id');
	}

	
	
}