<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use \Model\BaseModel as BaseModel;

class VariabelEmailGlobal extends BaseModel {

    protected $fillable = ['nama', 'nilai'];
    protected $table = 'variabel_email_global';
    protected $primaryKey = 'id_variabel_global';
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        self::$modelName = $this->table;
    }
}
