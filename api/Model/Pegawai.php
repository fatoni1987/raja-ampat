<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Model\BaseModel as BaseModel;

class Pegawai extends BaseModel {

    protected $fillable = [
        'nama_pegawai',
        'nik',
        'email',
        'telepon',
        'created_by',
        'created_date',
        'updated_by',
        'updated_date',
        'username',
        'password',
        'flag_active',
        'jabatan',
        'bagian',
        'role_id',
        'minta_reset',
        'tgl_minta',
        'keterangan_reset',
        'departemen'];
    
    protected $table = 'pegawai';
    protected $primaryKey = 'pegawai_id';
    public $timestamps = false;

    public function __construct() {
        parent::__construct();
        self::$modelName = $this->table;
    }
    
    public function roles() {
        return $this->belongsTo('Model\Roles', 'role_id', 'role_id');
    }
    
    public function departemen() {
        return $this->belongsTo('Model\Department', 'departemen', 'departemen_id');
    }
    
    public function updatedby() {
        return $this->belongsTo('Model\Pegawai', 'updated_by', 'pegawai_id');
    }

    public function createdby() {
        return $this->belongsTo('Model\Pegawai', 'created_by', 'pegawai_id');
    }

}
