<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Model\BaseModel as BaseModel;

class UserLogView extends BaseModel
{
	public function __construct() {
        parent::__construct();
        self::$modelName = $this->table;
    }

    protected $fillable = [
        'user_log_id',
        'username',
        'user_activity_id',
        'detail',
        'tanggal', 
        'user_activity_description'];

	protected $table = 'user_log_view';
	public $timestamps = false;
	
}


