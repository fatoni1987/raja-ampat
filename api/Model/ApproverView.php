<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Model\BaseModel as BaseModel;

class ApproverView extends BaseModel
{
	protected $table = 'approver_view';
	public $timestamps = false;
	
}
