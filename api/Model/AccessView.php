<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Model\BaseModel as BaseModel;

class AccessView extends BaseModel
{
	protected $table = 'view_access';
	public $timestamps = false;
	
}


