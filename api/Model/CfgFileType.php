<?php
namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;


class CfgFileType extends Eloquent
{



	protected $fillable = ["id_file_type",
"file_type_name",
"flag_active"

];
	protected $table = 'cfg_file_type';
	protected $primaryKey = 'id_file_type';
	public $timestamps = false;
	
	//	public function createdby() {
	//	return $this->belongsTo('Model\Pegawai', 'created_by', 'pegawai_id');
	//}

	//public function updatedby() {
	//	return $this->belongsTo('Model\Pegawai', 'update_by', 'pegawai_id');
	//}
	
}