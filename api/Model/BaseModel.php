<?php
namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;
use Utils\QueryBuilder as Query;


class BaseModel extends Eloquent
{

	protected $fillable;
	protected $table;
	protected $primaryKey; 
	public $timestamps = false;
	protected static $modelName;

	public static function setModelName($modelName) {
		self::$modelName = $modelName;
	}

	public function getKeyName() {
    	return $this->primaryKey;
	}	
	
	protected static function select($criteria = array(), $offset = -1, $limit = -1, $foreignKeys = array()) {
		if(empty($criteria) && $offset === -1 && $limit === -1) { // select all
			if(empty($foreignKeys))
				return self::all();
			else 
				return self::with($foreignKeys);
		} else if(!empty($criteria)) { // find by criteria
			if(empty($foreignKeys)) {
				if($offset === -1 || $limit === -1){
					$builder = DB::table(self::$modelName);
					$builder = Query::filter($builder, $criteria);
					return $builder->get();
				}
				else {
					$builder = DB::table(self::$modelName);
					$builder = Query::filter($builder, $criteria);
					return $builder->take($limit)->skip($offset)->get();
				}
					
			}	
			else {
				if($offset === -1 || $limit === -1)
				{
					$builder = DB::with($foreignKeys);
					$builder = Query::filter($builder, $criteria);
					return $builder->get();
				}
					
				else
					return $builder->take($limit)->skip($offset)->get();
			}
				
		} else { // select with offset and limit
			if(empty($foreignKeys))
				return self::take($limit)->skip($offset)->get();
			else
				return self::with($foreignKeys)->take($limit)->skip($offset)->get();
		}
	}

	public static function selectActive($activeColumn = "flag_active", $activeValue = "true", $foreignKeys = array(), $offset = -1, $limit = -1) {
		$criteria = array(['column' => $activeColumn, 'operator' => '=', 'value' => $activeValue]);
		return self::selectWhere($criteria, $foreignKeys, $offset, $limit);
	}

	public static function selectAll($foreignKeys = array(), $offset = -1, $limit = -1) {

		return self::select(array(), $offset, $limit, $foreignKeys);
	}

	public static function selectWhere($criteria, $foreignKeys = array(), $offset = -1, $limit = -1) {
		return self::select($criteria, $offset, $limit, $foreignKeys);
	}

	public static function selectLimit($offset, $limit, $foreignKeys = array()){
		return self::select(array(), $offset, $limit, $foreignKeys);
	}

	public function update($criteria, $data) {
		$old = self::where($criteria)->first();
		foreach ($data as  $col => $value) {
			$prop = $col;
			$old->$prop = $value;
		}
		return $old->save();

	}

	public function delete($criteria, $soft = false, $statusCol = 'flag_active', $deletedValue = 0) {
		
		if(!$soft) {
			return $this->delete($id);
		} else {
			$old = self::where($criteria)->first();
			$old->$statusCol = $deletedValue;
			return $old->save();
		}
	}
	public function fromArray($array)
	{
	    foreach($array as $attrName => $attrValue)
	    	$this->{$attrName} = $array[$attrName];
	}

}