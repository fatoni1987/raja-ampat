<?php
namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;


class Access extends Eloquent
{



	protected $fillable = ["page_id",
"ispermitted",
"access_id",
"role_id",
"bisa_mengatur",
"bisa_tambah",
"bisa_ubah",
"bisa_hapus"
];
	protected $table = 'access';
	protected $primaryKey = 'access_id';
	public $timestamps = false;
	
	//	public function createdby() {
	//	return $this->belongsTo('Model\Pegawai', 'created_by', 'pegawai_id');
	//}

	//public function updatedby() {
	//	return $this->belongsTo('Model\Pegawai', 'update_by', 'pegawai_id');
	//}
	
}