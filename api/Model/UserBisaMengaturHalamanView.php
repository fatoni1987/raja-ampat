<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Model\BaseModel as BaseModel;

class UserBisaMengaturHalamanView extends BaseModel {
    protected $table = 'view_user_bisa_mengatur_halaman';
    public $timestamps = false;
}