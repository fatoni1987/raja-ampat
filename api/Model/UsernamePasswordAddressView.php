<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Model\BaseModel as BaseModel;

class ViewUsernamePasswordAddress extends BaseModel {
    protected $table = 'view_username_password_address';
    public $timestamps = false;
}
