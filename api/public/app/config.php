<?php
use Illuminate\Database\Capsule\Manager as Capsule;
require("../vendor/autoload.php");
$capsule = new Capsule();


$capsule->addConnection([
    'driver' => 'pgsql',
    'host' => '216.244.94.124', 
    'port' => '5432', // pake ini jika port bukan 5432 (port default postgres)
    'username' => 'postgres', 
    'password' => 'system', 
    'database' => 'pajak',//'ManProject_15042020',//'Update130318',//'Update15032018', 
//  'schema' => 'public', // line ini tidak perlu karena schema 'public' sudah default value postgres
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => ''
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();