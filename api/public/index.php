<?php
//require("../vendor/autoload.php");
require_once 'app/config.php';
require_once 'routes.php';

use \Model\ApiRoutes as Entity;
$router = new \Nettuts\Router;

// routes template: ControllerName:methodName@requestMethod
if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }
$router->set404Handler("Main:notFound"); 
$router->addRoutes([
	'/' => 'Main:index@get',
	'/routes' => 'ApiRoutesController:index@get',
	'/routes/create' => 'ApiRoutesController:create@post',
	'/routes/update' => 'ApiRoutesController:put@post',
	'/routes/find' => 'ApiRoutesController:listByController@post',
	'/routes/exist' => 'ApiRoutesController:pathExist@post'
	]);

/*$fromDb = Entity::all()->toArray();
$routes = array();

foreach ($fromDb as $value) {
    $paths = explode(",", $value["path"]);
    $methods = explode(",", $value["method"]);
    $functions = explode(",", $value["functions"]);
    $controller = $value["controller"];
    for ($i = 0; $i < min([sizeof($paths), sizeof($methods)]) ; $i++) { 
        $routes["/" . $paths[$i]] = $controller . ":" . $functions[$i]. "@" . $methods[$i]; 
    }       
}

function implodeItem(&$item, $key) // Note the &$item
{
  $item = "'" . $key . "' => '" . $item . "'";
}

array_walk($routes, "implodeItem");
print_r(implode(", \n", $routes));
die();
*/

$router->addRoutes($routes);
$router->run();