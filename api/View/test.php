<html lang="en">
<head>
	<title>API Routes</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body ng-app="app">
	<div class="col-md-12">
		<div class="container panel panel-primary">
			<h1>API Routes List</h1>
			<div class="alert alert-primary" ng-repeat="route in $root.routes">
				<h3>{{route}}</h3>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.3/angular.min.js"></script>
	<script type="text/javascript">
		var app = angular.module("app", []);
		app.run(['$rootScope', '$http', function ($rootScope, $http) {
			
			$http.get("http://localhost:88/eproc/itpi-eproc/api/public/routes").then(function(result){
				$rootScope.routes = result.data.result.data;
				console.info($rootScope.routes);
				//$rootScope.$apply();
			});

		}]);

	</script>
</body>
