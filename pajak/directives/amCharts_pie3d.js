/** 
 * Options : {
 *      id : '',
 *      width : '',
 *      height : '',
 *      data : [{}],
 *      value : '',
 *      category : ''
 * }
*/
rajampat
  .directive("pieChart", function () {
    return {
      restrict: "A",
      scope: {
        options : '='
      },
      // templateUrl: 'pie.html',
      // template : "<div class='chartdivPie3d'></div>",
      // replace: true,
      link: function ($scope) {
        am4core.useTheme(am4themes_animated);
        var width = $scope.options.width || '100%';
        var height = $scope.options.height || '100%';

        $('.chartdivPie3d').attr('id', $scope.options.id);
        $('.chartdivPie3d').css('width', width);
        $('.chartdivPie3d').css('height', height);

        var chart = am4core.create($scope.options.id, am4charts.PieChart3D);

        chart.hiddenState.properties.opacity = 0;
        chart.data = $scope.options.data;
        chart.innerRadius = am4core.percent(40);
        chart.depth = 120;

        chart.legend = new am4charts.Legend();

        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = $scope.options.value;
        series.dataFields.depthValue = $scope.options.value;
        series.dataFields.category = $scope.options.category;
        series.slices.template.cornerRadius = 5;
        series.colors.step = 3;

        $scope.$on("$destroy", function () {
          chart.dispose();
        });
      }
    };
  });