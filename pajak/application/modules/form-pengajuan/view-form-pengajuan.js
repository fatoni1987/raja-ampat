angular.module("rajampat").controller("viewNpwpdCtrl", function($scope, $http, $rootScope, $modal, SweetAlert, $filter, $stateParams, $state) {
    // console.info($stateParams.id);    
    $scope.load = function() {
        $rootScope.authorize().then(function(result) {
            loadData();
        })        
    };   

    function loadData() {
        $http.post($rootScope.url_api + "npwpd/viewData",{
            id : $stateParams.id
        }).success(function(reply) {
            if (reply.status === 200) {
                $scope.DataTab = reply.result.data.data_pajak[0];
                $scope.verifikasi = reply.result.data.npwpd[0];
                $scope.tab1 = {
                    // 'nama_badan_usaha' : '',
                    'kewarganegaraan' : $scope.DataTab.kewarganegaraan,
                    'type_identitas' : '',
                    'nama' : '',
                    'alamat' : '',
                    'email' : '',
                    'no_telp' : '',
                    'nik' : ''
                }
                $scope.tab2 = {
                    'nama_bidang_usaha' : '',
                    'alamat_usaha' : '',
                    'rt' : '',
                    'kecamatan' : '',
                    'kelurahan_usaha' : '',
                    'kabupaten_usaha' : '',
                    'kode_pos' : '',
                    'telp_usaha' : '',
                    'fax_usaha' : '',
                    'email_usaha' : ''
                }
                $scope.tab3 = {
                    'no_pbb' : '',
                    'no_izin_usaha' : '',
                    'nama_izin_usaha' : '',
                    'datePerkiraan' : ''
                }
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data Form!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        });
    }

    $scope.backList = function(){
        $state.go("lihatNpwpd");
    }

    $scope.print = function(){
        html2canvas(document.getElementById('print'), {
            onrendered: function (canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500,
                    }]
                };
                pdfMake.createPdf(docDefinition).download( $scope.verifikasi.no_npwpd + ".pdf");
            }
        });
     }

});