angular.module("rajampat").controller("formPengajuanCtrl", function($scope, $http, $rootScope, $modal, SweetAlert, $filter, $stateParams, $state) {
    $scope.tab1 = {
        'dateFormulir' : $rootScope.dateFormat(new Date()),
        'nama_badan_usaha' : 'Hotel',
        'kewarganegaraan' : '',
        'type_identitas' : '',
        'nama' : '',
        'alamat' : '',
        'email' : '',
        'no_telp' : '',
        'nik' : '',
        'bukti_identitas_user_pajak' : ''
    }
    $scope.tab2 = {
        'nama_bidang_usaha' : '',
        'alamat_usaha' : '',
        'rt' : '',
        'kecamatan' : '',
        'kelurahan_usaha' : '',
        'kabupaten_usaha' : 'Raja Ampat',
        'kode_pos' : '',
        'telp_usaha' : '',
        'fax_usaha' : '',
        'email_usaha' : '',
        'Lainnya' : '',
        'bukti_akta_data_pajak' : '',
        'bukti_sewa_data_pajak' : '',
        'bukti_sertifikat_data_pajak' : ''
    }
    $scope.tab3 = {
        'no_pbb' : '',
        'no_izin_usaha' : '',
        'nama_izin_usaha' : '',
        'datePerkiraan' : '',
        'bukti_pelunasan_pbb_data_pajak' : '',
        'foto_depan_data_pajak' : '',
        'denah_data_pajak' : '',
        'brosur_data_pajak' : '',
        'rekap_data_pajak' : ''
    }
    $scope.tab4 = {
        'pendapatan_bulan' : 0,
        'bayar' : '',
        // 'bayarLainnya' : '',
    }

    $scope.tab4HotelResto = {
        'mejaRest' : 0,
        'kursiRest' : 0
    }
    $scope.tab4HotelHiburan = {
        'jenis_hiburan' : '',
        'kursiHiburan' : 0,
        'mejaHiburan' : 0,
        'jumlahTiket' : 0,
        'hargaTiket' : 0,
    }
    $scope.tab4HotelParkir = {
        'motor' : 0,
        'mobil' : 0,
        'seluruhnya' : 0,
    }
    $scope.tab4HotelLaundry = {
        'nama_laundry' : 0,
        'telp_laundry' : 0,
    }

    $scope.tab4Resto = {
        'mulai' : 0,
        'buka' : 0,
        'meja' : '',
        'kursi' : 0,
        'makanan_termahal' : 0,
        'makanan_termurah' : 0,
        'minuman_termahal' : 0,
        'minuman_termurah' : 0,
        'pendapatan_harian' : 0,
        'seluruhnya' : 0,
        'service' : 0,
        'bayar' : 0,
        'bayarLainnya' : 0,
    }
    $scope.tab4Parkir = {
        'luas_area' : 0,
        'kapasitas_mobil' : 0,
        'kapasitas_motor' : '',
        'jumlah_pegawai' : 0,
    }
    $scope.tab4Hiburan = {
        'jumlah_kursi' : 0,
        'jumlah_tiket' : 0,
        'tarif' : '',
        'jumlah_pegawai' : 0,
    }
    $scope.tab4Reklame = {
        'isi_reklame' : '',
        'jenis_reklame' : '',
        'tempat_reklame' : '',
        // 'kawasan_reklame' : '',
        // 'kelas_reklame' : '',
        'lebarReklame' : '',
        'panjang_reklame' : '',
        'luas_reklame' : '',
        'sudut_pandang' : '',
        'masa_pasang' : '',
        'tinggi_reklame' : ''
    }
    $scope.tab3Kamar = [
        {
            'nama_layanan_hotel' : 'Suite',
            'jml_kamar' : 0,
            'nominalHigh' : 0,
            'nominalLow' : 0,
            'periodeLow' : '',
            'periodeHigh' : '',
        },
        {
            'nama_layanan_hotel' : 'Deluxe',
            'jml_kamar' : 0,
            'nominalHigh' : 0,
            'nominalLow' : 0,
            'periodeLow' : '',
            'periodeHigh' : '',
        },
        {
            'nama_layanan_hotel' : 'Standart',
            'jml_kamar' : 0,
            'nominalHigh' : 0,
            'nominalLow' : 0,
            'periodeLow' : '',
            'periodeHigh' : '',
        },
        {
            'nama_layanan_hotel' : 'Lain -lain',
            'jml_kamar' : 0,
            'nominalHigh' : 0,
            'nominalLow' : 0,
            'periodeLow' : '',
            'periodeHigh' : '',
        },
        {
            'nama_layanan_hotel' : 'Ruang Meeting / Pertemuan',
            'jml_kamar' : 0,
            'nominalHigh' : 0,
            'nominalLow' : 0,
            'periodeLow' : '',
            'periodeHigh' : '',
        },
    ];

    $scope.nama_bukti_identitas_user_pajak = '';
    $scope.nama_bukti_akta_data_pajak = '';
    $scope.nama_bukti_sewa_data_pajak = '';
    $scope.nama_bukti_sertifikat_data_pajak = '';
    $scope.nama_bukti_pelunasan_pbb_data_pajak = '';
    $scope.nama_foto_depan_data_pajak = '';
    $scope.nama_denah_data_pajak = '';
    $scope.nama_brosur_data_pajak = '';
    $scope.nama_rekap_data_pajak = '';
    
    $scope.infoTab = 1;
    
    // $scope.myConfigKel = {
    //     // create: true,
    //     maxItems: 1,
    //     searchField: ['nama_mst_kelurahan'],
    //     valueField: 'kode_mst_kelurahan',
    //     labelField: 'nama_mst_kelurahan',
    // }
    $scope.myConfig = {
        // create: true,
        maxItems: 1,
        searchField: ['nama_jalan_nilai_strategis'],
        valueField: 'id_nilai_jalan',
        labelField: 'nama_jalan_nilai_strategis',
    }
    $scope.myConfigJenisReklame = {
        // create: true,
        maxItems: 1,
        searchField: ['nama_jenis_reklame'],
        valueField: 'nama_jenis_reklame',
        labelField: 'nama_jenis_reklame',
    }
    $scope.myConfigKawasanReklame = {
        // create: true,
        maxItems: 1,
        searchField: ['lokasi_kawasan'],
        valueField: 'id_kawasan',
        labelField: 'lokasi_kawasan',
    }
    $scope.myConfigSudutReklame = {
        // create: true,
        maxItems: 1,
        searchField: ['arah_sudut_pandang'],
        valueField: 'id_sudut_pandang',
        labelField: 'arah_sudut_pandang',
    }
    // $scope.myConfigKecamatan = {
    //     // create: true,
    //     maxItems: 1,
    //     searchField: ['nama_mst_distrik'],
    //     valueField: 'kode_mst_distrik',
    //     labelField: 'nama_mst_distrik',
    // }
    $scope.load = function() {
        $rootScope.fileuploadconfig(34);
        $rootScope.authorize().then(function(result) {
            loadData();
        })        
        
    };   

    function loadData() {
        $http.post($rootScope.url_api + "formPengajuan/select",{
            userID : $rootScope.userID,
            role : $rootScope.roleID
        }).success(function(reply) {
            if (reply.status === 200) {
                $scope.DataTab = reply.result.data.form;
                $scope.DataBidangUsaha = reply.result.data.bidang;
                $scope.DataJenisKamar = reply.result.data.kamar;
                $scope.DataJenisBadanUsaha = reply.result.data.badan;
                $scope.DataDistrik = reply.result.data.distrik;
                $scope.DataKelurahan = reply.result.data.kelurahan;
                $scope.DataUserPajak = reply.result.data.user[0];
                $scope.tab1 = {
                    'dateFormulir' : $rootScope.dateFormat(new Date()),
                    'nama_badan_usaha' : 'Hotel',
                    'kewarganegaraan' : $scope.DataUserPajak.warga_negara_user_pajak,
                    'type_identitas' : $scope.DataUserPajak.tipe_identitas_user_pajak,
                    'nik' : $scope.DataUserPajak.no_identitas_user_pajak,
                    'nama' : $scope.DataUserPajak.pemilik_user_pajak,
                    'alamat' : $scope.DataUserPajak.alamat_user_pajak,
                    'email' : $scope.DataUserPajak.email_user_pajak,
                    'no_telp' : $scope.DataUserPajak.telp_user_pajak
                }
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data Form!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        });
        $http.post($rootScope.url_api + "user/selectMstReklame",{
        }).success(function(reply) {
            if (reply.status === 200) {
                $scope.DataReklame = reply.result.data;
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data Form!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        });
    }

    $scope.filterKelurahan = function(){
        $http.post($rootScope.url_api + "formPengajuan/selectKelurahan",{
            data : $scope.tab2.kecamatan,
        }).success(function(reply) {
            if (reply.status === 200) {
                $scope.DataKelurahan = reply.result.data;
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data Form!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        });
    }

    $scope.hitungLuasReklame = function(){
        $scope.tab4Reklame.luas_reklame = parseFloat($scope.tab4Reklame.lebarReklame * $scope.tab4Reklame.panjang_reklame * $scope.tab4Reklame.tinggi_reklame);
    }

    $scope.next = function(event, target, param){
        if(param == 1){
            if($scope.tab1.nama_badan_usaha == ""){
                $.growl.error({message: "Pilih Bidang Usaha!"});
            }else if($scope.tab1.kewarganegaraan == ""){
                $.growl.error({message: "Kewarganegaraan tidak boleh kosong!"});
            }else if($scope.tab1.type_identitas == ""){
                $.growl.error({message: "Pilih Type Identitas!"});
            }else if($scope.tab1.nik == ""){
                $.growl.error({message: "No Identitas tidak boleh kosong!"});
            }else if($scope.tab1.nama == ""){
                $.growl.error({message: "Nama Pemilik tidak boleh kosong!"});
            }else if($scope.tab1.alamat == ""){
                $.growl.error({message: "Alamat tidak boleh kosong!"});
            }else if($scope.tab1.email == ""){
                $.growl.error({message: "Email tidak boleh kosong!"});
            }else if($scope.tab1.no_telp == ""){
                $.growl.error({message: "No Telp tidak boleh kosong!"});
            }else{
                $scope.infoTab = parseInt(param + 1);
            }
        }else if(param == 2){
            // console.info($scope.tab2);
            if($scope.tab2.nama_bidang_usaha == ""){
                $.growl.error({message: "Pilih Jenis Badan Usaha!"});
            }else if($scope.tab2.alamat_usaha == ""){
                $.growl.error({message: "Alamat usaha tidak boleh kosong!"});
            }else if($scope.tab2.rt == ""){
                $.growl.error({message: "RT / RW tidak boleh kosong!"});
            }else if($scope.tab2.kecamatan == ""){
                $.growl.error({message: "Kecamatan tidak boleh kosong!"});
            }else if($scope.tab2.kelurahan_usaha == ""){
                $.growl.error({message: "Kelurahan tidak boleh kosong!"});
            }else if($scope.tab2.kabupaten_usaha == ""){
                $.growl.error({message: "Kabupaten tidak boleh kosong!"});
            }else if($scope.tab2.kode_pos == ""){
                $.growl.error({message: "Kode Pos tidak boleh kosong!"});
            }else if($scope.tab2.telp_usaha == ""){
                $.growl.error({message: "No Telp tidak boleh kosong!"});
            }else if($scope.tab2.email_usaha == ""){
                $.growl.error({message: "Email usaha tidak boleh kosong!"});
            }else{
                $scope.infoTab = parseInt(param + 1);
            }
        }else if(param == 3){
            if($scope.tab3.no_pbb == ""){
                $.growl.error({message: "No PBB tidak boleh kosong"});
            }else if($scope.tab3.no_izin_usaha == ""){
                $.growl.error({message: "No izin usaha tidak boleh kosong!"});
            }else if($scope.tab3.nama_izin_usaha == ""){
                $.growl.error({message: "Nama izin usaha tidak boleh kosong!"});
            }else if($scope.tab3.datePerkiraan == ""){
                $.growl.error({message: "Tanggal perkiraan tidak boleh kosong!"});
            }else{
                $scope.infoTab = parseInt(param + 1);
            }
        }
    }

    $scope.previous = function(param){      
        if(param == 0){
            $state.go("lihatNpwpd");
        }else{
            $scope.infoTab -= 1;
        }
       
    }

    $scope.simpan = function(){
        swal({
            title: 'Apakah anda yakin akan menyimpan data ini?',
            text: 'Data akan tersimpan',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then(function(result) {
            if (result.value == true) {
                var Pendukung = [];
                if($scope.tab1.nama_badan_usaha == 'Hotel'){
                    Pendukung = $scope.tab4Hotel;
                }else if($scope.tab1.nama_badan_usaha == 'Restaurant'){
                    Pendukung = $scope.tab4Resto;
                }else if($scope.tab1.nama_badan_usaha == 'Hiburan'){
                    Pendukung = $scope.tab4Hiburan;
                }else if($scope.tab1.nama_badan_usaha == 'Parkir'){
                    Pendukung = $scope.tab4Parkir;
                }

                $http.post($rootScope.url_api + "npwpd/insert", {
                    "badanUsaha" : $scope.tab1.nama_badan_usaha,
                    "userID" : $rootScope.userID,
                    "data1" : $scope.tab1,
                    "data2" : $scope.tab2,
                    "data3" : $scope.tab3,
                    "data4" : $scope.tab4,
                    "data3Kamar" : $scope.tab3Kamar,
                    "dataPendukung" : Pendukung,
                    "dataHotelResto" : $scope.tab4HotelResto,
                    "dataHotelHiburan" : $scope.tab4HotelHiburan,
                    "dataHotelParkir" : $scope.tab4HotelParkir,
                    "dataHotelLaundry" : $scope.tab4HotelLaundry,
                    "dataReklame" : $scope.tab4Reklame
                }).success(function(reply){
                    if(reply.status === 200){
                        $.growl.notice({
                            title: "[INFO]",
                            message: "Berhasil menambah data"
                        });
                        $state.go("lihatNpwpd");
                    }
                }).error(function(err){
                    $.growl.error({message: "Gagal menambah data!"});
                    return
                })
            }
        });
        
    }

    $scope.back = function(){
        $state.go("lihatNpwpd");
    }

    $scope.deleteData = function(lembaga){
        swal({
            title: 'Apakah anda yakin akan menghapus data lembaga?',
            text: 'Ini akan menghapus data lembaga',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then(function(value) {
            if (value) {
                $http.post($rootScope.url_api + "mstLembaga/delete", {
                    "id" : lembaga.mst_lembaga_id,
                    "username" : $rootScope.userLogin,
                    "nama_lembaga" : lembaga.mst_lembaga_nama_lembaga
                }).success(function(reply){
                    if(reply.status === 200){
                        $.growl.notice({
                            title: "[INFO]",
                            message: "Berhasil menghapus lembaga"
                        });
                        location.reload();
                    }
                }).error(function(err){
                    $.growl.error({
                        message: "Gagal menghapus Lembaga!"
                    });
                    return
                })
            }
        });
    }

    $scope.tes = function(){
        console.info($scope.tab3Kamar);
    }
    //////////////////////////Upload////////////////////
    $scope.fileMChanged = function (elm) {
        $scope.fileDocument = elm.files;  
        
        if ($scope.fileDocument !== '' || $scope.fileDocument !== undefined) {
            for (var i = 0; i < $scope.fileDocument.length; i++) {
                $scope.fName = $scope.fileDocument[i].name;
                $scope.fSize = ($scope.fileDocument[i].size / 1000).toFixed(1);
                if ($scope.filesToUpload.length > 0) {
                    for (i = 0; i < $scope.filesToUpload.length; i++) {
                        if ($scope.fName === $scope.filesToUpload[i].fileName) {
                            $.growl.error({title: "[PERINGATAN]", message: "File sudah ada "});
                            document.getElementById('uploadFile').value = '';
                            $scope.fileDocument = '';
                            $scope.fName = '';
                            $scope.fSize = '';
                            return;
                        }
                    }
                }
            }
        }
        // console.info($scope.filesToUpload);
    };

    $scope.tes = function(){
        console.info($scope.tab3Kamar);
    }

    $scope.filesToUpload = [];
    $scope.Dataimage =[];
    $scope.nama_file = '';
    $scope.uploads = function (current) {
        if ($scope.fileDocument === '' || $scope.fileDocument === undefined) {
            $.growl.error({title: "[PERINGATAN]", message: "File belum dipilih"});
            return;
        }else{
            $scope.filesToUpload = [];
            $scope.filesToUpload.push({
                fileName: $scope.fName,
                fileSize: $scope.fSize,
                file: $scope.fileDocument
            });            
            // console.info($scope.filesToUpload);
            if(current == 1){
                $scope.nama_bukti_identitas_user_pajak = $scope.filesToUpload[0].fileName;
            }else if(current == 2){
                $scope.nama_bukti_akta_data_pajak = $scope.filesToUpload[0].fileName;
            }else if(current == 3){
                $scope.nama_bukti_sewa_data_pajak = $scope.filesToUpload[0].fileName;
            }else if(current == 4){
                $scope.nama_bukti_sertifikat_data_pajak = $scope.filesToUpload[0].fileName;
            }else if(current == 5){
                $scope.nama_bukti_pelunasan_pbb_data_pajak = $scope.filesToUpload[0].fileName;
            }else if(current == 6){
                $scope.nama_foto_depan_data_pajak = $scope.filesToUpload[0].fileName;
            }else if(current == 7){
                $scope.nama_denah_data_pajak = $scope.filesToUpload[0].fileName;
            }else if(current == 8){
                $scope.nama_brosur_data_pajak = $scope.filesToUpload[0].fileName;
            }else if(current == 9){
                $scope.nama_rekap_data_pajak = $scope.filesToUpload[0].fileName;
            }
            uploadZip($scope.filesToUpload, current);
        }       
    };

    function uploadZip(param, current) {
        var fd = new FormData();
        for (var i = 0; i < param.length; i++) {
            angular.forEach(param[i].file, function (item) {
                console.info(item)
                fd.append("uploads", item);
            });
        }
        // if(param.length > 0){
        //     fd.append("uploads", param[0].file[0]); 
        // }
        console.info(fd);
        $http.post($rootScope.url_api + "upload/file_" + "identitas_" + $rootScope.userID , fd, {
            withCredentials: true,
            transformRequest: angular.identity(),
            headers: {'Content-Type': undefined}
        }).success(function (reply) {
            $rootScope.unloadLoadingModal();
            if (reply.status === 200) {
                var filezip = reply.result.data;
                if(current == 1){
                    $scope.tab1.bukti_identitas_user_pajak = filezip.files[0].url;
                }else if(current == 2){
                    $scope.tab2.bukti_akta_data_pajak = filezip.files[0].url;
                }else if(current == 3){
                    $scope.tab2.bukti_sewa_data_pajak = filezip.files[0].url;
                }else if(current == 4){
                    $scope.tab2.bukti_sertifikat_data_pajak = filezip.files[0].url;
                }else if(current == 5){
                    $scope.tab3.bukti_pelunasan_pbb_data_pajak = filezip.files[0].url;
                }else if(current == 6){
                    $scope.tab3.foto_depan_data_pajak = filezip.files[0].url;
                }else if(current == 7){
                    $scope.tab3.denah_data_pajak = filezip.files[0].url;
                }else if(current == 8){
                    $scope.tab3.brosur_data_pajak = filezip.files[0].url;
                }else if(current == 9){
                    $scope.tab3.rekap_data_pajak = filezip.files[0].url;
                }
                
            } else {
                $.growl.error({message: "Gagal mengupload file"});
                return;
            }
        });
    };

});