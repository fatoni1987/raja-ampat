angular.module("rajampat").controller("editNpwpdCtrl", function($scope, $http, $rootScope, $modal, SweetAlert, $filter, $stateParams, $state) {
    console.info($stateParams.id);
    $scope.tab1 = {
        'dateFormulir' : $rootScope.dateFormat(new Date()),
        'nama_badan_usaha' : 'Hotel',
        'kewarganegaraan' : '',
        'type_identitas' : '',
        'nama' : '',
        'alamat' : '',
        'email' : '',
        'no_telp' : '',
        'nik' : ''
    }
    $scope.tab2 = {
        'nama_bidang_usaha' : '',
        'alamat_usaha' : '',
        'rt' : '',
        'kecamatan' : '',
        'kelurahan_usaha' : '',
        'kabupaten_usaha' : '',
        'kode_pos' : '',
        'telp_usaha' : '',
        'fax_usaha' : '',
        'email_usaha' : ''
    }
    $scope.tab3 = {
        'no_pbb' : '',
        'no_izin_usaha' : '',
        'nama_izin_usaha' : '',
        'datePerkiraan' : ''
    }
    $scope.tab4 = {
        'pendapatan' : 0
    }
    $scope.tab4Hotel = {
        'mejaRest' : 0,
        'kursiRest' : 0,
        'jenis_hiburan' : '',
        'kursiHiburan' : 0,
        'mejaHiburan' : 0,
        'jumlahTiket' : 0,
        'hargaTiket' : 0,
        'seluruhnya' : 0,
        'service' : 0,
        'motor' : 0,
        'mobil' : 0,
        'nama_laundry' : 0,
        'telp_laundry' : 0,
        'bayar' : 0,
        'bayarLainnya' : 0,
        'mejaRest' : 0,
        'mejaRest' : 0,
    }
    $scope.tab4Resto = {
        'mulai' : 0,
        'buka' : 0,
        'meja' : '',
        'kursi' : 0,
        'makanan_termahal' : 0,
        'makanan_termurah' : 0,
        'minuman_termahal' : 0,
        'minuman_termurah' : 0,
        'pendapatan_harian' : 0,
        'seluruhnya' : 0,
        'service' : 0,
        'bayar' : 0,
        'bayarLainnya' : 0,
    }
    $scope.tab4Parkir = {
        'luas_area' : 0,
        'kapasitas_mobil' : 0,
        'kapasitas_motor' : '',
        'jumlah_pegawai' : 0,
    }
    $scope.tab4Hiburan = {
        'jumlah_kursi' : 0,
        'jumlah_tiket' : 0,
        'tarif' : '',
        'jumlah_pegawai' : 0,
    }

    $scope.load = function() {
        $rootScope.authorize().then(function(result) {
            loadData();
        })        
    };   

    function loadData() {
        $http.post($rootScope.url_api + "formPengajuan/select",{
            userID : $rootScope.userID,
            role : $rootScope.roleID
        }).success(function(reply) {
            if (reply.status === 200) {
                $scope.DataTab = reply.result.data.form;
                $scope.DataBidangUsaha = reply.result.data.bidang;
                $scope.DataJenisKamar = reply.result.data.kamar;
                $scope.DataJenisBadanUsaha = reply.result.data.badan;
                $scope.DataUserPajak = reply.result.data.user[0];
                $scope.tab1 = {
                    'dateFormulir' : $rootScope.dateFormat(new Date()),
                    'nama_badan_usaha' : 'Hotel',
                    'kewarganegaraan' : $scope.DataUserPajak.warga_negara_user_pajak,
                    'type_identitas' : $scope.DataUserPajak.tipe_identitas_user_pajak,
                    'nik' : $scope.DataUserPajak.no_identitas_user_pajak,
                    'nama' : $scope.DataUserPajak.pemilik_user_pajak,
                    'alamat' : $scope.DataUserPajak.alamat_user_pajak,
                    'email' : $scope.DataUserPajak.email_user_pajak,
                    'no_telp' : $scope.DataUserPajak.telp_user_pajak
                }
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data Form!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        });
    }

    
    $scope.next = function(event, target, param){
        if(param == 1){
            if($scope.tab1.nama_badan_usaha == ""){
                $.growl.error({message: "Pilih Bidang Usaha!"});
            }else if($scope.tab1.kewarganegaraan == ""){
                $.growl.error({message: "Kewarganegaraan tidak boleh kosong!"});
            }else if($scope.tab1.type_identitas == ""){
                $.growl.error({message: "Pilih Type Identitas!"});
            }else if($scope.tab1.nik == ""){
                $.growl.error({message: "No Identitas tidak boleh kosong!"});
            }else if($scope.tab1.nama == ""){
                $.growl.error({message: "Nama Pemilik tidak boleh kosong!"});
            }else if($scope.tab1.alamat == ""){
                $.growl.error({message: "Alamat tidak boleh kosong!"});
            }else if($scope.tab1.email == ""){
                $.growl.error({message: "Email tidak boleh kosong!"});
            }else if($scope.tab1.no_telp == ""){
                $.growl.error({message: "No Telp tidak boleh kosong!"});
            }else{
                // document.getElementById(target).style.display = "block";
                // event.cu
                // event.currentTarget.className += " active";
            }
        }else if(param == 2){
            // console.info($scope.tab2);
            if($scope.tab2.nama_bidang_usaha == ""){
                $.growl.error({message: "Pilih Jenis Badan Usaha!"});
            }else if($scope.tab2.alamat_usaha == ""){
                $.growl.error({message: "Alamat usaha tidak boleh kosong!"});
            }else if($scope.tab2.rt == ""){
                $.growl.error({message: "RT / RW tidak boleh kosong!"});
            }else if($scope.tab2.kecamatan == ""){
                $.growl.error({message: "Kecamatan tidak boleh kosong!"});
            }else if($scope.tab2.kelurahan_usaha == ""){
                $.growl.error({message: "Kelurahan tidak boleh kosong!"});
            }else if($scope.tab2.kabupaten_usaha == ""){
                $.growl.error({message: "Kabupaten tidak boleh kosong!"});
            }else if($scope.tab2.kode_pos == ""){
                $.growl.error({message: "Kode Pos tidak boleh kosong!"});
            }else if($scope.tab2.telp_usaha == ""){
                $.growl.error({message: "No Telp tidak boleh kosong!"});
            }else if($scope.tab2.email_usaha == ""){
                $.growl.error({message: "Email usaha tidak boleh kosong!"});
            }else{
                // document.getElementById(target).style.display = "block";
                // event.cu
                // event.currentTarget.className += " active";
            }
        }else if(param == 3){
            // console.info($scope.tab3);
            if($scope.tab3.no_pbb == ""){
                $.growl.error({message: "No PBB tidak boleh kosong"});
            }else if($scope.tab3.no_izin_usaha == ""){
                $.growl.error({message: "No izin usaha tidak boleh kosong!"});
            }else if($scope.tab3.nama_izin_usaha == ""){
                $.growl.error({message: "Nama izin usaha tidak boleh kosong!"});
            }else if($scope.tab3.datePerkiraan == ""){
                $.growl.error({message: "Tanggal perkiraan tidak boleh kosong!"});
            }else{
                // document.getElementById(target).style.display = "block";
                // event.cu
                // event.currentTarget.className += " active";
            }
        }
        console.info($scope.tab3Kamar);
    }

    $scope.simpan = function(){
        swal({
            title: 'Apakah anda yakin akan menyimpan data ini?',
            text: 'Data akan tersimpan',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then(function(result) {
            if (result.value == true) {
                var Pendukung = [];
                if($scope.tab1.nama_badan_usaha == 'Hotel'){
                    Pendukung = $scope.tab4Hotel;
                }else if($scope.tab1.nama_badan_usaha == 'Restaurant'){
                    Pendukung = $scope.tab4Resto;
                }else if($scope.tab1.nama_badan_usaha == 'Hiburan'){
                    Pendukung = $scope.tab4Hiburan;
                }else if($scope.tab1.nama_badan_usaha == 'Parkir'){
                    Pendukung = $scope.tab4Parkir;
                }

                $http.post($rootScope.url_api + "npwpd/insert", {
                    "badanUsaha" : $scope.tab1.nama_badan_usaha,
                    "userID" : $rootScope.userID,
                    "data1" : $scope.tab1,
                    "data2" : $scope.tab2,
                    "data3" : $scope.tab3,
                    "dataPendukung" : Pendukung
                }).success(function(reply){
                    if(reply.status === 200){
                        $.growl.notice({
                            title: "[INFO]",
                            message: "Berhasil menambah data"
                        });
                        $state.go("lihatNpwpd");
                    }
                }).error(function(err){
                    $.growl.error({message: "Gagal menambah data!"});
                    return
                })
            }
        });
        
    }

    $scope.back = function(){
        $state.go("lihatNpwpd");
    }

    $scope.deleteData = function(lembaga){
        swal({
            title: 'Apakah anda yakin akan menghapus data lembaga?',
            text: 'Ini akan menghapus data lembaga',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then(function(value) {
            if (value) {
                $http.post($rootScope.url_api + "mstLembaga/delete", {
                    "id" : lembaga.mst_lembaga_id,
                    "username" : $rootScope.userLogin,
                    "nama_lembaga" : lembaga.mst_lembaga_nama_lembaga
                }).success(function(reply){
                    if(reply.status === 200){
                        $.growl.notice({
                            title: "[INFO]",
                            message: "Berhasil menghapus lembaga"
                        });
                        location.reload();
                    }
                }).error(function(err){
                    $.growl.error({
                        message: "Gagal menghapus Lembaga!"
                    });
                    return
                })
            }
        });
    }

});