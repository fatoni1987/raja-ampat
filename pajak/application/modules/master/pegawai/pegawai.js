angular.module("rajampat").controller("pegawaiCtrl", function($scope, $http, $rootScope, $modal) {
    $scope.totalItems = 0;
    $scope.currentPage = 0;
    $scope.maxSize = 10;
    $scope.Pegawais = [];
    $scope.page_id = 35;
    $scope.userBisaMengatur = false;
    $scope.userBisaMenambah = false;
    $scope.userBisaMengubah = false;
    $scope.userBisaMenghapus = false;
    $scope.kata = new Kata("");
    $scope.menuhome = 0;
    $scope.userId = 0;
   
    $scope.loadPegawai = function() {
        $rootScope.authorize().then(function(result) {
            $scope.userId = $rootScope.userID;
            loadData();
            hakAksesUSer();
        })
    };
    $scope.cariPegawai = function() {
        $rootScope.authorize().then(function(){
            $http.post($rootScope.url_api + "pegawai/pegawaicountsearch", {
                nama_pegawai: "%" + $scope.kata.srcText.toLowerCase() + "%",
                "offset": $scope.currentPage,
                "limit": $scope.maxSize
            }).success(function(reply) {
                if (reply.status === 200) {
                    var data = reply.result.data;
                    $scope.totalItems = data
                } else $.growl.error({
                    message: "Gagal mendapatkan data jumlah pegawai!"
                })
            }).error(function(err) {
                $.growl.error({
                    message: "Gagal Akses API >" + err
                })
            });
            $http.post($rootScope.url_api + "pegawai/pegawaisearch", {
                nama_pegawai: "%" + $scope.kata.srcText.toLowerCase() + "%",
                "offset": $scope.currentPage,
                "limit": $scope.maxSize
            }).success(function(reply) {
                if (reply.status === 200) {
                    var data = reply.result.data.pegawai;
                    $scope.Pegawais = data
                } else $.growl.error({
                    message: "Gagal mendapatkan data pegawai!"
                })
            }).error(function(err) {
                $.growl.error({
                    message: "Gagal Akses API >" + err
                })
            })
        })
        
    };

    function loadData() {
        var param = [];
        param.push($rootScope.userlogged);
        param.push($scope.page_id);        
        $http.post($rootScope.url_api + "pegawai/pegawaicountsearch", {
            nama_pegawai: "%" + $scope.kata.srcText + "%",
            "offset": $scope.currentPage,
            "limit": $scope.maxSize
        }).success(function(reply) {
            if (reply.status === 200) {
                var data = reply.result.data;
                $scope.totalItems = data
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data jumlah pegawai!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        });
        $http.post($rootScope.url_api + "pegawai/pegawaisearch", {
            nama_pegawai: "%" + $scope.kata.srcText.toLowerCase() + "%",
            "offset": 0,
            "limit": $scope.maxSize
        }).success(function(reply) {
            if (reply.status === 200) {
                var data = reply.result.data.pegawai;
                $scope.Pegawais = data
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data pegawai!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        })
    }

    function hakAksesUSer() {
        // cek hak akses
        $rootScope.cekHakAksesUser($scope.page_id,1).then((response) => {
            $scope.userBisaMengatur = response;
          }).catch((response) => {
            $scope.userBisaMengatur = response;
          });
          $rootScope.cekHakAksesUser($scope.page_id,2).then((response) => {
            $scope.userBisaMenambah = response;
          }).catch((response) => {
            $scope.userBisaMenambah = response;
          });
          $rootScope.cekHakAksesUser($scope.page_id,3).then((response) => {
            $scope.userBisaMengubah = response;
          }).catch((response) => {
            $scope.userBisaMengubah = response;
          });
          $rootScope.cekHakAksesUser($scope.page_id,4).then((response) => {
            $scope.userBisaMenghapus = response;
          }).catch((response) => {
            $scope.userBisaMenghapus = response;
          });
    }

    $scope.jLoad2 = function(current) {
        $scope.Pegawais = [];
        $scope.currentPage = current;
        $scope.offset = current * 10 - 10;
        $rootScope.authorize().then(function(){
            $http.post($rootScope.url_api + "pegawai/pegawaisearch", {
                nama_pegawai: "%" + $scope.kata.srcText.toLowerCase() + "%",
                "offset": $scope.offset,
                "limit": $scope.maxSize
            }).success(function(reply) {
                if (reply.status === 200) {
                    var data = reply.result.data.pegawai;
                    $scope.Pegawais = data
                } else {
                    $.growl.error({
                        message: "Gagal mendapatkan data pegawai!"
                    });
                    return
                }
            }).error(function(err) {
                $.growl.error({
                    message: "Gagal Akses API >" + err
                });
                return
            })
        })
    };
    $scope.viewPegawai = function(idPeg) {
        var item = {
            userId: $scope.userId,
            data: idPeg
        };
        var modalInstance = $modal.open({
            templateUrl: "viewModalPegawai.html",
            controller: editModalPegawai,
            resolve: {
                item: function() {
                    return item
                }
            }
        });
        modalInstance.result.then(function() {
            $rootScope.authorize().then(function(){
                loadData();
            })
        })
    };
    $scope.addPegawai = function(size) {
        $rootScope.authorize().then(function(){
            var item = {
                userId: $scope.userId
            };
            var modalInstance = $modal.open({
                templateUrl: "addModalPegawai.html",
                controller: addModalPegawai,
                backdrop: 'static',
                size: size,
                resolve: {
                    item: function() {
                        return item
                    }
                }
            });
            
            modalInstance.result.then(function(newPeg) {
                $rootScope.authorize().then(function(){
                    loadData()
                })
            })
        })
        
    };
    $scope.editPegawai = function(idPeg) {
        var item = {
            userId: $scope.userId,
            data: idPeg
        };
        var modalInstance = $modal.open({
            templateUrl: "editModalPegawai.html",
            controller: editModalPegawai,
            resolve: {
                item: function() {
                    return item
                }
            }
        });
        modalInstance.result.then(function() {
            $rootScope.authorize().then(function(){
                loadData();
            })
        })
    };

    $scope.remove = function(idPeg) {
        swal({
            title: 'Apakah anda yakin akan menghapus data ini?',
            text: 'Data akan terhapus',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then(function(result) {
            if (result.value == true) {
                $http.post($rootScope.url_api + "pegawai/deletepegawai", {
                    UserID: idPeg
                }).success(function(reply) {
                    if (reply.status === 200) {
                        $.growl.notice({
                            title: "[INFO]",
                            message: "Berhasil Menghapus Data Admin Tersebut!!"
                        });
                        loadData();
                        $rootScope.unloadLoadingModal();
                        $modalInstance.close()
                        
                    } else {
                        $.growl.error({
                            message: "Gagal mendapatkan data Admin!!"
                        });
                        return
                    }
                }).error(function(err) {
                    $.growl.error({
                        message: "Gagal Akses API >" + err
                    });
                    return
                })
            }
        });
        
    }
}).controller("ResetPasswordPegawaiCtrl", function($scope, $rootScope, $modal, $http) {
    $scope.totalItems = 0;
    $scope.currentPage = 0;
    $scope.maxSize = 10;
    $scope.Pegawais = [];
    $scope.page_id = 36;
    $scope.userBisaMengatur = false;
    $scope.menuhome = 0;
    $scope.loadResetPass = function() {
        $scope.menuhome = $rootScope.menuhome;
        $rootScope.authorize().then(function(result) {
            loadDataReset();
        })
    };

    function loadDataReset() {
        $scope.currentPage = 1;
        $http.get($rootScope.url_api + "pegawai/hitungreset", {
            username: $rootScope.userLogin,
            page_id: $scope.page_id
        }).success(function(reply) {
            if (reply.status === 200) {
                var data = reply.result.data;
                $scope.totalItems = data
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data hak akses!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        });
        $scope.Pegawais = [];
        var offset = 0;
        var limit = 10;
        $http.post($rootScope.url_api + "pegawai/find", {
            offset: offset,
            limit: limit
        }).success(function(reply) {
            if (reply.status ===
                200) {
                var data = reply.result.data;
                $scope.Pegawais = data
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data hak akses!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        })
    }
    $scope.jLoad = function(current, urutan) {
        $scope.Pegawais = [];
        $scope.currentPage = current;
        $scope.offset = current * 10 - 10;
        var limit = 10;
        $http.post($rootScope.url_api + "pegawai/find", {
            offset: $scope.offset,
            limit: limit
        }).success(function(reply) {
            if (reply.status === 200) {
                var data = reply.result.data;
                $scope.Pegawais =
                    data
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data hak akses!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        })
    };
    $scope.viewPegawai = function(idPeg) {
        var modalInstance = $modal.open({
            templateUrl: "viewModalPegawai.html",
            controller: editModalPegawai,
            resolve: {
                item: function() {
                    return idPeg
                }
            }
        });
        modalInstance.result.then(function() {
            loadData()
        })
    };
    $scope.reset = function(idPeg) {
        var lempar = {
            data: idPeg,
            username: $rootScope.userLogin
        };
        var modalInstance = $modal.open({
            templateUrl: "resetModalPegawai.html",
            controller: resetPassword,
            resolve: {
                item: function() {
                    return lempar
                }
            }
        });
        modalInstance.result.then(function() {
            loadDataReset()
        })
    }
});
var editModalPegawai = function($scope, $http, $modalInstance, item, $rootScope, $modal) {
    $scope.page_id = 35;
    var data = item.data;
    console.info(data);

    $scope.editPegawai = new Pegawai(data.pegawai_id, data.nama, data.nik, data.email, data.telepon, data.username, data.password, data.created_by, data.created_date, data.updated_by, data.updated_date, data.flag_active, data.password, data.jabatan, data.bagian, data.atasan_id);

    $scope.editPegawaiMurni = new Pegawai(data.pegawai_id, data.nama, data.nik, data.email, data.telepon, data.username, data.password, data.created_by, data.created_date, data.updated_by, data.updated_date, data.flag_active, data.password, data.jabatan, data.bagian, data.atasan_id);

    var old_nik = $scope.editPegawai.Nik;
    $scope.nik_atasan = data.nik_atasan;
    $scope.nama_atasan = data.nama_atasan;
    $scope.departemen_nama = data.departemen_nama;
    $scope.editPass = new resetPass("", "", "");
    $scope.selectedOption;
    $scope.selectedDepartemen;
    $scope.departemen = [];
    $scope.authority = data.authority;
    $scope.userId = item.userId;
    $scope.selectedJabatan;
    $scope.jabatans = [];
    $scope.role_id = data.role_id;
    $scope.departemenId = data.departemen;
    $scope.jabatan = data.jabatan;
    $scope.atasanTerpilih = {
        pegawai_id: data.atasan_id,
        nama_pegawai: data.nama_atasan
    };
    $scope.txt_disabled = true;
    $scope.newJabatan = "";

    function loadData() {
        $http.post($rootScope.url_api + "roles/get_role_user", {
            jenis_role: "USER",
            is_active: 1
        }).success(function(reply) {
            if (reply.status === 200) {
                var data = reply.result.data;
                $scope.roleUser = data;
                for (var i = 0; i < $scope.roleUser.length; i++)
                    if ($scope.roleUser[i].role_id === $scope.role_id) {
                        $scope.selectedOption = $scope.roleUser[i];
                        break
                    }
            } else {
                $.growl.error({
                    message: "Gagal Mendapatkan Data Role!!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            $http.post($rootScope.url_api + "logging", {
                message: "Tidak berhasil akses API : " + JSON.stringify(err),
                source: "pajak.js - rekanan/cekBisaMengubahData"
            }).then(function(response) {});
            return
        });
    }

    $scope.init = function() {
        $rootScope.loadLoadingModal('Silahkan Tunggu ...')
        $rootScope.authorize().then(function(result) {
            loadData();
        })
    };

    $scope.change = function(obj) {
        $scope.selectedOption = obj
    };

    $scope.compileTemplate = function(param){
        var dataubah = `<table border="1">
                        <thead>
                        <th>No</th>
                        <th>Data Yang Diubah</th>
                        <th>Data Lama</th>
                        <th>Data Baru</th>
                        </thead>
                        <tbody>
                        `;
        for (let i = 0; i < param.length; i++) {
            dataubah += `<tr>
                         <td style="text-align: center">`+(i+1)+`</td>
                         <td>`+param[i].kolom+`</td>
                         <td style="text-align: center">`+param[i].Lama+`</td>
                         <td style="text-align: center">`+param[i].Baru+`</td>
                         </tr>`;
            
        }
            dataubah += '</tbody></table>';
        $scope.compiledKonten = $scope.kontenEmail
                                      .replace('#signature',$scope.signature.nilai)
                                      .replace('#dataubah',dataubah)
                                      .replace('#siteeprocadmin',$scope.siteEproc.nilai)
                                      .replace('#siteeprocadmin',$scope.siteEproc.nilai)
        

    };

    $scope.updatePegawai = function() {
        var dataGanti = []
        
        var pwd = $scope.editPass.newPass;
        // cek apakah user menginputkan password baru / tidak
        if(pwd){
            new_pwd = $.md5($scope.editPass.newPass);
            // Regex password minimal terdiri dari 1 huruf kapital dan tidak boleh pakai tanda ./? dll
            var passValid = true;
            if(!/\d/.test($scope.editPass.newPass))
                passValid = false;
            if(!/[a-z]/.test($scope.editPass.newPass))
                passValid = false;
            if(!/[A-Z]/.test($scope.editPass.newPass))
                passValid = false;
            if(/[^0-9a-zA-Z]/.test($scope.editPass.newPass))
                passValid = false;
            if (passValid == false){
                $.growl.error({
                    title: "[PERINGATAN]", 
                    message: "Password yg Dimasukkan Bukan Merupakan Gabungan Huruf Kecil, Huruf Besar, dan Angka"
                });
                return;
            }
            if($scope.editPass.newPass.length < 8 ){
                $.growl.error({
                    title: "[PERINGATAN]", 
                    message: "Panjang Password Kurang dari 8 Karakter"
                });
                return;
            }
        }
        else{
            $.growl.error({
                title: "[PERINGATAN]", 
                message: "Harap Isi Password"
            });
            return;
            // new_pwd = $scope.editPegawai.Password;
        }         
       
        if ($scope.selectedOption === undefined) {
            $.growl.error({
                title: "[PERINGATAN]",
                message: "User role belum dipilih"
            });
            return
        }
        if ($scope.editPass.newPass !== $scope.editPass.confirmPass) {
            $.growl.error({
                title: "[PERINGATAN]",
                message: "Konfirmasi Password Tidak Sesuai"
            });
            return
        }
       
        $rootScope.loadLoadingModal("Silahkan Tunggu...");

        if($scope.editPegawai.Nik != $scope.editPegawaiMurni.Nik){
            dataGanti.push({Baru:$scope.editPegawai.Nik,kolom:'Nik',Lama:$scope.editPegawaiMurni.Nik});
        }
        if($scope.editPegawai.Nama != $scope.editPegawaiMurni.Nama){
            dataGanti.push({Baru:$scope.editPegawai.Nama,kolom:'Nama',Lama:$scope.editPegawaiMurni.Nama});
        }
        if($scope.editPegawai.Email != $scope.editPegawaiMurni.Email){
            dataGanti.push({Baru:$scope.editPegawai.Email,kolom:'Email',Lama:$scope.editPegawaiMurni.Email});
        }
        if($scope.editPegawai.Telepon != $scope.editPegawaiMurni.Telepon){
            dataGanti.push({Baru:$scope.editPegawai.Telepon,kolom:'Telepon',Lama:$scope.editPegawaiMurni.Telepon});
        }
        if($scope.editPegawai.Username != $scope.editPegawaiMurni.Username){
            dataGanti.push({Baru:$scope.editPegawai.Username,kolom:'Username',Lama:$scope.editPegawaiMurni.Username});
        }
        if($scope.editPass.newPass != ''){
            dataGanti.push({Baru:$scope.editPass.newPass,kolom:'Password',Lama:'-'})
        }
       
        // return;

        // var pwd = $scope.editPass.newPass;
        // var new_pwd;
        // if (pwd) new_pwd = $.md5($scope.editPass.newPass);
        // else new_pwd = $scope.editPegawai.Password;
        var arr = {
            nik: $scope.editPegawai.Nik,
            nama_pegawai: $scope.editPegawai.Nama,
            email: $scope.editPegawai.Email,
            telepon: $scope.editPegawai.Telepon,
            username: $scope.editPegawai.Username,
            password: pwd,
            //passSblm: pwd,
            flag_active: true,
            role_id: $scope.selectedOption.role_id,
        };
        $rootScope.authorize().then(function(){
            $http.post($rootScope.url_api + "pegawai/editpegawai", {
                pegawai_id: $scope.editPegawai.Id,
                data: arr,
                old_nik: old_nik,
                username: $rootScope.userLogin
            }).success(function(reply) {
                if (reply.status ===200 && reply.result.data.inserted_id === true) {
                    console.info(reply);
                    $.growl.notice({
                        title: "[INFO]",
                        message: "Berhasil mengubah data pegawai"
                    });
                    $rootScope.unloadLoadingModal();
                    $modalInstance.close()
                } else if (reply.status === 300 && reply.result.data === "duplicate") {
                    $.growl.warning({
                        title: "[INFO]",
                        message: "NIK sudah ada"
                    });
                    $rootScope.unloadLoadingModal();
                }else {
                    $.growl.error({
                        message: "Gagal mendapatkan data pegawai!!"
                    });
                    $rootScope.unloadLoadingModal();
                }
            }).error(function(err) {
                $.growl.error({
                    message: "Gagal Akses API >" + err
                });
                $rootScope.unloadLoadingModal();
                $modalInstance.close()
            })
        })
    };
    $scope.cancel = function() {
        $modalInstance.dismiss("cancel")
    }
};

var delPegawai = function($scope, $http, $modalInstance, item, $rootScope) {
    $scope.page_id = 35;
    $scope.selectedPeg = item;
    $scope.selectedId = $scope.selectedPeg.pegawai_id;
    $scope["delete"] = function() {
        $rootScope.authorize().then(function(){
            $http.post($rootScope.url_api + "pegawai/deletepegawai", {
                where: {
                    pegawai_id: $scope.selectedPeg.pegawai_id
                },
                data: {
                    flag_active: false
                }
            }).success(function(reply) {
                if (reply.status === 200) {
                    $.growl.notice({
                        title: "[INFO]",
                        message: "Berhasil Menghapus Data Pegawai Tersebut!!"
                    });
                    $rootScope.unloadLoadingModal();
                    $modalInstance.close()
                } else {
                    $.growl.error({
                        message: "Gagal mendapatkan data pegawai!!"
                    });
                    return
                }
            }).error(function(err) {
                $.growl.error({
                    message: "Gagal Akses API >" + err
                });
                return
            })
        })
    };
    $scope.cancel = function() {
        $modalInstance.dismiss("cancel")
    }
};
var resetPassword = function($scope, $modalInstance, item, $rootScope, $http) {
    $scope.page_id = 36;
    $scope.editPass = new resetPass(item.data.pegawai_id, "", "");
    $scope.username_pegawai = item.data.username;
    $scope.resetPass = function() {
        $scope.isSaving = true;
        $http.post($rootScope.url_api + "pegawai/resetpwd", {
            where: {
                pegawai_id: $scope.editPass.Id
            },
            data: {
                password: $.md5("user12345")
            },
            username: item.username
        }).success(function(reply) {
            if (reply.status === 200) $http.post($rootScope.url_api + "pegawai/getemail", {
                username: $scope.username_pegawai
            }).success(function(reply1) {
                if (reply1.status ===
                    200) {
                    var data1 = reply1.result.data[0];
                    $scope.email_pegawai = data1.email;
                    $.growl.notice({
                        title: "[INFO]",
                        message: "Reset password berhasil"
                    });
                    var variables = [];
                    var mailBody = "";
                    var mailSubject = "";
                    $http.post($rootScope.url_api + "mailconfig/getcontent", {
                        id_konten_email: 17,
                        variables: variables
                    }).success(function(reply2) {
                        if (reply2.status === 200) {
                            mailBody = reply2.result.data[0].mailBody;
                            mailSubject = reply2.result.data[0].mailSubject
                        } else {
                            $.growl.error({
                                message: "Gagal mendapatkan data konten email!"
                            });
                            return
                        }
                    }).error(function(err2) {
                        $.growl.error({
                            message: "Gagal Akses API Email Konten>" +
                                err2
                        });
                        return
                    })
                } else {
                    $.growl.error({
                        message: "Gagal melakukan reset password!"
                    });
                    return
                }
            }).error(function(err1) {
                $.growl.error({
                    message: "Gagal Akses API Email>" + err1
                });
                return
            });
            else {
                $.growl.error({
                    message: "Gagal melakukan reset password!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        })
    };
    $scope.cancel = function() {
        $modalInstance.dismiss("cancel")
    }
};
var addModalPegawai = function($scope, $modalInstance, $http, item, $rootScope, $modal) {
    $scope.page_id = 35;
    var today = new Date;
    $scope.newPegawai = new Pegawai("", "", "", "", "", "", "", 0, today, 0, today, true, "", "", "");
    $scope.roleUser = [];
    $scope.departemen = [];
    $scope.selectedOption;
    $scope.selectedDepartemen;
    $scope.selectedJabatan;
    $scope.jabatans = [];
    $scope.atasanTerpilih;
    $scope.txt_disabled = true;
    $scope.newJabatan = "";
    $scope.userId = item.userId;
    
    $scope.init = function() {
        $rootScope.authorize().then(function(){
            $http.post($rootScope.url_api + "roles/get_role_user", {
                jenis_role: "USER",
                is_active: 1
            }).success(function(reply) {
                if (reply.status === 200) {
                    var data = reply.result.data;
                    $scope.roleUser = data
                } else {
                    $.growl.error({
                        message: "Gagal Mendapatkan Data Role!!"
                    });
                    return
                }
            }).error(function(err) {
                $.growl.error({
                    message: "Gagal Akses API >" + err
                });
                return
            });
            
        })
    };
    $scope.change = function(obj) {
        $scope.selectedOption = obj
    };
    $scope.changeDepartemen =
        function(obj) {
            $scope.selectedDepartemen = obj
    };

    $scope.savePegawai = function() {
        var ck_username = /^[A-Za-z0-9_.-]{1,20}$/;
        var txt_username = $scope.newPegawai.Username;
        // Regex password minimal terdiri dari 1 huruf kapital dan tidak boleh pakai tanda ./? dll
        var passValid = true;
        if(!/\d/.test($scope.newPegawai.Password))
            passValid = false;
        if(!/[a-z]/.test($scope.newPegawai.Password))
            passValid = false;
        if(!/[A-Z]/.test($scope.newPegawai.Password))
            passValid = false;
        if(/[^0-9a-zA-Z]/.test($scope.newPegawai.Password))
            passValid = false;

        if (passValid == false){
            $.growl.error({
                title: "[PERINGATAN]", 
                message: "Password yg Dimasukkan Bukan Merupakan Gabungan Huruf Kecil, Huruf Besar, dan Angka"
            });
            return;
        }
        if($scope.newPegawai.Password.length < 8 ){
            $.growl.error({
                title: "[PERINGATAN]", 
                message: "Panjang Password Kurang dari 8 Karakter"
            });
            return;
        }
        
        // if (!ck_username.test(txt_username)) {
        //     $.growl.error({
        //         title: "[PERINGATAN]",
        //         message: "Username anda tidak valid atau mungkin terlalu panjang.\nMohon tidak menggunakan spasi, atau spesial character.\n Gunakan Username yang mudah diingat."
        //     });
        //     return
        // }
        if ($scope.newPegawai.Password !==
            $scope.newPegawai.ConfirmPassword) {
            $.growl.error({
                title: "[PERINGATAN]",
                message: "Konfirmasi Password Tidak Sama"
            });
            return
        }
        if ($scope.selectedOption === undefined) {
            $.growl.error({
                title: "[PERINGATAN]",
                message: "User role belum dipilih"
            });
            return
        }
       
        $rootScope.loadLoadingModal("Silahkan Tunggu...");
        $rootScope.authorize().then(function(){
            $http.post($rootScope.url_api +
                "pegawai/insertpegawai", {
                    nik: $scope.newPegawai.Nik,
                    nama_pegawai: $scope.newPegawai.Nama,
                    email: $scope.newPegawai.Email,
                    telepon: $scope.newPegawai.Telepon,
                    username: $scope.newPegawai.Username,
                    password: $.md5($scope.newPegawai.Password),
                    flag_active: true,
                    role_id: $scope.selectedOption.role_id,
                    created_by: $scope.userId
                }).success(function(reply) {
                $rootScope.unloadLoadingModal();
                if (reply.status === 200 && reply.result.data.inserted_id === true) {
                    $.growl.notice({
                        title: "[INFO]",
                        message: "Berhasil menambah data pegawai"
                    });
                    $modalInstance.close()
                } else if (reply.status === 300 && reply.result.data === "duplicate") {
                    $.growl.warning({
                        title: "[INFO]",
                        message: "Username atau NIK sudah ada"
                    });
                    return
                } else {
                    $.growl.error({
                        message: "Gagal mendapatkan data pegawai!!"
                    });                
                    return
                }
            }).error(function(err) {
                $.growl.error({
                    message: "Gagal Akses API >" + err
                });
                $http.post($rootScope.url_api + "logging", {
                    message: "Tidak berhasil akses API : " +
                        JSON.stringify(err),
                    source: "pajak.js - rekanan/cekBisaMengubahData"
                }).then(function(response) {});
                $rootScope.unloadLoading();
                return
            })
        })
    };
    $scope.cancel = function() {
        $modalInstance.dismiss("cancel")
    }
};
var PilihAtasanCtrl = function($scope, $modalInstance, $http, $rootScope) {
    $scope.pegawai = [];
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxSize = 10;
    $scope.kata = new Kata("");
    $scope.page_id = 10;
    $scope.initialize = function() {
        $rootScope.authorize().then(function(){
            loadAwal()
        })
    };

    function loadAwal() {
        $rootScope.loadLoadingModal("Silahkan Tunggu...");
        $http.post($rootScope.url_api + "pegawai/countsearchpanitia", {
            nama_pegawai: "%" + $scope.kata.srcText + "%"
        }).success(function(reply) {
            $rootScope.unloadLoadingModal();
            if (reply.status === 200) {
                var data =
                    reply.result.data;
                $scope.totalItems = data
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data jumlah pegawai!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        });
        $http.post($rootScope.url_api + "pegawai/selectsearchpanitia", {
            nama_pegawai: "%" + $scope.kata.srcText + "%",
            offset: 0,
            limit: 10
        }).success(function(reply) {
            $rootScope.unloadLoadingModal();
            if (reply.status === 200) {
                var data = reply.result.data;
                $scope.pegawai = data
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data pegawai!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        })
    }
    $scope.jLoad = function(current) {
        $rootScope.loadLoadingModal("Silahkan Tunggu...");
        $scope.pegawai = [];
        $scope.currentPage = current;
        $scope.offset = current * 10 - 10;
        $http.post($rootScope.url_api + "pegawai/selectsearchpanitia", {
            nama_pegawai: "%" + $scope.kata.srcText + "%",
            offset: $scope.offset,
            limit: 10
        }).success(function(reply) {
            $rootScope.unloadLoadingModal();
            if (reply.status === 200) {
                var data = reply.result.data;
                $scope.pegawai = data
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data pegawai!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        })
    };
    $scope.cariPegawai = function() {
        $rootScope.authorize().then(function(){
            $rootScope.loadLoadingModal("Silahkan Tunggu...");
            $http.post($rootScope.url_api + "pegawai/countsearchpanitia", {
                nama_pegawai: "%" + $scope.kata.srcText.toLowerCase() + "%"
            }).success(function(reply) {
                $rootScope.unloadLoadingModal();
                if (reply.status === 200) {
                    var data = reply.result.data;
                    $scope.totalItems = data
                } else {
                    $.growl.error({
                        message: "Gagal mendapatkan data jumlah pegawai!"
                    });
                    return
                }
            }).error(function(err) {
                $.growl.error({
                    message: "Gagal Akses API >" +
                        err
                });
                return
            });
            $http.post($rootScope.url_api + "pegawai/selectsearchpanitia", {
                nama_pegawai: "%" + $scope.kata.srcText + "%",
                offset: 0,
                limit: 10
            }).success(function(reply) {
                $rootScope.unloadLoadingModal();
                if (reply.status === 200) {
                    var data = reply.result.data;
                    $scope.pegawai = data
                } else {
                    $.growl.error({
                        message: "Gagal mendapatkan data pegawai!"
                    });
                    return
                }
            }).error(function(err) {
                $.growl.error({
                    message: "Gagal Akses API >" + err
                });
                return
            })
        })
        
    };
    $scope.pilihAtasan = function(pgw) {
        $modalInstance.close(pgw)
    };
    $scope.keluar = function() {
        $modalInstance.dismiss("cancel")
    }
};

function Pegawai(Id, Nama, Nik, Email, Telepon, Username, Password, CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, FlagActive, ConfirmPassword, Jabatan, Bagian, Tgl_reset, atasan_id) {
    var self = this;
    self.Id = Id;
    self.Nama = Nama;
    self.Nik = Nik;
    self.Email = Email;
    self.Telepon = Telepon;
    self.Username = Username;
    self.Password = Password;
    self.Jabatan = Jabatan;
    self.Bagian = Bagian;
    self.Createdby = CreatedBy;
    self.CreatedDate = CreatedDate;
    self.UpdatedBy = UpdatedBy;
    self.UpdatedDate = UpdatedDate;
    self.FlagActive = FlagActive;
    self.ConfirmPassword = ConfirmPassword;
    self.Tgl_reset = Tgl_reset;
    self.atasan_id = atasan_id
}

function resetPass(Id, newPass, confirmPass) {
    var self = this;
    self.Id = Id;
    self.newPass = newPass;
    self.confirmPass = confirmPass
}

function Kata(srcText) {
    var self = this;
    self.srcText = srcText
};