angular.module("rajampat").controller("viewSptpdCtrl", function($scope, $http, $rootScope, $modal, SweetAlert, $filter, $stateParams, $state) {
    
    $scope.load = function() {
        $rootScope.authorize().then(function(result) {
            loadData();
        })        
    };   

    function loadData() {
        $http.post($rootScope.url_api + "sptpd/selectSptpdById",{
            id : $stateParams.id
        }).success(function(reply) {
            if (reply.status === 200) {
                $scope.DataSptpd = reply.result.data[0];
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data Form!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        });
    }

    $scope.goBack = function() {
        window.history.back();
      }
    

});