angular.module("rajampat").controller("formsptpdCtrl", function($scope, $http, $rootScope, $modal, SweetAlert, $filter, $stateParams, $state) {
    $scope.jenis_pajak = $stateParams.jenis;
    $scope.id_data_pajak = $stateParams.id;
    
    $scope.jmlHari = [];
    $scope.dataTahun = [];
    $scope.count = 0;
    $scope.tahunNow = new Date().getFullYear();
    $scope.dataBulan = [
        {id : '1', bulan : 'Januari', hari : '31'},
        {id : '2', bulan : 'Februari', hari : '29'},
        {id : '3', bulan : 'Maret', hari : '31'},
        {id : '4', bulan : 'April', hari : '30'},
        {id : '5', bulan : 'Mei', hari : '31'},
        {id : '6', bulan : 'Juni', hari : '30'},
        {id : '7', bulan : 'Juli', hari : '31'},
        {id : '8', bulan : 'Agustus', hari : '31'},
        {id : '9', bulan : 'September', hari : '30'},
        {id : '10', bulan : 'Oktober', hari : '31'},
        {id : '11', bulan : 'November', hari : '30'},
        {id : '12', bulan : 'Desember', hari : '31'}
    ]

    for(var i=0; i<5; i++){
        $scope.dataTahun.push({tahun : $scope.tahunNow - i});
    }
    
    var currentDate = new Date();
    var bln = (currentDate.getMonth() + 1);
    var tgl = currentDate.getDate();

    $scope.harian = {
        'omset' : '',
        'terutang' : '',
        'periode' : '',
    }

    $scope.sptpd = {
        'pajak_terutang' : '',
        'prosentase' : '',
        'omset' : '',
        'periode' : '',
        'date_new' : currentDate.getFullYear() + "-" + bln + "-" + tgl
    }

    $scope.load = function() {
        $rootScope.fileuploadconfig(34);
        $rootScope.authorize().then(function(result) {
            loadData();
        })        
    };   

    // $scope.periode = [];
    $scope.pilihBulan = function(param){//console.info(JSON.parse(param));
        $scope.periode = JSON.parse(param);
        $scope.count = $scope.periode.hari;
        $scope.jmlHari = [];
        for(var i=0; i<$scope.count; i++){
            var temp = i + 1;
            $scope.jmlHari.push({'id':i, 'tgl':$rootScope.dateFormat(new Date().getFullYear() +'-'+ $scope.periode.id +'-'+ temp)});
        }
    }

    $scope.pilihProsentase = function(param){
        $scope.sptpd.prosentase = param;
        $scope.hitung();
    }

    $scope.hitung = function(){
        $scope.sptpd.pajak_terutang = parseFloat($scope.sptpd.omset * $scope.sptpd.prosentase)/100;
        // console.info($scope.sptpd.prosentase);
    }

    $scope.pilihTahun = function(param){
        $scope.count = param;
    }

    $scope.backList = function(){//console.info($scope.jenis_pajak);
        $state.go("form-sptpd",{jenis: $stateParams.jenis, id: $scope.id_data_pajak});
    }

    $scope.filesToUpload1 = [];
    $scope.uploads1 = function () {
        var idx = -1; //console.info("masuk");
        $.each($scope.filesToUpload1, function (index, item) {
            if (item.fileName === $scope.fName) {
                idx = index;
            }
        });
        //console.info($scope.filesToUpload1);
        if ($scope.fileDocument === '' || $scope.fileDocument === undefined) {
            $.growl.error({title: "[PERINGATAN]", message: "File belum dipilih"});
            return;
        } else if (idx > -1) {
            $.growl.error({title: "[PERINGATAN]", message: "File sudah ada"});
            return;
        } else {
            var fileInput = $('.upload-file');
            var extFile = $('.upload-file').val().split('.').pop().toLowerCase();
            var maxSize = fileInput.data('max-size');
            if (fileInput.get(0).files.length) {
                var fileSize = fileInput.get(0).files[0].size;
                if (fileSize > maxSize) {
                    $rootScope.unloadLoadingModal();
                    $.growl.error({title: "[WARNING]", message: "Ukuran file terlalu besar"});
                    return;
                } else {
                    var restrictedExt = $rootScope.limitfiletype;
                    if ($.inArray(extFile, restrictedExt) === -1) {
                        $rootScope.unloadLoadingModal();
                        $.growl.error({title: "[WARNING]", message: "Format file tidak valid"});
                        return;
                    } else {
                        $scope.filesToUpload1.push({
                            fileName: $scope.fName,
                            fileSize: $scope.fSize,
                            file: $scope.fileDocument
                        });
                        // console.info($scope.filesToUpload1);
                    }
                }
            }
        }
    };

    function uploadDoc() {
        var fd = new FormData();
        for (var i = 0; i < $scope.filesToUpload1.length; i++) {
            angular.forEach($scope.filesToUpload1[i].file, function (item) {
                fd.append("uploads", item);
            });
        }
        
        $http.post($rootScope.url_api + "upload/sptd_" + $scope.data_npwpd.nop, fd, {
            withCredentials: true,
            transformRequest: angular.identity(),
            headers: {'Content-Type': undefined}
        }).success(function (reply) {
            $rootScope.unloadLoadingModal();
            if (reply.status === 200) {
                var filezip = reply.result.data.files[0].url;
                // update(filezip);
            } else {
                $.growl.error({message: "Gagal mengupload file"});
                return;
            }
        });
        
    };

    $scope.fileMChanged = function (elm) {
        $scope.fileDocument = elm.files;
        // console.info($scope.fileDocument);
        if ($scope.fileDocument !== '' || $scope.fileDocument !== undefined) {
            for (var i = 0; i < $scope.fileDocument.length; i++) {
                $scope.fName = $scope.fileDocument[i].name;
                $scope.fSize = ($scope.fileDocument[i].size / 1000).toFixed(1);
                //console.log($scope.fileDocument[i]);
                if ($scope.filesToUpload1.length > 0) {
                    for (i = 0; i < $scope.filesToUpload1.length; i++) {
                        if ($scope.fName === $scope.filesToUpload1[i].fileName) {
                            $.growl.error({title: "[PERINGATAN]", message: "File sudah ada "});
                            document.getElementById('uploadFile').value = '';
                            $scope.fileDocument = '';
                            $scope.fName = '';
                            $scope.fSize = '';
                            return;
                        }
                    }
                }
            }
        }
    };

    $scope.filesToUpload2 = [];
    $scope.uploads2 = function () {
        var idx = -1;
        $.each($scope.filesToUpload2, function (index, item) {
            if (item.fileName === $scope.fName) {
                idx = index;
            }
        });console.info($scope.filesToUpload2)
        if ($scope.fileDocument1 === '' || $scope.fileDocument1 === undefined) {
            $.growl.error({title: "[PERINGATAN]", message: "File belum dipilih"});
            return;
        } else if (idx > -1) {
            $.growl.error({title: "[PERINGATAN]", message: "File sudah ada"});
            return;
        } else {
            var fileInput = $('.upload-files');
            var extFile = $('.upload-files').val().split('.').pop().toLowerCase();
            var maxSize = fileInput.data('max-size');
            if (fileInput.get(0).files.length) {
                var fileSize = fileInput.get(0).files[0].size;
                if (fileSize > maxSize) {
                    $rootScope.unloadLoadingModal();
                    $.growl.error({title: "[WARNING]", message: "Ukuran file terlalu besar"});
                    return;
                } else {
                    var restrictedExt = $rootScope.limitfiletype;
                    if ($.inArray(extFile, restrictedExt) === -1) {
                        $rootScope.unloadLoadingModal();
                        $.growl.error({title: "[WARNING]", message: "Format file tidak valid"});
                        return;
                    } else {
                        $scope.filesToUpload2.push({
                            fileName: $scope.fName,
                            fileSize: $scope.fSize,
                            file: $scope.fileDocument
                        });
                    }
                }
            }
        }
    };

    $scope.fileMChanged1 = function (elm) {
        $scope.fileDocument1 = elm.files;
        // console.info($scope.fileDocument1);
        if ($scope.fileDocument1 !== '' || $scope.fileDocument1 !== undefined) {
            for (var i = 0; i < $scope.fileDocument1.length; i++) {
                $scope.fName = $scope.fileDocument1[i].name;
                $scope.fSize = ($scope.fileDocument1[i].size / 1000).toFixed(1);
                //console.log($scope.fileDocument[i]);
                if ($scope.filesToUpload2.length > 0) {
                    for (i = 0; i < $scope.filesToUpload2.length; i++) {
                        if ($scope.fName === $scope.filesToUpload2[i].fileName) {
                            $.growl.error({title: "[PERINGATAN]", message: "File sudah ada "});
                            document.getElementById('uploadFiles').value = '';
                            $scope.fileDocument1 = '';
                            $scope.fName = '';
                            $scope.fSize = '';
                            return;
                        }
                    }
                }
            }
        }
    };

    $scope.removeFile1 = function (fileName) {
        var idx = -1;
        $.each($scope.filesToUpload1, function (index, item) {
            if (item.fileName === fileName) {
                idx = index;
            }
        });
        $scope.filesToUpload1.splice(idx, 1);
    };

    $scope.removeFile2 = function (fileName) {
        var idx = -1;
        $.each($scope.filesToUpload2, function (index, item) {
            if (item.fileName === fileName) {
                idx = index;
            }
        });
        $scope.filesToUpload2.splice(idx, 1);
    };

    function loadData() {
        $rootScope.authorize().then(function(){
            $http.post($rootScope.url_api + "npwpd/selectNpwpdById", {
                "id": $scope.id_data_pajak
            })
            .success(function (reply) {
                if (reply.status === 200) {
                    var data = reply.result.data[0];
                    $scope.npwpd = data;
                    if($scope.npwpd.jenis_usaha_pajak == 'Restaurant'){
                       $scope.sptpd.prosentase = 10; 
                    }else if($scope.npwpd.jenis_usaha_pajak == 'Hotel'){
                        $scope.sptpd.prosentase = 10;
                    }else if($scope.npwpd.jenis_usaha_pajak == 'Parkir'){
                        $scope.sptpd.prosentase = 10;
                    }else if($scope.npwpd.jenis_usaha_pajak == 'Mineral Bukan Logam'){
                        $scope.sptpd.prosentase = 25;
                    }

                    $scope.data_npwpd = {
                        "nop" : $scope.npwpd.no_npwpd,
                        "nama" : $scope.npwpd.nama_usaha_data_pajak,
                        "alamat" :$scope.npwpd.alamat_data_pajak
                    }

                    $http.post($rootScope.url_api + "sptpd/select", {
                        "id": $scope.npwpd.id_npwpd
                    })
                    .success(function (reply) {
                        if (reply.status === 200) {
                            $scope.sptpdList = reply.result.data;
                        } else {
                            $.growl.error({ message: "Gagal mendapatkan data berita!!" });
                            $rootScope.unloadLoading();
                            return;
                        }
                        $rootScope.unloadLoading();
                    })
                    .error(function (err) {
                        $.growl.error({ message: "Gagal Akses API >" + err });
                        $rootScope.unloadLoading();
                        return;
                    })

                } else {
                    $.growl.error({ message: "Gagal mendapatkan data berita!!" });
                    $rootScope.unloadLoading();
                    return;
                }
                $rootScope.unloadLoading();
            })
            .error(function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                $rootScope.unloadLoading();
                return;
            })

            
            
        })
    }
    
    $scope.pilihValidSptpd = function(param){//console.info(param.is_checked)
        if ($('#selectCheck').is(":checked"))
        {
            $scope.flag = 2;
            $scope.status = "Proses Verifikasi"
        }else{
            $scope.flag = 1;
            $scope.status = "Draft"
        }

        $http.post($rootScope.url_api + "sptpd/updateFlag", {
            "id": param.id_detil_sptpd,
            "flag" : $scope.flag,
            "status" : $scope.status
        })
        .success(function (reply) {
            if (reply.status === 200) {
                //$state.go("form-sptpd",{jenis: $stateParams.jenis, id : $scope.id_data_pajak});
            } else {
                $.growl.error({ message: "Gagal mendapatkan data!!" });
                $rootScope.unloadLoading();
                return;
            }
            $rootScope.unloadLoading();
        })
        .error(function (err) {
            $.growl.error({ message: "Gagal Akses API >" + err });
            $rootScope.unloadLoading();
            return;
        })
    }

    $scope.selesaiKirim = function(){//alert("");
        swal({
            title: 'Apakah anda yakin akan mengirim data ini untuk di verifikasi?',
            text: 'Data akan tersimpan',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then(function(result) {
            if (result.value == true) {
                $http.post($rootScope.url_api + "sptpd/updateVerifikasi", {
                    "id": $scope.npwpd.id_npwpd,
                    "jenis": $scope.jenis_pajak
                })
                .success(function (reply) {
                    if (reply.status === 200) {
                        loadData();
                        //$state.go("form-sptpd",{jenis: $stateParams.jenis, id : $scope.id_data_pajak});
                    } else if(reply.status === 300){
                        $.growl.error({ message: "Pilih data terlebih dahulu!!" });
                        $rootScope.unloadLoading();
                        return;
                    }else {
                        $.growl.error({ message: "Gagal mendapatkan data berita!!" });
                        $rootScope.unloadLoading();
                        return;
                    }
                    $rootScope.unloadLoading();
                })
                .error(function (err) {
                    $.growl.error({ message: "Gagal Akses API >" + err });
                    $rootScope.unloadLoading();
                    return;
                })    
            }
        });
    }

    $scope.hapusSptpd = function(param){
        swal({
            title: 'Apakah anda yakin akan menghapus data ini?',
            text: 'Data akan tersimpan',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then(function(result) {
            if (result.value == true) {
                $http.post($rootScope.url_api + "sptpd/deleteSptpd", {
                    "id": param.id_detil_sptpd
                })
                .success(function (reply) {
                    if (reply.status === 200) {
                        loadData();
                        //$state.go("form-sptpd",{jenis: $stateParams.jenis, id : $scope.id_data_pajak});
                    } else {
                        $.growl.error({ message: "Gagal mendapatkan data berita!!" });
                        $rootScope.unloadLoading();
                        return;
                    }
                    $rootScope.unloadLoading();
                })
                .error(function (err) {
                    $.growl.error({ message: "Gagal Akses API >" + err });
                    $rootScope.unloadLoading();
                    return;
                })
            }
        });
        
    }

    $scope.simpan = function(){
        swal({
            title: 'Apakah anda yakin akan menyimpan data ini?',
            text: 'Data akan tersimpan',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then(function(result) {
            if (result.value == true) {
                
            }
        });
        
    }

    $scope.simpanSptpdBulanan = function(param){
        if($scope.sptpd.prosentase == ""){
            $.growl.error({ message: "Silahkan pilih prosentase!!" });
        }else if($scope.sptpd.omset == "" || $scope.sptpd.omset == "0"){
            $.growl.error({ message: "Omset tidak boleh kosong!!" });
        }else if($scope.sptpd.periode == ""){
            $.growl.error({ message: "Silahkan pilih bulan!!" });
        }else{
            swal({
                title: 'Apakah anda yakin akan menyimpan data ini?',
                text: 'Data akan tersimpan',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya'
            }).then(function(result) {
                if (result.value == true) {
                    $http.post($rootScope.url_api + "sptpd/insert", {
                        "id": $scope.npwpd.id_npwpd,
                        "jenis_input" : param,
                        "data" : $scope.sptpd
                    })
                    .success(function (reply) {
                        if (reply.status === 200) {                            
                            window.history.back();
                            //$state.go("form-sptpd",{jenis: $scope.jenis_pajak, id : $scope.id_data_pajak});
                        } else {
                            $.growl.error({ message: "Gagal mendapatkan data berita!!" });
                            $rootScope.unloadLoading();
                            return;
                        }
                        $rootScope.unloadLoading();
                    })
                    .error(function (err) {
                        $.growl.error({ message: "Gagal Akses API >" + err });
                        $rootScope.unloadLoading();
                        return;
                    })
                }
            });
        }
       
        
    }

    // $scope.simpanSptpdharian = function(simpanSptpdharian){
    //     alert("");
    // }

    $scope.simpanSptpdHarian = function(param){
        if($scope.sptpd.prosentase == ""){
            $.growl.error({ message: "Silahkan pilih prosentase!!" });
        }else if($scope.sptpd.omset == "" || $scope.sptpd.omset == "0"){
            $.growl.error({ message: "Omset tidak boleh kosong!!" });
        }else if($scope.sptpd.date_new == ""){
            $.growl.error({ message: "Silahkan pilih tanggal!!" });
        }else{
            swal({
                title: 'Apakah anda yakin akan menyimpan data ini?',
                text: 'Data akan tersimpan',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya'
            }).then(function(result) {
                if (result.value == true) {
                    $http.post($rootScope.url_api + "sptpd/insert", {
                        "id": $scope.npwpd.id_npwpd,
                        "jenis_input" : param,
                        "data" : $scope.sptpd,
                    })
                    .success(function (reply) {
                        if (reply.status === 200) {
                            uploadDoc();
                            window.history.back();
                            // $state.go("form-sptpd",{jenis: $stateParams.jenis, id : $scope.id_data_pajak});
                        } else {
                            $.growl.error({ message: "Gagal mendapatkan data berita!!" });
                            $rootScope.unloadLoading();
                            return;
                        }
                        $rootScope.unloadLoading();
                    })
                    .error(function (err) {
                        $.growl.error({ message: "Gagal Akses API >" + err });
                        $rootScope.unloadLoading();
                        return;
                    })
                }
            });
        }
       
        
    }

    $scope.back = function(){
        $state.go("form-sptpd");
    }

    $scope.backPengajuan = function(){
        $state.go("lihatSptpd");
    }

    $scope.lihat = function(param){//console.info(param)
        $state.go("viewSptpd", {id: param.id_sptpd});
    }

    $scope.deleteData = function(lembaga){
        swal({
            title: 'Apakah anda yakin akan menghapus data lembaga?',
            text: 'Ini akan menghapus data lembaga',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then(function(value) {
            if (value) {
                
            }
        });
    }

    $scope.pilih = function(param){
        Swal.fire({
            title: 'Pilih Input Data Pajak !!',
            imageUrl: 'https://matabanua.co.id/wp-content/uploads/2020/06/7-Kemenkeu-Berikan-Insentif-ke-355-Ribu-Wajib-Pajak-1024x538.jpg',
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Harian`,
            cancelButtonText: 'Bulanan'
          }).then((result) => {console.info(param)
            /* Read more about isConfirmed, isDenied below */
            if (result.value == true) {
                $state.go('form-sptpd-harian',{jenis: $stateParams.jenis, id : $scope.id_data_pajak});
            //   Swal.fire('Saved!', '', 'success')
            } else if (result.dismiss != 'overlay') {
                $state.go('form-sptpd-bulanan',{jenis: $stateParams.jenis, id : $scope.id_data_pajak});
            //   Swal.fire('Changes are not saved', '', 'info')
            }
          })
    }

});