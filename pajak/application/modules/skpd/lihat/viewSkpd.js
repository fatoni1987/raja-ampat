angular.module('rajampat')
.controller('viewDataUserSkpdCtrl', function ($scope, $modal, $rootScope, $http, $state) { // $cookieStore, 
    $scope.berita = [];
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxSize = 10;
    $scope.data = 0;
    $scope.kata = "";
    
    $scope.init = function () {
        $rootScope.authorize().then(function (result) {
            $scope.jLoad(1);           
        });
    };


    $scope.cariData = function () {
        $scope.jLoad(1);
    };

    $scope.remHtml = function (text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };

    $scope.goBack = function() {
        window.history.back();
    }

    $scope.jLoad = function (current) {
        $scope.berita = [];
        $scope.currentPage = current;
        $scope.offset = (current * 10) - 10;
        var arr = [];
        arr.push($scope.offset);
        var limit = 10;
        arr.push(limit);
        $rootScope.loadLoading('Silahkan Tunggu...');
        $rootScope.authorize().then(function(){
            $http.post($rootScope.url_api + "skpd/selectSkpd", {
                "offset": $scope.offset, 
                "pageSize": $scope.maxSize, 
                "limit": limit
            })
            .success(function (reply) {
                if (reply.status === 200) {
                    var data = reply.result.data;                    
                    $scope.skpd = data.list[0];
                    $scope.skpdCount = data.count;
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data berita!!" });
                    $rootScope.unloadLoading();
                    return;
                }
                $rootScope.unloadLoading();
            })
            .error(function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                $rootScope.unloadLoading();
                return;
            })
        })
    };


});


function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}