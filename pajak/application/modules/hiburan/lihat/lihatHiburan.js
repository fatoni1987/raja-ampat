angular.module('rajampat')
.controller('lihatHiburanCtrl', function ($scope, $modal, $rootScope, $http, $state) { // $cookieStore, 
    $scope.berita = [];
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxSize = 10;
    $scope.data = 0;
    $scope.kata = "";
    
    $scope.init = function () {
        $rootScope.authorize().then(function (result) {
            $scope.jLoad(1);           
        });
    };


    $scope.cariData = function () {
        $scope.jLoad(1);
    };

    $scope.remHtml = function (text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };

    $scope.jLoad = function (current) {
        $scope.berita = [];
        $scope.currentPage = current;
        $scope.offset = (current * 10) - 10;
        var arr = [];
        arr.push($scope.offset);
        var limit = 10;
        arr.push(limit);
        $rootScope.loadLoading('Silahkan Tunggu...');
        $rootScope.authorize().then(function(){
            // $http.post($rootScope.url_api + "berita/select", {
            //     search: "%" + $scope.kata.srcText + "%", "offset": $scope.offset, "pageSize": $scope.maxSize, "limit": limit, keyword: "%" + $scope.kata.srcText + "%", "author": $rootScope.userLogin
            // })
            //     .success(function (reply) {
            //         if (reply.status === 200) {
            //             var data = reply.result.data;
            //             $scope.berita = data;
            //             $http.post($rootScope.url_api + "berita/count",
            //                 { keyword: "%" + $scope.kata.srcText + "%", "author": $rootScope.userLogin })
            //                 .success(function (reply) {
            //                     if (reply.status === 200) {
            //                         var data = reply.result.data;
            //                         $scope.totalItems = data;

            //                     } else {
            //                         $.growl.error({ message: "Gagal mendapatkan data berita!!" });
            //                         $rootScope.unloadLoading();
            //                         return;
            //                     }
            //                     $rootScope.unloadLoading();
            //                 })
            //                 .error(function (err) {
            //                     $.growl.error({ message: "Gagal Akses API >" + err });
            //                     $rootScope.unloadLoading();
            //                     return;
            //                 });
            //         } else {
            //             $.growl.error({ message: "Gagal mendapatkan data berita!!" });
            //             $rootScope.unloadLoading();
            //             return;
            //         }
            //         $rootScope.unloadLoading();
            //     })
            //     .error(function (err) {
            //         $.growl.error({ message: "Gagal Akses API >" + err });
            //         $rootScope.unloadLoading();
            //         return;
            //     })
        })
    };

    $scope.tambah = function () {
        $state.go('form-pengajuan');
    };

    $scope.ubah = function (berita) {
        $scope.flagTombol = true;
        var kirim = {
            id_news: berita.id_news,
            title_news: berita.title_news,
            content_news: berita.content_news,
            picture_news: berita.picture_news,
            date_news: berita.date_news
        };
        var modalInstance = $modal.open({
            templateUrl: 'tambahBerita.html',
            controller: editBeritaCtrl,
            resolve: {
                item: function () {
                    return kirim;
                }
            }
        });
        modalInstance.result.then( function(){
            $scope.init();
        });
    };
    
    $scope.delete = function (berita) { // id
        var modalInstance = $modal.open({
            templateUrl: 'delModalBerita.html',
            controller: delBerita,
            resolve: {
                item: function () {
                    return {
                        id_news: berita.id_news,
                        title_news: berita.title_news                        
                    };
                }
            }
        });
        modalInstance.result.then(function () {
            $scope.init();

        });
    };
});

var tambahBeritaCtrl = function ($scope, $modalInstance, $http, $rootScope, item) { // $cookieStore, 
//    var page_id = 158;
    $scope.action = "Tambah ";
    $scope.berita = item;
    $scope.user = $rootScope.userLogin;
    $scope.flagTombol = false;

    var id_news=0; 
    // $rootScope.fileuploadconfig(34);
    $scope.customTinymce = {
        theme: "modern",
        menu: {
            file: {title: 'File', items: 'newdocument'},
            edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Insert', items: 'link image hr'},
            view: {title: 'View', items: 'visualaid'},
            format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
            table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
            tools: {title: 'Tools', items: 'spellchecker code'}
        },
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview",
        image_advtab: true,
        height: "300px", // 200px
        width: "auto"
    };

    $scope.file;
    $scope.filesToUpload = [];
    $scope.filesTChanged = function (elm) {
        $scope.file = elm.files;
    };

    $scope.simpan = function () {
        if ($scope.berita.title_news === '' || $scope.berita.title_news === undefined) {
            $.growl.error({title: "[PERINGATAN]", message: "Judul berita belum diisi!!"});
            return;
        }
        if ($scope.berita.content_news === '' || $scope.berita.content_news === undefined) {
            $.growl.error({title: "[PERINGATAN]", message: "Isi berita belum diisi!!"});
            return;
        }
        insert("");
    };

    $scope.uploads = function () {
        var idx = -1;
        $.each($scope.filesToUpload, function (index, item) {
            if (item.fileName === $scope.fName) {
                idx = index;
            }
        });
        if ($scope.fileDocument === '' || $scope.fileDocument === undefined) {
            $.growl.error({title: "[PERINGATAN]", message: "File belum dipilih"});
            return;
        } else if (idx > -1) {
            $.growl.error({title: "[PERINGATAN]", message: "File sudah ada"});
            return;
        } else {
            var fileInput = $('.upload-file');
            var extFile = $('.upload-file').val().split('.').pop().toLowerCase();
            var maxSize = fileInput.data('max-size');
            if (fileInput.get(0).files.length) {
                var fileSize = fileInput.get(0).files[0].size;
                if (fileSize > maxSize) {
                    $rootScope.unloadLoadingModal();
                    $.growl.error({title: "[WARNING]", message: "Ukuran file terlalu besar"});
                    return;
                } else {
                    var restrictedExt = $rootScope.limitfiletype;
                    if ($.inArray(extFile, restrictedExt) === -1) {
                        $rootScope.unloadLoadingModal();
                        $.growl.error({title: "[WARNING]", message: "Format file tidak valid"});
                        return;
                    } else {
                        $scope.filesToUpload.push({
                            fileName: $scope.fName,
                            fileSize: $scope.fSize,
                            file: $scope.fileDocument
                        });
                    }
                }
            }
        }
    };

    $scope.fileMChanged = function (elm) {
        $scope.fileDocument = elm.files;
        if ($scope.fileDocument !== '' || $scope.fileDocument !== undefined) {
            for (var i = 0; i < $scope.fileDocument.length; i++) {
                $scope.fName = $scope.fileDocument[i].name;
                $scope.fSize = ($scope.fileDocument[i].size / 1000).toFixed(1);
                //console.log($scope.fileDocument[i]);
                if ($scope.filesToUpload.length > 0) {
                    for (i = 0; i < $scope.filesToUpload.length; i++) {
                        if ($scope.fName === $scope.filesToUpload[i].fileName) {
                            $.growl.error({title: "[PERINGATAN]", message: "File sudah ada "});
                            document.getElementById('uploadFile').value = '';
                            $scope.fileDocument = '';
                            $scope.fName = '';
                            $scope.fSize = '';
                            return;
                        }
                    }
                }
            }
        }
    };

    $scope.removeFile = function (fileName) {
        var idx = -1;
        $.each($scope.filesToUpload, function (index, item) {
            if (item.fileName === fileName) {
                idx = index;
            }
        });
        $scope.filesToUpload.splice(idx, 1);
    };

    function insert(url) {
        $rootScope.loadLoadingModal("Silahkan Tunggu...");
        var param = [];
        param.push($scope.berita.title_news);
        param.push($scope.berita.content_news);
        param.push(url)
        param.push($scope.berita.date_news);
        param.push($rootScope.userLogin);        
        param.push($scope.berita.id_news);
        param.push($rootScope.nama);
        console.info(param);
        //POST zip file
        //jika berhasil return url zipnya
        //simpan sebagai picturenewsurl
        $http.post($rootScope.url_api + "berita/save", {
            username: $rootScope.userSession.nama_pegawai,
            action: "add",
            param: param
        })
        .success(function (reply) {
            if (reply.status == '200') {
                id_news = reply.result.data.id;
                if($scope.filesToUpload.length > 0){
                    upload();
                }else{
                    $rootScope.unloadLoadingModal();
                    $modalInstance.close();
                    var msgpp = "Berhasil Tambah Berita!!";
                    $.growl.notice({title: "[INFO]", message: msgpp});
                }
            } else {
                var msgpp = "Gagal Tambah Berita!!";
                $.growl.error({title: "[PERINGATAN]", message: msgpp});
            }   
        })
        .error( function (reply) {
            if (reply.status === 'ok') {
                var msgpp = "Berhasil Simpan Berita!!";
                $.growl.notice({title: "[INFO]", message: msgpp});
            } else {
                var msgpp = "Gagal Simpan Berita!!";
                $.growl.error({title: "[PERINGATAN]", message: msgpp});
            }
        });
    
    };

    function update(url) {
        var param = [];
        param.push($scope.berita.title_news);
        param.push($scope.berita.content_news);
        param.push(url)
        param.push($scope.berita.date_news);
        param.push($scope.user);
        param.push(id_news);

        //POST zip file
        //jika berhasil return url zipnya
        //simpan sebagai picturenewsurl
        $http.post($rootScope.url_api + "berita/save", {
            username: $scope.user,
            action: "edit",
            param: param
        })
        .success(function (reply) {
            if (reply.status === 200) {
                // console.info(JSON.stringify(reply));
                var msgpp = "Berhasil Tambah Berita!!";
                $.growl.notice({title: "[INFO]", message: msgpp});
            } else {
                var msgpp = "Gagal Tambah Berita!!";
                $.growl.error({title: "[PERINGATAN]", message: msgpp});
            }
            // $scope.$parent.jLoad(1);
        })
        .error( function (reply) {
            if (reply.status === 'ok') {
                var msgpp = "Berhasil Simpan Berita!!";
                $.growl.notice({title: "[INFO]", message: msgpp});
            } else {
                var msgpp = "Gagal Simpan Berita!!";
                $.growl.error({title: "[PERINGATAN]", message: msgpp});
            }
        });
        $modalInstance.close();
    };

    // function upload() {
    //     $rootScope.loadLoadingModal("Silahkan tunggu....");
    //     var fd = new FormData();
    //      angular.forEach($scope.file, function(file) {
    //          fd.append("uploads", file);
    //      });
    //      console.info($scope.file);
    //      $http.post($rootScope.url_api + "upload/berita_id_" + id_news, fd, {
    //          withCredentials: true,
    //          transformRequest: angular.identity(),
    //          headers: {'Content-Type': undefined}
    //      })
    //      .success(function(reply) {
    //          $rootScope.unloadLoading();
    //          if(reply.status === 200){
    //              var result = reply.result.data;
    //             //  updatedokumenpemilihan(result.files[0].url);
    //          }
    //          else{
    //              $.growl.error({ message:$scope.message8 });
    //          }
    //      })
    //      .error(function(err) {
    //          $rootScope.unloadLoading();
    //          $.growl.error({ message:$scope.message1+err });
    //              $http.post($rootScope.url_api + "logging", {
    //                      message:$scope.message2 + JSON.stringify(err),
    //                      source: "download-dokumen-pemilihan.js - uploadfile"
    //                })
    //                .then(function(response){});
    //          $.growl.error({ message:$scope.message1+err });
    //      });
    //  };
    function upload() {
        console.info($scope.filesToUpload.length);
        var fd = new FormData();
        for (var i = 0; i < $scope.filesToUpload.length; i++) {
            angular.forEach($scope.filesToUpload[i].file, function (item) {
                fd.append("uploads", item);
            });
        }
        console.info(fd);
        $http.post($rootScope.url_api + "upload/berita_id_" + id_news, fd, {
            withCredentials: true,
            transformRequest: angular.identity(),
            headers: {'Content-Type': undefined}
        }).success(function (reply) {
            $rootScope.unloadLoadingModal();
            // console.info(JSON.stringify(reply));
            if (reply.status === 200) {
                var filezip = reply.result.data.files[0].url;
                update(filezip);
            } else {
                $.growl.error({message: "Gagal mengupload file"});
                return;
            }
        });
    };

    $scope.batal = function () {
        $modalInstance.dismiss('cancel');
    };
};

var editBeritaCtrl = function ($scope, $modalInstance, $http, $rootScope, item) { // $cookieStore, 
//    var page_id = 158;
    $scope.action = " Ubah ";
    $scope.berita = item;
    $scope.user = $rootScope.userLogin;
    var id_news = $scope.berita.id_news;
    $scope.adafileberita = false;
    $scope.flagTombol = true;
    // console.log($scope.berita);
    if ($scope.berita.picture_news !== null && $scope.berita.picture_news !== '') {
        $scope.adafileberita = true;
    }

    //$rootScope.fileuploadconfig(34);
    $scope.customTinymce = {
        theme: "modern",
        menu: {
            file: {title: 'File', items: 'newdocument'},
            edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Insert', items: 'link image hr'},
            view: {title: 'View', items: 'visualaid'},
            format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
            table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
            tools: {title: 'Tools', items: 'spellchecker code'}
        },
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview",
        image_advtab: true,
        height: "300px", // 200px
        width: "auto"
    };

    $scope.file;
    $scope.filesToUpload = [];
    $scope.filesTChanged = function (elm) {
        $scope.file = elm.files;
    };

    $scope.simpan = function () {
        if ($scope.berita.title_news === '' || $scope.berita.title_news === undefined) {
            $.growl.error({title: "[PERINGATAN]", message: "Judul berita belum diisi!!"});
            return;
        }
        if ($scope.berita.content_news === '' || $scope.berita.content_news === undefined) {
            $.growl.error({title: "[PERINGATAN]", message: "Isi berita belum diisi!!"});
            return;
        }
        if($scope.filesToUpload.length > 0){
            upload();
        }else{
            insert("");    
        }
        
    };

    $scope.uploads = function () {
        var idx = -1;
        $.each($scope.filesToUpload, function (index, item) {
            if (item.fileName === $scope.fName) {
                idx = index;
            }
        });
        if ($scope.fileDocument === '' || $scope.fileDocument === undefined) {
            $.growl.error({title: "[PERINGATAN]", message: "File belum dipilih"});
            return;
        } else if (idx > -1) {
            $.growl.error({title: "[PERINGATAN]", message: "File sudah ada"});
            return;
        } else {
            var fileInput = $('.upload-file');
            var extFile = $('.upload-file').val().split('.').pop().toLowerCase();
            var maxSize = fileInput.data('max-size');
            if (fileInput.get(0).files.length) {
                var fileSize = fileInput.get(0).files[0].size;
                if (fileSize > maxSize) {
                    $rootScope.unloadLoadingModal();
                    $.growl.error({title: "[WARNING]", message: "Ukuran file terlalu besar"});
                    return;
                } else {
                    var restrictedExt = $rootScope.limitfiletype;
                    if ($.inArray(extFile, restrictedExt) === -1) {
                        $rootScope.unloadLoadingModal();
                        $.growl.error({title: "[WARNING]", message: "Format file tidak valid"});
                        return;
                    } else {
                        $scope.filesToUpload.push({
                            fileName: $scope.fName,
                            fileSize: $scope.fSize,
                            file: $scope.fileDocument
                        });
                    }
                }
            }
        }
    };

    $scope.fileMChanged = function (elm) {
        $scope.fileDocument = elm.files;
        if ($scope.fileDocument !== '' || $scope.fileDocument !== undefined) {
            for (var i = 0; i < $scope.fileDocument.length; i++) {
                $scope.fName = $scope.fileDocument[i].name;
                $scope.fSize = ($scope.fileDocument[i].size / 1000).toFixed(1);
                //console.log($scope.fileDocument[i]);
                if ($scope.filesToUpload.length > 0) {
                    for (i = 0; i < $scope.filesToUpload.length; i++) {
                        if ($scope.fName === $scope.filesToUpload[i].fileName) {
                            $.growl.error({title: "[PERINGATAN]", message: "File sudah ada "});
                            document.getElementById('uploadFile').value = '';
                            $scope.fileDocument = '';
                            $scope.fName = '';
                            $scope.fSize = '';
                            return;
                        }
                    }
                }
            }
        }
    };

    $scope.removeFile = function (fileName) {
        var idx = -1;
        $.each($scope.filesToUpload, function (index, item) {
            if (item.fileName === fileName) {
                idx = index;
            }
        });
        $scope.filesToUpload.splice(idx, 1);
    };

    function insert(url) {
        var param = [];
        param.push($scope.berita.title_news);
        param.push($scope.berita.content_news);
        param.push(url)
        param.push($scope.berita.date_news);
        param.push($scope.user);
        param.push($scope.berita.id_news);
        //POST zip file
        //jika berhasil return url zipnya
        //simpan sebagai picturenewsurl
        $http.post($rootScope.url_api + "berita/save", {
            username: $scope.user,
            action: "edit",
            param: param
        })
        .success(function (reply) {
            if (reply.status === 200) {
                var msgpp = "Berhasil Ubah Berita!!";
                $.growl.notice({title: "[INFO]", message: msgpp});
            } else {
                var msgpp = "Gagal Ubah Berita!!";
                $.growl.error({title: "[PERINGATAN]", message: msgpp});
            }
            // $scope.$parent.jLoad(1);
        })
        .error( function (reply) {
            if (reply.status === 'ok') {
                var msgpp = "Berhasil Simpan Berita!!";
                $.growl.notice({title: "[INFO]", message: msgpp});
            } else {
                var msgpp = "Gagal Simpan Berita!!";
                $.growl.error({title: "[PERINGATAN]", message: msgpp});
            }
        });
        $modalInstance.close();
    };

    function upload() {
        $rootScope.loadLoadingModal("Silahkan Tunggu...");
        //("files:"+JSON.stringify($scope.filesToUpload));
        var fd = new FormData();
        for (var i = 0; i < $scope.filesToUpload.length; i++) {
            angular.forEach($scope.filesToUpload[i].file, function (item) {
                fd.append("uploads", item);
            });
        }
        console.info("masuk");
        $http.post($rootScope.url_api + "upload/berita_id_" + id_news, fd, {
            withCredentials: true,
            transformRequest: angular.identity(),
            headers: {'Content-Type': undefined}
        }).success(function (reply) {
            $rootScope.unloadLoadingModal();
            //console.info(JSON.stringify(reply));
            if (reply.status === 200) {
                var filezip = reply.result.data.files[0].url;
                insert(filezip);
            } else {
                $.growl.error({message: "Gagal mengupload file"});
                return;
            }
        });
    };

    $scope.batal = function () {
        $modalInstance.dismiss('cancel');
    };
};

var delBerita = function ($scope, $modalInstance, $http, item, $rootScope) { // $cookieStore, 
    $scope.page_id = 158;
    $scope.user = $rootScope.userLogin;
    $scope.title_news = item.title_news;
    
    $scope.delBerita = function () {
        var param = [];
        param.push( item.title_news ); 
        param.push( item.id_news ); 
        
        $http.post( $rootScope.url_api + "berita/save", {
            username: $scope.user,
            action: "delete",
            param: param
        })
        .success(function (reply) {
            if (reply.status === 200) {
                var msgpp = "Berhasil Hapus Berita";
                $.growl.notice({title: "[INFO]", message: msgpp});
            } else {
                var msgpp = "Gagal Hapus Berita!";
                $.growl.error({title: "[PERINGATAN]", message: msgpp});
            }
        })
        .error(function (err) {
            $.growl.error({title: "[ERROR]", message: "Gagal menghapus Berita!"});
            $rootScope.unloadLoadingModal();
            return;
        });
        $modalInstance.close();
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};

function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}