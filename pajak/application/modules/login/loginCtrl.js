angular.module('rajampat')

        .controller('loginCtrl', function ($scope, $cookieStore, ipCookie, $state, $rootScope, ngProgress, $http, SocketService, $stateParams)
        {
            $scope.username = "";
            $scope.password = "";
            $scope.tampung = [];
            $scope.enterCaptcha = "";
            $rootScope.idModule = $stateParams.id;            
            ipCookie("type",$stateParams.id,1);
            
            $scope.init = function () {
                $rootScope.checkUAT();
                $scope.captcha();
                //$rootScope.checkSession();
            };

            $scope.captcha = function () {
                var alpha = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
                var i;
                var tCtx = document.getElementById('textCanvas').getContext('2d');
                var imageElem = document.getElementById('image');

                for (i = 0; i < 4; i++) {
                    var a = alpha[Math.floor(Math.random() * alpha.length)];
                    var b = alpha[Math.floor(Math.random() * alpha.length)];
                    var c = alpha[Math.floor(Math.random() * alpha.length)];
                    var d = alpha[Math.floor(Math.random() * alpha.length)];
                }

                var code = a + ' ' + b + ' ' + c + ' ' + d;

                tCtx.canvas.width = tCtx.measureText(code).width+3;
                tCtx.fillText(code, 0, 10);
                imageElem.src = tCtx.canvas.toDataURL();
                $scope.passCaptcha = $.md5(removeSpaces(code));
            };

            $scope.validCaptcha = function () {
                var string1 = $scope.passCaptcha;
                var string2 = $.md5(removeSpaces($scope.enterCaptcha));

                if (string1 === string2)
                    return true;
                else {
                    $.growl.error({title: "[PERINGATAN]", message: "Captcha salah"});
                    $rootScope.unloadLoading();
                    $scope.enterCaptcha = "";
                    // document.getElementById("txtInput").value = "";
                    $scope.captcha();
                    return false;
                }
            };

            function removeSpaces(string) {
                return string.split(' ').join('');
            }

            $scope.submitCaptcha = function(evt){
                let charCode = (evt.keyCode ? evt.keyCode : evt.which);
                if (charCode == 13) {
                    $scope.login();
                  }
            }

            $scope.login = function () {
                if ($.trim($scope.username) === "" || $.trim($scope.password) === "") {
                    $.growl.warning({message: "Masukkan username dan password!!"});
                    return;
                }
                //ngProgress.start();
                $rootScope.loadLoading("Verifying credential. . . ");

                if ($scope.validCaptcha())
                {
                    // console.log("session id: " + ipCookie("panitiaSessId"));
                    if (ipCookie("panitiaSessId") != undefined && ipCookie("panitiaSessId") != null) {
                        $rootScope.authorize().then(function (result) {
                            $.growl.notice({message: 'Login berhasil! Selamat datang, ' + $rootScope.userSession.session_data.nama_pegawai});
                            $state.go('homeadmin');
                            $rootScope.isLogged = true;
                        });
                    }
                    $http.post($rootScope.url_api + "auth/login/admin", {
                        username: $scope.username,
                        password: $scope.password,
                        api_key: $rootScope.api_key
                    }).success(function (result) {
                        if (result.status === 200) {
                            // console.log(result);
                            $rootScope.unloadLoading();
                            var data = result.data;
                            
                            if (data.success) {
                                ipCookie("panitiaSessId", data.session_id,{ path: '/' });
                               
                                $http.post($rootScope.url_api + "auth/get_session", {
                                    session_id: ipCookie("panitiaSessId")
                                })
                                        .success(function (result2) {
                                            if (result2.status === 200) {
                                                $rootScope.userSession = result2.data.data;
                                                // console.info($rootScope.userSession.data);
                                                $.growl.notice({message: 'Login berhasil! Selamat datang, ' + $rootScope.userSession.nama});
                                                $state.go('homeadmin');
                                                $rootScope.isLogged = true;
                                            }
                                        })
                                        .error(function (err) {
                                            $.growl.error({message: "Gagal Akses API >" + err});

                                        });
                            } else {
                                if (data.session_id === 0) {
                                    $.growl.error({message: data.message});
                                    return;
                                } else {
                                    
                                    $http.post($rootScope.url_api + "auth/get_session", {
                                        session_id: data.session_id
                                    }).success(function (result2) {
                                        if (result2.status === 200) {
                                            var user = result2.data;
                                            $http.post($rootScope.url_api + "auth/logout", {
                                                session_id: data.session_id
                                            }).success(function (result) {
                                                if (result.status === 200) {
                                                    if (result.data.success) {
                                                        ipCookie.remove("panitiaSessId")
                                                        // ipCookie.remove("IP")

                                                        $rootScope.isLogged = false;
                                                        SocketService.emit('forceLogoff', user, function () {
                                                        });
                                                        $scope.login();
                                                    } else {
                                                        $.growl.error({message: result.data.message});
                                                    }
                                                }
                                            }).error(function (err) {
                                                $.growl.error({message: "Gagal Akses API >" + err});

                                            });
                                        }
                                    }).error(function (err) {
                                        $.growl.error({message: "Gagal Akses API >" + err});
                                    });
                                }
                            }
                        } else {
                            $rootScope.unloadLoading();
                            $.growl.error({message: result.message});
                        }
                    }).error(function (error) {
                        $rootScope.unloadLoading();
                        $.growl.error({message: "Gagal Akses API >" + error});
                    });
                } else {
                    return;
                }// end: if $scope.validCaptcha()
            };
        }); // end: loginCtrl