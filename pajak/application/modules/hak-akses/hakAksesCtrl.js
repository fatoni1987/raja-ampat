angular.module('rajampat')
    .controller('hakAksesCreateCtrl', function($scope, $http, $cookieStore, $rootScope, $modal) {
        $scope.roles = [];
        $scope.page_id = 21;
        $scope.userBisaMengatur = false;
        $scope.userBisaMenambah = false;
        $scope.userBisaMengubah = false;
        $scope.userBisaMenghapus = false;

        $scope.init = function() {
            $rootScope.authorize().then(function(result){
                loadRoles();
            });  

            $rootScope.addBreadcrumbItem({
                url: "hak-akses/create",
                label: "Master Hak Akses",
                icon: "fa-building",
                lastIndex: true
            });
        };

        function loadRoles() {
            // cek hak akses
            $rootScope.cekHakAksesUser($scope.page_id, 1).then((response) => {
                $scope.userBisaMengatur = response;
            }).catch((response) => {
                $scope.userBisaMengatur = response;
            });
            $rootScope.cekHakAksesUser($scope.page_id, 2).then((response) => {
                $scope.userBisaMenambah = response;
            }).catch((response) => {
                $scope.userBisaMenambah = response;
            });
            $rootScope.cekHakAksesUser($scope.page_id, 3).then((response) => {
                $scope.userBisaMengubah = response;
            }).catch((response) => {
                $scope.userBisaMengubah = response;
            });
            $rootScope.cekHakAksesUser($scope.page_id, 4).then((response) => {
                $scope.userBisaMenghapus = response;
            }).catch((response) => {
                $scope.userBisaMenghapus = response;
            });
            //itp.role.selectAll
            $rootScope.loadLoading('Silahkan Tunggu...');
            $http.get($rootScope.url_api+"roles/list")
            .success(function(reply){
                if(reply.status === 200){
                    var data = reply.result.data;
                    $scope.roles = data;
                    $rootScope.unloadLoading();
                }
                else{
                    $.growl.error({ message: "Gagal mendapatkan data hak akses!" });
                    $rootScope.unloadLoading();
                    return;
                }
            })
            .error(function(err) {
                $.growl.error({ message: "Gagal Akses API >"+err });
                return;
            });

        };

        $scope.tambahRole = function() {
            var modalInstance = $modal.open({
                templateUrl: 'tambahRole.html',
                controller: tambahRoleCtrl
            });
            modalInstance.result.then(function() {
                loadRoles();
            });
        };

        $scope.viewRoleDetail = function(role) {
            var modalInstance = $modal.open({
                templateUrl: 'viewRoleDetail.html',
                controller: viewRoleDetailCtrl,
                resolve: {
                    item: function() {
                        return role;
                    }
                }
            });
        };

        $scope.ubahRoleDetail = function(role) {
            var modalInstance = $modal.open({
                templateUrl: 'ubahRoleDetail.html',
                controller: ubahRoleDetailCtrl,
                resolve: {
                    item: function() {
                        return role;
                    }
                }
            });
            modalInstance.result.then(function() {
                loadRoles();
            });
        };
    });

var viewRoleDetailCtrl = function($scope, $modalInstance, $http, item, $cookieStore, $rootScope) {
    $scope.accessrole = [];
    $scope.role = item;
    $scope.page_id = 21;

    $scope.init = function() {
        
        $http.get($rootScope.url_api+"roles/detail/"+item.role_id)
        .success(function(reply){
            console.info(item.role_id+" rep: "+JSON.stringify(reply));
            if(reply.status === 200){
                $scope.listMenu = reply.result.data;
                var submenu;
                var submenus = [];
                var subsubmenus = [];
                var masuk = [];
                for (var p = 0; p < $scope.listMenu.length; p++) {
                    masuk[p] = false;
                }
                for (var i = 0; i < $scope.listMenu.length; i++) {
                    if (!($scope.listMenu[i].menu_parent == 0) || masuk[i] == true)
                        continue;
                    for (var j = 0; j < $scope.listMenu.length; j++) {
                        if (j === i || masuk[j] == true)
                            continue;
                        if ($scope.listMenu[j].menu_parent === $scope.listMenu[i].menu_id) {
                            submenu = $scope.listMenu[j];
                            for (var k = 0; k < $scope.listMenu.length; k++) {
                                if (k === i || k === j || masuk[k] == true)
                                    continue;
                                if ($scope.listMenu[k].menu_parent === $scope.listMenu[j].menu_id) {
                                    subsubmenus.push($scope.listMenu[k]);
                                    masuk[k] = true;
                                }
                            }
                            submenu.childs = subsubmenus;
                            subsubmenus = [];
                            submenus.push(submenu);
                            masuk[j] = true;
                        }
                    }
                    $scope.listMenu[i].childs = submenus;
                    submenus = [];
                    $scope.accessrole.push($scope.listMenu[i]);
                    masuk[i] = true;
                }
                console.log($scope.accessrole);
            }
            else{
                $.growl.error({ message: "Gagal mendapatkan data detail roles" });
                return;
            }
        })
        .error(function(err) {
            $.growl.error({ message: "Gagal Akses API >"+err });
            return;
        });
    };

    $scope.keluar = function() {
        $modalInstance.dismiss('cancel');
    };
};

var tambahRoleCtrl = function($scope, $modalInstance, $http, $cookieStore, $rootScope) {
    $scope.listMenu = [];
    $scope.accessrole = [];
    $scope.newRole = new Role("");
    $scope.jenisRole = "";
    $scope.page_id = 21;

    $scope.ubahJenisRole = function(obj) {
        $scope.jenisRole = obj;
        $scope.init($scope.jenisRole)
    };
    
    $scope.toogleCheckChild = function(parent){        
        if(parent.ispermitted == true){
            if(parent.childs.length > 0){
                parent.childs.forEach(function(child){
                    child.ispermitted = true;
                });
            }
        }
        else{
            if(parent.childs.length > 0){
                parent.childs.forEach(function(child){
                    child.ispermitted = false;
                });
            } 
        }
    };

    $scope.unCheckParent = function(parent,child){
        if(child.ispermitted == false){
            parent.ispermitted = false;
        }
    };

    $scope.init = function(rolenya) {        
        $scope.listMenu = [];
        $scope.accessrole = [];
        $scope.tampil = false;
        $rootScope.loadLoading('Silahkan Tunggu...');
        $rootScope.authorize().then(function(){
            //itp.menu.select
            $http.post($rootScope.url_api + 'menu/select', {
                jenisRole : rolenya
            }).success(function(reply) {
                if (reply.status === 200) {
                    $scope.tampil = true;
                    $scope.listMenu = reply.result.data;
                    console.log($scope.listMenu)
                    for (var p = 0; p < $scope.listMenu.length; p++) {
                        $scope.listMenu[p].ispermitted = false;
                        $scope.listMenu[p].bisa_mengatur = "0";
                        $scope.listMenu[p].bisa_tambah = "0";
                        $scope.listMenu[p].bisa_ubah = "0";
                        $scope.listMenu[p].bisa_hapus = "0";
                    }
                    var submenu;
                    var submenus = [];
                    var subsubmenus = [];
                    var masuk = [];
                    for (var p = 0; p < $scope.listMenu.length; p++) {
                        masuk[p] = false;
                    }
                    for (var i = 0; i < $scope.listMenu.length; i++) {
                        if (!($scope.listMenu[i].menu_parent == 0) || masuk[i] === true)
                            continue;
                        for (var j = 0; j < $scope.listMenu.length; j++) {
                            if (j === i || masuk[j] === true)
                                continue;
                            if ($scope.listMenu[j].menu_parent == $scope.listMenu[i].menu_id) {
                                submenu = $scope.listMenu[j];
                                for (var k = 0; k < $scope.listMenu.length; k++) {
                                    if (k === i || k === j || masuk[k] === true)
                                        continue;
                                    if ($scope.listMenu[k].menu_parent == $scope.listMenu[j].menu_id) {
                                        subsubmenus.push($scope.listMenu[k]);
                                        masuk[k] = true;
                                    }
                                }
                                submenu.childs = subsubmenus;
                                subsubmenus = [];
                                submenus.push(submenu);
                                masuk[j] = true;
                            }
                        }
                        $scope.listMenu[i].childs = submenus;
                        submenus = [];
                        $scope.accessrole.push($scope.listMenu[i]);
                        masuk[i] = true;
                    }
                    
                }
            }).error(function(err) {
                $.growl.error({ message: "Gagal Akses API >"+err });
                return;
            })
        });
        $rootScope.unloadLoading();
    };

    $scope.addRole = function() {
        if ($scope.newRole.authority === "") {
            $.growl.warning({ message: "Nama Role belum diisi!!" });
            return;
        }
        
        var master = {};
        master.authority = $scope.newRole.authority;

        var inputElements = document.getElementsByClassName('uacheck');
        var label;
        var arr;
        var arr2 = [];
        
        var detail_address;
        var detail_address2 = [];

        console.log(inputElements);

        for (var i = 0; inputElements[i]; ++i) {
            label = inputElements[i].value;
            arr = {};
            arr.ispermitted = inputElements[i].checked;
            arr.page_id = Number(label);//label=page_id
            for (var j = 0; j < $scope.listMenu.length; j++) {
                if ($scope.listMenu[j].page_id == label) {
                    arr.bisa_mengatur = ($scope.listMenu[j].bisa_mengatur === '1');
                    arr.bisa_tambah = ($scope.listMenu[j].bisa_tambah === '1');
                    arr.bisa_ubah = ($scope.listMenu[j].bisa_ubah === '1');
                    arr.bisa_hapus = ($scope.listMenu[j].bisa_hapus === '1');
                    break;
                }
            }
            arr2.push(arr);
        }
        console.log(arr2);
        
        for (var i = 0; inputElements[i]; ++i) {
            label = inputElements[i].value;
            detail_address = {};
            detail_address.ispermitted = inputElements[i].checked;
            detail_address.page_id = Number(label);//label=page_id
            
            for (var j = 0; j < $scope.listMenu.length; j++) {
                if ($scope.listMenu[j].page_id == label) {
                    detail_address.bisa_mengatur = ($scope.listMenu[j].bisa_mengatur === '1');
                    detail_address.bisa_tambah = ($scope.listMenu[j].bisa_tambah === '1');
                    detail_address.bisa_ubah = ($scope.listMenu[j].bisa_ubah === '1');
                    detail_address.bisa_hapus = ($scope.listMenu[j].bisa_hapus === '1');
                    detail_address.adasub_pengecekan = $scope.listMenu[j].ada_sub_pengecekan;
                    break;
                }
            }
            detail_address2.push(detail_address);
        }
        
        $rootScope.authorize().then(function(){
            //itp.role.insert
            $http.post($rootScope.url_api + 'roles/create', {
                master: master, 
                details: arr2,
                address:detail_address2,
                logging: {
                    username : $rootScope.userLogin,
                    user_activity_id : 13
                }
            }).success(function(reply) {
                if (reply.status === 200) {
                    $.growl.notice({title: "[INFO]", message: "Create role berhasil"});
                    $modalInstance.close();
                }
                else
                    $.growl.error({ message: "Gagal membuat role"+err });
                    $modalInstance.close();
            }).error(function(err) {
                $.growl.error({ message: "Gagal Akses API >"+err });
                return;
            })
        })
    };

    $scope.keluar = function() {
        $modalInstance.dismiss('cancel');
    };
};

var ubahRoleDetailCtrl = function($scope, $modalInstance, $http, item, $cookieStore, $rootScope) {
    $scope.accessrole = [];
    $scope.role = item;
    $scope.access = [];
    var page_id = 21;
    $scope.jenisRole = item.jenis_role;

    $scope.checkChild = function(parent){
        if(parent.ispermitted == true){
            if(parent.childs.length > 0){
                parent.childs.forEach(function(child){
                    child.ispermitted = true;
                });
            }
        }
    }

    // untuk show/hide child dari accordion
    $scope.showChild = function($event){
        var button = $event.currentTarget;
        var accordion = $(button).parents(".accordion");
        var panel = accordion.find(".accordion-panel");
        $(button).toggleClass('active');
        if($(button).hasClass('active')){
            $(button).text("-");
        }
        else{
            $(button).text("+");
        }
        if($(panel).css('display') == 'block'){
            $(panel).css('display','none');
        }
        else{
            $(panel).css('display','block');
        }
    }

    $scope.unCheckParent = function(parent,child){
        if(child.ispermitted == false){
            parent.ispermitted = false;
        }
    }
    
    $scope.ubahJenisRole = function(obj) {
        $scope.jenisRole = obj;
        $scope.accessrole = [];
        $scope.access = [];
        $scope.initialize($scope.jenisRole);
    };

    $scope.init = function() {
        $rootScope.authorize().then(function(){
            $scope.initialize(item.jenis_role)
        })
    };

    $scope.initialize = function(rolenya) {
        $rootScope.loadLoadingModal("Silahkan Tunggu...");
        //itp.menu.select
        $http.post($rootScope.url_api + 'menu/select', {
            jenisRole : rolenya
        }).success(function(reply) {
            if (reply.status === 200) {
                $scope.listMenu = reply.result.data;
                var param = [];
                param.push(item.role_id);
                //itp.role.getDetail
                $http.get($rootScope.url_api + 'roles/detail/' + item.role_id)
                .success(function(reply2) {
                    if (reply2.status === 200) {
                        $scope.access = reply2.result.data;
                        for (var p = 0; p < $scope.listMenu.length; p++) {
                            $scope.listMenu[p].ispermitted = false;
                            $scope.listMenu[p].bisa_mengatur = "0";
                            $scope.listMenu[p].bisa_tambah = "0";
                            $scope.listMenu[p].bisa_ubah = "0";
                            $scope.listMenu[p].bisa_hapus = "0";
                            for (var q = 0; q < $scope.access.length; q++) {
                                if ($scope.listMenu[p].menu_id == $scope.access[q].menu_id) {
                                    $scope.listMenu[p].ispermitted = $scope.access[q].ispermitted;
                                    if ($scope.access[q].bisa_mengatur == true)
                                        $scope.listMenu[p].bisa_mengatur = "1";
                                    else
                                        $scope.listMenu[p].bisa_mengatur = "0";
                                    if ($scope.access[q].bisa_tambah == true)
                                        $scope.listMenu[p].bisa_tambah = "1";
                                    else
                                        $scope.listMenu[p].bisa_tambah = "0";
                                    if ($scope.access[q].bisa_ubah == true)
                                        $scope.listMenu[p].bisa_ubah = "1";
                                    else
                                        $scope.listMenu[p].bisa_ubah = "0";
                                    if ($scope.access[q].bisa_hapus == true)
                                        $scope.listMenu[p].bisa_hapus = "1";
                                    else
                                        $scope.listMenu[p].bisa_hapus = "0";
                                    break;
                                }
                            }
                        }
                        var submenu;
                        var submenus = [];
                        var subsubmenus = [];
                        var masuk = [];
                        for (var p = 0; p < $scope.listMenu.length; p++) {
                            masuk[p] = false;
                        }
                        for (var i = 0; i < $scope.listMenu.length; i++) {
                            if (!($scope.listMenu[i].menu_parent == 0) || masuk[i] === true)
                                continue;
                            for (var j = 0; j < $scope.listMenu.length; j++) {
                                if (j === i || masuk[j] === true)
                                    continue;
                                if ($scope.listMenu[j].menu_parent == $scope.listMenu[i].menu_id) {
                                    submenu = $scope.listMenu[j];
                                    for (var k = 0; k < $scope.listMenu.length; k++) {
                                        if (k === i || k === j || masuk[k] === true)
                                            continue;
                                        if ($scope.listMenu[k].menu_parent == $scope.listMenu[j].menu_id) {
                                            subsubmenus.push($scope.listMenu[k]);
                                            masuk[k] = true;
                                        }
                                    }
                                    submenu.childs = subsubmenus;
                                    subsubmenus = [];
                                    submenus.push(submenu);
                                    masuk[j] = true;
                                }
                            }
                            $scope.listMenu[i].childs = submenus;
                            submenus = [];
                            $scope.accessrole.push($scope.listMenu[i]);
                            masuk[i] = true;
                        }
                        $rootScope.unloadLoadingModal();
                    }
                }).error(function(err) {
                    $.growl.error({ message: "Gagal Akses API >"+err });
                    return;
                }); 
            }
        }).error(function(err) {
            $.growl.error({ message: "Gagal Akses API >"+err });
            return;
        }); 
    };

    $scope.updateRoleDetail = function() {
        var inputElements = document.getElementsByClassName('uacheck');
        var label;
        var arr;
        var arr2 = [];
        var paramForInsert;
        var paramsForInsert = [];

        for (var i = 0; inputElements[i]; ++i) {
            label = inputElements[i].value;
            var page_id = Number(label);
            var accessBaruNih = true;
            for (var j = 0; j < $scope.access.length; j++) {
                if (page_id == $scope.access[j].menu_id) {
                    accessBaruNih = false;
                    break;
                }
            }
            if (accessBaruNih === false) {
                arr = {};
                arr.ispermitted = inputElements[i].checked;
                arr.page_id = page_id;
                for (var j = 0; j < $scope.listMenu.length; j++) {
                    if ($scope.listMenu[j].page_id == label) {
                        arr.bisa_mengatur = ($scope.listMenu[j].bisa_mengatur === '1');
                        arr.bisa_tambah = ($scope.listMenu[j].bisa_tambah === '1');
                        arr.bisa_ubah = ($scope.listMenu[j].bisa_ubah === '1');
                        arr.bisa_hapus = ($scope.listMenu[j].bisa_hapus === '1');
                        break;
                    }
                }
                arr2.push(arr);
            } else {
                paramForInsert = {};
                paramForInsert.ispermitted = inputElements[i].checked;
                paramForInsert.page_id = page_id; //label=page_id
                for (var j = 0; j < $scope.listMenu.length; j++) {
                    if ($scope.listMenu[j].page_id == label) {
                        paramForInsert.bisa_mengatur = ($scope.listMenu[j].bisa_mengatur === '1');
                        paramForInsert.bisa_tambah = ($scope.listMenu[j].bisa_tambah === '1');
                        paramForInsert.bisa_ubah = ($scope.listMenu[j].bisa_ubah === '1');
                        paramForInsert.bisa_hapus = ($scope.listMenu[j].bisa_hapus === '1');
                        break;
                    }
                }
                paramsForInsert.push(paramForInsert);
            }
        }
        
        $rootScope.loadLoadingModal("Silahkan Tunggu...");
        $rootScope.authorize().then(function(){
            $http.post($rootScope.url_api + 'roles/update', {
                role: {
                    jenis_role: $scope.jenisRole,
                    role_id: item.role_id,
                    authority: item.authority
                },
                paramsUpdate: arr2,
                paramsInsert: paramsForInsert,
                logging: {
                    username : $rootScope.userLogin,
                    user_activity_id : 12
                }
            }).success(function(reply) {
                $rootScope.unloadLoadingModal();
                if (reply.status === 200) {
                    $.growl.notice({title: "[INFO]", message: "Update role berhasil"});
                    $modalInstance.close();
                }
                else
                    $.growl.error({title: "[PERINGATAN]", message: "Gagal update role"});
            }).error(function(err) {
                $.growl.error({ message: "Gagal Akses API >"+err });
                return;
            })
        })
    };

    $scope.keluar = function() {
        $modalInstance.dismiss('cancel');
    };
};

function Role(authority) {
    var self = this;
    self.authority = authority;
}