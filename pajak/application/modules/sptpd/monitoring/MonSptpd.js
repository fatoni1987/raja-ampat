angular.module('rajampat')
.controller('approveSptpdCtrl', function ($scope, $modal, $rootScope, $http, $state) { // $cookieStore, 
    $scope.berita = [];
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxSize = 10;
    $scope.data = 0;
    $scope.kata = "";
    
    $scope.init = function () {
        $rootScope.authorize().then(function (result) {
            $scope.jLoad(1);           
        });
    };


    $scope.cariData = function () {
        $scope.jLoad(1);
    };

    $scope.remHtml = function (text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };

    $scope.jLoad = function (current) {
        $scope.berita = [];
        $scope.currentPage = current;
        $scope.offset = (current * 10) - 10;
        var arr = [];
        arr.push($scope.offset);
        var limit = 10;
        arr.push(limit);
        $rootScope.loadLoading('Silahkan Tunggu...');
        $rootScope.authorize().then(function(){
            $http.post($rootScope.url_api + "sptpd/selectAll", {
                "offset": $scope.offset, 
                "pageSize": $scope.maxSize, 
                "limit": limit,
            })
            .success(function (reply) {
                if (reply.status === 200) {
                    var data = reply.result.data;                    
                    $scope.sptpd = data.list;
                    $scope.sptpdCount = data.count;
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data berita!!" });
                    $rootScope.unloadLoading();
                    return;
                }
                $rootScope.unloadLoading();
            })
            .error(function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                $rootScope.unloadLoading();
                return;
            })
        })
    };

    $scope.approve = function(param){
        swal({
            title: 'Apakah anda yakin akan approve data ini?',
            text: 'Anda akan menerbitkan No SPTPD',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            closeOnClickOutside: false,
            allowOutsideClick: false
        }).then(function(result) {
            if (result.value == true) {
                $http.post($rootScope.url_api + "sptpd/approve", {
                    "data" : param.id_sptpd,
                    "id_data_pajak" : param.id_data_pajak
                }).success(function(reply){
                    if(reply.status === 200){
                        $.growl.notice({
                            title: "[INFO]",
                            message: "Berhasil menghapus data"
                        });
                        $scope.jLoad(1);
                        // $state.go("lihatNpwpd");
                    }
                }).error(function(err){
                    $.growl.error({message: "Gagal menambah data!"});
                    return
                })
            }
        });
    }

    $scope.reject = function(param){
        swal({
            title: 'Apakah anda yakin akan reject data ini?',
            text: 'Masukkan alasan reject',
            icon: 'warning',
            input: 'text',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            closeOnClickOutside: false,
            allowOutsideClick: false
        }).then(function(result) {
            if (result.value != "") {
                $http.post($rootScope.url_api + "sptpd/reject", {
                    "data" : param.id_sptpd,
                    "alasan" : result.value
                }).success(function(reply){
                    if(reply.status === 200){
                        $.growl.notice({
                            title: "[INFO]",
                            message: "Berhasil menghapus data"
                        });
                        $scope.jLoad(1);
                    }
                }).error(function(err){
                    $.growl.error({message: "Gagal menambah data!"});
                    return
                })
            }else{
                $.growl.error({message: "Alasan tolak harus di isi!"});
                $scope.reject(param);
            }
        });
    }

    $scope.pengajuan = function (param) {console.info(param);
        $state.go('form-sptpd',{jenis : param.jenis_usaha_pajak, id : param.id_data_pajak});
    };
    
    $scope.delete = function (param) { // id
        var modalInstance = $modal.open({
            templateUrl: 'delModalBerita.html',
            controller: delBerita,
            resolve: {
                item: function () {
                    return {
                        id_news: berita.id_news,
                        title_news: berita.title_news                        
                    };
                }
            }
        });
        modalInstance.result.then(function () {
            $scope.init();

        });
    };
});


function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}