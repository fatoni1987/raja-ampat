angular.module('rajampat')
.controller('lihatSptpdCtrl', function ($scope, $modal, $rootScope, $http, $state) { // $cookieStore, 
    $scope.berita = [];
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxSize = 10;
    $scope.data = 0;
    $scope.kata = "";
    
    $scope.init = function () {
        $rootScope.authorize().then(function (result) {
            $scope.jLoad(1);           
        });
    };


    $scope.cariData = function () {
        $scope.jLoad(1);
    };

    $scope.remHtml = function (text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };

    $scope.jLoad = function (current) {
        $scope.berita = [];
        $scope.currentPage = current;
        $scope.offset = (current * 10) - 10;
        var arr = [];
        arr.push($scope.offset);
        var limit = 10;
        arr.push(limit);
        $rootScope.loadLoading('Silahkan Tunggu...');
        $rootScope.authorize().then(function(){
            $http.post($rootScope.url_api + "npwpd/selectNpwpd", {
                "offset": $scope.offset, 
                "pageSize": $scope.maxSize, 
                "limit": limit, 
                "status": "Verifikasi",
                "user_id": $rootScope.userID
            })
            .success(function (reply) {
                if (reply.status === 200) {
                    var data = reply.result.data;                    
                    $scope.npwpd = data.list;
                    $scope.npwpdCount = data.count;
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data berita!!" });
                    $rootScope.unloadLoading();
                    return;
                }
                $rootScope.unloadLoading();
            })
            .error(function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                $rootScope.unloadLoading();
                return;
            })
        })
    };

    $scope.pengajuan = function (param) {console.info(param);
        $state.go('form-sptpd',{jenis : param.jenis_usaha_pajak, id : param.id_data_pajak});
    };
    
    $scope.delete = function (param) { // id
        var modalInstance = $modal.open({
            templateUrl: 'delModalBerita.html',
            controller: delBerita,
            resolve: {
                item: function () {
                    return {
                        id_news: berita.id_news,
                        title_news: berita.title_news                        
                    };
                }
            }
        });
        modalInstance.result.then(function () {
            $scope.init();

        });
    };
});


function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}