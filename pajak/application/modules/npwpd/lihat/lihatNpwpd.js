angular.module('rajampat')
.controller('lihatNpwpdCtrl', function ($scope, $modal, $rootScope, $http, $state) { // $cookieStore, 
    $scope.berita = [];
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxSize = 10;
    $scope.data = 0;
    $scope.kata = "";    
    
    $scope.init = function () {
        $rootScope.authorize().then(function (result) {
            $scope.jLoad(1);           
        });
    };


    $scope.cariData = function () {
        $scope.jLoad(1);
    };

    $scope.jLoad = function (current) {
        $scope.berita = [];
        $scope.currentPage = current;
        $scope.offset = (current * 10) - 10;
        var arr = [];
        arr.push($scope.offset);
        var limit = 10;
        arr.push(limit);
        $rootScope.loadLoading('Silahkan Tunggu...');
        $rootScope.authorize().then(function(){
            $http.post($rootScope.url_api + "npwpd/selectNpwpd", {
                "offset": $scope.offset, 
                "pageSize": $scope.maxSize, 
                "limit": limit, 
                "user_id": $rootScope.userID
            })
            .success(function (reply) {
                if (reply.status === 200) {
                    var data = reply.result.data;
                    $scope.npwpd = data.list;
                    $scope.npwpdCount = data.count;
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data berita!!" });
                    $rootScope.unloadLoading();
                    return;
                }
                $rootScope.unloadLoading();
            })
            .error(function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                $rootScope.unloadLoading();
                return;
            })
            
        })
    };

    $scope.view = function(param){
        // console.info(param.id_data_pajak);
        $state.go('viewNpwpd',{id: param.id_data_pajak});
    }   

    $scope.tambah = function () {
        $state.go('form-pengajuan');
    };

    $scope.ubah = function (param) {
        $state.go('editNpwpd',{id: param.id_data_pajak});
    };
    
    $scope.delete = function (param) { // id
        swal({
            title: 'Apakah anda yakin akan menghapus data ini?',
            text: 'Data akan dihapus',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then(function(result) {
            if (result.value == true) {
                $http.post($rootScope.url_api + "npwpd/delete", {
                    "data" : param
                }).success(function(reply){
                    if(reply.status === 200){
                        $.growl.notice({
                            title: "[INFO]",
                            message: "Berhasil menghapus data"
                        });
                        $scope.jLoad(1);
                        // $state.go("lihatNpwpd");
                    }
                }).error(function(err){
                    $.growl.error({message: "Gagal menambah data!"});
                    return
                })
            }
        });
    };
});


function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}