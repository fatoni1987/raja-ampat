angular.module('rajampat')
.controller('approveNpwpdCtrl', function ($scope, $modal, $rootScope, $http, $state) { // $cookieStore, 
    $scope.berita = [];
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxSize = 10;
    $scope.data = 0;
    $scope.kata = "";    
    
    $scope.init = function () {
        $rootScope.authorize().then(function (result) {
            $scope.jLoad(1);           
        });
    };


    $scope.cariData = function () {
        $scope.jLoad(1);
    };

    $scope.jLoad = function (current) {
        $scope.berita = [];
        $scope.currentPage = current;
        $scope.offset = (current * 10) - 10;
        var arr = [];
        arr.push($scope.offset);
        var limit = 10;
        arr.push(limit);
        $rootScope.loadLoading('Silahkan Tunggu...');
        $rootScope.authorize().then(function(){
            $http.post($rootScope.url_api + "npwpd/selectNpwpd", {
                "offset": $scope.offset, 
                "pageSize": $scope.maxSize, 
                "limit": limit, 
                "user_id": $rootScope.userID
            })
            .success(function (reply) {
                if (reply.status === 200) {
                    var data = reply.result.data;
                    $scope.npwpd = data.list;
                    $scope.npwpdCount = data.count;
                } else {
                    $.growl.error({ message: "Gagal mendapatkan data berita!!" });
                    $rootScope.unloadLoading();
                    return;
                }
                $rootScope.unloadLoading();
            })
            .error(function (err) {
                $.growl.error({ message: "Gagal Akses API >" + err });
                $rootScope.unloadLoading();
                return;
            })
            
        })
    };

    $scope.tambah = function () {
        $state.go('form-pengajuan');
    };

    $scope.view = function(param){
        $state.go('checkNpwpd',{id: param.id_data_pajak});
    }

    $scope.ubah = function (berita) {
        $scope.flagTombol = true;
        var kirim = {
            id_news: berita.id_news,
            title_news: berita.title_news,
            content_news: berita.content_news,
            picture_news: berita.picture_news,
            date_news: berita.date_news
        };
        var modalInstance = $modal.open({
            templateUrl: 'tambahBerita.html',
            controller: editBeritaCtrl,
            resolve: {
                item: function () {
                    return kirim;
                }
            }
        });
        modalInstance.result.then( function(){
            $scope.init();
        });
    };

    $scope.approve = function(param){
        swal({
            title: 'Apakah anda yakin akan approve data ini?',
            text: 'Anda akan menerbitkan no NPWPD',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            closeOnClickOutside: false,
            allowOutsideClick: false
        }).then(function(result) {
            if (result.value == true) {
                $http.post($rootScope.url_api + "npwpd/approve", {
                    "data" : param.id_data_pajak
                }).success(function(reply){
                    if(reply.status === 200){
                        $.growl.notice({
                            title: "[INFO]",
                            message: "Berhasil menghapus data"
                        });
                        $scope.jLoad(1);
                        // $state.go("lihatNpwpd");
                    }
                }).error(function(err){
                    $.growl.error({message: "Gagal menambah data!"});
                    return
                })
            }
        });
    }

    $scope.reject = function(param){
        swal({
            title: 'Apakah anda yakin akan reject data ini?',
            text: 'Masukkan alasan reject',
            icon: 'warning',
            input: 'text',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            closeOnClickOutside: false,
            allowOutsideClick: false
        }).then(function(result) {
            if (result.value != "") {
                $http.post($rootScope.url_api + "npwpd/reject", {
                    "data" : param.id_data_pajak,
                    "alasan" : result.value
                }).success(function(reply){
                    if(reply.status === 200){
                        $.growl.notice({
                            title: "[INFO]",
                            message: "Berhasil menghapus data"
                        });
                        $scope.jLoad(1);
                    }
                }).error(function(err){
                    $.growl.error({message: "Gagal menambah data!"});
                    return
                })
            }else{
                $.growl.error({message: "Alasan tolak harus di isi!"});
                $scope.reject(param);
            }
        });
    }

    $scope.delete = function (param) { // id
        swal({
            title: 'Apakah anda yakin akan menghapus data ini?',
            text: 'Data akan dihapus',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then(function(result) {
            if (result.value == true) {
                $http.post($rootScope.url_api + "npwpd/delete", {
                    "data" : param
                }).success(function(reply){
                    if(reply.status === 200){
                        $.growl.notice({
                            title: "[INFO]",
                            message: "Berhasil menghapus data"
                        });
                        $scope.jLoad(1);
                        // $state.go("lihatNpwpd");
                    }
                }).error(function(err){
                    $.growl.error({message: "Gagal menambah data!"});
                    return
                })
            }
        });
    };
});


function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}