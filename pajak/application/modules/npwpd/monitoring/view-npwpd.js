angular.module("rajampat").controller("checkNpwpdCtrl", function($scope, $http, $rootScope, $modal, SweetAlert, $filter, $stateParams, $state) {
    console.info($stateParams.id);    
    $scope.load = function() {
        $rootScope.authorize().then(function(result) {
            loadData();
        })        
    };   

    function loadData() {
        $http.post($rootScope.url_api + "npwpd/viewData",{
            id : $stateParams.id
        }).success(function(reply) {
            if (reply.status === 200) {
                $scope.DataTab = reply.result.data.data_pajak[0];
                $scope.DataNpwpd = reply.result.data.npwpd[0];
                $scope.tab1 = {
                    // 'nama_badan_usaha' : '',
                    'kewarganegaraan' : $scope.DataTab.kewarganegaraan,
                    'type_identitas' : '',
                    'nama' : '',
                    'alamat' : '',
                    'email' : '',
                    'no_telp' : '',
                    'nik' : ''
                }
                $scope.tab2 = {
                    'nama_bidang_usaha' : '',
                    'alamat_usaha' : '',
                    'rt' : '',
                    'kecamatan' : '',
                    'kelurahan_usaha' : '',
                    'kabupaten_usaha' : '',
                    'kode_pos' : '',
                    'telp_usaha' : '',
                    'fax_usaha' : '',
                    'email_usaha' : ''
                }
                $scope.tab3 = {
                    'no_pbb' : '',
                    'no_izin_usaha' : '',
                    'nama_izin_usaha' : '',
                    'datePerkiraan' : ''
                }
            } else {
                $.growl.error({
                    message: "Gagal mendapatkan data Form!"
                });
                return
            }
        }).error(function(err) {
            $.growl.error({
                message: "Gagal Akses API >" + err
            });
            return
        });
    }

    $scope.backList = function(){
        $state.go("monNpwpd");
    }

    $scope.approve = function(){
        swal({
            title: 'Apakah anda yakin akan approve data ini?',
            text: 'Anda akan menerbitkan no NPWPD',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            closeOnClickOutside: false,
            allowOutsideClick: false
        }).then(function(result) {console.info(result)
            if (result.value == true) {
                $http.post($rootScope.url_api + "npwpd/approve", {
                    "data" : $scope.DataNpwpd.id_data_pajak
                }).success(function(reply){
                    if(reply.status === 200){
                        $.growl.notice({
                            title: "[INFO]",
                            message: "Berhasil menghapus data"
                        });
                        $state.go("monNpwpd");
                    }
                }).error(function(err){
                    $.growl.error({message: "Gagal menambah data!"});
                    return
                })
            }
        });
    }

    $scope.reject = function(){
        swal({
            title: 'Apakah anda yakin akan reject data ini?',
            text: 'Masukkan alasan reject',
            icon: 'warning',
            input: 'text',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya',
            closeOnClickOutside: false,
            allowOutsideClick: false
        }).then(function(result) {
            if (result.value != "") {
                $http.post($rootScope.url_api + "npwpd/reject", {
                    "data" : $scope.DataNpwpd.id_data_pajak,
                    "alasan" : result.value
                }).success(function(reply){
                    if(reply.status === 200){
                        $.growl.notice({
                            title: "[INFO]",
                            message: "Berhasil menghapus data"
                        });
                        $state.go("monNpwpd");
                    }
                }).error(function(err){
                    $.growl.error({message: "Gagal menambah data!"});
                    return
                })
            }else{
                $.growl.error({message: "Alasan tolak harus di isi!"});
                $scope.reject(param);
            }
        });
    }

});