angular.module('rajampat').controller('dashboardCtrl', function( $scope, $rootScope, $state, $cookieStore, $http,$filter){ // alert("Tekan Tombol Refresh (F5)");
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxSize = 10;

    $scope.thisYear = $filter('date')(new Date(), 'yyyy');
    $scope.thisMoon = new Date().getMonth();
    $scope.appSrcText = '';
    $scope.appPageSize = 10;
    $scope.appCurrPage = 1;
    $scope.approval = [];
    $scope.appCount = 0;
    $scope.tahun = ['2019','2020','2021','2022'];
    $scope.bulan = [];
    $scope.bulan[0] = 'All Month';
    $scope.bulan[1] = "January";
    $scope.bulan[2] = "February";
    $scope.bulan[3] = "Maret";
    $scope.bulan[4] = "April";
    $scope.bulan[5] = "Mei";
    $scope.bulan[6] = "Juni";
    $scope.bulan[7] = "Juli";
    $scope.bulan[8] = "Agustus";
    $scope.bulan[9] = "September";
    $scope.bulan[10] = "Oktober";
    $scope.bulan[11] = "November";
    $scope.bulan[12] = "Desember";

    $scope.bulanVmsOpsi = [];
    $scope.bulanVmsOpsi[0] = "January";
    $scope.bulanVmsOpsi[1] = "February";
    $scope.bulanVmsOpsi[2] = "Maret";
    $scope.bulanVmsOpsi[3] = "April";
    $scope.bulanVmsOpsi[4] = "Mei";
    $scope.bulanVmsOpsi[5] = "Juni";
    $scope.bulanVmsOpsi[6] = "Juli";
    $scope.bulanVmsOpsi[7] = "Agustus";
    $scope.bulanVmsOpsi[8] = "September";
    $scope.bulanVmsOpsi[9] = "Oktober";
    $scope.bulanVmsOpsi[10] = "November";
    $scope.bulanVmsOpsi[11] = "Desember";
    $scope.tahunKategori = $scope.thisYear;
    $scope.bulanKategori = $scope.thisMoon+1;

    $scope.tahunVms = $scope.thisYear;
    $scope.bulanVms = $scope.thisMoon + 1;
    $scope.tahunRichPo = $scope.thisYear;
    $scope.bulanRichPo = $scope.thisMoon + 1;
    $scope.tahunPrtoPo = $scope.thisYear;
    $scope.bulanPrtoPo = $scope.thisMoon + 1;
    $scope.tahunPrtoPo2 = $scope.thisYear;
    $scope.bulanPrtoPo2 = $scope.thisMoon + 1;
    $scope.tahunCost = $scope.thisYear;
    $scope.bulanCost = $scope.thisMoon+1;

    $scope.tahunProsesPengadaaanInvoiceModel = $scope.thisYear;
    $scope.bulanProsesPengadaaanInvoice = $scope.thisMoon + 1;
    $scope.tahunProsesPengadaaanExpectedModel = $scope.thisYear;
    $scope.bulanProsesPengadaaanExpected = $scope.thisMoon + 1;
    $scope.vendorProsesPengadaaanExpectedModel = 0;

    $scope.periodeKpi = 1;

    $scope.init = function(){
        $rootScope.checkUAT();
        $rootScope.authorize().then(function(){
            $scope.dashboard1();
            $scope.dashboard2();
            $scope.dashboard3();

        });
    };

    // $scope.dashboard1 = function(){
    //     am4core.useTheme(am4themes_animated);
    //         var chartDash1 = am4core.create("chartdivDash1", am4charts.PieChart);

    //         // Add data
    //         chartDash1.data = [ {
    //         "country": "Malang",
    //         "litres": 501.9
    //         }, {
    //         "country": "Sidoarjo",
    //         "litres": 301.9
    //         }, {
    //         "country": "Tuban",
    //         "litres": 201.1
    //         }, {
    //         "country": "Gresik",
    //         "litres": 165.8
    //         }, {
    //         "country": "Surabaya",
    //         "litres": 139.9
    //         }, {
    //         "country": "Jombang",
    //         "litres": 128.3
    //         }, {
    //         "country": "Kediri",
    //         "litres": 99
    //         }, {
    //         "country": "Lamongan",
    //         "litres": 60
    //         }, {
    //         "country": "Blitar",
    //         "litres": 50
    //         } ];

    //         // Add and configure Series
    //         var pieSeries = chartDash1.series.push(new am4charts.PieSeries());
    //         pieSeries.dataFields.value = "litres";
    //         pieSeries.dataFields.category = "country";
    //         pieSeries.slices.template.stroke = am4core.color("#fff");
    //         pieSeries.slices.template.strokeWidth = 2;
    //         pieSeries.slices.template.strokeOpacity = 1;

    //         // This creates initial animation
    //         pieSeries.hiddenState.properties.opacity = 1;
    //         pieSeries.hiddenState.properties.endAngle = -90;
    //         pieSeries.hiddenState.properties.startAngle = -90;
    // }

    // $scope.dashboard2 = function(){
    //         am4core.useTheme(am4themes_animated);
    //         var chartDash2 = am4core.create("chartdivDash2", am4charts.XYChart);
    //         var data = [];
    //         var value = 120;
    //         var names = ["Raina",
    //         "Demarcus",
    //         "Carlo",
    //         "Jacinda",
    //         "Richie",
    //         "Antony",
    //         "Amada",
    //         "Idalia",
    //         "Janella",
    //         "Marla",
    //         "Curtis",
    //         "Shellie",
    //         "Meggan",
    //         "Nathanael",
    //         "Jannette",
    //         "Tyrell",
    //         "Sheena",
    //         "Maranda",
    //         "Briana",
    //         "Rosa",
    //         "Rosanne",
    //         "Herman",
    //         "Wayne",
    //         "Shamika",
    //         "Suk",
    //         "Clair",
    //         "Olivia",
    //         "Hans",
    //         "Glennie",
    //         ];

    //         for (var i = 0; i < names.length; i++) {
    //             value += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 5);
    //             data.push({ category: names[i], value: value });
    //         }

    //         chartDash2.data = data;
    //         var categoryAxis = chartDash2.xAxes.push(new am4charts.CategoryAxis());
    //         categoryAxis.renderer.grid.template.location = 0;
    //         categoryAxis.dataFields.category = "category";
    //         categoryAxis.renderer.minGridDistance = 15;
    //         categoryAxis.renderer.grid.template.location = 0.5;
    //         categoryAxis.renderer.grid.template.strokeDasharray = "1,3";
    //         categoryAxis.renderer.labels.template.rotation = -90;
    //         categoryAxis.renderer.labels.template.horizontalCenter = "left";
    //         categoryAxis.renderer.labels.template.location = 0.5;

    //         categoryAxis.renderer.labels.template.adapter.add("dx", function(dx, target) {
    //             return -target.maxRight / 2;
    //         })

    //         var valueAxis = chartDash2.yAxes.push(new am4charts.ValueAxis());
    //         valueAxis.tooltip.disabled = true;
    //         valueAxis.renderer.ticks.template.disabled = true;
    //         valueAxis.renderer.axisFills.template.disabled = true;

    //         var series = chartDash2.series.push(new am4charts.ColumnSeries());
    //         series.dataFields.categoryX = "category";
    //         series.dataFields.valueY = "value";
    //         series.tooltipText = "{valueY.value}";
    //         series.sequencedInterpolation = true;
    //         series.fillOpacity = 0;
    //         series.strokeOpacity = 1;
    //         series.strokeDashArray = "1,3";
    //         series.columns.template.width = 0.01;
    //         series.tooltip.pointerOrientation = "horizontal";

    //         var bullet = series.bullets.create(am4charts.CircleBullet);

    //         chartDash2.cursor = new am4charts.XYCursor();

    //         chartDash2.scrollbarX = new am4core.Scrollbar();
    //         chartDash2.scrollbarY = new am4core.Scrollbar();

    // }

    // $scope.dashboard3 = function(){
    //     var chartDash3 = am4core.create("chartdivDash3", am4charts.XYChart);

    //     // Add percent sign to all numbers
    //     chartDash3.numberFormatter.numberFormat = "#.#'%'";

    //     // Add data
    //     chartDash3.data = [{
    //         "country": "USA",
    //         "year2004": 3.5,
    //         "year2005": 4.2
    //     }, {
    //         "country": "UK",
    //         "year2004": 1.7,
    //         "year2005": 3.1
    //     }, {
    //         "country": "Canada",
    //         "year2004": 2.8,
    //         "year2005": 2.9
    //     }, {
    //         "country": "Japan",
    //         "year2004": 2.6,
    //         "year2005": 2.3
    //     }, {
    //         "country": "France",
    //         "year2004": 1.4,
    //         "year2005": 2.1
    //     }, {
    //         "country": "Brazil",
    //         "year2004": 2.6,
    //         "year2005": 4.9
    //     }];

    //     // Create axes
    //     var categoryAxis = chartDash3.xAxes.push(new am4charts.CategoryAxis());
    //     categoryAxis.dataFields.category = "country";
    //     categoryAxis.renderer.grid.template.location = 0;
    //     categoryAxis.renderer.minGridDistance = 30;

    //     var valueAxis = chartDash3.yAxes.push(new am4charts.ValueAxis());
    //     valueAxis.title.text = "GDP growth rate";
    //     valueAxis.title.fontWeight = 800;

    //     // Create series
    //     var series = chartDash3.series.push(new am4charts.ColumnSeries());
    //     series.dataFields.valueY = "year2004";
    //     series.dataFields.categoryX = "country";
    //     series.clustered = false;
    //     series.tooltipText = "GDP grow in {categoryX} (2004): [bold]{valueY}[/]";

    //     var series2 = chartDash3.series.push(new am4charts.ColumnSeries());
    //     series2.dataFields.valueY = "year2005";
    //     series2.dataFields.categoryX = "country";
    //     series2.clustered = false;
    //     series2.columns.template.width = am4core.percent(50);
    //     series2.tooltipText = "GDP grow in {categoryX} (2005): [bold]{valueY}[/]";

    //     chartDash3.cursor = new am4charts.XYCursor();
    //     chartDash3.cursor.lineX.disabled = true;
    //     chartDash3.cursor.lineY.disabled = true;

    // }
});
