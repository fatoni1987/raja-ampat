angular.module('rajampat')
.controller('aktivasiCtrl', function ($scope, $modal, $rootScope, $http) { 
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.maxSize = 10;
    $scope.kata = new Kata("");

    $scope.init = function () {
        $rootScope.authorize().then(function (result) {
            $scope.jLoad(1);
        });
    };


    $scope.jLoad = function (current) {
        $scope.berita = [];
        $scope.currentPage = current;
        $scope.offset = (current * 10) - 10;
        var arr = [];
        arr.push($scope.offset);
        var limit = 10;
        arr.push(limit);
        $rootScope.loadLoading('Silahkan Tunggu...');
        $rootScope.authorize().then(function(){
            $http.post($rootScope.url_api + "user/selectUser", {
                search: "%" + $scope.kata.srcText + "%", 
                offset: $scope.offset, 
                pageSize: $scope.maxSize, 
                limit: limit, 
                keyword: "%" + $scope.kata.srcText + "%", 
                author: $rootScope.nama
            })
                .success(function (reply) {
                    // console.info("reply: "+JSON.stringify(reply));
                    if (reply.status === 200) {
                        $scope.data = reply.result.data;
                    } else {
                        $.growl.error({ message: "Gagal mendapatkan data berita!!" });
                        $rootScope.unloadLoading();
                        return;
                    }
                    $rootScope.unloadLoading();
                })
                .error(function (err) {
                    $.growl.error({ message: "Gagal Akses API >" + err });
                    $rootScope.unloadLoading();
                    return;
                })
        })
    };
    
    $scope.ubahStatus = function (param, flag) { // id
        var aktif = 'Aktif'
        if(flag == 0){
            aktif = 'Non Aktif'
        }

        swal({
            title: 'Apakah anda yakin akan ' + aktif + 'kan data ini?',
            text: 'Data akan terupdate',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya'
        }).then(function(result) {
            if (result.value == true) { //console.info(param.id_user);
                $http.post($rootScope.url_api + "user/ubahStatus", {
                    UserID: param.id_user,
                    status: flag
                }).success(function(reply) {
                    if (reply.status === 200) {
                        $.growl.notice({
                            title: "[INFO]",
                            message: "Berhasil Menghapus Data Admin Tersebut!!"
                        });
                        $scope.jLoad($scope.currentPage);
                        $rootScope.unloadLoadingModal();
                        $modalInstance.close()
                        
                    } else {
                        $.growl.error({
                            message: "Gagal mendapatkan data Admin!!"
                        });
                        return
                    }
                }).error(function(err) {
                    $.growl.error({
                        message: "Gagal Akses API >" + err
                    });
                    return
                })
            }
        });
    };
});


function Kata(srcText) {
    var self = this;
    self.srcText = srcText;
}