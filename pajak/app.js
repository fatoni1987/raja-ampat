'use strict';

var $stateProviderRef = null;
var $urlRouterProviderRef = null;
var rajampat = angular
    .module('rajampat', [
        'oc.lazyLoad', 'ui.router', 'ui.bootstrap', 'angular-loading-bar', 'ngCookies', 'ngSanitize', 'ngProgress',
        'ui.tinymce', 'treeGrid', 'ui.sortable', 'checklist-model','ipCookie','dndLists','selectize','ng-sweet-alert'
    ])
    .filter("fixKoma",function(){
        return function(text){
            return text.toFixed(2);
        }
    })
    .factory('upload', function($http, $q, $rootScope) {
        return {
            url: $rootScope.url_api + 'upload',
            uploadFile: function(fileUpload, folder) {
                var deferred = $q.defer();
                var fd = new FormData();
                angular.forEach(fileUpload, function(file) {
                    fd.append("upload", file);
                });
                $http.post(this.url + "/" + "pengadaan_" + folder + "/", fd,
                {
                    withCredentials: true,
                    transformRequest: angular.identity(),
                    headers: {'Content-Type': undefined}
                }).success(function(data) {
                    deferred.resolve(data);
                }).error(function(error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            }
        };
    });

rajampat.run(['$rootScope', '$state', '$stateParams', '$cookieStore','ipCookie', '$http', 'BreadcrumbService', 'SocketService', '$q', '$filter', '$location',
    function($rootScope, $state, $stateParams, $cookieStore, ipCookie, $http, BreadcrumbService, SocketService, $q, $filter,$location) {
        window.addEventListener("load", () => {
            if(window.location.host != 'localhost'){
                window.addEventListener("online", $rootScope.handleNetworkChange);
                window.addEventListener("offline", $rootScope.handleNetworkChange);
            }
        });
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.applicationName = "";
        $rootScope.root_location = window.location.pathname;
        $rootScope.url = window.location.pathname + "#/";
        var path = window.location.pathname;
        // console.info(path.replace("/pajak", ""));
        $rootScope.url_api = path.replace("/pajak", "") + "api/public/";
        $rootScope.manual_root_url = $rootScope.url_api + "download-manual/";
        $rootScope.isLogged = false;
        $rootScope.sessId;
        $rootScope.defaultPage = window.location.pathname.replace("/pajak","");
        $rootScope.url_image = path.replace("pajak/", "");
        console.info();
        $rootScope.api_key = 'c44acc2327ef633b4d754dcb69dd07e5';
        $rootScope.userSession = undefined;
        $rootScope.childPageActive = 0; /*untuk set menu*/
        $rootScope.pageActive = 0; /*untuk set menu*/
        $rootScope.accessrole = []; /*untuk set menu*/
        $rootScope.tampung = []; /*untuk set menu*/
        $rootScope.userLogin = "";
        $rootScope.breadcrumb = [];
        /*untuk konfigurasi file upload*/
        $rootScope.listconfig;
        $rootScope.ukuranfile;
        $rootScope.filemaksimal;
        $rootScope.filetypelist = [];
        $rootScope.limitfiletype = [];
        $rootScope.dateNow = $filter('date')(new Date(), 'yyyy-MM-dd');
        $rootScope.yearNow = $filter('date')(new Date(), 'yyyy');
        $rootScope.datemonthNow = $filter('date')(new Date(), 'MM-dd');
        /*Filter Notifikasi*/
        $rootScope.berhakOlahRekanan = false;
        $rootScope.berhakUnlock = false;
        $rootScope.isUAT = false;

        $rootScope.$on("$stateChangeSuccess", function(event, currentRoute) {
            $rootScope.pageTitle = currentRoute.pageTitle;
            $rootScope.namePage = currentRoute.name;
//            $rootScope.pageID = currentRoute.pageID;
            $rootScope.authorize().then(function(){
                // $rootScope.countNotif();
            })
            $rootScope.resetBreadcrumb();
            $rootScope.sessId = ipCookie("panitiaSessId");
            if (window.location.host != 'localhost') {
                $rootScope.handleNetworkChange();
            }
        });
        $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error){
        // this is required if you want to prevent the $UrlRouter reverting the URL to the previous valid location
            console.log('HTML not found');
        })

        $rootScope.handleNetworkChange = function(){
            if (navigator.onLine) {
                $rootScope.isOnline = true;
                $rootScope.unloadLoading();
                // $("#notif-offline").css('opacity',"0");
            } else {
                $rootScope.isOnline = false;
                $rootScope.loadingOffline();
                // $("#notif-offline").css('opacity',"1");
            }
        };

        $rootScope.checkUAT = function(){
            var link = window.location.pathname.split("/");
            if(link[1] == 'uat' || link[1] == 'uat-new'){
                $rootScope.isUAT = true;
            }
            else{
                $rootScope.isUAT = false;
            }
        };

        $rootScope.$on('$stateChangeStart', function() {
            //$rootScope.resetBreadcrumb();
            //$rootScope.checkSession();
        });

        SocketService.on('feed', function(data) {
            $rootScope.serversekarang = data.timestamp;
        });

        SocketService.on('actionLogoff', function(data) {
            if (ipCookie("panitiaSessId") == data.session_id) {
                alert("You are logged out!");
                ipCookie.remove("panitiaSessId",{ path: '/' });
                $state.go('login');
                $rootScope.isLogged = false;
            }
        });

        $rootScope.upload = function(fileDocument, folder) {
            $rootScope.loadLoading("Uploading...");
            var deferred = $q.defer();
            var fd = new FormData();

            angular.forEach(fileDocument, function(item) {
                fd.append("uploads", item);
            });

            $http.post($rootScope.url_api + "upload/" + folder + "/", fd, {
                withCredentials: true,
                transformRequest: angular.identity(),
                headers: {'Content-Type': undefined}
            }).success(function(reply) {

                deferred.resolve(reply);
                $rootScope.unloadLoading();
                //console.info("reply di app " + JSON.stringify(reply));
            }).error(function(err) {

                deferred.reject(err);
                //console.info("error di app " + JSON.stringify(err));
                $rootScope.unloadLoading();
                $http.post($rootScope.url_api + "logging", {
                    message: "Tidak berhasil akses API : " + JSON.stringify(err),
                    source: "app.js - upload"
                }).then(function(response) {
                    // do nothing
                    // don't have to feedback
                });
                $.growl.error({title: "[PERINGATAN]", message: "Upload gagal app.js"}); });
            return deferred.promise;
        };

        $rootScope.uploadZip = function(files, folder) {
            $rootScope.loadLoading("Uploading...");
            var deferred = $q.defer();
            var fd = new FormData();
            for (var i = 0; i < files.length; i++) {
                angular.forEach(files[i].file, function(item) {
                    fd.append("uploads[]", item);
                }); }
//            angular.forEach(fileDocument, function(item) {
//                fd.append("uploads", item);
//            });

            $http.post($rootScope.url_api + "uploadZip/" + folder, fd, {
                withCredentials: true,
                transformRequest: angular.identity(),
                headers: {'Content-Type': undefined}
            }).success(function(reply) {
                $rootScope.unloadLoading();
                deferred.resolve(reply);
            }).error(function(err) {
                $rootScope.unloadLoading();
                deferred.reject(err);
                $http.post($rootScope.url_api + "logging", {
                    message: "Tidak berhasil akses API : " + JSON.stringify(err),
                    source: "app.js - uploadZip"
                }).then(function(response) {
                    // do nothing
                    // don't have to feedback
                });
                $.growl.error({title: "[PERINGATAN]", message: "Upload gagal"}); });
            return deferred.promise;
        };

        $rootScope.uploadZipByUrl = function(url) {
            $rootScope.loadLoading("Uploading...");
            var deferred = $q.defer();
            $http.post($rootScope.url_api + "uploadZip", {
                url: url
            }).success(function(reply) {
                $rootScope.unloadLoading();
                deferred.resolve(reply);
            }).error(function(err) {
                $rootScope.unloadLoading();
                deferred.reject(err);
                $http.post($rootScope.url_api + "logging", {
                    message: "Tidak berhasil akses API : " + JSON.stringify(err),
                    source: "app.js - uploadZip"
                }).then(function(response) {
                });
                $.growl.error({title: "[PERINGATAN]", message: "Upload gagal"}); });
            return deferred.promise;
        };

        $rootScope.deleteFileSingle = function(url) {
            $rootScope.loadLoading("Deleting...");
            var deferred = $q.defer();
            $http.post($rootScope.url_api + "deleteFile", {
                url: url
            }).success(function(reply) {
                $rootScope.unloadLoading();
                deferred.resolve(reply);
            }).error(function(err) {
                $rootScope.unloadLoading();
                deferred.reject(err);
                $http.post($rootScope.url_api + "logging", {
                    message: "Tidak berhasil akses API : " + JSON.stringify(err),
                    source: "app.js - delete"
                }).then(function(response) {
                });
                $.growl.error({title: "[PERINGATAN]", message: "Upload gagal"}); });
            return deferred.promise;
        };

        $rootScope.deleteFileMultiple = function(urls) {
            $rootScope.loadLoading("Deleting...");
            var deferred = $q.defer();
            $http.post($rootScope.url_api + "deleteFile", {
                urls: urls
            }).success(function(reply) {
                $rootScope.unloadLoading();
                deferred.resolve(reply);
            }).error(function(err) {
                $rootScope.unloadLoading();
                deferred.reject(err);
                $http.post($rootScope.url_api + "logging", {
                    message: "Tidak berhasil akses API : " + JSON.stringify(err),
                    source: "app.js - delete"
                }).then(function(response) {
                });
                $.growl.error({title: "[PERINGATAN]", message: "Upload gagal"}); });
            return deferred.promise;
        };

        /*loading login progress */
        $rootScope.loadLoading = function(test) {
            // old spinner
            //new spinner
            $.blockUI({
                message: '<img src="../assets/rekanan/image/logos.png"/ class="blink-image" style="width:120px;height:150px">',
                css: {border: '0px solid a49db3', opacity: '1', filter: 'alpha(opacity=90)', background: 'rgba(255, 255, 255, 0.02)'}
            });
        };

        $rootScope.loadingOffline = function() {
            var template = `
            <div style="padding: 20px 0">
                <div style="font-size:40px;color:#f9ca24"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                <div>Maaf Anda Tidak Dapat Melanjutkan Proses Karena Koneksi Anda Tidak Stabil</div>
            </div>
            `;
            $.blockUI({
                message: template,
                css: {border: '1px solid a49db3', opacity: '0.9', filter: 'alpha(opacity=90)', 'font-size': '18px', 'font-family': 'afta_sansregular, Arial, sans-serif!important'}
            });
        };

        $rootScope.loadLoadingModal = function(test) {
            $.blockUI({
                message: '<img src="../assets/rekanan/image/logos.png"/ class="blink-image" sstyle="width:120px;height:150px">',
                css: {border: '0px solid a49db3', opacity: '1', filter: 'alpha(opacity=90)', background: 'rgba(255, 255, 255, 0.02)'}
            });
        };

        /*unloading */
        $rootScope.unloadLoading = function() {
            $.unblockUI();
        };
        $rootScope.unloadLoadingModal = function() {
            $('.modal-content').unblock();
        };

        
        $rootScope.checkSession = function() {
            return new Promise(function(resolve,reject){
                $rootScope.idModule = ipCookie("type");
                var session = ipCookie("panitiaSessId");
                if (session !== undefined && session !== null) {
                    $http.post($rootScope.url_api + "auth/get_session", {
                        session_id: ipCookie("panitiaSessId")
                    })
                    .success(function(result2) {
                        if (result2.status === 200) {
                            if(result2.data.is_maintenance == true){
                                $rootScope.unloadLoading();
                                window.location.reload();
                                reject('maintenance');
                            }
                            else{
                                $rootScope.unloadLoading();
                                $rootScope.userSession = result2.data.data;                                
                                $rootScope.userLogin = $rootScope.userSession.username;
                                $rootScope.nama = $rootScope.userSession.nama;
                                $rootScope.userID = $rootScope.userSession.id;
                                $rootScope.isLogged = true;
                                $rootScope.roleID = $rootScope.userSession.role_id;
                                // $rootScope.cekNotif();
                                $rootScope.accessMenu($rootScope.userSession.role_id);
                                // mencegah user akses state 'login' jika sudah login
                                if ($state.current.name === 'login') {
                                    $state.go('homeadmin');
                                }
                                resolve('session valid');
                            }
                        } else {
                            if (result2.status === 403) {
                                $rootScope.unloadLoading();
                                if ($state.current.name !== 'login'){
                                    $.growl.error({message: 'Session anda telah habis! Silahkan login kembali!'});
                                }
                                $rootScope.isLogged = false;
                                $state.go('login');
                                reject('session expired');
                            } else {
                                $rootScope.unloadLoading();
                                if ($state.current.name !== 'login'){
                                    $.growl.error({message: 'Session anda tidak aktif! Silahkan login!'});
                                }
                                $rootScope.isLogged = false;
                                $state.go('login');
                                reject('session not active');
                            }
                        }
                    })
                    .error(function(err) {
                        reject(err);
                    });
                } else {
                    $rootScope.unloadLoading();
                    $rootScope.isLogged = undefined;
                    $state.go('login');
                    reject('tidak ada cookie')
                }
            });
        };

        $rootScope.logout = function() {        //console.info($location)    
            $http.post($rootScope.url_api + "auth/logout", {
                session_id: ipCookie("panitiaSessId")
            })
            .success(function(result) {
                if (result.status === 200) {
                    if (result.data.success) {
                        ipCookie.remove("panitiaSessId",{ path: '/' });
                        ipCookie.remove("type",{ path: '/' });
                        
                        window.close();
                        var loc = $location.$$protocol + "://" + $location.$$host + ":" + $location.$$port + "/pajak/#/beranda";
                        // console.info(loc);
                        window.open(loc,'');

                        $.growl.notice({message: result.data.message});
                        $state.go('login');
                        
                        $rootScope.isLogged = false;
                    } else {
                        $.growl.error({message: result.data.message});
                        $state.go('login');
                    }
                }
            })
            .error(function(err) {
                $.growl.error({message: "Gagal Akses API >" + err});
            });
        };

        /*get list menu*/
        $rootScope.accessMenu = function(role_id) {
            ////console.info(role_id+" roleeeeee");
            $http.post($rootScope.url_api + "roles/menu_by_role",
                    {role_id: role_id})
                    .success(function(reply) {
                        if (reply.status === 200) {

                            $rootScope.tampung = reply.result.data;
                            ////console.info("menu::: "+JSON.stringify($rootScope.tampung));
                            $rootScope.accessrole = [];
                            var submenu;
                            var submenus = [];
                            var subsubmenus = [];
                            var masuk = [];

                            for (var p = 0; p < $rootScope.tampung.length; p++)
                            {
                                if ($rootScope.tampung[p].menu_id == '1' || $rootScope.tampung[p].menu_id == '2')
                                    $rootScope.tampung[p].menu_name = 'BERANDA';
                                masuk[p] = false;
                            }
                            for (var i = 0; i < $rootScope.tampung.length; i++)
                            {
                                if (!($rootScope.tampung[i].menu_parent == '0') || masuk[i] == 'true')
                                    continue;
                                for (var j = 0; j < $rootScope.tampung.length; j++)
                                {
                                    if (j === i || masuk[j] == true)
                                        continue;
                                    if ($rootScope.tampung[j].menu_parent === $rootScope.tampung[i].menu_id)
                                    {
                                        submenu = $rootScope.tampung[j];
                                        for (var k = 0; k < $rootScope.tampung.length; k++)
                                        {
                                            if (k === i || k === j || masuk[k] == 'true')
                                                continue;
                                            if ($rootScope.tampung[k].menu_parent === $rootScope.tampung[j].menu_id)
                                            {
                                                subsubmenus.push($rootScope.tampung[k]);
                                                masuk[k] = true;
                                            }
                                        }
                                        submenu.childs = subsubmenus;
                                        subsubmenus = [];
                                        submenus.push(submenu);
                                        masuk[j] = true;
                                    }
                                }
                                $rootScope.tampung[i].childs = submenus;
                                submenus = [];
                                $rootScope.accessrole.push($rootScope.tampung[i]);
                                masuk[i] = true;
                                ////console.info("mana?"+$rootScope.accessrole);
                            }

                        }
                        else {
                            return false;
                        }
                        // logika multi level menu yg baru
                        var accordionsMenu = $('.sidebar-menu');

                        if (accordionsMenu.length > 0) {

                            accordionsMenu.each(function () {
                                var accordion = $(this);
                                //detect change in the input[type="checkbox"] value
                                accordion.on('change', 'input[type="checkbox"]', function () {
                                    var checkbox = $(this);
                                    (checkbox.prop('checked')) ? checkbox.siblings('ul.treeview-menu').slideDown() : checkbox.siblings('ul.treeview-menu').slideUp();
                                });
                            });
                        }
                        $rootScope.loadMenu = false;
                    })
                    .error(function(err) {
                        $.growl.error({message: "Gagal Akses API >" + err});
                        return false;
                    });
        };

        /*start > cek menu css*/
        $rootScope.isActive = function(test) {
            return test === $rootScope.pageActive;
        };

        $rootScope.setActive = function(test, test2) {
            $rootScope.pageActive = test;
            $rootScope.childPageActive = test2;
        };
        /*end > cek menu css*/

        /*fungsi authorisasi*/
        $rootScope.authorize = function() {
            $rootScope.loadLoading("Mendapatkan data session, silahkan tunggu ...");
            return new Promise(function(resolve,reject){
                $rootScope.checkSession().then(function(response){
                    $rootScope.unloadLoading();
                    resolve();
                }).catch(function(response){
                    $rootScope.unloadLoading();
                    // console.log(response)
                    // $.growl.error({message: "Gagal Akses API >" + response});
                });
            });
        };

        // fungsi cek hak akses pada halaman yg di akses user
        $rootScope.cekHakAksesUser = function(page_id,jenis_mengatur){
            return new Promise(function(resolve,reject){
                $http.post($rootScope.url_api+"roles/check_authority",{
                    username:  $rootScope.userLogin,  page_id: page_id, jenis_mengatur: jenis_mengatur
                }).success(function(reply){
                    if(reply.status === 200){
                        if(reply.result.size > 0){
                            resolve(reply.result.data[0].bisa_mengatur);
                        }
                        else{
                            resolve(false);
                        }
                    }
                    else{
                        $.growl.error({ message: "Gagal mendapatkan data hak akses!" });
                        resolve("gagal");
                    }
                }).error(function(err) {
                    $.growl.error({ message: "Gagal Akses API >"+err });
                    reject("gagal");
                });
            });
        };

        $rootScope.writeLog = function(err, file, apiroute) {
            $http.post($rootScope.url_api + "logging", {
                message: "Tidak berhasil akses API : " + err,
                source: file + " - " + apiroute
            })
            .then(function(response) {
                $rootScope.unloadLoading();
            });
        };

        $rootScope.strtobool = function( str ){
            return( str === "1" || str === "true" || str === 1 || str === true );
        };

        $rootScope.readNotif = function(type) {
            // kok kosong?
        };

        /*cek konfigurasi limit size & type file */
        $rootScope.fileuploadconfig = function(obj) {
            $http.post($rootScope.url_api + "limitfile", {id_page_config: obj, layer: "Panitia"})
                .success(function(reply) {
                    if (reply.status === 200) {
                        var data = reply.result.data;
                        $rootScope.listconfig = data; /* untuk config file upload */
                        $rootScope.ukuranfile = data[0].limit_size;
                        var ukuranfilefixed = Number($rootScope.ukuranfile);
                          var hasil = ukuranfilefixed * 1024;
                        $rootScope.filemaksimal = hasil.toString();

                        $rootScope.filetypelist = [];
                        $rootScope.limitfiletype = [];
                        for (var i = 0; i < $rootScope.listconfig.length; i++) {
                            $rootScope.filetypelist.push({
                                file_type_name: $rootScope.listconfig[i].file_type_name
                            });

                            $rootScope.limitfiletype.push(
                                    $rootScope.listconfig[i].file_type_name
                                    );
                        }
                    } else {
                        // Gagal mendapatkan rekanan.configuration.limitfile
                        $.growl.error({message: "Gagal mendapatkan limit dan tipe data file upload"});
                        return;
                    }
                })
                .error(function(err) {
                    $.growl.error({message: "Gagal Akses API >" + err});
                    return;
                });
        };

        $rootScope.isInputNotValid = function( z ){
            return !z || z === undefined || z === null || z === '' || z === 'N/A';
        };

        /* input: yyyy-mm-dd hh:mm | output: dd-mm-yyyy hh:mm */
        $rootScope.convertTanggalWaktu = function( z ){
            return $rootScope.isInputNotValid(z) ? '-' : z.substring(8,10) +'-'+ z.substring(5,7) +'-'+ z.substring(0,4) +' '+ z.substring(11,16);
        };

        /* input: yyyy-mm-dd hh:mm | output: dd-mm-yyyy */
        $rootScope.convertTanggal = function( z ){
            return $rootScope.isInputNotValid(z) ? '-' : z.substring(8,10) +'-'+ z.substring(5,7) +'-'+ z.substring(0,4);
        };
        $rootScope.convertSimpleTanggal = function( z ){
            return $rootScope.convertTanggal(z);
        };

        $rootScope.arrayFullMonths = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

        /* input: yyyy-mm-dd hh:mm | output: dd-fullmonth-yyyy hh:mm */
        $rootScope.convertDateTime = function( z ){
            return $rootScope.isInputNotValid(z) ? '-' : z.substring(8,10) +'-'+ $rootScope.arrayFullMonths[ parseInt(z.substring(5,7)) - 1 ] +'-'+ z.substring(0,4) +' '+ z.substring(11,16);
        };

        /* input: yyyy-mm-dd hh:mm | output: dd-fullmonth-yyyy */
        $rootScope.convertLongDate = function( z ){
            return $rootScope.isInputNotValid(z) ? '-' : z.substring(8,10) +'-'+ $rootScope.arrayFullMonths[ parseInt(z.substring(5,7)) - 1 ] +'-'+ z.substring(0,4);
        };

        /* input: yyyy-mm-dd hh:mm | output: dd-fullmonth-yyyy */
        $rootScope.convertLongDateNoMonth = function( z ){
            return $rootScope.isInputNotValid(z) ? '-' : z.substring(8,10) +'-0'+  (parseInt(z.substring(5,7))) +'-'+ z.substring(0,4);
        };

        /* input: dd-mm-yyyy | output: dd-fullmonth-yyyy */
        $rootScope.convertMonthDate = function( z ){
            return $rootScope.isInputNotValid(z) ? '-' : z.substring(0,2) +'-'+ $rootScope.arrayFullMonths[ parseInt(z.substring(3,5)) - 1 ] +'-'+ z.substring(6,10);
        };

        $rootScope.convertToYear = function(z){
            return $rootScope.isInputNotValid(z) ? '-' : z.substring(6,10) +'-'+ ( parseInt(z.substring(3,5))) +'-'+ z.substring(0,2) +' '+ z.substring(11,16);
        }

        /* input: dd-mm-yyyy | output: yyyy-mm-dd */
        $rootScope.convertToYearNoJam = function(z){
            return $rootScope.isInputNotValid(z) ? '-' : z.substring(6,10) +'-'+  (parseInt(z.substring(3,5))) +'-'+ z.substring(0,2);
        }

        $rootScope.dateFormat = function(param) {

            param = new Date(param);

            var curr_date = param.getDate();
            var curr_month = param.getMonth();
            var curr_year = param.getFullYear();
            var date = curr_date + " " + $rootScope.arrayFullMonths[curr_month] + " "+ curr_year ;
            return date;
        }



        $rootScope.resetBreadcrumb = function(){
            $rootScope.breadcrumb = [];
            $rootScope.breadcrumb.push({ url: "homeadmin", label: "Beranda", icon: "fa-home", lastIndex: false }); // console.log("Breadcrumb Items: " + JSON.stringify($rootScope.breadcrumb));
        };

        $rootScope.addBreadcrumbItem = function( z ){
            $rootScope.breadcrumb.push(z);
        };

        //--------------------------- numberToWords
        var _1_9 = ['', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan'];
        var _10_19 = ['Sepuluh', 'Sebelas', 'Dua Belas', 'Tiga Belas', 'Empat Belas', 'Lima Belas', 'Enam Belas', 'Tujuh Belas', 'Delapan Belas', 'Sembilan Belas'];
        var _20_90_s10 = ['', '', 'Dua Puluh', 'Tiga Puluh', 'Empat Puluh', 'Lima Puluh', 'Enam Puluh', 'Tujuh Puluh', 'Delapan Puluh', 'Sembilan Puluh'];

        $rootScope.numberToWords = function(num) { // input = 111111111 (111.111.111) | output = 'Seratus Sebelas Juta Seratus Sebelas Ribu Seratus Sebelas'
            if (num > 0)
                return toTriliun(num);
            else if (num === 0)
                return 'Nol';
            else
                return '';
        };

        function toTriliun(num) { // 1.000.000.000.000 - 999.999.999.999.999 (trillions)
            if (num >= 1000000000000)
                return toTriliun(Math.floor(num / 1000000000000)) + ' Triliun ' + toMiliar(num % 1000000000000);
            else
                return toMiliar(num); // < 1.000.000.000.000
        }

        function toMiliar(num) { // 1.000.000.000 - 999.999.999.999 (billions)
            if (num >= 1000000000)
                return toMiliar(Math.floor(num / 1000000000)) + ' Miliar ' + toJuta(num % 1000000000);
            else
                return toJuta(num); // < 1.000.000.000
        }

        function toJuta(num) { // 1.000.000 - 999.999.999  (millions)
            if (num >= 1000000)
                return toJuta(Math.floor(num / 1000000)) + ' Juta ' + toRibu(num % 1000000);
            else
                return toRibu(num); // < 1.000.000
        }

        function toRibu(num) { // 1000 - 999.999 (thousands)
            if (num >= 1000) {
                var ar = toRibu(Math.floor(num / 1000)); // ar = angka ribuan
                return (ar === _1_9[1] ? 'Seribu ' : ar + ' Ribu ') + toRatus(num % 1000);
            }
            else
                return toRatus(num); // < 1000
        }

        function toRatus(num) { // 100 - 999 (hundreds)
            if (num >= 100) {
                var ar = _1_9[ Math.floor(num / 100) ]; // ar = angka ratusan
                return (ar === _1_9[1] ? 'Seratus ' : ar + ' Ratus ') + toPuluh(num % 100);
            }
            else
                return toPuluh(num); // < 100
        }

        function toPuluh(num) { // 1 - 99 (tens)
            if (num < 10)
                return _1_9[num]; // 1 - 9
            else if (num >= 10 && num < 20)
                return _10_19[num - 10]; // 10 - 19
            else
                return _20_90_s10[ Math.floor(num / 10) ] + ' ' + _1_9[num % 10]; // > 20
        }

        $rootScope.getDayName = function( z ){ // input = 'yyyy-mm-dd' | output = 'Minggu' / 'Senin' / dst...
            var arrayDayNames = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
            return arrayDayNames[ new Date(z).getDay() ];
        };

        $rootScope.formatMoney = function(n){
            var c = 2;
            var d = ",";
            var t =  ".";
            var s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
    }
]);

rajampat.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', function( $stateProvider, $urlRouterProvider, $ocLazyLoadProvider ){
    var zPath = "application/modules/";
    var zApp = "rajampat";

    $ocLazyLoadProvider.config({
        debug: false,
        events: true
    });

    $stateProvider
    .state('hakAksesCreate', { //name: 'hakAksesCreate',
        url: '/hak-akses/create',
        templateUrl: 'application/modules/hak-akses/hakAksesCreate.html',
        pageTitle: 'Setting Hak Akses',
        controller: 'hakAksesCreateCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load([
                    { name: zApp, files: [ 'application/modules/hak-akses/hakAksesCtrl.js' ] },
                    { name: zApp, files: [ 'application/modules/hak-akses/hakAkses.css' ] },
                ])
            }
        }
    })
    .state('berita', { //name: 'berita',
        url: '/berita',
        templateUrl: 'application/modules/master/berita/berita.html',
        pageTitle: 'Master Berita',
        controller: 'beritaCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: [ 'application/modules/master/berita/beritaCtrl.js' ] }); } }
    })
    .state('aktivasiUser', { //name: 'berita',
        url: '/aktivasiUser',
        templateUrl: 'application/modules/aktivasi-user/aktivasi-user.html',
        pageTitle: 'Aktivasi User',
        controller: 'aktivasiCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: [ 'application/modules/aktivasi-user/aktivasi-userCtrl.js' ] }); } }
    })
    .state('pegawai', { //name: 'berita',
        url: '/pegawai',
        templateUrl: 'application/modules/master/pegawai/pegawai.html',
        pageTitle: 'Master Pegawai',
        controller: 'pegawaiCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: [ 'application/modules/master/pegawai/pegawai.js' ] }); } }
    })

    .state('homeadmin', { //name: 'homeadmin',
        url: '/homeadmin',
        templateUrl: 'application/modules/dashboard/dashboard.html',
        pageTitle: 'Dashboard',
        controller: 'dashboardCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: [ 'application/modules/dashboard/dashboardCtrl.js' ] }); } }
    })

    .state('login', { //name: 'login',
        url: '/login',
        templateUrl: 'application/modules/login/login-admin.html',
        pageTitle: 'Login Panitia',
        controller: 'loginCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: [ 'application/modules/login/loginCtrl.js' ] }); } }
    })
    
    .state('lihatNpwpd', { 
        url: '/lihatNpwpd',
        templateUrl: 'application/modules/npwpd/lihat/lihatNpwpd.html',
        pageTitle: 'Lihat NPWPD',
        controller: 'lihatNpwpdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/npwpd/lihat/lihatNpwpd.js'] }); } }
    })

    .state('lihatSptpd', { 
        url: '/lihatSptpd',
        templateUrl: 'application/modules/sptpd/lihat/lihatSptpd.html',
        pageTitle: 'Lihat SPTPD',
        controller: 'lihatSptpdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/sptpd/lihat/lihatSptpd.js'] }); } }
    })
    .state('lihatSspd', {
        url: '/lihatSspd',
        templateUrl: 'application/modules/sspd/lihat/lihatSspd.html',
        pageTitle: 'Lihat SSPD',
        controller: 'lihatSspdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/sspd/lihat/lihatSspd.js'] }); } }
    })
    .state('monSspd', {
        url: '/monSspd',
        templateUrl: 'application/modules/sspd/monitoring/monSspd.html',
        pageTitle: 'Lihat SSPD',
        controller: 'monSspdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/sspd/monitoring/monSspd.js'] }); } }
    })
    .state('lihatMineral', { 
        url: '/lihatMineral',
        templateUrl: 'application/modules/mineral/lihat/lihatMineral.html',
        pageTitle: 'Lihat Mineral',
        controller: 'lihatMineralCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/mineral/lihat/lihatMineral.js'] }); } }
    })
    .state('lihatSkpd', { 
        url: '/lihatSkpd',
        templateUrl: 'application/modules/skpd/lihat/lihatSkpd.html',
        pageTitle: 'Lihat SKPD',
        controller: 'lihatRestaurantCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/skpd/lihat/lihatSkpd.js'] }); } }
    })
    .state('form-pengajuan', { 
        url: '/form-pengajuan',
        templateUrl: 'application/modules/form-pengajuan/form-pengajuan.html',
        pageTitle: 'Form Pengajuan',
        controller: 'formPengajuanCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/form-pengajuan/form-pengajuan.js'] }); } }
    })
    .state('form-sptpd', { 
        url: '/form-sptpd/:jenis/:id',
        templateUrl: 'application/modules/form-sptpd/form-sptpd.html',
        pageTitle: 'Form SPTPD',
        controller: 'formsptpdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/form-sptpd/form-sptpd.js'] }); } }
    })
    .state('form-sptpd-harian', { 
        url: '/form-sptpd-harian/:id',
        templateUrl: 'application/modules/form-sptpd/form-sptpd-harian.html',
        pageTitle: 'Form SPTPD',
        controller: 'formsptpdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/form-sptpd/form-sptpd.js'] }); } }
    })
    .state('form-sptpd-bulanan', { 
        url: '/form-sptpd-bulanan/:jenis/:id',
        templateUrl: 'application/modules/form-sptpd/form-sptpd-bulanan.html',
        pageTitle: 'Form SPTPD',
        controller: 'formsptpdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/form-sptpd/form-sptpd.js'] }); } }
    })
    .state('monNpwpd', { 
        url: '/monNpwpd',
        templateUrl: 'application/modules/npwpd/monitoring/MonNpwpd.html',
        pageTitle: 'Approve NPWPD',
        controller: 'approveNpwpdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/npwpd/monitoring/MonNpwpd.js'] }); } }
    })
    .state('viewNpwpd', { 
        url: '/viewNpwpd/:id',
        templateUrl: 'application/modules/form-pengajuan/view-form-pengajuan.html',
        pageTitle: 'View NPWPD',
        controller: 'viewNpwpdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/form-pengajuan/view-form-pengajuan.js'] }); } }
    })
    .state('editNpwpd', { 
        url: '/editNpwpd/:id',
        templateUrl: 'application/modules/form-pengajuan/edit-form-pengajuan.html',
        pageTitle: 'View NPWPD',
        controller: 'editNpwpdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/form-pengajuan/edit-form-pengajuan.js'] }); } }
    })
    .state('checkNpwpd', { 
        url: '/checkNpwpd/:id',
        templateUrl: 'application/modules/npwpd/monitoring/view-npwpd.html',
        pageTitle: 'View NPWPD',
        controller: 'checkNpwpdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/npwpd/monitoring/view-npwpd.js'] }); } }
    })
    .state('monSptpd', { 
        url: '/monSptpd',
        templateUrl: 'application/modules/sptpd/monitoring/MonSptpd.html',
        pageTitle: 'Approve NPWPD',
        controller: 'approveSptpdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/sptpd/monitoring/MonSptpd.js'] }); } }
    })
    .state('viewSptpd', { 
        url: '/viewSptpd/:id',
        templateUrl: 'application/modules/form-sptpd/view-form-pengajuan.html',
        pageTitle: 'Form SPTPD',
        controller: 'viewSptpdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/form-sptpd/view-form-pengajuan.js'] }); } }
    })
    .state('monSkpd', { 
        url: '/monSkpd',
        templateUrl: 'application/modules/skpd/monitoring/monSkpd.html',
        pageTitle: 'Form SKPD',
        controller: 'viewSkpdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/skpd/monitoring/monSkpd.js'] }); } }
    })
    .state('viewSkpd', { 
        url: '/viewSkpd/:id',
        templateUrl: 'application/modules/skpd/monitoring/lihatSkpd.html',
        pageTitle: 'Form SKPD',
        controller: 'viewDataSkpdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/skpd/monitoring/lihatSkpd.js'] }); } }
    })
    .state('viewSspd', { 
        url: '/viewSspd/:id',
        templateUrl: 'application/modules/sspd/monitoring/lihatSspd.html',
        pageTitle: 'Form SKPD',
        controller: 'viewDataSspdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/sspd/monitoring/lihatSspd.js'] }); } }
    })
    .state('viewSkpdUser', { 
        url: '/viewSkpdUser/:id',
        templateUrl: 'application/modules/skpd/lihat/viewSkpd.html',
        pageTitle: 'Form SKPD',
        controller: 'viewDataUserSkpdCtrl',
        resolve: {
            loadMyFiles: function($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: zApp, files: ['application/modules/skpd/lihat/viewSkpd.js'] }); } }
    })

    //$urlRouterProvider.deferIntercept();
    $urlRouterProvider.otherwise('/homeadmin');
    $stateProviderRef = $stateProvider;
    $urlRouterProviderRef = $urlRouterProvider;
}]);


rajampat.filter("ucwords", function() {
    return function(input) {
        if (input) { //when input is defined the apply filter
            input = input.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
        }
        return input;
    };
});
