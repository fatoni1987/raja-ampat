# Project Pajak

![](logo-small.jpg)

## Installation
e-Procurement ITDC menggunakan kompresi gzip untuk mengkompresi file asset (js/css/image) sehingga ukuran mejadi lebih kecil
Pastikan module mod_filter.so dan mod_deflate.so sudah terinstall di apache

Berikut caranya :
```sh
1. Buka httpd.conf
2. Hilangkan Tanda '#' pada 2 module berikut

LoadModule filter_module modules/mod_filter.so
LoadModule deflate_module modules/mod_deflate.so